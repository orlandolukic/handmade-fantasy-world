	<div data-modal-id="ws-comments" class="ui-modal ui-modal-dark">
		<!-- Modal Placeholder -->
		<div class="ui-modal-content">
			<div class="ui-modal-heading">
				<h4 class="strong" style="margin: 0"><span class="ui-modal-heading-icon"><i class=""></i></span> Workshop Comments</h4>
				<div class="smaller">See workshop comments</div>
			</div>

			<div class="ui-modal-body">
			
			</div>

			<div class="ui-modal-footer small">				
				<div class="text-right div-inl">
					<div class="ui-modal-footer-close-btn ui-modal-close-btn btn-base bordered btn-ord"><i class="fa fa-times"></i> Close</div>
				</div>		
			</div>
			<div class="ui-modal-close ui-modal-close-btn"><i class="fa fa-times" data-toggle="tooltip" data-placement="left" title="Close"></i></div>

		</div>
		<!-- Loading Placeholder -->
		<div class="ui-modal-loader-placeholder">
			<div class="ui-modal-loader-text">
				<img src="<?= WROOT ?>img/logo.png" style="width: 250px;">
			</div>
			<div class="ui-modal-loader"></div>
		</div>
		<!-- /Loading Placeholder -->

	</div>