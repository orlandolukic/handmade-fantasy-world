1
00:00:09,475 --> 00:00:13,490
Good evening, here, together once again. No need to repeat myself,

2
00:00:13,492 --> 00:00:18,670
about time difference depending on your whereabouts.

3
00:00:18,671 --> 00:00:19,743
I salut you according to my whereabouts right now.

4
00:00:19,943 --> 00:00:28,570
We are going create a vintage project which will remind you a liitle of steampunk

5
00:00:28,571 --> 00:00:32,104
We are going to use gears and a very nice stencil by "Cadence"

6
00:00:32,304 --> 00:00:40,570
We will work on this box, which is a gift from... now I'm gonna talk you about my friends in Turkey,

7
00:00:42,000 --> 00:00:51,790
a store where I was feasted "art sanatsal" in Kadikoy region, my friend Filiz and Ohran the owner

8
00:00:51,990 --> 00:01:00,760
invited me , had fun, gave me some presents too, so it's an opportunity to work on this box

9
00:01:00,761 --> 00:01:05,210
and wait to see the reuslt. Ok? Let's start!

10
00:01:10,350 --> 00:01:20,100
We will use this stencil by "Cadence" which is the steampunk butterfly with the clock and the gears.

11
00:01:22,300 --> 00:01:29,900
We will put it on the cover of the box slightly to the top leaving space for some decorations.

12
00:01:29,901 --> 00:01:39,130
We will use a heavy body paste by "Stamperia" a modeling like paste, white for formation.

13
00:01:39,131 --> 00:01:55,470
How can I explain it to you... it's more solid for extra volume, 3D. I'll fill up the stencil using a spatula

14
00:01:55,471 --> 00:02:04,880
the paste is collected on the bottom of the spatula and we lay our stuff with curt movements like this

15
00:02:05,080 --> 00:02:09,040
Personally, I don't use the stencil glue for my project.

16
00:02:09,041 --> 00:02:13,020
As you may see,  lay the paste without even holding the stencil down. I just do short and curt moves.

17
00:02:13,220 --> 00:02:15,040
Keeping the spatula on an upward position and not totally bottom down,

18
00:02:15,041 --> 00:02:19,130
I face not problem.The stencil doesn't move at all

19
00:02:19,330 --> 00:02:27,570
I keep on spreading with paste the butterfly, I change the direction of my movements

20
00:02:27,571 --> 00:02:31,230
so as to have better control of the paste regarding the filling up.

21
00:02:32,430 --> 00:02:39,290
Here I' m gonna remove this heart cause, I don't like it for this project.

22
00:02:39,490 --> 00:02:52,990
Since we have all the stencil covered, we add some more paste for a 3D effect.

23
00:03:02,000 --> 00:03:05,400
I just over cover it with paste.

24
00:03:05,401 --> 00:03:15,900
And, we remove the stencil like this... very very nice,  here is the result

25
00:03:16,100 --> 00:03:33,590
We put the stencil aside, we may collect any paste left on it back to the box. And we dry...

26
00:03:33,591 --> 00:03:39,100
I use hot air keeping a medium distance from the project.

27
00:03:39,101 --> 00:03:45,890
Leaving it for a llitle time in room temperature will work nicely too as this alternative wil stone it.

28
00:05:23,020 --> 00:05:31,270
We are back. We fast forwarded the video so as to allow it dry well.

29
00:05:31,271 --> 00:05:38,600
In the mean time I used some gears. The truth is that I cant' help you on where to find them,

30
00:05:38,601 --> 00:05:42,200
because I've bought them from Russia Moscow, being there for seminars.

31
00:05:42,400 --> 00:05:50,580
It's written in Russian, so I can't explain it. They are chipboards made of paper.

32
00:05:50,581 --> 00:06:01,570
I used the clock and some gears and I put them exactly on the same points on the stencil imprint

33
00:06:01,571 --> 00:06:10,990
On the center clock and right on the stencil shaped gear point.

34
00:06:11,190 --> 00:06:18,730
During seminars I've seen some ladies putting gears on the stencil in a random way.

35
00:06:18,930 --> 00:06:23,730
I truly believe that it is very important where the gears will be put and that's what I insist on

36
00:06:23,930 --> 00:06:32,280
not letting the gears get out of the stencil gear design. It is best to be exact on the shape.

37
00:06:32,281 --> 00:06:38,110
After choosing  the right gear position,

38
00:06:38,310 --> 00:06:46,670
I'll use the media gloss gel paste  (Mixed Media Art line) by "Stamperia" .

39
00:06:48,340 --> 00:06:58,120
Which apart from being transparent paste, it is very good for glueing this elements.

40
00:06:58,320 --> 00:07:13,620
So we apply some small quantity on the gears and start glueing them. Same thing for all.

41
00:08:12,500 --> 00:08:21,860
After finnishing this procedure, with a strong brush we dab with glue our decorations.

42
00:08:21,861 --> 00:08:34,020
Not on the box. Thus we give our decorations a repousse texture but also a better result

43
00:08:34,021 --> 00:08:46,320
Do the same thing to all the gears, not everywhere though, chose some points.

44
00:08:48,520 --> 00:08:51,870
At some points more, at others less.

45
00:09:05,070 --> 00:09:08,280
Very nice! ... We put our brush in a cup of water...

46
00:09:13,700 --> 00:09:18,930
I wrongly put the brush in water, we'll need it again.

47
00:09:19,810 --> 00:09:38,580
Let's add some chain... here ... we use the pliers... very nice. We measure.

48
00:09:50,780 --> 00:09:58,800
And cut right to the lenght we need.

49
00:09:59,000 --> 00:10:12,620
We will add the chain on the same way. We will apply the gel paste following the stencil motive.

50
00:10:16,000 --> 00:10:34,590
And we glue the chain. There is quite of a quantity of paste and the chain will lose its beauty

51
00:10:34,591 --> 00:10:42,350
Anyway, we don't care about it, as everything here will be painted. Now we need to paste it right.

52
00:10:42,550 --> 00:10:53,100
Thus I'll cover the chain with even more paste.

53
00:10:53,300 --> 00:10:57,900
I don't mind cause in the end it will not show at all.

54
00:11:07,100 --> 00:11:20,000
Using the gel paste I try to fill the chain links to help it glue. I'll use one more

55
00:11:25,200 --> 00:11:31,260
I'll put it in the same way just above the first one.

56
00:11:46,000 --> 00:11:49,880
We need it to begin from the inner part of th first one so we'll cut right here

57
00:11:53,000 --> 00:12:00,760
And we start filling the chain links with paste to help it glue.

58
00:12:12,000 --> 00:12:15,130
Nice!

59
00:12:17,330 --> 00:12:24,700
Now we put our brush in the water cup, as well as wipe the paste out of our hands.

60
00:12:24,900 --> 00:12:33,900
We'll dry it now, same way, not too close, medium distance,warm air. We'll be back soon!

61
00:12:50,800 --> 00:13:00,185
Here we are again! After drying the paste, the stencil, the chain and the gears well,

62
00:13:00,186 --> 00:13:07,749
we'll use some primer, I'm gonna use the "Paco art" Line primer.

63
00:13:08,274 --> 00:13:12,837
A supplier I colaborate with has created his own line of materials.

64
00:13:13,037 --> 00:13:17,276
Which we are going to use in many of our projects.

65
00:13:17,476 --> 00:13:22,562
Let's see what the primer does. We will actually cover only the chain.

66
00:13:22,762 --> 00:13:28,880
I don't consider that is usefull to cover the chipboards too.

67
00:13:29,080 --> 00:13:30,541
cause we may paint them as they are.

68
00:13:33,334 --> 00:13:41,112
Just a little primer το help the acrylic paint be applied well on the chain.

69
00:13:41,312 --> 00:13:46,528
Dry very little.

70
00:14:06,868 --> 00:14:21,577
Let's paint. The upper part will be painted with the color "Amber", Style Matt line by "Cadence"

71
00:14:28,136 --> 00:14:34,275
When using "Cadence" paints, I personally wet my brush because I find them a bit thick.

72
00:14:36,301 --> 00:14:48,699
And I start painting the box. The chipboards, the stencil, the chains, everything.

73
00:14:48,899 --> 00:14:52,751
What I mainly want is that your brushes go only up and down

74
00:14:52,951 --> 00:15:07,013
Same direction as you look at the box. Every mistaken brush we make it will be visible after oiling.

75
00:15:07,213 --> 00:15:09,540
The oil reveals everything. So we should be careful.

76
00:15:09,553 --> 00:15:17,365
We dab the stencil gaps, but make long up and down brushes on the rest of the box surface.

77
00:15:17,565 --> 00:15:20,351
Work the paint out well, very well!

78
00:15:43,000 --> 00:15:50,322
You may see here the paste repousse texture. Ι paint the metal corners too. 

79
00:15:51,522 --> 00:15:56,477
We don't mind if they are not painted well that's why I didn't use primer on them.

80
00:15:56,677 --> 00:16:01,046
One way or another our project will look old.

81
00:16:07,246 --> 00:16:09,355
Here ...dab well!

82
00:17:16,589 --> 00:17:22,083
We painted the lid of the box and we will continue with rest lower part.

83
00:17:22,084 --> 00:17:25,131
I've painted the metallic decorations too, as I always do.

84
00:17:26,331 --> 00:17:34,026
We leave this color, we will use an allegro, acrylic by "Stamperia" my favourite color, actually,

85
00:17:34,027 --> 00:17:40,577
it's Blue avio (aviation) the code is KAL 42

86
00:17:40,777 --> 00:17:47,834
Now we will paint the rest box.

87
00:18:23,224 --> 00:18:30,087
We are done with the box painting. Let's dry to apply the oil then.

88
00:18:54,465 --> 00:19:04,051
At this point, we need to decide how we will apply the oil. There are two ways.

89
00:19:04,251 --> 00:19:09,814
The first way is to apply the oil right after the acrylic paint, without using any gloss.

90
00:19:10,014 --> 00:19:16,748
The result will be more rough and it will be harder to remove oil.

91
00:19:16,948 --> 00:19:28,268
The other way is to apply hybrid gloss paint, very very well, so as the brushes are not discrete

92
00:19:28,468 --> 00:19:33,397
as the oil may reveal any brushes done, either from the paste or the gloss.

93
00:19:33,597 --> 00:19:38,543
However the oil will be removed easily and the result will be lighter.

94
00:19:38,544 --> 00:19:44,983
We will do the second one. We will apply hybrid gloss paint.

95
00:19:44,984 --> 00:19:52,580
I'll use the gloss by "Sherwing" a danish company.

96
00:19:52,780 --> 00:20:00,418
The materials for Greece may be found either at Konstantinart.gr or Pacoartcenter.gr

97
00:20:00,618 --> 00:20:15,080
Using a soft brush ww apply the gloss doing up and down brush according to he box direction

98
00:20:15,280 --> 00:20:20,674
Also very important. Long brushes from side to side.

99
00:21:38,204 --> 00:21:44,192
We painted all the box with gloss and dried it. I repeat, it's a water gloss,

100
00:21:44,392 --> 00:21:49,044
so as the oil doesn't glides on it, so as not kick against, in other words

101
00:21:49,244 --> 00:21:59,105
We dried it... and let's oil it now. Painting oil. As already mentioned in a previous video, 

102
00:21:59,305 --> 00:22:06,832
I like "Van Gogh" company... 
In Greece you may find it at pacoartcenter.gr.

103
00:22:10,032 --> 00:22:20,034
Well, this time we will use "van gogh" raw umber oil colour. With a strong brush

104
00:22:20,536 --> 00:22:36,226
we pick a small quantity and start covering all the surfaces. Cover thoroughly and dab when needed.

105
00:22:36,426 --> 00:22:43,106
No yellow color should be left and as I always say we must drain the color out of our brush.

106
00:22:43,306 --> 00:22:45,700
To make it easy when it's time for oil to be removed.

107
00:22:45,900 --> 00:22:47,495
We'll be back...

108
00:23:25,500 --> 00:23:34,801
Well, the upper side is ready. I'm used to cover with oil and remove it for each side separately.

109
00:23:35,001 --> 00:23:46,305
I've applied oil to one more side. I want to show you how to remove oil from the box cover with the gears

110
00:23:46,306 --> 00:23:48,807
and how to remove oil from the other side where there is no decoration.

111
00:23:48,809 --> 00:23:54,331
When oiling, it's of no importance, the way we do our brushes.

112
00:23:54,531 --> 00:23:56,836
We brush any way we like. How we remove oil makes a difference.

113
00:23:57,036 --> 00:24:03,298
And there is a specific way, using our favourite tool, the baby wipes. We throw away the first one,

114
00:24:03,299 --> 00:24:05,750
as the first ones are usually dried out

115
00:24:05,950 --> 00:24:12,897
I take a lot of baby wipes together, I make a ball of them in my hand and start removing the oil,

116
00:24:16,485 --> 00:24:21,103
by wiping to the same direction of the front side of the box.

117
00:24:23,178 --> 00:24:26,865
Up and down movements only.

118
00:24:27,065 --> 00:24:38,472
As I move to the edges, where old boxes look more dirty, in order to have that effect,

119
00:24:38,672 --> 00:24:44,123
I make more gentle moves, removing less oil and make it look darker.

120
00:24:44,776 --> 00:24:50,375
As you can see, I'm moving only up and down and I don't hustle

121
00:24:50,376 --> 00:24:53,311
This is the part of the project that you should enjoy.

122
00:24:53,312 --> 00:25:02,212
Be careful, the butterfly and the gears part must be darker too.

123
00:25:04,412 --> 00:25:11,761
You see! Short moves and always following the box direction.

124
00:25:24,000 --> 00:25:30,972
I try not to have too many lines. That's what I'm more careful for, when I remove oil.

125
00:25:35,500 --> 00:25:42,609
At the gears' point, we will change the way we remove oil. I make a smaller ball of baby wipes.

126
00:25:42,610 --> 00:25:54,994
I start removing oil, little by little. I take care to leave, though, some in between.

127
00:25:55,194 --> 00:26:05,215
On the gears. Maybe by putting our finger in the baby wipe, to clear specifically the points we want 

128
00:26:05,415 --> 00:26:16,323
I'm not sure if you can see it through video, the gel paste we used has given us a 3D effect.

129
00:26:16,523 --> 00:26:23,882
Maybe here it's better if we dabed, in order to remove more oil from the inner parts.

130
00:26:24,082 --> 00:26:28,584
And piece by piece we clear the rest points as well.

131
00:26:28,784 --> 00:26:33,455
Carefully, we don't want to take all the oil out. Not that we may not re apply some,

132
00:26:33,456 --> 00:26:39,851
but it's better this way, so as to have a more natural result.

133
00:26:39,852 --> 00:26:43,877
By adding and removing oil again and again, the effect result will change, for sure.

134
00:26:44,077 --> 00:26:54,974
I've retained some points darker. We will do the same thing at this point here.

135
00:27:02,035 --> 00:27:07,931
I like this. I cleared the one corner more than the other, so it doesn't look all the same.

136
00:27:13,508 --> 00:27:18,121
Nice!. Let's see how we remove oil from the other sides.

137
00:27:18,321 --> 00:27:25,147
For the side, we follow another procedure. A large ball of baby wipes,due to the size of the surface.

138
00:27:25,347 --> 00:27:33,255
The larger the surface, the bigger the wipes ball. We move from left to right and bacwards.

139
00:27:33,256 --> 00:27:40,660
We clear the center and pull to the one direction and then to the other.

140
00:27:40,860 --> 00:27:48,937
Thus we clear the center and the corners are darker.  I think this is the easiest and the quickest.

141
00:27:48,938 --> 00:27:52,701
I'm gonna get the box ready in fast forward mode and we'll be back soon!

142
00:29:04,730 --> 00:29:11,552
Here we are again! Since we have finished with the oil, you may see the result. 

143
00:29:11,752 --> 00:29:20,060
We must be careful,  how we touch our project  and not to let fingers on. So on the corners it's best.

144
00:29:20,260 --> 00:29:29,801
You can see that we leave more oil at the corners, the alcoves and on the metallic decorations.

145
00:29:30,001 --> 00:29:36,301
Now, using oil again , but an orange colored one, we will create the rust effect.

146
00:29:36,501 --> 00:29:44,395
You'd better check if there are any fingers left on your project.

147
00:29:45,029 --> 00:30:02,304
Using a smaller brush, and the Venetian Yellow Orange oil by "Pebeo". Let's put some here.

148
00:30:05,340 --> 00:30:23,548
We apply the oil at some points and dab it with the baby wipe, so as to look like powder.

149
00:30:23,748 --> 00:30:32,798
It's an effect of which the result is easily seen if you are close, maybe it's not shown well on video.

150
00:30:32,998 --> 00:30:45,503
Specifically here the two colors look the same. So you cannot see it well. 

151
00:30:45,504 --> 00:30:53,499
Perhaps, If my friend here made a "super" zoom, you would be able to see what happens

152
00:30:53,699 --> 00:31:01,925
with the orange color. We need to dab it to collect any extra stuff using a baby wipe.

153
00:31:01,926 --> 00:31:16,180
It's a detail that gives us the rust effect but only if you look it closely.

154
00:31:16,380 --> 00:31:22,558
Especially when it is applied on a same shade color, like the one we have used, the amber.

155
00:31:22,758 --> 00:31:29,499
If it was for another color the orange that I'm using now, it would distinguish better.

156
00:31:29,699 --> 00:31:34,498
Not too much, at some points only.

157
00:31:35,698 --> 00:31:49,168
Dabbing, don't be afraid of it. With a baby wipe we can remove as much as we want.

158
00:31:58,815 --> 00:32:08,569
Let's dab some on the edges too, same procedure

159
00:32:11,822 --> 00:32:14,196
A llitle bit more...

160
00:32:31,439 --> 00:32:39,829
I wish you were here to see the result from close. It is very nice. It is the one I want.

161
00:32:40,029 --> 00:32:44,326
You just can't see it through video due to the color used.

162
00:32:44,526 --> 00:32:47,694
Let's see what we may do here. You 'll be able to see it on blue.

163
00:32:48,666 --> 00:32:59,381
The same procedure. Do you see how different it looks?

164
00:33:15,821 --> 00:33:26,887
Check how it looks on here!

165
00:33:27,087 --> 00:33:37,936
Dabbing, but at the same time as if the rust is leaking. We clean with the baby wipe.

166
00:33:43,895 --> 00:33:47,929
The leaking rust effect is slightly left

167
00:33:51,129 --> 00:33:52,456
The same thing on the other one.

168
00:34:24,024 --> 00:34:32,551
My friends, here, around me are laughing at me. This is the result, I hope it's shown well on camera.

169
00:34:32,751 --> 00:34:41,573
It's a steampunk but  at my weights and measures. The truth is that I don't like metallic colors

170
00:34:41,773 --> 00:34:47,728
and most people work on the steampunk using metallic colors.I've used matte colors.

171
00:34:47,928 --> 00:34:55,719
Even though, the gloss I used was shiny, the oil was matte, the result was matte one too.

172
00:34:55,919 --> 00:35:00,551
This is the steampunk butterfly that I love, the stencil by "Cadence".

173
00:35:00,751 --> 00:35:07,057
We used some materials by "Stamperia", some others by "Cadence", one material by "Paco art"

174
00:35:07,257 --> 00:35:09,785
That's it for today, I hope you liked it. Thank you very much.

175
00:35:09,985 --> 00:35:14,314
I hope you have fun with "handmadefantasyworld.com"

176
00:35:14,514 --> 00:35:17,349
See you next time. Bye!!!