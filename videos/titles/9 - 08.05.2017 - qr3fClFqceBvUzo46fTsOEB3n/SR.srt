1
00:00:03,724 --> 00:00:07,341
Dobar dan. Dobro došli na www.hndmadefantasyworld.com.

2
00:00:07,541 --> 00:00:08,980
Nadam se da sam Vam nedostajao.

3
00:00:09,180 --> 00:00:14,699
Prošlog meseca dva projekta je radila moja prijateljica Boka Ranković.

4
00:00:14,899 --> 00:00:18,567
Odmenila me je jer sam ja bio na putu.

5
00:00:18,767 --> 00:00:25,237
Prvi projekat u maju.

6
00:00:25,437 --> 00:00:33,800
Prošli ponedeljak smo preskočili.ALi ćemo svakako imati 4 projekta u maju.

7
00:00:34,000 --> 00:00:41,608
Hajde da počnemo mix media projekat.

8
00:00:41,808 --> 00:00:49,381
Koristićemo razne materijale. Videćete sve tokom videa.

9
00:00:49,581 --> 00:00:52,733
Hvala Vam što me pratite.

10
00:00:52,933 --> 00:01:01,098
Praviću tag - mix media.

11
00:01:01,298 --> 00:01:07,676
Počećemo sa Stamperia šablonom.

12
00:01:07,876 --> 00:01:23,295
Nanećemo preko njega Paco art pastu srednje granulacije.

13
00:01:23,495 --> 00:01:26,748
Zahvaljujući granulaciji, rdja će se više istaći...ali videćete sve kasnije.

14
00:01:26,948 --> 00:01:34,958
Koristimo spatulu. Nanosim veću količinu i razmazujem je.

15
00:01:35,158 --> 00:01:39,089
Spatulu držim pod uglom.

16
00:02:00,847 --> 00:02:09,252
Ostavićemo da se osuši.

17
00:02:09,452 --> 00:02:19,265
Sada ću uzeti drugi Stamperia šablon i stonepaper,

18
00:02:19,465 --> 00:02:28,664
i na glatku stranu ću naneti pastu.

19
00:02:33,221 --> 00:02:42,625
Stamperia stonepaper je vrlo otporan i možemo nanositi razne materijale na njega.

20
00:02:42,825 --> 00:02:49,719
Zato ga često i koristim. Kasnije ću prskati i Aquacolor,

21
00:02:49,919 --> 00:02:57,887
a običan papir bi se od tečnosti iskrivio.

22
00:02:58,531 --> 00:03:04,638
Sa stonepaperom smo sigurni da nećemo imati tih problema.

23
00:03:06,479 --> 00:03:09,058
Ponavljam proceduru kao na drvenoj podlozi.

24
00:03:18,741 --> 00:03:23,067
Sada ćemo sušiti fenom.

25
00:03:23,267 --> 00:03:31,279
Pastu moram da sušim par minuta toplim vazduhom.

26
00:03:31,479 --> 00:03:35,894
Nakon toga pastu puštam da se ohladi i osuši na sobnoj temperaturi.

27
00:03:52,476 --> 00:03:57,794
Važno je da se pasta ohladi.

28
00:03:59,430 --> 00:04:06,852
Ovo sam ranije naneo. Sad ću da sečem trake makazama.

29
00:04:21,240 --> 00:04:30,822
Pasta je suva, papir isečen, ali pre nego što sve zalepim, 

30
00:04:31,022 --> 00:04:34,323
potrebno je da ofarbam tag.

31
00:04:34,523 --> 00:04:45,834
Koristiću tirkiz boju firme Artebella.

32
00:04:46,034 --> 00:04:50,101
Volim i pakovanje. Veliko i lako za upotrebu.

33
00:04:50,301 --> 00:04:51,542
Vrlo su zgodne.

34
00:04:51,742 --> 00:04:58,054
Bojimo celu podlogu.

35
00:05:03,331 --> 00:05:13,876
Vodimo računa da se pasta dobro oboji, ali da ne ostavimo debeli sloj boje, jer bi tako izmenili motiv.

36
00:05:36,961 --> 00:05:44,512
Obojili smo. Nije važno ako nije savršeno.

37
00:05:45,496 --> 00:05:50,769
Boju nanosim dugim pokretima četke.

38
00:05:50,969 --> 00:05:53,697
Četkicu stavljam u posudu sa vodom i sušim.

39
00:06:15,139 --> 00:06:20,029
Sada ćemo napraviti kompoziciju od osečenih komada papira.

40
00:06:23,022 --> 00:06:28,823
Prvo im odredim pozicije, pa ih lepim.

41
00:06:50,990 --> 00:07:00,960
Lepiću komade Stamperia Glossy Heavy Body pastom.

42
00:07:01,160 --> 00:07:06,618
Ova pasta je i odličan lepak.

43
00:07:06,818 --> 00:07:09,606
Možemo lepiti različite predmete, kao što ćete kasnije videti.

44
00:07:09,806 --> 00:07:15,664
Stavljamo malu količinu paste na poledjinu...

45
00:07:19,329 --> 00:07:22,798
Spuštamo ppir na tag, pritisnemo i zalepljeno je.

46
00:07:23,401 --> 00:07:25,557
Ponavljamo sa svakim komadom.

47
00:08:04,012 --> 00:08:11,967
Sad kad smo ovo zalepili, prelazimo na centralni motiv.

48
00:08:13,143 --> 00:08:24,328
Makaze će nam poslužiti kao centralni dekor.

49
00:08:24,528 --> 00:08:29,126
I njih lepimo Heavy body pastom.

50
00:08:29,326 --> 00:08:33,667
Ovu pastu ćemo koristiti za lepljenje svih elemenata.

51
00:08:33,867 --> 00:08:39,517
Proveriću koja strana makaza mi više odgovara.

52
00:08:45,152 --> 00:08:52,743
Ova strana će se lakše zalepiti.

53
00:08:52,943 --> 00:08:55,471
Nanosim pastu na makaze.

54
00:09:05,879 --> 00:09:07,654
Stavljam veću količinu.

55
00:09:12,813 --> 00:09:16,806
Pritiskam makaze, naročito tamo gde se dodiruju sa tagom.

56
00:09:23,762 --> 00:09:27,289
Negde skidam višak lepka.

57
00:09:27,489 --> 00:09:38,484
Ne smeta mi što izbija pasta, svakako ću raditi preko nje.

58
00:09:38,684 --> 00:09:46,160
Tražimo oslonac na makazama i ta tačka je najvažnija da bude dobro zalepljena.

59
00:10:05,221 --> 00:10:12,614
Makaze su skoro zalepljene. Pasta se neće brzo osušiti, možda tek sutra.

60
00:10:12,814 --> 00:10:15,598
Ali je dovoljno suva da možemo da radimo dalje.

61
00:10:15,798 --> 00:10:20,193
Malo će se pomerati, ali će se svakako zalepiti.

62
00:10:20,393 --> 00:10:25,190
Dok nanesemo sve elemente, makaze se neće više pomerati.

63
00:10:25,390 --> 00:10:29,431
Kad budemo nanosili Aquacolor, pasta možda malo omekša.

64
00:10:29,631 --> 00:10:35,064
Ali će se svakako osušiti, potrebno je vreme.

65
00:10:35,264 --> 00:10:39,224
Imam i neke metalne elemente.

66
00:10:39,424 --> 00:10:49,656
Možwtw ih naći na www.decomagia.gr.

67
00:10:49,856 --> 00:10:51,442
I ja sam ih tamo kupio.

68
00:10:51,642 --> 00:11:04,463
Počeću sa dekorisanjem, nanoseči dosta paste na pozadinu elemenata.

69
00:11:04,663 --> 00:11:09,575
Pasta će da poveže sve elemente.

70
00:11:09,775 --> 00:11:19,138
I potrebna je veća količina zbog razmaka izmedju elemenata.

71
00:11:22,304 --> 00:11:26,473
Lepim metalne elemente.

72
00:11:44,643 --> 00:11:50,058
AKo imate višak paste, utapkajte ga četkicom.

73
00:12:08,778 --> 00:12:15,142
Višak možete skinuti i četkicom.

74
00:12:56,810 --> 00:13:09,115
Napravio sam raspored kakav želim. Vidite višak paste, ali ga neću sušiti.

75
00:13:09,315 --> 00:13:13,858
Inače, ako Vam kapne negde pasta, nemojte je brisati.

76
00:13:14,058 --> 00:13:17,477
Brisanjem je tepko možete skinuti, smao ćete napraviti još veću fleku.

77
00:13:17,677 --> 00:13:37,058
Preko paste ću posuti Pentart puder. 

78
00:13:40,103 --> 00:13:48,997
Fenom ću oduvati višak praha.

79
00:13:59,214 --> 00:14:07,899
Četkicom takodje možete skinuti višak, ili prah postaviti na poziciju koja Vam odgovara.

80
00:14:19,983 --> 00:14:44,721
Malo ću prosušiti pa ću naneti još gela....

81
00:14:44,921 --> 00:14:48,542
Nanosim oko centralnog motiva...

82
00:14:51,415 --> 00:14:55,484
I ovde...

83
00:15:06,688 --> 00:15:14,811
Ako mi se nešto ne dopada, pokupim i stavim na poziciju koja mi se dopada.

84
00:15:19,110 --> 00:15:24,301
Nasumice nanosim pastu i posipam Pentart 3D kuglice.

85
00:15:24,501 --> 00:15:28,863
Postoje tri veličine, ja koristim najmanju i srednju za ovaj projekat.

86
00:15:29,063 --> 00:15:36,917
Prvo stavljam srednju veličinu.


87
00:15:40,828 --> 00:15:48,947
Zatim nanosim male kuglice da popunim praznine.

88
00:15:53,990 --> 00:16:01,676
Gde god ima paste, kuglice će se zalepiti.

89
00:16:01,876 --> 00:16:14,917
Višak možemo da oduvamo fenom, ili istresemo tag pažljivo (zbog makaza).

90
00:16:15,117 --> 00:16:22,285
Makaze mogu da otpadnu jer pasta nije potpuno suva.

91
00:16:22,485 --> 00:16:24,773
Ističemo elemente koji nam se svidjaju.

92
00:16:31,050 --> 00:16:40,714
Na drške ću naneti Stamperia monokomponentnu krekl pastu.

93
00:16:40,914 --> 00:16:49,419
Nanosimo na plastične delove.

94
00:16:49,619 --> 00:16:57,660
To će nam dati dodatni efekat, ali ne želim da nanesem ni previše.

95
00:17:07,487 --> 00:17:09,777
Sušimo fenom.

96
00:17:26,965 --> 00:17:34,104
Ne mora da bude potpuno suva pasta, jer je nećemo dirati, dalje ćemo koristiti samo sprejeve.

97
00:17:34,304 --> 00:17:37,916
Krekl pasta kako se suši sve više puca.

98
00:17:38,116 --> 00:17:40,865
Prosušio sam je tek malo.

99
00:17:41,065 --> 00:17:48,971
Kad nanesemo metalne elemente i napravimo konačnu kompoziciju,

100
00:17:49,171 --> 00:17:53,402
sve ćemo premazati gessom.

101
00:17:53,602 --> 00:17:58,231
Možete koristiti bilo koji gesso, brend nije važan.

102
00:17:58,431 --> 00:17:59,983
S obzirom da elementi još nisu fiksirani i da ne mogu da koristim četkicu,

103
00:18:10,713 --> 00:18:18,506
stavio sam gesso na plastični tanjir i dodao vodu, 

104
00:18:18,706 --> 00:18:33,769
i gesso je postao vrlo tečan.

105
00:18:33,969 --> 00:18:42,242
Promešam četkicom...

106
00:18:42,442 --> 00:18:54,393
Sada ću razredjenim gessom da prskam metalne elemente.

107
00:18:54,593 --> 00:18:58,073
Dodajem vode ako je potrebno.

108
00:19:01,950 --> 00:19:09,946
I prskam sve elemente razredjenim gessom.

109
00:19:10,146 --> 00:19:15,063
Ovo će nam dati u vrlo zanimljiv finalni rezultat.

110
00:19:15,263 --> 00:19:23,654
I might as well do them on purpose. I work with the brush against my finger.

111
00:19:23,854 --> 00:19:33,658
Nanosim podjednako na metalne elemente i na makaze.

112
00:19:37,520 --> 00:19:45,834
Negde nežno mogu četkicom naneti gesso.

113
00:19:53,407 --> 00:19:57,856
Vrlo nežno jer se elementi još uvek pomeraju.

114
00:20:08,524 --> 00:20:14,186
Važno nam je da pokrijemo makaze,naročito plastične drške.

115
00:20:22,879 --> 00:20:30,878
Možete koristiti elemente bilo kojih boja, metalne elemente, papir, cvetove,

116
00:20:31,078 --> 00:20:34,115
bilo šta možete koristiti, čak i najružnije materijale

117
00:20:34,315 --> 00:20:40,951
jer sve možete da predjete gessom i bojite u boju koja Vam odgovara.

118
00:21:00,762 --> 00:21:07,561
S obzirom da je gesso prilično razredjen, nazire se boja metalnih elemenata.

119
00:21:17,252 --> 00:21:18,185
Sada sušimo.

120
00:22:12,201 --> 00:22:18,629
Metalni elementi nisu potpuno beli.

121
00:22:18,829 --> 00:22:32,719
Ako želite da neki element bude svteliji, dodajte gesso.

122
00:22:32,919 --> 00:22:36,876
Meni se dopada ovako.

123
00:22:38,331 --> 00:22:40,747
SVidja mi se što se nazire metalna boja.

124
00:22:50,127 --> 00:23:01,702
Sada ćemo naneti Aquacolor. Prvo Pentart mist white coffee.

125
00:23:01,902 --> 00:23:16,816
Zatim, Aquacolor žuti, orah, tirkiz i naravno vodu u spreju.

126
00:23:17,016 --> 00:23:23,949
Rezultat možemo da podešavamo i postignemo ono štonam se dopada.

127
00:23:24,149 --> 00:23:30,240
Prvo tirkiz na stonepaper.

128
00:23:37,381 --> 00:23:44,367
Vidite! Pumpica ne radi dobro i npravio sam grešku. Ali ću dodati i na drugoj poziciji isto pa će biti dobro.

129
00:23:55,752 --> 00:24:00,001
Ova greška će nam dati dobar rezultat.

130
00:24:00,201 --> 00:24:03,364
Prskam i sušim.

131
00:24:22,193 --> 00:24:25,846
Ako mi se nešto ne dopada, obrišem ubrusom.

132
00:24:27,605 --> 00:24:33,463
Vidite? Obrisao sam višak boje.

133
00:24:33,663 --> 00:24:45,668
Sada ćemo to uraditi oko motiva. Orah koristimo da potamnimo podlogu, kao i stonepaper.

134
00:24:51,635 --> 00:25:00,944
Vodom prskam tag i puštam da se boja sliva.

135
00:25:21,149 --> 00:25:24,709
Aquacolor može da se koristi na više načina. Možete da naprskate i odmah sušite.

136
00:25:24,909 --> 00:25:32,709
Ja prskam i sušim odmah, zatim prskam ponovo i pravim slojeve. Naravno sve zavisi i 

137
00:25:32,909 --> 00:25:34,332
koju boju želite da postignete.

138
00:25:34,532 --> 00:25:45,633
Na makazama ću prskati prvo žutu boju i malo je prosušiti.

139
00:26:01,018 --> 00:26:03,778
Zatim ću prskati Pentart Mist White coffee.

140
00:26:19,087 --> 00:26:27,378
Nastaviću sa orahom da pojačam braon boju oko centralnog motiva.

141
00:26:51,173 --> 00:27:00,331
Kao što vidite nema odedjene tehnike kako se koristi quacolor. Koja će boja biti naneta prva,

142
00:27:00,531 --> 00:27:05,386
koja druga, da li koristiti fen ili ne. Sve je stvar šta želimo da postignemo.

143
00:27:05,586 --> 00:27:10,250
Probam i ako mi se svidja nastavljam.

144
00:27:10,450 --> 00:27:14,844
SLobodno eksperimentišite.

145
00:27:15,044 --> 00:27:18,667
Birajte boju koja god vam se dopada.

146
00:27:21,171 --> 00:27:22,719
Vidite, ja uvek isprobavam mnogo stvari.

147
00:27:22,919 --> 00:27:32,542
Ubrusom brišem višak boje i dobijam vrlo zanimljiv efekat postarivanja.

148
00:27:41,112 --> 00:27:45,961
Radim sa bojom sve dok nisam zadovoljan rezultatima.

149
00:27:48,711 --> 00:27:53,206
Vidite šta smo postigli i kako sad izgleda gesso koji smo prskali.

150
00:28:28,271 --> 00:28:35,978
Sada ću naneti rdju firme TEX.CO.

151
00:28:36,178 --> 00:28:38,363
Slučajno sam naišao na ovaj proizvod.

152
00:28:38,364 --> 00:28:42,456
Prijateljica Maria Gratsa iz Tive mi je dala da probam.

153
00:28:42,656 --> 00:28:48,339
Šokirao sam se koliko je jednostavno za upotrebu, a efektno.

154
00:28:48,539 --> 00:28:58,232
Sve je u spreju. Jedna je osnova, a druge dve flašice su boje, zelena-tirkiz rdja 

155
00:28:58,432 --> 00:29:02,791
i narandžasta. Procedura je jednostavna.

156
00:29:02,991 --> 00:29:12,007
Takodje se može ubrzati fenom. Prvo prskate osnovnu tečnost,

157
00:29:12,008 --> 00:29:18,322
na mesta gde želite rdju.

158
00:29:18,522 --> 00:29:25,591
Odmah zatim prskate drugu tečnost, boju koju želite.

159
00:29:27,214 --> 00:29:29,999
Prskate i sušite.

160
00:29:50,360 --> 00:29:54,062
Tokom sušenja možete naneti još osnovne tečnosti.

161
00:30:03,125 --> 00:30:07,199
Zatim ponovo boje koju želite.

162
00:30:29,465 --> 00:30:36,364
Ne znam koliko odbro vidite efekat rdje.

163
00:30:36,564 --> 00:30:43,555
Rdja se pojavljuje vrlo brzo, a sledeći dan je još intenzivnija.

164
00:30:43,755 --> 00:30:50,410
Znači, osnovna tečnost, boja, pa sušenje,

165
00:30:50,610 --> 00:30:55,151
a tokom sušenja možete dodati još, ako želite intenzivniju rdju.

166
00:30:55,351 --> 00:31:01,861
Takodje vidite kako se Aquacolor na cvetovima menja,

167
00:31:02,061 --> 00:31:07,965
i dobijamo efekat budji. Ne plašite se, eksperimentišite sa materijalima.

168
00:31:08,165 --> 00:31:10,993
Nikad ne znate kakav efekat ćete postići.

169
00:31:11,193 --> 00:31:16,891
Ja sam zadovoljan rezultatom.

170
00:31:17,091 --> 00:31:27,170
Nadam se da se i vama dopada.

171
00:31:27,370 --> 00:31:33,726
Ovo je današnji projekat, hvala što ste gledali.

172
00:31:33,926 --> 00:31:37,820
I zapamtite, ne plašite se, eksperimentišite.

173
00:31:38,020 --> 00:31:44,082
To sam rekao na hiljade puta tokom radionica, pokažite drugu stranu svoje ličnosti, probajte nešto novo.

174
00:31:44,282 --> 00:31:46,482+
I tako ćete postići fntastične rezultate.

175
00:31:53,445 --> 00:31:53,694
Hvala što pratite naše radionice!

176
00:31:58,706 --> 00:32:03,559
Vidimo se sledeće nedelje.

177
00:32:03,759 --> 00:32:10,057
Oslobodite se granica, oslobodite svoju maštu, eksperimentišite...

178
00:32:10,257 --> 00:32:11,992
Hvala i vidimo se uskoro.