1
00:00:03,724 --> 00:00:07,341
How are you? Good evening. Here we are again at handmadefantasyworld.com.

2
00:00:07,541 --> 00:00:08,980
I hope you missed me.

3
00:00:09,180 --> 00:00:14,699
Last month, the two previous videos were made by my friend Boca Rancovic

4
00:00:14,899 --> 00:00:18,567
I had some errands to do and I couldn't be here to film the videos.

5
00:00:18,767 --> 00:00:25,237
So here we are, first video for May as we skipped last Monday.

6
00:00:25,437 --> 00:00:33,800
It was 1st of May and there was no way to film the video, most of us were missing.

7
00:00:34,000 --> 00:00:41,608
First video for May. We'll run this month together. So we start with a mixed media project.

8
00:00:41,808 --> 00:00:49,381
We'll combine many materials from different companies. You'll see them during the procedure.

9
00:00:49,581 --> 00:00:52,733
So let's start. Thanks for being here.

10
00:00:52,933 --> 00:01:01,098
We'll deal with a tag, as I already mentioned it's a mixed media project.

11
00:01:01,298 --> 00:01:07,676
We'll first use a stencil from the new line of "Stamperia".

12
00:01:07,876 --> 00:01:23,295
We'll cover the stencil with a medium grain paste by "PacoArt" line. There are small grains in it.

13
00:01:23,495 --> 00:01:26,748
Which is something that helps us a lot as the rust will be seen better, in the end.

14
00:01:26,948 --> 00:01:34,958
We use a spatula. As I have already said, it's better to take a big quantity of paste and spread it

15
00:01:35,158 --> 00:01:39,089
always by keeping your spatula with an upwards angle.

16
00:02:00,847 --> 00:02:09,252
The stencil is ready. We'll put it aside to let it dry.

17
00:02:09,452 --> 00:02:19,265
Using another stencil by "Stamperia", this one with the mesures design, as well as a stone paper

18
00:02:19,465 --> 00:02:28,664
on the smooth side, we'll spread paste on the stencil.

19
00:02:33,221 --> 00:02:42,625
The "Stamperia" stone paper, is stone substanced and it's very resistant in anything we put on it.

20
00:02:42,825 --> 00:02:49,719
That's why we use this paper. Later on we'll spray with aqua color and if we used a common

21
00:02:49,919 --> 00:02:57,887
paper, it would soon get damaged by the liquid paints.

22
00:02:58,531 --> 00:03:04,638
So we work on a stone paper and we are absolutely sure that there will not be any problem.

23
00:03:06,479 --> 00:03:09,058
It's the same procedure, as I did on the tag.

24
00:03:18,741 --> 00:03:23,067
Now, we'll dry them with the hair dryer.

25
00:03:23,267 --> 00:03:31,279
These "PacoArt" pastes need at least two or three minutes of hot air drying.

26
00:03:31,479 --> 00:03:35,894
Then we have to let them dry by their own. Let's dry them.

27
00:03:52,476 --> 00:03:57,794
We dried it a little with the hair dryer and now we let them completely dry in room temperature.

28
00:03:59,430 --> 00:04:06,852
I have one already made. So now we will scissor the mesures one by one.

29
00:04:21,240 --> 00:04:30,822
The stencil is dry, the stone paper is cut, but before glueing the mesures on the wooden tag,

30
00:04:31,022 --> 00:04:34,323
we need to paint the whole tag surface.

31
00:04:34,523 --> 00:04:45,834
We'll use a light turquoise color from the "Artebella" multi surface line.

32
00:04:46,034 --> 00:04:50,101
I like its bottling, it's big and not because I'm using it, but because the colors are very good.

33
00:04:50,301 --> 00:04:51,542
They are easy to apply.

34
00:04:51,742 --> 00:04:58,054
We'll use some color and we'll paint the whole surface.

35
00:05:03,331 --> 00:05:13,876
On the stencil we brush well, so as not to leave much paint and unshape the stencil's design.

36
00:05:36,961 --> 00:05:44,512
Our color is applied. I don't mind any color differences here, as I'll glue the mesures papers.

37
00:05:45,496 --> 00:05:50,769
I apply the color with long straight brushes from side to side... and we are ready.

38
00:05:50,969 --> 00:05:53,697
I put the brush in a water can and I dry.

39
00:06:15,139 --> 00:06:20,029
Now we'll create our composition using the mesures design papers.

40
00:06:23,022 --> 00:06:28,823
At the beginning and before glueing them I put them on the tag to arrange their setting.

41
00:06:50,990 --> 00:07:00,960
Since we are ready, we'll use the Glossy Gel Heavy Body paste by "Stamperia"

42
00:07:01,160 --> 00:07:06,618
Οccasionally, we've said that the Glossy Gel works as a glue too.

43
00:07:06,818 --> 00:07:09,606
We may glue different kinds of objects, that's the way we are going to work on today.

44
00:07:09,806 --> 00:07:15,664
We put a small layer of gel on the surface...

45
00:07:19,329 --> 00:07:22,798
we put the paper on the tag, we press a little and this one is already glued.

46
00:07:23,401 --> 00:07:25,557
That's what we will do for each one.

47
00:08:04,012 --> 00:08:11,967
Now that they are all glued, we'll deal with out main theme.

48
00:08:13,143 --> 00:08:24,328
Well, that's what we are going to do. We'll use a scissors as the basic decoration of our project.

49
00:08:24,528 --> 00:08:29,126
We'll use again the heavy body paste by "Stamperia" to glue it.

50
00:08:29,326 --> 00:08:33,667
In general, we'll use this gel to glue any other decoration or metalic element of our project.

51
00:08:33,867 --> 00:08:39,517
Now regarding the procedure... we check which side of the scissors attaches best on the surface.

52
00:08:45,152 --> 00:08:52,743
I find that the finger handles are the best part to be glued.

53
00:08:52,943 --> 00:08:55,471
We hold the scissors and apply gel paste on it.

54
00:09:05,879 --> 00:09:07,654
I apply much of a quantity.

55
00:09:12,813 --> 00:09:16,806
And I put the scissors on that point.

56
00:09:23,762 --> 00:09:27,289
I may take glue out from some points...

57
00:09:27,489 --> 00:09:38,484
We don't mind if any extra paste is protruding, we'll so many things on, so it will be covered.

58
00:09:38,684 --> 00:09:46,160
We take care to find the best point that the scissors attaches so that it glues well.

59
00:10:05,221 --> 00:10:12,614
The scissors is almost glued. This, surely, will be better a day after we finish the project.

60
00:10:12,814 --> 00:10:15,598
We've dried it so as to be able to work on it now.

61
00:10:15,798 --> 00:10:20,193
In general, they'll be moving, but in the end they will be stone glued.

62
00:10:20,393 --> 00:10:25,190
Until we put all the elements on the project the paste will have dried more.

63
00:10:25,390 --> 00:10:29,431
Then we'll use aqua color and the paste will get softer.

64
00:10:29,631 --> 00:10:35,064
As soon as we finish the project and after we have left it at least for a day, it will be completely dry.

65
00:10:35,264 --> 00:10:39,224
I have some metalic decorations.

66
00:10:39,424 --> 00:10:49,656
For Greeks, you will be able to find lots of things in the Hobby show "Decomagia.gr"

67
00:10:49,856 --> 00:10:51,442
This where I've also bought them from.

68
00:10:51,642 --> 00:11:04,463
Let's start decorating, always applying a lot of paste at the bottom of each decoration.

69
00:11:04,663 --> 00:11:09,575
We need the paste to be the attaching mean between each element.

70
00:11:09,775 --> 00:11:19,138
We use as much paste needed to fill in the gap between the two decorations.

71
00:11:22,304 --> 00:11:26,473
We start adding some decorations.

72
00:11:44,643 --> 00:11:50,058
If there is any extra paste, you dab brush it away.

73
00:12:08,778 --> 00:12:15,142
It's good to have another more brush to take out the exceeding quantity of paste.

74
00:12:56,810 --> 00:13:09,115
We've reached the desired result. As you can see there is exceeding paste. We won't dry it right away.

75
00:13:09,315 --> 00:13:13,858
Furthermore, in case any gel paste falls on the project, I dropped my brush here,

76
00:13:14,058 --> 00:13:17,477
we won't clear it so as not to leave a stain. We leave it as it is.

77
00:13:17,677 --> 00:13:37,058
I thought of using the "PentArt" powder on the exceeding paste. The result will be a nice one.

78
00:13:40,103 --> 00:13:48,997
I cover it and I use the hair dryer to remove the unnecessary powder.

79
00:13:59,214 --> 00:14:07,899
I may either use a brush to remove the powder and help the repousse design to be seen more.

80
00:14:19,983 --> 00:14:44,721
I dry a bit and then I put some gel underneath, I also aplly some here, you'll next see why.

81
00:14:44,921 --> 00:14:48,542
I put some around the main theme...

82
00:14:51,415 --> 00:14:55,484
May be here too... depending on where i want to...

83
00:15:06,688 --> 00:15:14,811
If there is anything bothering me, I pick it up and then I put it back on its position.

84
00:15:19,110 --> 00:15:24,301
I random apply gel paste at some points and I take the 3D balls by "Pentart".

85
00:15:24,501 --> 00:15:28,863
There are three sizes, I'll use the smallest and the medium size.

86
00:15:29,063 --> 00:15:36,917
I drop the medium size fisrst to let it glue.

87
00:15:40,828 --> 00:15:48,947
As soon as I finish with the medium balls, I drop the small ones to fill in all the rest spaces.

88
00:15:53,990 --> 00:16:01,676
Wherever there is gel, the balls glue and then with we blow the extra ones with the hair dryer.

89
00:16:01,876 --> 00:16:14,917
Or if we lift it up, they will drop down but carefully, cause the scissors glue is not yet well dried.

90
00:16:15,117 --> 00:16:22,285
The scissors might come off. We may either use the hair dryer or a brush to reveal

91
00:16:22,485 --> 00:16:24,773
the elements we want to be seen more.

92
00:16:31,050 --> 00:16:40,714
We'll also add some crackle monocomponent paste by "Stamperia", the mixed media line.

93
00:16:40,914 --> 00:16:49,419
I'll try to apply some paste on the scissors grip.

94
00:16:49,619 --> 00:16:57,660
So as to give an extra effect on our project. I won't put too much. I'll choose two points.

95
00:17:07,487 --> 00:17:09,777
And now we use the hair dryer to dry it.

96
00:17:26,965 --> 00:17:34,104
We don't need to dry it too much. We'll use spray colors so we won't touch it and spoil the paste.

97
00:17:34,304 --> 00:17:37,916
The crackle paste, as long as the time goes by,  the more it dries the more it will crack.

98
00:17:38,116 --> 00:17:40,865
We barely dried it to help it stabilize.

99
00:17:41,065 --> 00:17:48,971
As soon as with glue al the metalic elements and the rest of the decorations we need,

100
00:17:49,171 --> 00:17:53,402
we'll cover them with gesso so as to have an equable result.

101
00:17:53,602 --> 00:17:58,231
I'll use a gesso by "Campus". You may use any gesso you like.

102
00:17:58,431 --> 00:17:59,983
Due to the fact that the decorations are not yet solid glued and we cannot use a brush, I thought of

103
00:18:10,713 --> 00:18:18,506
getting some gesso with a spatula put it in a plastic plate

104
00:18:18,706 --> 00:18:33,769
and then spray it with much of water so as to make a fluid gesso.

105
00:18:33,969 --> 00:18:42,242
We mix it with a brush and there we have a watery gesso!

106
00:18:42,442 --> 00:18:54,393
Then I splash on the metalic elements until they become completely white.

107
00:18:54,593 --> 00:18:58,073
I continuously spray with water, depending on how much I need.

108
00:19:01,950 --> 00:19:09,946
I keep on splashing with liquid gesso. The splashes scattered around,

109
00:19:10,146 --> 00:19:15,063
will add an extra effect on the final result. Their being there don't mind ne at all.

110
00:19:15,263 --> 00:19:23,654
I might as well do them on purpose. I work with the brush against my finger.

111
00:19:23,854 --> 00:19:33,658
Thus I give an equable color on the metalic elements and the scissors too.

112
00:19:37,520 --> 00:19:45,834
I may also gently apply extra gesso at some points which are more stable.

113
00:19:53,407 --> 00:19:57,856
Very gently, though, as the elements are still moving.

114
00:20:08,524 --> 00:20:14,186
Not so much on the 3D balls as on the scissors, which must be covered.

115
00:20:22,879 --> 00:20:30,878
You could use any color you like. The scissors, the metalic elements, the papercrafts, the flowers,

116
00:20:31,078 --> 00:20:34,115
anything you use may be of any color. Even the worst materials you have and you don't use

117
00:20:34,315 --> 00:20:40,951
We may change their color so you don't have to think about their being of the same color or not.

118
00:21:00,762 --> 00:21:07,561
Since the gesso is so much diluted, we somehow keep the metalic look of the decorations.

119
00:21:17,252 --> 00:21:18,185
Now let's dry it.

120
00:22:12,201 --> 00:22:18,629
After drying the gesso, you may see that the metalic elements are not totally white.

121
00:22:18,829 --> 00:22:32,719
If you think that some points should be more white, now, you may use a brush and add gesso. 

122
00:22:32,919 --> 00:22:36,876
I personally think that no more is needed, it's ok.

123
00:22:38,331 --> 00:22:40,747
I like the fact that they keep their metalic color.

124
00:22:50,127 --> 00:23:01,702
Now we will work with aqua colors. We'll use the white coffee mist by "Pentart",

125
00:23:01,902 --> 00:23:16,816
theto pino, the wallnut and the turquoise aqua colors by "Stamperia" and of course, a water spray.

126
00:23:17,016 --> 00:23:23,949
The result depends on what look we want to give on the project. So we'll see and do accordingly.

127
00:23:24,149 --> 00:23:30,240
I'll start with the turquoise color by spraying on the stone papers.

128
00:23:37,381 --> 00:23:44,367
See! the color spray broke and I made a mistake, however, I'll add some extra splashes here to fix it.

129
00:23:55,752 --> 00:24:00,001
Even that mistake, may give us something startling on the final result of the project.

130
00:24:00,201 --> 00:24:03,364
I spray with the broken sprayer and I dry.

131
00:24:22,193 --> 00:24:25,846
Anything I don't like I dab it off with a paper towel.

132
00:24:27,605 --> 00:24:33,463
See? I dab off any extra color with the paper towel.

133
00:24:33,663 --> 00:24:45,668
Now we'll do all the arounds. I'll use the wallnut to make the arounds darker. On the stone papers too.

134
00:24:51,635 --> 00:25:00,944
I'll also have my project water sprayed and raise it so as the colors start bilging.

135
00:25:21,149 --> 00:25:24,709
You can work on aqua colors in different ways. You may spray and dry after you finish spraying.

136
00:25:24,909 --> 00:25:32,709
I personally spray and dry right away. Then a spray again creating layers. It depends, clearly,

137
00:25:32,909 --> 00:25:34,332
on the color you wish to have on your project.

138
00:25:34,532 --> 00:25:45,633
On the scissors, I want to use first the topino color by "Stamperia", I'll dry a bit.

139
00:26:01,018 --> 00:26:03,778
Then I'll add some white coffee mist by "Pentart".

140
00:26:19,087 --> 00:26:27,378
I'll continue using the wallnut color for an even brownish color around the main theme .

141
00:26:51,173 --> 00:27:00,331
As you can see, there is not a specific technique on how we use the colors. What the first color will be,

142
00:27:00,531 --> 00:27:05,386
what the second one will be, whether we'll use the hairdryer or not. It's up to how we want our project

143
00:27:05,586 --> 00:27:10,250
to be evolved. I check what is needed to be done and I proceed accordingly.

144
00:27:10,450 --> 00:27:14,844
You just have to let yourself free and you won't face any problem.

145
00:27:15,044 --> 00:27:18,667
You may choose any color you want.

146
00:27:21,171 --> 00:27:22,719
See, I try lots of things.

147
00:27:22,919 --> 00:27:32,542
I use paper towel to remove any extra color and see what a nice ageing effect is created!

148
00:27:41,112 --> 00:27:45,961
I keep on working on it until I'm satisfied with the result.

149
00:27:48,711 --> 00:27:53,206
You see here how they look, what color the white splashes are turned to. 

150
00:28:28,271 --> 00:28:35,978
After finishing with the aqua colors, we'll use a rust effect product by "TEX.CO".

151
00:28:36,178 --> 00:28:38,363
It, incidentally, came up on my way...

152
00:28:38,364 --> 00:28:42,456
my friend Maria Gratsa, from the art shop "ΟΝΕΙΡΟΚΟΣΜΟΣ" in Thiva, gave it to me.

153
00:28:42,656 --> 00:28:48,339
The first I used it , I was shocked! It is very simple, there are three bottles in the package.

154
00:28:48,539 --> 00:28:58,232
They are all sprays. One is the base and the two colors, the green one, turquoise for the mold effect

155
00:28:58,432 --> 00:29:02,791
and the rust, the orange color, today we'll use this one. The procedure is as follows.

156
00:29:02,991 --> 00:29:12,007
A quick one, you may use the hairdryer too. You spray with the bigger bottle the trasparent one,

157
00:29:12,008 --> 00:29:18,322
on the points you want to add the rust effect.

158
00:29:18,522 --> 00:29:25,591
Then right away you spray using the color spray of your choise.

159
00:29:27,214 --> 00:29:29,999
You spray and dry.

160
00:29:50,360 --> 00:29:54,062
During drying you may spray again using the first bottle.

161
00:30:03,125 --> 00:30:07,199
And then spray again with the second bottle.

162
00:30:29,465 --> 00:30:36,364
I don't know if you can distinguish, very well,  the result through camera, rust effect is already visible. 

163
00:30:36,564 --> 00:30:43,555
This rust product acts very fast. Next day, the rust effect is more intense.

164
00:30:43,755 --> 00:30:50,410
You surely start with the first material, then the color and dry. But in the meantime, you may

165
00:30:50,610 --> 00:30:55,151
spray again on some points, according to the intensity of rust you wanna give.

166
00:30:55,351 --> 00:31:01,861
You can also see that the turquoise aqua color that was clumsy sprayed on the flowers,

167
00:31:02,061 --> 00:31:07,965
created the mold effect. At this point, I would like to say that you should try things, don't be afraid.

168
00:31:08,165 --> 00:31:10,993
Use materials on your project, you never know what the result will be.

169
00:31:11,193 --> 00:31:16,891
I'm satisfied from the result and how the project looks all around.

170
00:31:17,091 --> 00:31:27,170
I think, you should be satisfied too. Let me reverse the project, so as to see it in its final form.

171
00:31:27,370 --> 00:31:33,726
From me, That's for today, thanks for watching. I hope you are satisfied. 

172
00:31:33,926 --> 00:31:37,820
What you have to bear in mind is not to be afraid, try new things.

173
00:31:38,020 --> 00:31:44,082
I've said it hundreds of times during seminars. Experiment, take another self from what you are.

174
00:31:44,282 --> 00:31:46,482
The results will be incredible.

175
00:31:53,445 --> 00:31:53,694
Thank you a lot for being again here with us. That was this week's project.

176
00:31:58,706 --> 00:32:03,559
We'll be together again next week. Wish you all a nice month,

177
00:32:03,759 --> 00:32:10,057
Don't forget to create without any limitation. 
Use your imagination and experiment a lot..

178
00:32:10,257 --> 00:32:11,992
Thank you very much, see you next time.