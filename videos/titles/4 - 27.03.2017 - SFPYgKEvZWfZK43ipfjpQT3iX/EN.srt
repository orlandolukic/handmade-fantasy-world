1
00:00:05,709 --> 00:00:10,697
Good morning from Belgrade, we are at handmadefantasyworld.com.

2
00:00:10,698 --> 00:00:16,756
Today we will work on a book box. We we make a mix media project.

3
00:00:16,956 --> 00:00:27,650
We will use some stamps by "Prima marketing, you see here, lots of designs. We wiil do a mixture.

4
00:00:27,850 --> 00:00:36,326
We wil use some acrylic paints by "Decoart", the "Americana" ones, some inks by " Tim Holtz",

5
00:00:36,526 --> 00:00:43,734
you'll see it during the process. We will also use clay by "Fimo" which is air dry.

6
00:00:43,735 --> 00:00:49,267
And finally,  as always a very nice material  by "Stamperia", the stone paper.

7
00:00:49,467 --> 00:00:50,651
Let's start!

8
00:00:50,851 --> 00:00:57,910
As you may see, I've unscrewed the lid of the box, cause we will use the stone paper here.

9
00:00:58,110 --> 00:01:05,663
We will begin painting the box. For the first coat, we are going to use the "Decoart"  Americana acrylic,

10
00:01:05,664 --> 00:01:09,205
whichr is light french blue

11
00:01:10,482 --> 00:01:19,245
Well, here we go. As you see the Americana paints are quite runny

12
00:01:20,640 --> 00:01:25,728
and it is very easy to paint without applying a primer, as long as you make long brushes.

13
00:01:56,272 --> 00:02:02,838
As you see, we've painted the box. It is very important to make long brushes,

14
00:02:03,038 --> 00:02:05,325
from the one edge to the other, to let the paint spread nicely

15
00:02:05,525 --> 00:02:10,905
however, you'll see that at some points, like here, it is not painted well, but I don't mind,

16
00:02:10,906 --> 00:02:12,277
because it gives me a vintage result.

17
00:02:12,477 --> 00:02:20,019
We've painted the sides. Again, I don't mind if it is not well painted. Maybe the wood is visible too.

18
00:02:20,219 --> 00:02:22,182
We' ve painted the back side as well, in the same way.

19
00:02:22,382 --> 00:02:27,550
There is no paint here, cause we are going to attach the stone paper, as if it is a bookbinding.

20
00:02:27,750 --> 00:02:29,729
I don't care if it is not painted.

21
00:02:30,634 --> 00:02:33,564
Since we finished painting, we dry.

22
00:02:45,000 --> 00:02:49,979
Now, let's do the color mix.

23
00:02:51,536 --> 00:03:01,311
We are going to use another color. we will start with a light turquoise.

24
00:03:01,312 --> 00:03:04,225
Again by "Decoart Americana" the "sea aqua".

25
00:03:05,055 --> 00:03:07,247
We don't need too much color.

26
00:03:07,447 --> 00:03:23,536
Now, We'll pick some paint and we'll apply it, in a patchy way, starting from the outer to the inner.

27
00:03:37,752 --> 00:03:53,089
Using a baby wipe, we process the paint in the same direction with the book, up or down.

28
00:03:53,289 --> 00:03:58,463
The baby wipe, due to the fact that it is wet,

29
00:03:58,464 --> 00:04:02,373
gives us the opportunity to spread the paint and do the two color mix.

30
00:04:05,688 --> 00:04:16,263
So, now we are going to use a darker turquoise, the "Desert Turquoise", again, by "Americana",

31
00:04:17,864 --> 00:04:20,783
These paints need to be shaked well, first.

32
00:04:26,112 --> 00:04:29,192
And let's follow the same procedure.

33
00:04:36,725 --> 00:04:46,194
We will add paint in the same patchy way, over the second color or even over the lighter turquoise.

34
00:04:46,394 --> 00:04:47,272
We use again the baby wipe.

35
00:04:56,847 --> 00:05:01,008
I create my shades.

36
00:05:06,000 --> 00:05:09,729
You see, I add and then remove, as many times as I want.

37
00:05:17,600 --> 00:05:21,851
Let's add a forth colour, as well. The "plum".

38
00:05:25,000 --> 00:05:32,166
You will be able to find the whole material list used, in the site, under this video.

39
00:06:06,517 --> 00:06:12,170
At some points we may add some grey again

40
00:06:18,975 --> 00:06:21,600
And using the baby wipe, I spread it.

41
00:06:27,000 --> 00:06:31,225
We follow the same procedure as many times as we want, until we have the desired result.

42
00:06:31,425 --> 00:06:36,060
This procedure will be for the front cover, but for the back side as well, Actually, all sides painted grey.

43
00:07:33,405 --> 00:07:42,531
After painting the box and blending the colors using the baby wipe, we will take a strong brush,

44
00:07:42,532 --> 00:07:48,020
wet it, and we will pick some white color.

45
00:07:49,384 --> 00:08:02,968
And we will splash at some points. Not in the center. This is where the clay will be put.

46
00:08:04,262 --> 00:08:05,695
Splash a lot, though.

47
00:08:13,195 --> 00:08:20,328
We will use a hair dryer to dry, from a distance, the color so as not to drip.

48
00:08:24,176 --> 00:08:35,688
Not too long. Just let only the first paint dry. Then we'll use a paper towel to dab dry out extra paint.

49
00:08:44,000 --> 00:08:46,980
So it gives us, this effect, right here.

50
00:08:47,180 --> 00:08:52,912
We will do the same thing at the back side, too and then we will go on with the stamps.

51
00:09:19,000 --> 00:09:29,030
We are through with this effect. Let's add some stamps for a background.

52
00:09:30,689 --> 00:09:37,443
I will use a stamp by "Prima marketing". It's like a broken window, I would say... like crackeled.

53
00:09:37,643 --> 00:09:50,091
We use permanent ink by "StazOn" cause it doesn't spoil no matter what we apply on it.

54
00:10:01,000 --> 00:10:06,907
The stamp will be put at the corners. As I've already said, the center is for the clay.

55
00:10:13,940 --> 00:10:14,887
I put some pressure.

56
00:10:37,037 --> 00:10:42,385
Not too many, only at some points. I'll do the say thing at the back side.

57
00:11:19,000 --> 00:11:19,931
Very nice!

58
00:11:23,191 --> 00:11:29,121
Maybe if I added another one stamp for the same background.

59
00:11:30,000 --> 00:11:31,979
We'll see what matches...

60
00:11:39,266 --> 00:11:42,029
Again a "Prima marketing" stamp

61
00:12:08,053 --> 00:12:12,374
You'll see that it's not well stamped, but that's the effect we wanna give.

62
00:12:12,595 --> 00:12:15,211
It is not necessary for the stamps to be perfectly seen.

63
00:12:18,171 --> 00:12:22,540
You see? close to the previous stamps

64
00:12:31,379 --> 00:12:39,866
And I try not to let the stamp outline extinguish.

65
00:12:42,636 --> 00:12:44,535
One more...

66
00:12:48,225 --> 00:12:51,087
The tiny clock... a "Prima marketing" stamp too.

67
00:13:04,638 --> 00:13:10,573
Very good! we will add one more.

68
00:13:17,889 --> 00:13:22,992
We continue stamping.

69
00:13:25,051 --> 00:13:31,826
It doesn't make any difference, how we put them. We create a mix for our background.

70
00:13:32,026 --> 00:13:35,678
I've stamped the back side too. ok?

71
00:13:35,878 --> 00:13:43,980
Let's move to the next step. Or maybe not. I'll add one more stamp.

72
00:13:47,180 --> 00:13:53,187
All the stamps I've used is by "Prima marketing" the Finnabair line.

73
00:13:54,162 --> 00:13:58,976
I press enough. As you may see I put the one stamp over the other

74
00:14:03,692 --> 00:14:10,519
Now let's draw our lines, so as to make them look like wooden slats.

75
00:14:11,821 --> 00:14:20,008
We will need a ruler. It is better to have a wide one, so as not measure.

76
00:14:20,208 --> 00:14:28,392
With a soft pencil we draw gross lines.

77
00:14:28,592 --> 00:14:43,666
We want bold lines, so we draw them many times.

78
00:15:01,977 --> 00:15:12,821
I will draw one more line at the edge of the box lid.

79
00:15:14,950 --> 00:15:17,584
Unfortunately I cannot do it with the ruler because of the hinges.

80
00:15:20,299 --> 00:15:29,361
After drawing the lines, we will use a baby wipe to pass over them.

81
00:15:31,800 --> 00:15:45,784
In this way, the pencil gets wet and with our finger we shade it to give depth.

82
00:15:59,634 --> 00:16:01,842
We don't mind if the lines disappear partially.

83
00:16:05,077 --> 00:16:07,723
Ths the result is more realistic.

84
00:16:16,547 --> 00:16:18,804
I work it until I'm satisfied with the result.

85
00:16:38,557 --> 00:16:40,344
The same thing at the back side.

86
00:17:05,283 --> 00:17:12,588
Our background is ready. We will create one more effect. We will use inks by "Tim Holtz"

87
00:17:12,788 --> 00:17:17,968
"distress ink". We are going to use two colors. The Walnut stain and the Broken China.

88
00:17:18,168 --> 00:17:28,320
This is what we do! We start with the ligher one and apply it at the corners.

89
00:17:28,520 --> 00:17:46,968
Then I spray the point with water and I dry it out.

90
00:17:54,605 --> 00:18:04,379
I want to create these lines... I use the air to "push" the paint. I'll do the same with the darker ink.

91
00:18:05,695 --> 00:18:16,640
Little water spray, this time. We don't mind about the dripping lines, they match the stamp.

92
00:18:21,363 --> 00:18:24,028
Now, let's adjust this effect to all box sides.

93
00:18:33,660 --> 00:18:35,024
...but only at the corners.

94
00:18:37,678 --> 00:18:43,518
Just a liitle water spray...and with the hair drier we create our leaking effect.

95
00:20:20,608 --> 00:20:27,863
At the end, we remove anything we don't like with a baby wipe. I've done nothing in the center

96
00:20:28,063 --> 00:20:29,591
because I'll attach the clay.

97
00:20:36,392 --> 00:20:38,364
Softly, with the baby wipe.

98
00:20:39,542 --> 00:20:44,925
If I happen to spoil it, I may re apply ink.

99
00:21:10,962 --> 00:21:12,583
Same things for the back side.

100
00:22:01,234 --> 00:22:05,781
I think that we have a satisfactory background result.

101
00:22:06,476 --> 00:22:12,184
We can use lots of things in order to create a beautiful background.

102
00:22:12,185 --> 00:22:20,271
As you can see, the colors are more intense at the back side, since there will be no clay here.

103
00:22:20,471 --> 00:22:24,304
Now we will paint the rest parts of the box.

104
00:22:24,504 --> 00:22:35,563
We will use the Americana "Melon" by "Decoart". Acrylic as always.

105
00:22:36,032 --> 00:22:54,985
We will paint the box sides. I use a color that contrasts with the ones already used.

106
00:22:56,052 --> 00:23:01,484
Don't be afraid to used intense colors. We can change them anytime, using patch or oil.

107
00:23:07,533 --> 00:23:11,804
As you can see, if I happen to spoil it, I blend the colors with my fingers.

108
00:23:30,462 --> 00:23:32,088
Let's paint the last side too.

109
00:23:45,719 --> 00:23:47,146
We dry!

110
00:24:00,377 --> 00:24:12,951
Now we will stamp the side. A "Prima marketing" stamp.

111
00:24:17,155 --> 00:24:22,400
I repeat, you will be able to find all the materials used underneath this video, as well as,

112
00:24:22,600 --> 00:24:24,158
the sites, where you may order the materials from.

113
00:24:26,092 --> 00:24:31,137
I see, that some materials may not be available in Greece, or in the country where you are.

114
00:24:32,797 --> 00:24:51,913
Actually now, I'm gonna... I'll use the same stamp, until the whole side is covered.

115
00:25:16,458 --> 00:25:28,895
We are done with the stamps. We will create a hue using the "Distress" inks by "Tim Holtz".

116
00:25:29,095 --> 00:25:46,650
The same procedure as previously. Some ink , then water spray and the ink is leaking.

117
00:25:46,850 --> 00:26:10,936
I repeat it using the brown ink. Not too intense, as much as that. And finally, we dry.

118
00:26:19,366 --> 00:26:22,108
The rest sides, as well.

119
00:26:34,308 --> 00:26:37,175
The dark color last.

120
00:26:45,195 --> 00:26:53,809
At some points, we may add some using our fingers. Then water spray and hair dryer.

121
00:26:58,050 --> 00:26:59,908
Now we will apply it on the last side.

122
00:27:24,580 --> 00:27:26,228
A little bit of brown.

123
00:28:13,978 --> 00:28:23,091
I'll do the same thing here... using my finger to put some ink. I'll spray water...

124
00:29:08,088 --> 00:29:17,033
Let's create the clay part. I have mine already made.

125
00:29:20,894 --> 00:29:30,334
It's an air dry clay by "Fimo". it's a self dry one. You just take a small piece, I'll show you how to do it.

126
00:29:30,335 --> 00:29:33,684
You take a pice and you place it on the book so that you can measure it.

127
00:29:33,685 --> 00:29:42,230
Then with a rolling pin you spread the clay. You have to spread equable both sides.

128
00:29:42,231 --> 00:29:51,795
I'll make a small piece, just to show you how we do it. It would be nice to turn over sides.

129
00:29:51,995 --> 00:29:58,744
So, I create (using more clay), this piece.

130
00:29:58,944 --> 00:30:05,337
I'm gonna show you, how we are going to glue this piece of clay. We turn up side down and using,

131
00:30:05,537 --> 00:30:14,752
hobby glue, I will use the extra forte by "Stamperia", we apply glue perimetrically.

132
00:30:20,952 --> 00:30:26,425
At some points, not too much, in the center as well.

133
00:30:27,177 --> 00:30:42,204
I pick up the clay piece and glue it on the project.

134
00:30:45,596 --> 00:30:52,120
Using a baby wipe, I'll clean any remaining glue.

135
00:31:01,585 --> 00:31:12,715
And now, as long as the clay is still malleable, I shape it the way I want.

136
00:31:21,716 --> 00:31:25,718
Maybe, with the rolling pin to make it more flat.

137
00:31:29,040 --> 00:31:39,998
Now, let's create our subject. Again a "Prima marketing" stamp of "Finnabair" line.

138
00:31:41,785 --> 00:31:47,608
I press well the stamp on the clay. I firstly press on the center and then at the outer of the stamp,

139
00:31:47,609 --> 00:31:49,687
as the clay is still soft and transforming.

140
00:31:57,965 --> 00:32:06,051
Slowly, I remove the stamp. Very, very nice!

141
00:32:06,582 --> 00:32:09,992
We will add our phrasal stamps too.

142
00:32:19,589 --> 00:32:21,885
We carefully, stamp.

143
00:32:30,701 --> 00:32:38,888
Using the stamp of our background, we create some textures on the clay too.

144
00:32:40,035 --> 00:32:47,863
Wherever, we see the stamp outline, we fade it out with our finger.

145
00:32:56,108 --> 00:33:10,015
With a knife or a spatula, we may create some cracks and then soften them with our finger.

146
00:33:17,153 --> 00:33:19,212
Nice, ... some cracks here too.

147
00:33:22,367 --> 00:33:25,722
now, we remove the phrasal stamps so as to dry it.

148
00:33:30,243 --> 00:33:33,772
Normally, the clay needs at least one day or two to dry,

149
00:33:33,774 --> 00:33:38,653
but now, we will dry its surface, so as to show you the procedure.

150
00:33:50,166 --> 00:33:55,731
It is expected for the clay not to have dried well, especially when it is thick,

151
00:33:55,931 --> 00:33:59,194
but today we did it skin deep with the hair dryer, in order to work on it.

152
00:33:59,394 --> 00:34:03,764
It cracked by itself at some points, but that's what we need for an even vintage result.

153
00:34:03,964 --> 00:34:11,701
Well, after the clay is dried, we use again the same stamps, but now, we apply them with ink, to keep

154
00:34:11,901 --> 00:34:20,825
the motive on the clay. I'll use "Archival" black ink. We need something very intense.

155
00:34:22,348 --> 00:34:31,537
I've put ink on my stamp and then attach it to same point that I originally stamped.

156
00:34:39,727 --> 00:34:53,680
As you can see the stamp is visible in within the gaps.We do this so as the stamp is better involved.

157
00:34:53,681 --> 00:35:00,289
We will do the same thing with the phrases and the letters. We just try to stamp on the same point.

158
00:35:06,433 --> 00:35:08,336
Maybe, you need to press even more.

159
00:35:12,323 --> 00:35:15,411
Nice, I'll stamp this one again.

160
00:35:33,821 --> 00:35:41,878
This is a permanent ink, so there will be no problem, no matter what we use over it.

161
00:35:41,879 --> 00:35:52,248
Let's give our clay the ageing effect.We will use "Bitumen". It's a technique that my friend,

162
00:35:52,448 --> 00:35:57,270
Maria at the "Ονειρόκοσμος" (Dreamworld) workshop in Thiva, showed me.

163
00:35:57,470 --> 00:36:00,906
I liked it very much, so I thoght of using it.

164
00:36:01,106 --> 00:36:06,697
I had programmed to work on my clay on a different way, but you will like this way, very much too.

165
00:36:06,897 --> 00:36:07,993
I thank Maria.

166
00:36:09,812 --> 00:36:18,787
You see, I pick some "Bitumen" and start covering all the clay.

167
00:36:18,987 --> 00:36:21,997
I insist on the gaps so as to let the "Bitumen" intrude.

168
00:36:26,430 --> 00:36:35,046
The "Bitumen" doesn't affect the stamp ink at all. So you may fill the gaps.

169
00:36:40,870 --> 00:36:51,046
At the edge, we brush under the clay, like a shading.If we spoil the box, we  use a baby wipe.

170
00:36:52,701 --> 00:36:55,978
Or our hand, whatever it's easiest. It's ok!

171
00:37:14,443 --> 00:37:21,958
Since we finished with the "Bitumen", we start removing it with a baby wipe.

172
00:37:24,270 --> 00:37:27,955
We remove as much as we want, at some points more, at some others less.

173
00:37:35,652 --> 00:37:43,167
Let's take a new baby wipe... and softly wipe, leaving more wherever we want to make it look old.

174
00:38:10,000 --> 00:38:17,548
The stamp face may not be easily seen, but we don't care, cause the project is vintage.

175
00:38:20,748 --> 00:38:22,223
You see? Softly.

176
00:38:24,414 --> 00:38:34,227
Ah! here for example, I'll let it darker. At the corner, I like it... here too, under the letters.

177
00:38:35,000 --> 00:38:40,497
This side too. I think that the clay is ready. We'll clean the perimeter...

178
00:38:40,697 --> 00:38:58,942
...now, we are going to use four nails, so as to give our clay a better view.

179
00:39:05,484 --> 00:39:11,748
I won't nail it to the end, I want it to look broken.

180
00:39:20,011 --> 00:39:30,268
And not even straight nailed, a bit askew... the result looks better

181
00:39:39,468 --> 00:39:41,340
We should change this one direction!

182
00:39:46,782 --> 00:39:49,156
Our clay is ready.

183
00:39:57,499 --> 00:40:04,429
Let's create the box bookbinding. We will use a stone paper by "Stamperia".

184
00:40:04,629 --> 00:40:13,183
We will paint it using "Americana" acrylic paint by "Decoart", the shade is the "Bleached Sand"

185
00:40:15,383 --> 00:40:26,519
We'll take a brush to apply paint on the stone paper.

186
00:40:26,520 --> 00:40:30,539
We use stone paper, cause, it's resistant to any material we may use.

187
00:40:30,739 --> 00:40:37,476
If we used any other kind of paper, it would get destroyed by the liquids, the paints we apply on.

188
00:40:38,397 --> 00:40:43,278
You see, very important as I have already said, make long brushes.

189
00:40:59,223 --> 00:41:03,550
Now it's time to dry it a bit.

190
00:41:16,465 --> 00:41:26,601
We will again use the stamps and the brown "StazOn" ink to create a background.

191
00:41:26,801 --> 00:41:35,346
I'll use this stamp, that I have already used on both the box and the clay.

192
00:41:35,347 --> 00:41:38,283
It's associated with everything.

193
00:41:38,284 --> 00:41:43,101
It is very important to have little of something used everywhere.

194
00:41:49,364 --> 00:42:09,789
This one here. You'll see the stamp square, so with a baby wipe, we remove it.

195
00:42:09,989 --> 00:42:32,258
I don't like the square lines. We'll take the bigger stamp and put some. Here... and here...

196
00:42:33,363 --> 00:42:45,226
Sometimes I let things barely seen. It is not necessary for everything to be visible.

197
00:42:45,426 --> 00:43:01,179
I created my background and now using a stencil and media gloss gel paste by" Stamperia",

198
00:43:01,379 --> 00:43:19,410
we are going to cover the stencil. I keep my spatula slant, the stencil doesn't move this way.

199
00:43:45,856 --> 00:43:53,401
Let's cover the rest.

200
00:43:59,236 --> 00:44:02,071
We don't mind if it is not everywhere covered.

201
00:44:04,818 --> 00:44:13,136
It's ready to dry it. Use warm air, not too close and then let it adjust to the room temperature.

202
00:44:20,476 --> 00:44:25,400
After the paste is dried we will glue the stone paper on the book box.

203
00:44:25,600 --> 00:44:35,938
We will use the gel paste by "Stamperia", the one we used on the stencil. I put much of a quantity

204
00:44:36,138 --> 00:44:50,929
I don't spread it, I dab. I'll start from this side,and press until I see the glue emerge out.

205
00:44:51,129 --> 00:44:56,668
See? the glue emerges out which means that it is glued.

206
00:44:59,800 --> 00:45:04,808
Using a baby wipe I clean the remaining glue.

207
00:45:10,080 --> 00:45:21,620
I' ll turn the box over and put some glue by dabbing.

208
00:45:26,119 --> 00:45:32,505
And do the same. From the bottom to the top, I press so as the glue emerges out.

209
00:45:42,001 --> 00:45:43,309
We will clean the glue.

210
00:45:45,367 --> 00:45:49,214
I check again, cause the stone paper moved a bit.

211
00:45:52,454 --> 00:46:01,249
And now the most difficult part. I'll put glue straight on the paper, not on the wood.

212
00:46:04,586 --> 00:46:09,909
Dabbing not spreading, I just let it be. We need extra glue.

213
00:46:10,109 --> 00:46:14,859
As long as glue emerges out, it means that it glues.

214
00:46:15,059 --> 00:46:20,307
I messed up the stencil here, but it's ok , we want it to look old.

215
00:46:25,112 --> 00:46:37,885
And now just like that, we may use the brush and some water, we clean the emerging glue.

216
00:46:41,320 --> 00:46:47,297
With a new baby wipe, we finish it.

217
00:46:51,614 --> 00:46:59,600
The stone paper is glue. I collect any remains.

218
00:46:59,800 --> 00:47:04,899
So, it's now time to put the lid back on its place.

219
00:47:07,000 --> 00:47:14,262
the color is perfect. i tried to find a color that's matches the clay color.

220
00:47:14,462 --> 00:47:17,536
So, when afterwards I use "Bitumen" it takes similar shade.

221
00:47:18,381 --> 00:47:20,171
Well, let's put the screws.

222
00:48:05,935 --> 00:48:25,409
We are ready. I'll paint the hinge under the same color we made the stone paper.

223
00:48:30,576 --> 00:48:33,612
Very nice, let's dry it.

224
00:48:39,756 --> 00:48:47,998
You see the result, now we will cover it with "bitumen" to make it look old.

225
00:48:48,200 --> 00:48:57,347
We will use "bitumen" again, cause the material I used on the clay too and make it connect.

226
00:49:00,882 --> 00:49:11,760
I'll take my brush. This is a water "bitumen", so there is no problem.

227
00:49:26,297 --> 00:49:34,777
On the hinge as well, to look old, a bit around it...and then wipe it with a baby wipe.

228
00:49:51,562 --> 00:50:01,619
And now I start... removing with the baby wipe which I see that it is very wet and too much

229
00:50:01,819 --> 00:50:09,282
"bitumen" is wiped out, so there are two ways to do it. Either by using a paper towel or by drying it.

230
00:50:17,110 --> 00:50:26,460
...now I'm gonna remove it with a baby wipe. I'll leave some extra close to the hinge.

231
00:50:34,012 --> 00:50:36,114
Very nice, let's dry...

232
00:50:57,351 --> 00:51:04,152
Since we are done drying ,we'll remove "bitumen" gently.

233
00:51:04,352 --> 00:51:10,560
Because of the gel paste the "bitumen" stays in the gaps. that's the result we want to give.

234
00:51:11,259 --> 00:51:19,925
Gently... maybe the stamp might not seen well. It doesn't matter.

235
00:51:20,125 --> 00:51:28,607
Sometimes, we do things and as the project continues, we realise that they were not necessary.

236
00:51:48,041 --> 00:51:57,022
This is the result, I'm checking, just in case, anything else is needed. i think we are ok.

237
00:51:58,000 --> 00:52:05,249
This was today's project, thank you for being again here. Mix Media book box...

238
00:52:05,449 --> 00:52:10,159
Acrylics by "Decoart" Americana, stamps "Prima marketing" by Finnabair,

239
00:52:10,359 --> 00:52:15,541
stone paper by "Stamperia", "Tim Holtz" inks.

240
00:52:15,741 --> 00:52:22,533
You saw that very easily, Just by blendind you may have a very good result.

241
00:52:22,733 --> 00:52:26,269
I hope you enjoyed it, Thank you very much. Ssee you next time! Bye!!!