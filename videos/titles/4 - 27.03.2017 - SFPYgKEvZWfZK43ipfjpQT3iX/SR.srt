﻿1
00:00:05,709 --> 00:00:10,697
Dodar dan iz Beograda!

2
00:00:10,698 --> 00:00:16,756
Danas ćemo raditi kutiju knjigu, mix media stil.

3
00:00:16,956 --> 00:00:27,650
Koristiću Prima marketing pečate, fantastični su.

4
00:00:27,850 --> 00:00:36,326
Koristiću DecoArt Americana akrilne boje, Tim Holtz mastila.

5
00:00:36,526 --> 00:00:43,734
Radiću i sa Fimo glinom koja se suši na vazduhu.

6
00:00:43,735 --> 00:00:49,267
I naravno Stamperia Stonepaper.

7
00:00:49,467 --> 00:00:50,651
Hajde da počnemo!

8
00:00:50,851 --> 00:00:57,910
Skinuo sam poklopac kutije jer ću lepiti stonepaper.

9
00:00:58,110 --> 00:01:05,663
Počećemo sa farbanjem. Prvo nanosim DecoArt Americana akrilnu boju

10
00:01:05,664 --> 00:01:09,205
svetlo plava (light french blue)

11
00:01:10,482 --> 00:01:19,245
Americana boje su vrlo razmazive.

12
00:01:20,640 --> 00:01:25,728
Nije potreban prajmer. Nanosimo boju dugim potezima.

13
00:01:56,272 --> 00:02:02,838
Jako su važni dugi potezi četkom,

14
00:02:03,038 --> 00:02:05,325
od ivice do ivice, kako bi se boja ravnomerno nanela.

15
00:02:05,525 --> 00:02:10,905
Videćete da neke tačke nisu dobro obojene, ali to mi ne smeta.

16
00:02:10,906 --> 00:02:12,277
To će doprineti vintage stilu.

17
00:02:12,477 --> 00:02:20,019
Obojili smo stranice. Ne smeta nam ako neki delovi ostanu ne obojeni.

18
00:02:20,219 --> 00:02:22,182
Na isti način bojim zadnju stranu.

19
00:02:22,382 --> 00:02:27,550
Ovaj deo ne bojimo, jer ćemo lepiti stonepaper. Napravićemo kao povez na knjizi od stone papira.

20
00:02:27,750 --> 00:02:29,729
Ne smeta mi ako nešto nije obojeno.

21
00:02:30,634 --> 00:02:33,564
I sušimo.

22
00:02:45,000 --> 00:02:49,979
Sada ćemo pomešati boje.

23
00:02:51,536 --> 00:03:01,311
Počećemo sa svetlo tirkiz.

24
00:03:01,312 --> 00:03:04,225
Ameicana Sea aqua.

25
00:03:05,055 --> 00:03:07,247
Ne treba nam mnogo boje.

26
00:03:07,447 --> 00:03:23,536
Nanosimo boju neravnomerno, od ivice ka unutrašnjosti.

27
00:03:37,752 --> 00:03:53,089
Koristimo vlažne maramice da boju razvučemo duž kutije, gore ili dole.

28
00:03:53,289 --> 00:03:58,463
Vlažna maramica, zato što je mokra,

29
00:03:58,464 --> 00:04:02,373
nam omogućava da razmažemo boju i na taj način pomešamo dve boje

30
00:04:05,688 --> 00:04:16,263
Sada ćemo naneti tamniju tirkiz boju,Americana Desert Turquoise.

31
00:04:17,864 --> 00:04:20,783
Boje moraju prvo dobro da se promućkaju.

32
00:04:26,112 --> 00:04:29,192
Ponavljamo postupak.

33
00:04:36,725 --> 00:04:46,194
Nanosimo neravnomerno i ovu boju, preko prethodno nanetih boja.

34
00:04:46,394 --> 00:04:47,272
Ponovo koristimo vlažne maramice.

35
00:04:56,847 --> 00:05:01,008
Napravio sam senku koju sam želeo.

36
00:05:06,000 --> 00:05:09,729
Vidite da nanosim i brišem koliko god puta želim dok ne dobijem željeni rezultat.

37
00:05:17,600 --> 00:05:21,851
Nanosimo i četvrtu boju, Americana Plum (boja šljive)

38
00:05:25,000 --> 00:05:32,166
Videćete popis svih materijala.

39
00:06:06,517 --> 00:06:12,170
Nanećemo ponegde ponovo sivu.

40
00:06:18,975 --> 00:06:21,600
I ponovo razmazujem vlažnom maramicom. 

41
00:06:27,000 --> 00:06:31,225
Dakle, možemo sve ponavljati koliko god želimo dok ne postignemo željeni rezultat.

42
00:06:31,425 --> 00:06:36,060
Na ovaj način ćemo obojiti prednju i zadnju stranu.

43
00:07:33,405 --> 00:07:42,531
Nakon nanošenja i mešanja boja, uzimamo tvrdu četku,

44
00:07:42,532 --> 00:07:48,020.
kvasimo je i uzimamo četkicom belu boju.

45
00:07:49,384 --> 00:08:02,968
Prskamo poklopac, ali ne centralno jer će tu biti glina.

46
00:08:04,262 --> 00:08:05,695
Prskamo dosta na odredjenim pozicijama.

47
00:08:13,195 --> 00:08:20,328
Fenom ćemo osušiti boju, ali fen držimo udaljeno od kutije kako vazduh ne bi razlio boju.

48
00:08:24,176 --> 00:08:35,688
Not too long. Just let only the first paint dry. Then we'll use a paper towel to dab dry out extra paint.
Ne previše.Samo prosušimo boju, a višak ćemo skinuti tapkanjem ubrusa. 

49
00:08:44,000 --> 00:08:46,980
To nam daje ovaj efekat koji vidite.

50
00:08:47,180 --> 00:08:52,912
Isto ćemo uraditi i na zadnjoj strani, zatim ćemo naneti pečate.

51
00:09:19,000 --> 00:09:29,030
Gotovi smo sa ovim efektom i sada ćemo početi sa nanošenjem pečata.

52
00:09:30,689 --> 00:09:37,443
Naneću Prima marketing pečat, liči na polomljeno staklo ili krekl.

53
00:09:37,643 --> 00:09:50,091
Na pečate nanosimo permanentno mastilo StazOn koje se neće razmazati kada nastavimo sa nanošenjem drugih boja. 

54
00:10:01,000 --> 00:10:06,907
Pečat nanosimo u uglove. Centralni deo ostavljamo čist za glinu.

55
00:10:13,940 --> 00:10:14,887
Potreban je jači pritisak da bi napravili dobar otisak pečata.

56
00:10:37,037 --> 00:10:42,385
Ne svuda, samo na odredjenim delovima.

57
00:11:19,000 --> 00:11:19,931
Dobro...

58
00:11:23,191 --> 00:11:29,121
Možda ću dodati još neki pečat.

59
00:11:30,000 --> 00:11:31,979
Hajde da vidimo koji se uklapa...

60
00:11:39,266 --> 00:11:42,029
Ponovo koristim Prima marketing pečate.

61
00:12:08,053 --> 00:12:12,374
Vidite da otisak nije ceo, ali to i želim da postignem.

62
00:12:12,595 --> 00:12:15,211
Nije uvek neophodno da se pečat savršeno vidi.

63
00:12:18,171 --> 00:12:22,540
Vidite da drugi pečat stavljam blizu prvog pečata.

64
00:12:31,379 --> 00:12:39,866
Pokupavam da ne "ugušim" pečat koji sam prvo naneo.

65
00:12:42,636 --> 00:12:44,535
Još jedan...

66
00:12:48,225 --> 00:12:51,087
Mali sat....Prima marketing ponovo.

67
00:13:04,638 --> 00:13:10,573
Odlično! Dodaću još jedan.

68
00:13:17,889 --> 00:13:22,992
Nastavljamo sa pečatima...

69
00:13:25,051 --> 00:13:31,826
Možemo ih naneti na razne načine. Pravimo miks pečata za podlogu.

70
00:13:32,026 --> 00:13:35,678
Isto sam uradio i na zadnjoj trani.

71
00:13:35,878 --> 00:13:43,980
Ipak ću dodati još jedan pečat, pre nego što nastavim...

72
00:13:47,180 --> 00:13:53,187
Svi Prima marketing pečati koje koristim su iz Finnabair linije.

73
00:13:54,162 --> 00:13:58,976
Jako pritiskam. Vidite da stavljam jedan pečat preko drugog.

74
00:14:03,692 --> 00:14:10,519
Sada ćemo iscrtati linije, da dobijemo efekat letvica.

75
00:14:11,821 --> 00:14:20,008
Potreban nam je lenjir. Nije potrebno da merimo.

76
00:14:20,208 --> 00:14:28,392
Mekom olovkom crtamo debele linije.

77
00:14:28,592 --> 00:14:43,666
Potrebne su nam baš debele linije tako da ponavljamo potez olovkom.

78
00:15:01,977 --> 00:15:12,821
Iscrtavam i ivicu poklopca.

79
00:15:14,950 --> 00:15:17,584
To ne mogu da uradim lenjirom zbog šarki.

80
00:15:20,299 --> 00:15:29,361
Nakon iscrtavanja, linije prelazimo vlažnim maramicama.

81
00:15:31,800 --> 00:15:45,784
Kad se grafit pokvasi, trljamo prsima i dobijamo "dubinu" linija.

82
00:15:59,634 --> 00:16:01,842
Nije važno ako se trag olovke negde obriše.

83
00:16:05,077 --> 00:16:07,723
Tako je još realniji efekat.

84
00:16:16,547 --> 00:16:18,804
Ponavljam sve do ne dobijem rezultat koji mi se dopada.

85
00:16:38,557 --> 00:16:40,344
Isto ponavljamo na zadnjoj strani.

86
00:17:05,283 --> 00:17:12,588
Pozadina je skoro spremna. Još samo jedan efekat. Koristiću Tim Holtz Ink.

87
00:17:12,788 --> 00:17:17,968
Distress ink. Koristiću dve boje: walnut stain i Broken China.

88
00:17:18,168 --> 00:17:28,320
Počećemo sa svetlijom bojom i nanosimo je po uglovima.

89
00:17:28,520 --> 00:17:46,968
Zatim prskam vodu i sušim.

90
00:17:54,605 --> 00:18:04,379
Želim da napravim ove linije...vazduhom "guram" boju. Isto ću uraditi i sa tamnijom bojom.

91
00:18:05,695 --> 00:18:16,640
Prskam manje vode sada. Tragovi boje se uklapaju sa motivima pečata.

92
00:18:21,363 --> 00:18:24,028
Sada ćemo da uklopimo ovo u celu pozadinu tj. kutiju.

93
00:18:33,660 --> 00:18:35,024
...ali nanosimo samo na ćoškove.

94
00:18:37,678 --> 00:18:43,518
Samo malo vode i fenom pravimo ovaj efekat curenja vode.

95
00:20:20,608 --> 00:20:27,863
Na kraju, sve što nam se ne dopada možemo obrisati vlažnim maramicama. Centar je ostao prazan.

96
00:20:28,063 --> 00:20:29,591
Tu ćemo staviti glinu.

97
00:20:36,392 --> 00:20:38,364
Nežno brišemo vlažnim maramicama.

98
00:20:39,542 --> 00:20:44,925
AKo nešto i pokvarimo, nanećemo Ink ponovo.

99
00:21:10,962 --> 00:21:12,583
Isto radimo i na poleđini.

100
00:22:01,234 --> 00:22:05,781
Mislim da smo postigli dobar efekat na podlogu.

101
00:22:06,476 --> 00:22:12,184
Možemo mnogo toga da koristimo da postignemo odličnu podlogu.

102
00:22:12,185 --> 00:22:20,271
Na poledjini su boje intenzivnije, jer tu nećemo stavljati glinu.

103
00:22:20,471 --> 00:22:24,304
Sada ćemo farbati stranice.

104
00:22:24,504 --> 00:22:35,563
Koristiću Americana Melon akrilnu boju.

105
00:22:36,032 --> 00:22:54,985
Koristim boju koja će napraviti dobar kontrast.

106
00:22:56,052 --> 00:23:01,484
Ne plašite se intenzivnih boja jer uvek možete da ih promenite uljanom bojom ili razmazivanjem druge boje preko nje.

107
00:23:07,533 --> 00:23:11,804
Ako negde i predjem bojom gde ne treba, razmažem je prstima.

108
00:23:30,462 --> 00:23:32,088
Bojimo i poslednju stranicu....

109
00:23:45,719 --> 00:23:47,146
Sada sušenje...

110
00:24:00,377 --> 00:24:12,951
I na stranice ćemo staviti pečat, Prima marketing opet.

111
00:24:17,155 --> 00:24:22,400
Videćete listu materijala, koja prati ovaj video, 

112
00:24:22,600 --> 00:24:24,158
tako da možete da poručite online šta Vam se dopada,

113
00:24:26,092 --> 00:24:31,137
a ne možete da kupite u Vašoj zemlji.

114
00:24:32,797 --> 00:24:51,913
Koristiću jedan pečat da prekrijem stranicu celom dužinom.

115
00:25:16,458 --> 00:25:28,895
Gotovi smo sa pečatom. Toniramo stranice Tim Holtz Distress Inkom.

116
00:25:29,095 --> 00:25:46,650
Ponavljamo proceduru, ink, voda i sušimo dok se voda sliva.

117
00:25:46,850 --> 00:26:10,936
I ovde koristim braon Ink, ne previše intenzivan nanos...

118
00:26:19,366 --> 00:26:22,108
Celom dužinom stranica.

119
00:26:34,308 --> 00:26:37,175
Ovde prvo svetlija pa tamnija boja.

120
00:26:45,195 --> 00:26:53,809
Negde ne možemo prići jastučićem pa boju nanosimo prstima. Prskamo vodom i sušimo.

121
00:26:58,050 --> 00:26:59,908
Nanosimo i na poslednju stranicu.

122
00:27:24,580 --> 00:27:26,228
Malo braon...

123
00:28:13,978 --> 00:28:23,091
Radim istu stvar i ovde...koristim prste...prskam vodom...

124
00:29:08,088 --> 00:29:17,033
Hajde da sada napravimo komad od gline. Ja sam već pripremio...

125
00:29:20,894 --> 00:29:30,334
Ovo je Fimo glina koja se suši na vazduhu. Uzeću mali komad da vam pokažem kako da je razvaljate.

126
00:29:30,335 --> 00:29:33,684
Komad postavite na kutiju kako biste mogli da odmerite koliko vam je potrebno.

127
00:29:33,685 --> 00:29:42,230 
Oklagijom valjate glinu, podjednako u oba pravca.

128
00:29:42,231 --> 00:29:51,795
Pravim manji komad, samo da vam pokažem. Bilo bi dobro da okrenete komad i razvaljate i drugu stranu. 

129
00:29:51,995 --> 00:29:58,744
Tako sam napravio ovaj komad (koristeći više gline naravno).

130
00:29:58,944 --> 00:30:05,337
Sada ću vam pokazati kako da zalepite glinu.

131
00:30:05,537 --> 00:30:14,752
Na poleđinu nanosim Stamperia Extra forte lepak. Nanosim na ivicu gline.

132
00:30:20,952 --> 00:30:26,425
Dodajem lepak u po sredini, ali ne previše lepka.

133
00:30:27,177 --> 00:30:42,204
Okrećem glinu i pozicioniram na kutiju.

134
00:30:45,596 --> 00:30:52,120
Vlažnom maramicom brišem višak lepka.

135
00:31:01,585 --> 00:31:12,715
Dok je glina još mekana, oblikujem je kako želim.

136
00:31:21,716 --> 00:31:25,718
Oklagijom ću razvaljati još malo.

137
00:31:29,040 --> 00:31:39,998
Sada pečatom sređujemo naš komad gline.Ponovo koristim Prima marketing Finnabair pečate.

138
00:31:41,785 --> 00:31:47,608
Jako pritiskam pečat. Pritiskam od centra na ivicama pečata.

139
00:31:47,609 --> 00:31:49,687
Glina je još meka i elastična. 

140
00:31:57,965 --> 00:32:06,051
Polako podižem pečat. Odlično!

141
00:32:06,582 --> 00:32:09,992
Sada dodajem pečat sa natpisima.

142
00:32:19,589 --> 00:32:21,885
Pažljivo utiskujemo pečat.

143
00:32:30,701 --> 00:32:38,888
Koristeći pečate sa podloge, pravimo teksturu i na glini.

144
00:32:40,035 --> 00:32:47,863
AKo se vidi ivica pečata na glini,"obrišemo" je prstima.

145
00:32:56,108 --> 00:33:10,015
Nožem ili špatulom pravimo pukotine na glini i definišemo ih dodatno prstima.

146
00:33:17,153 --> 00:33:19,212
I ovde dodajemo pukotinu...

147
00:33:22,367 --> 00:33:25,722
Sada pažljivo vadimo pečate sa natpisima. 

148
00:33:30,243 --> 00:33:33,772
Glini treba dan-dva da se osuši potpuno.

149
00:33:33,774 --> 00:33:38,653
Mi ćemo sada osušiti površinski sloj kako bih mogao da vam pokažem proceduru.

150
00:33:50,166 --> 00:33:55,731
Svakako ne očekujemo da se glina osuši za ovo kratko vreme, naročito što je deblji sloj.

151
00:33:55,931 --> 00:33:59,194
Dovoljno nam je da je osušimo površinski kako bi mogli da radimo.

152
00:33:59,394 --> 00:34:03,764
Glina će pucati na nekim mestima, ali to nam i odgovara.

153
00:34:03,964 --> 00:34:11,701
Kad je glina polusuva, pečatom nanosimo ink na već napravljen otisak.

154
00:34:11,901 --> 00:34:20,825
Kako bi istakli motiv, koristimo Archival crni ink koji je vrlo intenzivan.

155
00:34:22,348 --> 00:34:31,537
Nanosim ink na pečat i stavljam na isto mesto gde sam već napravio otisak.

156
00:34:39,727 --> 00:34:53,680
Vidite kako je pečat sad vidljiv na glini. 

157
00:34:53,681 --> 00:35:00,289
Isto radimo i sa tekstom.

158
00:35:06,433 --> 00:35:08,336
Možda treba jače da pritisnem.

159
00:35:12,323 --> 00:35:15,411
Ovde ću ponoviti.

160
00:35:33,821 --> 00:35:41,878
Ovo je permanentni ink, tako da se neće razmazati kad nastavim sa radom.

161
00:35:41,879 --> 00:35:52,248
Sada ćemo da postarimo glinu. Koristiću Bitumen. Ovo mi je pokazala moja prijateljica

162
00:35:52,448 --> 00:35:57,270
Marija iz prodavnice Dreamworld iz Tive.

163
00:35:57,470 --> 00:36:00,906
Jako mi se svidelo.

164
00:36:01,106 --> 00:36:06,697
Inače sam drugačije radio na glini, ali mi se ovo baš dopalo.

165
00:36:06,897 --> 00:36:07,993
Hvala Marija!

166
00:36:09,812 --> 00:36:18,787
Nanosim bitumen na celu površinu gline.

167
00:36:18,987 --> 00:36:21,997
Važno je da bitumen udje u teksturu.

168
00:36:26,430 --> 00:36:35,046
Bitumen neće razmazati ink tako da slobodno mogu da utrljam u udubljenja.

169
00:36:40,870 --> 00:36:51,046
Nanosim bitumen i na ivice. Brišem vlažnim maramicama ako nam pređe na podlogu. 

170
00:36:52,701 --> 00:36:55,978
Ili prstima ako vam je tako lakše.

171
00:37:14,443 --> 00:37:21,958
Pošto smo naneli bitumen, skidamo ga vlažnim maramicama.

172
00:37:24,270 --> 00:37:27,955
SKidamo koliko želimo, negde više, negde manje.

173
00:37:35,652 --> 00:37:43,167
Uzeću čistu maramicu...nežno brišem...ostavljam više bitumena gde želim da izgleda starije.

174
00:38:10,000 --> 00:38:17,548
Lice sa motiva pečata se ne vidi dobro, ali nema veze, ovo je vintage projekat.

175
00:38:20,748 --> 00:38:22,223
Vidite? Nežno brišem...

176
00:38:24,414 --> 00:38:34,227
Ovde ću na primer ostaviti tamnije...uglove....ovde takodje...ispod slova...

177
00:38:35,000 --> 00:38:40,497
Mislim da je glina gotova. Očistićemo okolo...

178
00:38:40,697 --> 00:38:58,942
Sada ću dodati četiri eksera na glinu.

179
00:39:05,484 --> 00:39:11,748
Neću ih zakucati do kraja, želim da izgledaju polomljeno.

180
00:39:20,011 --> 00:39:30,268
I pomalo nakrivo...bolje izgleda tako.

181
00:39:39,468 --> 00:39:41,340
Ovaj ćemo okrenuti na drugu stranu.

182
00:39:46,782 --> 00:39:49,156
Glina je gotova.

183
00:39:57,499 --> 00:40:04,429
Sada ćemo da napravimo "povez" koristeći Stamperia Stonepaper.

184
00:40:04,629 --> 00:40:13,183
Obojićemo Americana akrilnom bojom Bleached Sand.

185
00:40:15,383 --> 00:40:26,519
Četkicom nanosimo boju.

186
00:40:26,520 --> 00:40:30,539
Koristim stonepaper jer je on otporan na sve materijale i medijume.

187
00:40:30,739 --> 00:40:37,476
Svaki drugi papir bi se uništio. 

188
00:40:38,397 --> 00:40:43,278
Važno je da su potezi četke dugi.

189
00:40:59,223 --> 00:41:03,550
Malo ćemo prosušiti.

190
00:41:16,465 --> 00:41:26,601
Ponovo koristim pečat i braon StazOn mastilo da napravim podlogu.

191
00:41:26,801 --> 00:41:35,346
Koristim pečat koji sma koristio i za kutiju i za glinu.

192
00:41:35,347 --> 00:41:38,283
On se pobavlja na svim delovima.

193
00:41:38,284 --> 00:41:43,101
Važno je da se napravi ta veza izmedju delova.

194
00:41:49,364 --> 00:42:09,789 
Vidi se ivica pečata. Probaću da obrišem maramicom.

195
00:42:09,989 --> 00:42:32,258
Ne volim kad se vide ivice pečata. Dodaću veći pečat preko ovoga...

196
00:42:33,363 --> 00:42:45,226
Nekada se otisak jedva vidi. Nije neošhodno da sve bude jasno vidljivo kada pravimo pozadinu.

197
00:42:45,426 --> 00:43:01,179
Sada ću naneti Stamperia Gloss Gel pastu preko šablona.

198
00:43:01,379 --> 00:43:19,410
Pastu nanosim špatulom. Špatulu držim pod uglom, tako najlakpe mogu da nanesem pastu a da mi šablon ne klizi po podlozi.

199
00:43:45,856 --> 00:43:53,401
Pastu nanosimo svuda.

200
00:43:59,236 --> 00:44:02,071
Ne smeta mi ako pasta negde nije savršeno naneta.

201
00:44:04,818 --> 00:44:13,136
Sušimo pastu toplim vazduhom, a onda ostavimo da se ohladi na sobnoj temperaturi.

202
00:44:20,476 --> 00:44:25,400
Pošto se pasta osušila, stonepaper ćemo zalepiti na kutiju.

203
00:44:25,600 --> 00:44:35,938
Lepimo Stamperia Gloss pastom. Nanosim dosta paste.

204
00:44:36,138 --> 00:44:50,929
Ne razmazujem pastu, natapkam je četkicom. Pritiskam i višak paste će biti istisnut.

205
00:44:51,129 --> 00:44:56,668
Višak paste izlazi, papir se zalepio.

206
00:44:59,800 --> 00:45:04,808
Vlažnom maramicom brišem višak paste.

207
00:45:10,080 --> 00:45:21,620
Lepimo i drugu stranu na isti način.

208
00:45:26,119 --> 00:45:32,505
Pritiskam papir i višak paste izlazi.

209
00:45:42,001 --> 00:45:43,309
Brišemo višak paste.

210
00:45:45,367 --> 00:45:49,214
Proveravam da li se papir pomerio.

211
00:45:52,454 --> 00:46:01,249
Ovo je sad teži deo. Nanosim pastu direktno na papir, ne na kutiju.

212
00:46:04,586 --> 00:46:09,909
Tapkam, ne razmazujem. Potrebno je dosta paste.

213
00:46:10,109 --> 00:46:14,859
Istiskujemo pastu, tako se ona ravnomerno razliva ispod papira i lepi ga.

214
00:46:15,059 --> 00:46:20,307
Ovde nisam dobro naneo pastu preko šablona, ali nije važno, jer treba da izgleda staro.


215
00:46:25,112 --> 00:46:37,885
Možemo vlažnom četkicom skinuti višak paste.

216
00:46:41,320 --> 00:46:47,297
Na kraju prebrišemo vlažnom maramicom.

217
00:46:51,614 --> 00:46:59,600
Papir je zalepljen. Brišem višak paste.

218
00:46:59,800 --> 00:47:04,899
Sada ćemo vratiti poklopac kutije na mesto.

219
00:47:07,000 --> 00:47:14,262
Boja je odlična. Tražio sma boju koja se uklapa sa bojom gline.

220
00:47:14,462 --> 00:47:17,536
Kasnije ću naneti bitumen kako bih postigao istu nijansu.

221
00:47:18,381 --> 00:47:20,171
Vraćam šrafove.

222
00:48:05,935 --> 00:48:25,409
Obojiću i šarke istom bojom kao što sam bojio stonepaper.

223
00:48:30,576 --> 00:48:33,612
Sušim ih.

224
00:48:39,756 --> 00:48:47,998
Sada nanosimo bitumen kako bi postarili i ovaj deo.

225
00:48:48,200 --> 00:48:57,347
Ponavljam bitumen kako bih povezao boju/nijansu ove stranice sa glinom.

226
00:49:00,882 --> 00:49:11,760
Ovo je bitumen na bazi vode, tako da koristim četkicu koju koristim za akrilne boje.

227
00:49:26,297 --> 00:49:34,777
Nanosim bitumen i na šarke, da izgledaju staro...

228
00:49:51,562 --> 00:50:01,619
I sada počinjem sa brisanjem...vlažna maramica skida previše.

229
00:50:01,819 --> 00:50:09,282
Prvo ću ga prosušiti pa brisati vlažnom maramicom.

230
00:50:17,110 --> 00:50:26,460
Ne brišem previše blizu šarki.

231
00:50:34,012 --> 00:50:36,114
Very nice, let's dry...
Odlično! Još malo sušim...

232
00:50:57,351 --> 00:51:04,152
Sada nežno brišemo bitumen. 

233
00:51:04,352 --> 00:51:10,560
Zbog reljefa koji smo dobili pastom, bitumen ostaje u udubljenima. 

234
00:51:11,259 --> 00:51:19,925
Možda se pečat neće baš videti, ali nije važno.

235
00:51:20,125 --> 00:51:28,607
Nekada nešto uradimo i kako projekat odmiče, shvatamo da to i nije bilo neophodno.

236
00:51:48,041 --> 00:51:57,022
I evo rezultata! Proveravam da li još nešto treba da se uradi. Mislim da je sve kako treba.

237
00:51:58,000 --> 00:52:05,249
Ovo je današnji projekat. Mixed media kutija knjiga...Hvala vam što ste gledali.

238
00:52:05,449 --> 00:52:10,159
DecoArt Americana akrilne boje, Prima marketing Finnabair pečati,

239
00:52:10,359 --> 00:52:15,541
Stonepaper i gel pasta od Stamperie, Rim Holtz...

240
00:52:15,741 --> 00:52:22,533
Vidite da je vrlo lako. Mešanjem materijala smo dobili odličan rezultat.

241
00:52:22,733 --> 00:52:26,269
Nadam se da vam se dopalo. Hvala vam i vidimo se uskoro!
