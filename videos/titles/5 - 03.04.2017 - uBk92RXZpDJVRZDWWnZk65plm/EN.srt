﻿1
00:00:08,342 --> 00:00:13,653
Good morning again from Belgrade.This is the second month for handmadefantasyworld.com.

2
00:00:13,654 --> 00:00:18,765
I hope, you had fun, last month. This time, we are going to make a chest.

3
00:00:18,965 --> 00:00:28,485
We will use chalk paints by "Pentart", some stamps by "Docraft", a stencil  by "Stamperia",

4
00:00:28,685 --> 00:00:34,298
as well as, the crackeled stamp, the modeling paste so as to give a repousse effect.

5
00:00:34,498 --> 00:00:36,092
Let's start!

6
00:00:38,776 --> 00:00:50,516
Well, we put the stencil by "Stamperia" on the chest lid, we will use modeling paste by "Stamperia" too.

7
00:00:51,782 --> 00:01:01,405
We pick some quantity and with our spatula leaning upwards, we cover the stencil.

8
00:01:03,203 --> 00:01:05,876
As you can see I don't have to hold the stencil.

9
00:01:05,877 --> 00:01:10,244
I only have to pick much of a quantity and keep my spatula in an upright position.

10
00:01:12,456 --> 00:01:21,766
In case that the paste is not applied well, don't worry, we will paint it and we'll fix  anything wrong.

11
00:01:44,386 --> 00:01:51,039
We cover the stencil and we pull up towards one direction. Very nice!

12
00:01:51,239 --> 00:01:53,815
Let's dry it, we'll be back soon.

13
00:02:00,786 --> 00:02:04,109
We don't have to dry it too much, cause we are going to work on another chest side.

14
00:02:04,309 --> 00:02:07,167
So we may let it dry by its own.

15
00:02:07,168 --> 00:02:23,177
We use a black chalk paint and a strong brush, and we start painting the chest ,in a patchy way.

16
00:02:28,945 --> 00:02:31,726
I'll show you how the first side of the box is made

17
00:02:31,727 --> 00:02:38,951
and then the video will be fast forwarded to save time.

18
00:02:38,952 --> 00:02:46,513
I'll finish this side and you'll see what's next after I finish all the sides of the box.

19
00:02:47,216 --> 00:02:52,227
You see? I don't care. i just add color at some points.

20
00:02:54,228 --> 00:03:01,036
It's no concern of mine, where I'm gonna paint, how it looks, just stirred.

21
00:03:02,281 --> 00:03:04,250
I dry a bit.

22
00:03:15,208 --> 00:03:33,473
Another color, maybe some turquoise, I'll paint over the black and I'm gonna cover some other points too.

23
00:03:43,000 --> 00:03:47,044
At the beginning I use two dark colors. Then I'm gonna use some lighter ones and

24
00:03:47,244 --> 00:03:50,336
when I'm about to finish, I'll use a dark one again.

25
00:03:57,536 --> 00:04:15,011
Using a baby wipe, I spread the colors to any direction, so as to create effects.

26
00:04:17,117 --> 00:04:22,542
You can see how the colors mix.

27
00:04:30,115 --> 00:04:35,553
We've prepared our color mixed base. Now let's use the light color paints.

28
00:04:35,753 --> 00:05:02,025
Some yellow...  at a specific point....αnd finally spread it with a baby wipe.

29
00:05:02,225 --> 00:05:07,992
Of course, the color becomes more transparent, but that's what I want.

30
00:05:08,192 --> 00:05:12,111
because I use the same color on it over and over again.

31
00:05:38,667 --> 00:05:47,657
As you can see, the colors aren't so intense. I use the yellow and as sonn as I wipe it, it becomes transparent.

32
00:05:47,857 --> 00:05:59,350
I need this, for the start, in order to have the color mixture. But I'll show you a procedure,

33
00:05:59,550 --> 00:06:01,959
so as to start painting over.

34
00:06:23,806 --> 00:06:32,880
Now, let's paint at some points with even more quantity of paint

35
00:06:50,994 --> 00:06:51,861
and dry it!

36
00:06:59,686 --> 00:07:10,961
I scrub it with my hand and thus larger pieces of paint are peeled off.

37
00:07:19,204 --> 00:07:31,931
Then with the baby wipe I smoothen the shade edges of my hand scrubbing

38
00:07:37,178 --> 00:07:39,253
I'll take a clean baby wipe...

39
00:08:04,506 --> 00:08:08,841
At some points, we will add little extra turquoise color.

40
00:08:20,114 --> 00:08:26,928
It's clearly about color blending. I apply some color, I over paint it, wipe it, I spread it with my hands...

41
00:08:27,128 --> 00:08:40,857
It's up to your prospect, how much and how many colors you're going to use, what the mixture will be.

42
00:08:51,057 --> 00:08:55,612
Some black, here!

43
00:09:10,667 --> 00:09:20,351
Let's use a light color for finish...

44
00:09:31,702 --> 00:09:33,847
again with a baby wipe...

45
00:09:39,781 --> 00:09:51,126
It will give us a rocky view, I may add more at the edges and make shades with my hand

46
00:10:05,188 --> 00:10:09,332
You see... I work it with my hands and baby wipes too!

47
00:10:32,974 --> 00:10:43,320
You see the result. It's a color mix. I've used four different colors. The dark ones as  base colors,

48
00:10:43,520 --> 00:10:51,693
then the light ones and maybe a dark color in the end. I use baby wipes and my hands,

49
00:10:51,893 --> 00:10:54,470
whatever helps in creating this effect.

50
00:10:54,670 --> 00:11:04,136
Let's do something different. Take some aluminum foil, use a different chalk paint color

51
00:11:04,336 --> 00:11:13,572
and using a sea sponge, pick some color and spread it on the aluminum foil well. Then dab

52
00:11:13,772 --> 00:11:23,301
on the box edges to create shades.

53
00:11:23,501 --> 00:11:33,708
We work mostly on the edges not in the rest surface unless our sponge hasn't much paint on it.

54
00:11:39,156 --> 00:11:46,349
Mostly on the edges. I finish the first color and I dry.

55
00:11:53,217 --> 00:12:08,580
Then I'll use a darker color, of the same color shade and will do the same procedure using the sponge.

56
00:12:08,780 --> 00:12:13,106
The difference now is that I leave less paint.

57
00:12:13,306 --> 00:12:17,024
I want the two colors to distinguish. There isn't much color differece, we don't want that.

58
00:12:30,385 --> 00:12:37,093
As you may see, I dab on different points. Not too much on the upper as on the lower ones.

59
00:12:41,251 --> 00:12:51,548
Finally using the darker color, the black, we need very little quantity, we'll do the same procedure.

60
00:12:51,748 --> 00:13:02,011
We pick with our sponge as less color as possible. Barely seen. Not too much.

61
00:13:16,691 --> 00:13:29,138
At some points, I interfere more. Some black here. Maybe almost everywhere.

62
00:13:30,676 --> 00:13:33,624
Gently though, not too much paint. We must learn to control the quantity of paint.

63
00:13:44,526 --> 00:13:47,333
I dab here too, but almost with no paint.

64
00:14:03,215 --> 00:14:24,483
Now it's time for the stencil. We will choose a light color, using the sponge, same procedure,

65
00:14:24,683 --> 00:14:36,606
as previously, not too much paint. I don't mind if it is darker. I'll use the half stencil.

66
00:14:44,351 --> 00:14:52,029
I dab, slowly, We don't care if the stencil design is not well shaped. That's why we use a sponge.

67
00:15:06,018 --> 00:15:12,569
If the stencil doesn't look good, you re adjust it on the surface

68
00:15:12,769 --> 00:15:15,415
I hope, if I hold it like this, will help...

69
00:15:44,083 --> 00:15:53,379
Surely, the stencil result looks not good, but this is the result we want.

70
00:15:53,579 --> 00:16:01,283
We don't want the result to be perfect. We like that minimum we got.

71
00:16:01,483 --> 00:16:09,542
Again some color and let's create the half stencil on this side too.

72
00:16:29,740 --> 00:16:38,511
You see that the stencil looks bad. We don't care. We wil dry a little bit.

73
00:16:47,263 --> 00:16:53,930
Same stencil but this time, using black color...

74
00:17:01,411 --> 00:17:04,226
We always have to clean our sponge...

75
00:17:08,589 --> 00:17:13,273
Same black color and the stencil's other side...

76
00:17:25,911 --> 00:17:30,466
Hmm maybe same side, it doesn't match well...

77
00:17:38,703 --> 00:17:43,305
we dab at some points, it is not necessary everywhere...

78
00:17:46,458 --> 00:17:49,149
We just apply the one color over the other.

79
00:17:51,627 --> 00:17:54,143
It's a little bit difficult cause the stencil is heavy.

80
00:17:58,107 --> 00:17:59,581
Mostly on the center.

81
00:18:10,028 --> 00:18:16,669
It is not necessary and we don't mind if the stencil is not exact on point.

82
00:18:38,321 --> 00:18:43,142
Just so little, with my hand I wipe any extra color.

83
00:18:46,007 --> 00:18:58,436
Using a baby wipe, I scrub it a liitle and spoil it.

84
00:19:06,236 --> 00:19:15,173
I don't want to emphasize the stencil, I just want to let a design to be slightly seen.

85
00:19:15,373 --> 00:19:18,989
Let's add some ivory...

86
00:19:26,056 --> 00:19:33,140
Maybe at some points... again with the stencil.

87
00:19:33,934 --> 00:19:43,809
use a strong brush to pick some color and I start dabbing.

88
00:19:50,914 --> 00:19:52,985
Small quantity of paint. It needs just a little.

89
00:20:01,177 --> 00:20:04,301
And I give some brightness.

90
00:20:16,345 --> 00:20:17,860
Same procedure,

91
00:20:26,531 --> 00:20:32,945
Very nice! I'll use the hairdryer a bit.

92
00:20:48,385 --> 00:20:57,001
After finishing with stencil... I like the ivory color to prevail.

93
00:20:57,201 --> 00:21:12,134
So I add some  over my  shades and the stencil design too. I work it with my hand.

94
00:21:21,334 --> 00:21:24,741
The points on which we create this effect is up to one's choise.

95
00:21:25,515 --> 00:21:28,620
It would be better, if we didn't apply it everywhere. Only here and there.

96
00:21:38,978 --> 00:21:45,464
This is the result. I'll work on the rest of the box and I'll be back to you. Thank you

97
00:21:46,822 --> 00:21:52,446
We are back. I've nothing to say, look at my hands!

98
00:21:52,646 --> 00:22:02,911
It's a mess here. i've finished the chest. I realised that I wanted some light at the edges,

99
00:22:03,111 --> 00:22:11,437
so I added some yellow colored shades with my sponge and then I added some black too.

100
00:22:11,637 --> 00:22:17,308
Let's do the lid. In the meantime, I also painted the buckle of the chest.

101
00:22:20,980 --> 00:22:25,361
I found and used an acrylic by "Americana", the color is rasberry.

102
00:22:25,362 --> 00:22:37,222
I saw that it was nice to have an intense color. So let's paint the stencil and use a contrasting color.

103
00:22:37,223 --> 00:22:39,577
As I have already said, it is very important to have contrasts. 

104
00:22:39,777 --> 00:22:47,036
Here is what we do. We paint the stencil with black color. We don't give much of attention.

105
00:22:47,640 --> 00:22:53,254
I just try to paint it.

106
00:23:05,325 --> 00:23:15,813
Very nice. I paint the rest of the lid with a turquoise color.

107
00:23:43,579 --> 00:23:53,116
And as always, we use a baby wipe to spread and blend the colors.

108
00:24:02,868 --> 00:24:07,174
I'll put some here, where there is a small surface undone... 

109
00:24:11,643 --> 00:24:19,638
We'll follow the same procedure as before, but we are not going to work on the stencil.

110
00:24:27,431 --> 00:24:28,262
Let's dry it a bit...

111
00:24:46,228 --> 00:24:57,199
Again, using a baby wipe, we are going to give the ageing effect and mix the yellow color.

112
00:25:00,462 --> 00:25:03,746
Actually, we are trying for the same result but we do not work on the center.

113
00:25:03,946 --> 00:25:13,820
Let's create the same result as the chest sides. With our hand we'll give some more black shades.

114
00:25:16,275 --> 00:25:20,273
Baby wipe again...to fade out the lines so as not look bad.

115
00:25:20,901 --> 00:25:33,077
Now will use the color which we painted the buckle with and we'll pour some on the stencil.

116
00:25:35,350 --> 00:25:40,360
Nice... and spread it with our hand.

117
00:25:51,324 --> 00:26:06,037
Very simple, there is nothing difficult. I poured some color, I made shades of it and I think it's ready.

118
00:26:06,237 --> 00:26:19,861
And maybe some ivory to create extra shades.

119
00:26:45,139 --> 00:26:52,927
We will use again the sea sponge, some yellow at the beggining.

120
00:26:53,127 --> 00:26:59,709
I told you that I did that at the end. I didn't like the green colors I used, they were too dark,

121
00:26:59,909 --> 00:27:04,223
eventually I end up with a yellow color.

122
00:27:07,277 --> 00:27:09,767
I dab a little any extra color.

123
00:27:17,324 --> 00:27:24,116
And then, some black.

124
00:27:27,821 --> 00:27:30,981
Just a little and spread it with your hand.

125
00:27:53,487 --> 00:28:02,390
As you can see, I work with the sponge, with my hand, it's a mix of materials and techniques.

126
00:28:03,398 --> 00:28:14,099
Above all, to be in the spirit. If there is anything I don't like, I use the baby wipe to correct it.

127
00:28:16,666 --> 00:28:21,760
Well, there is a stain here, I'm gonna clear it.

128
00:28:35,066 --> 00:28:38,390
Now I'm thinking if there is anything else we may do.

129
00:28:38,590 --> 00:28:42,360
This color is very nice, we might create some splashes later.

130
00:28:42,560 --> 00:28:56,850
Now, let's put the stamps. In the second video, the clock project, we've used stamps by "Docrafts".

131
00:28:57,050 --> 00:29:08,724
The designs are magical. Today, we are going to use these plants.

132
00:29:10,788 --> 00:29:16,230
Watch, what we do. We'll take this large stamp.

133
00:29:26,021 --> 00:29:39,564
And we are going to put it here. Firstly, we are going to create the stamp with a chalk paint

134
00:29:49,025 --> 00:30:01,020
Using my sponge and dab on the stamp.

135
00:30:14,390 --> 00:30:28,783
You see, I dab. What if i used this sponge, I've been brought. It's much more better. Much easier

136
00:30:32,520 --> 00:30:48,417
I dab, not too long... and then press the stamp on the surface. Using ivory color.

137
00:30:48,617 --> 00:31:00,495
Steady... very nice. I remove anything I don't like.

138
00:31:00,695 --> 00:31:06,460
So I'll keep on stamping, until I cover the whole surface down here, using an ivory chalk paint.

139
00:31:11,615 --> 00:31:14,425
It's not necessary for the design to be perfectly shaped.

140
00:31:35,421 --> 00:31:40,252
Let's put the last piece.

141
00:31:50,317 --> 00:32:06,427
We'll take a baby wipe and we'll quickly clean the stamp, or else ,if the color dries, it will be destroyed

142
00:32:22,607 --> 00:32:34,680
Some paper towel too. Then we dry.

143
00:32:37,646 --> 00:32:49,080
We'll use the black Archival permanent ink to apply it on the stamp.

144
00:33:01,330 --> 00:33:12,384
And now we put the stamp on the project. Slightly lower than the ivory color.

145
00:33:12,584 --> 00:33:21,734
I dab and here is our result.

146
00:33:32,359 --> 00:33:35,222
I do the same thing on the whole surface.

147
00:34:06,291 --> 00:34:14,581
Here on the endings, I apply some ink so as not it looks bad.

148
00:34:14,781 --> 00:34:20,037
Let's see what else we can use!

149
00:34:21,638 --> 00:34:37,321
At some points I'll use this one which is much more bushing...ah very, very nice!

150
00:34:38,686 --> 00:34:40,614
I won't stamp it all over.

151
00:34:40,814 --> 00:34:43,748
Only at some points...

152
00:34:47,391 --> 00:35:00,647
We may interfere to anything we don't like. And re adjust some ink to cover it.

153
00:35:05,348 --> 00:35:07,156
What else do we have here? 

154
00:35:13,310 --> 00:35:18,922
Perhaps, that small detail.

155
00:35:32,959 --> 00:35:37,599
Here is the side result.

156
00:35:37,799 --> 00:35:49,530
We'll see. Surely I want to add something having this color, maybe this side and using some color

157
00:36:00,453 --> 00:36:04,814
...to add some shades...

158
00:36:17,293 --> 00:36:19,975
See? it matches!

159
00:36:20,175 --> 00:36:25,396
Now using a strong brush, let me find one...

160
00:36:39,413 --> 00:36:56,697
...and some water, I use the acrylic and create some splashes. Not everywhere though.

161
00:37:18,794 --> 00:37:27,151
I may dab them, to dry any extra paint and make them look as if they were stains. Look here!

162
00:37:34,860 --> 00:37:49,292
Look again! I'll show it to you. When we pick extra water...I dab and it looks like old.

163
00:38:09,157 --> 00:38:12,996
Some color here, it's required.

164
00:38:19,281 --> 00:38:22,962
I add some white, to give it some extra light.

165
00:38:28,586 --> 00:38:37,563
The stencil here, is barely seen. I myself, may not be totally satisfied with the design.

166
00:38:37,763 --> 00:38:42,153
I didn't use on this side, as well, although, I could.

167
00:38:42,353 --> 00:38:51,421
This is the box. I'll put the stamps on the rest sides too. I'll be back soon for the farewell.

168
00:38:52,122 --> 00:38:56,698
I'm done with the stamps all around. You may see the result.

169
00:38:56,898 --> 00:39:09,271
Let's use the brown "StazOn" ink and the very nice,crackeled stamp, to create some cracks

170
00:39:09,471 --> 00:39:19,701
You see that I don't stamp everywhere. I leave small quantity of ink on , to make them look fade.

171
00:39:21,170 --> 00:39:33,621
At this point we have to be careful, so as not, the square outline of the stamp, is seen.

172
00:39:33,821 --> 00:39:35,596
Not too many! see...

173
00:39:45,291 --> 00:39:48,711
I fade it, with my hand, even more...

174
00:39:49,478 --> 00:39:51,706
Let's put one more, here too!

175
00:39:53,992 --> 00:40:00,365
So, We are ready! This is the result, i hope you like it.

176
00:40:02,199 --> 00:40:07,595
This is today's project, I hope you had a good time... you like it and enjoyed it!

177
00:40:07,795 --> 00:40:20,681
I know, some time it's very strange... I laugh because the guys here, make fun of me...

178
00:40:20,881 --> 00:40:27,665
The truth is that sometimes I have a peculiar way of working. That's what I expect that you get from me.

179
00:40:27,865 --> 00:40:32,654
As you can see, using baby wipes, lots of colors, hands, we were able to create a beautiful result.

180
00:40:32,854 --> 00:40:40,696
We used some stamps and a little technique in the finish,  to give it something extra  

181
00:40:40,896 --> 00:40:46,578
as well as. the rasaberry color played a significant role on the contrast..

182
00:40:46,778 --> 00:40:50,781
I hope you enjoyed it. That was the first project of the second month of handmadefantasyworld.com

183
00:40:50,981 --> 00:40:55,551
Another three will follow, mix media, canvas for sure...

184
00:40:55,751 --> 00:40:58,167
See you soon, have a good time...Bye!!!