1
00:00:08,342 --> 00:00:13,653
Dobar dan iz Beograda! Ovo je drugi mesec naših radionica.

2
00:00:13,654 --> 00:00:18,765
Nadam se da su vam se svidele radionice prethodnog meseva. Danas ćemo raditi na drvenoj kutiji. 

3
00:00:18,965 --> 00:00:28,485
Koristićemo Pentart chalkpaint, DoCrafts pečate, Stamperia šablon.

4
00:00:28,685 --> 00:00:34,298
Zatim, crackel pečat, modeling pastu...

5
00:00:34,498 --> 00:00:36,092
Hajde da počnemo!

6
00:00:38,776 --> 00:00:50,516
Na poklopcu ćemo naneti pastu preko Stamperia šablona.

7
00:00:51,782 --> 00:01:01,405
Uzimamo pastu špatulom, držeći je pod uglom, nanosimo pastu preko šablona.

8
00:01:03,203 --> 00:01:05,876
Kao što vidite, šablon se ne pomera.

9
00:01:05,877 --> 00:01:10,244
Kad nanesemo malo paste i držimo pravilno špatulu, neće biti problema sa šablonom.

10
00:01:12,456 --> 00:01:21,766
Ukoliko pastu i ne nanesem lepo, nije problem, jer ćemo svakako bojiti kad se osuši.

11
00:01:44,386 --> 00:01:51,039
Nanesemo pastu na ceo šablon i zatim ga skidamo. 

12
00:01:51,239 --> 00:01:53,815
Sada ćemo osušiti pastu.

13
00:02:00,786 --> 00:02:04,109
Neću mnogo sušiti jer ću raditi stranice kutije.

14
00:02:04,309 --> 00:02:07,167
Tako da pasta ima vremena i sama da se osuši.

15
00:02:07,168 --> 00:02:23,177
Počinjemo sa crnom chalk bojom i grubom četkom, nanosimo neravnomerno.

16
00:02:28,945 --> 00:02:31,726
Uradićemo jednu stranu zajedno.

17
00:02:31,727 --> 00:02:38,951
Ostale strane nećemo snimati, da umrzamo prelazak na druge tehnike.

18
00:02:38,952 --> 00:02:46,513
Pokazaću vam sve strane kad završim.

19
00:02:47,216 --> 00:02:52,227
Vidite kako nanosim boju. 

20
00:02:54,228 --> 00:03:01,036
Nije važno kako nanosimo boju, ne mora da bude pravilo ni uredno, već nasumice.

21
00:03:02,281 --> 00:03:04,250
Malo prosušimo.

22
00:03:15,208 --> 00:03:33,473
Sada drugu boju, verovatno tirkiz. Nanosim je preko crne kao i neke tačke koje nismo farbali.

23
00:03:43,000 --> 00:03:47,044
Počinjem sa dve tamnije boje, a kasnije ćemo dodati svetlije.

24
00:03:47,244 --> 00:03:50,336
Kad budem pred kraj, dodaću ponovo neku tamniju.

25
00:03:57,536 --> 00:04:15,011
Uz pomoć vlažnih maramica razmazujem boju u svim pravcima kako bi dobio željeni efekat.

26
00:04:17,117 --> 00:04:22,542
Vidite kako se boje mešaju?

27
00:04:30,115 --> 00:04:35,553
Pripremili smo tamnu osnovu i sad nanosimo svetle boje.

28
00:04:35,753 --> 00:05:02,025
Malo žute...razmazujem je vlažnim maramicama...

29
00:05:02,225 --> 00:05:07,992
Boja postaje transparentna, ali to i želimo.

30
00:05:08,192 --> 00:05:12,111
Ponavljam istu boju.

31
00:05:38,667 --> 00:05:47,657
Boje nisu intenzivne, i kad ih razmažem vlažnom maramicom, postaju transparentne. 

32
00:05:47,857 --> 00:05:59,350
Ovako pravimo osnovu i mešamo boje. 

33
00:05:59,550 --> 00:06:01,959
Preko osnove ćemo dodati još boje.

34
00:06:23,806 --> 00:06:32,880
Sada na određene tačke nanosimo više boje.

35
00:06:50,994 --> 00:06:51,861
I sušimo.

36
00:06:59,686 --> 00:07:10,961
Trljam rukama i deblji slojevi boje se gule.

37
00:07:19,204 --> 00:07:31,931
Zatim vlažnim maramicama ublažavam prelaze i pravim nijanse.

38
00:07:37,178 --> 00:07:39,253
Uzeću čistu vlažnu maramicu...

39
00:08:04,506 --> 00:08:08,841
Dodaću još tirkiz boje.

40
00:08:20,114 --> 00:08:26,928
Dakle, kako se mešaju boje: nanesemo jednu boju, pa drugu, brišemo vlažnim maramicama, trljamo rukama...

41
00:08:27,128 --> 00:08:40,857
Ovo možete raditi sa različitim bojama, shodno vašem ukusu.

42
00:08:51,057 --> 00:08:55,612
Dodaćemo malo crne.

43
00:09:10,667 --> 00:09:20,351
Završićemo sa svetlijom bojom.

44
00:09:31,702 --> 00:09:33,847
I ponovo brišemo vlažnim maramicama...

45
00:09:39,781 --> 00:09:51,126
Ovako postižemo magloviti izgled...razmazujemo boju, koristim ruke za to...

46
00:10:05,188 --> 00:10:09,332
Koristimo i vlažne maramice i ruke.

47
00:10:32,974 --> 00:10:43,320
Vidite kako su se boje pomešale. Koristio sam 4 različite boje, prvo tamnije, zatim svetlije.

48
00:10:43,520 --> 00:10:51,693
Zavravamo sa jako malo tamnije boje. 

49
00:10:51,893 --> 00:10:54,470
Ruke i vlažne maramice nam pomažu da postignemo ovaj efekat.

50
00:10:54,670 --> 00:11:04,136
Sada ćemo uraditi još nešto. Uzimam aluminijumsku foliju, koristiću različite boje.

51
00:11:04,336 --> 00:11:13,572
Uz pomoć morskog sunđera, nanosim boju. Višak boje razmazujem po foliji.

52
00:11:13,772 --> 00:11:23,301
Tapkam boju sunđerom po ivicama.

53
00:11:23,501 --> 00:11:33,708
Krećemo od ivica, i tek kad na sunđeru skoro nema boje, nanosimo i unutar stranice.

54
00:11:39,156 --> 00:11:46,349
Ali uglavnom nanosimo na ivicama. Završio sam sa prvom bojom i sušim.

55
00:11:53,217 --> 00:12:08,580
Zatim na isti način nanosimo svetliju boju.

56
00:12:08,780 --> 00:12:13,106
Samo sada nanosimo još manje boje.

57
00:12:13,306 --> 00:12:17,024
Želim da se naziru dve boje, ali ne želim preveliki kontrast.

58
00:12:30,385 --> 00:12:37,093
Kao što vidite tapkam na drugim tačkama. Tretiram više donju ivicu.

59
00:12:41,251 --> 00:12:51,548
I na kraju nanosim jako malo crne boje na isti način.

60
00:12:51,748 --> 00:13:02,011
Uzimam sunđerom što je moguće manje boje, da bude jedva vidljiva.

61
00:13:16,691 --> 00:13:29,138
Na nekim mestima zalazim malo više.

62
00:13:30,676 --> 00:13:33,624
Nežno tapkam jer želim što manje boje da ostane. Morate naučiti da kontrolišete količinu boje.

63
00:13:44,526 --> 00:13:47,333
Tapkam i ovde, ali skoro bez boje.

64
00:14:03,215 --> 00:14:24,483
Sada ćemo naneti boju preko šablona. Uzimamo svetlu boju i nanosimo sunđerom.

65
00:14:24,683 --> 00:14:36,606
Uzimamo malo boje.  Koristićemo pola šablona.

66
00:14:44,351 --> 00:14:52,029
Tapkam polako. Nije važno ako nije vidljiv ceo šablon. Zato i koristim morski sunđer.

67
00:15:06,018 --> 00:15:12,569
Vratiću šablon i dodati još malo boje.

68
00:15:12,769 --> 00:15:15,415
Možda će ovako biti bolje...

69
00:15:44,083 --> 00:15:53,379
Šablon se ne vidi jasni, ali to smo i hteli.

70
00:15:53,579 --> 00:16:01,283
Ne želimo da izgleda savršeno, baš suprotno.

71
00:16:01,483 --> 00:16:09,542
Isto radimo i sa druge strane.

72
00:16:29,740 --> 00:16:38,511
Nije lep trag ostao, ali nije važno. Prosušićemo.

73
00:16:47,263 --> 00:16:53,930
Isti šablon, ali ovaj put ću naneti crnu boju.

74
00:17:01,411 --> 00:17:04,226
Uvek moramo da očistimo sunđer...

75
00:17:08,589 --> 00:17:13,273
Crna boja i isti šablon...

76
00:17:25,911 --> 00:17:30,466
Pogrešna strana šablona...

77
00:17:38,703 --> 00:17:43,305
Tapkamo ponegde...

78
00:17:46,458 --> 00:17:49,149
Nanosimo boju preko boje.

79
00:17:51,627 --> 00:17:54,143
Nije baš zgodno jer je šablon težak (od tragova paste i boje).

80
00:17:58,107 --> 00:17:59,581
Tapkamo uglavnom centralni deo.

81
00:18:10,028 --> 00:18:16,669
Šablon ne mora skroz da se poklopi.

82
00:18:38,321 --> 00:18:43,142
Naneo sam malo boje, ali višak skidam rukom.

83
00:18:46,007 --> 00:18:58,436
Vlažnom maramicom trljamo kako bismo delimično skinuli boju sa motiva.

84
00:19:06,236 --> 00:19:15,173
Ne želim da naglašavam dezen, želim da bude jedva vidljiv.

85
00:19:15,373 --> 00:19:18,989
Dodaćemo i malo bež boje.

86
00:19:26,056 --> 00:19:33,140
Ponegde....preko šablona.

87
00:19:33,934 --> 00:19:43,809
Koristim sad grubu četku.

88
00:19:50,914 --> 00:19:52,985
Samo malo boje je potrebno.

89
00:20:01,177 --> 00:20:04,301
To će nam malo razbistriti šablon.

90
00:20:16,345 --> 00:20:17,860
Ponavljamo proceduru.

91
00:20:26,531 --> 00:20:32,945
Malo ćemo prosušiti.

92
00:20:48,385 --> 00:20:57,001
SVidja mi se kako se uklapa bež boja na motivu.

93
00:20:57,201 --> 00:21:12,134
Dodaću bež boje još i mimo šablona.Razmazujem je rukama.

94
00:21:21,334 --> 00:21:24,741
Sami birate gde ćete dodati ovaj efekat.

95
00:21:25,515 --> 00:21:28,620
Bolje je da se ne nanese svuda bež boja...tek ponegde.

96
00:21:38,978 --> 00:21:45,464
Ovo je rezultat. Sada ću uraditi i ostale stranice. Brzo se vraćam :)

97
00:21:46,822 --> 00:21:52,446
Evo me...nemam šta da vam dodam...vidite moje ruke :)

98
00:21:52,646 --> 00:22:02,911
Napravio sam haos oko sebe,ali sam ofarbao kutiju :) Svidja mi se svetlija boja po ivicama.

99
00:22:03,111 --> 00:22:11,437
Dodao sam žutu boju i preko nje ponegde crnu.

100
00:22:11,637 --> 00:22:17,308
Sada ćemo srediti poklopac. Ofarbao sam i kopču.

101
00:22:20,980 --> 00:22:25,361
Americana akrilna boja Rasberry mi se odlično uklopila.

102
00:22:25,362 --> 00:22:37,222
Sviđa mi se kako se uklopila intenzivna boja, pa ću je dodati i na poklopac.

103
00:22:37,223 --> 00:22:39,577
Kontrast je jako važan za konačni rezultat.

104
00:22:39,777 --> 00:22:47,036
Evo šta ćemo: bojimo motiv crnom bojom, ne precizno.

105
00:22:47,640 --> 00:22:53,254
Važmo je da prekrijemo reljef.

106
00:23:05,325 --> 00:23:15,813
Ostatak poklopca farbamo tirkiz bojom.

107
00:23:43,579 --> 00:23:53,116
Koristimo vlažnu maramicu da razmažemo i pomešamo boje.

108
00:24:02,868 --> 00:24:07,174
Ovde sam preskočio kada sam farbao... 

109
00:24:11,643 --> 00:24:19,638
Ponavljamo postupak, ali nećemo to raditi preko motiva.

110
00:24:27,431 --> 00:24:28,262
Malo prosušimo...

111
00:24:46,228 --> 00:24:57,199
Ponovo koristimo vlažnu maramicu da pomešamo boje...

112
00:25:00,462 --> 00:25:03,746
Želimo isti efekat kao na stranicama, ali preskačemo centralni deo.

113
00:25:03,946 --> 00:25:13,820
Rukama utrljavamo malo crne boje, da dobijemo tamnu senku....

114
00:25:16,275 --> 00:25:20,273
Vlažnim maramicama ublažavamo prelaze.

115
00:25:20,901 --> 00:25:33,077
Sada ćemo boju koju smo koristili za kopču da sipamo na motiv.
116
00:25:35,350 --> 00:25:40,360
Boju razmazujemo rukama.

117
00:25:51,324 --> 00:26:06,037
Vrlo jednostavno, sipate boju, rukama razmažete, napravite prelaz...mislim da je dobro.

118
00:26:06,237 --> 00:26:19,861
I možda još malo bež boje.

119
00:26:45,139 --> 00:26:52,927
Ponovo ćemo koristiti morski sundjer, počeću od žute.

120
00:26:53,127 --> 00:26:59,709
Rekao sam vam da sam svuda na kraju dodao svetlije boje po ivicama, jer mi je boja bila dosta tamna.

121
00:26:59,909 --> 00:27:04,223
Zato sam dodao žutu.

122
00:27:07,277 --> 00:27:09,767
Tapkamo boju.

123
00:27:17,324 --> 00:27:24,116
Zatim dodajem vrlo malo crne.

124
00:27:27,821 --> 00:27:30,981
Razmazujemo je rukama.

125
00:27:53,487 --> 00:28:02,390
Kao što vidite, radim sunđerom, četkom, rukama, koristim razne materijale i tehnike.

126
00:28:03,398 --> 00:28:14,099
Na kraju, ako mi se nešto ne dopada, popravim vlažnim maramicama.

127
00:28:16,666 --> 00:28:21,760
Ovde sam napravio fleku, sad ću to da popravim.

128
00:28:35,066 --> 00:28:38,390
Da vidimo da li možemo još nešto sa bojom da uradimo.

129
00:28:38,590 --> 00:28:42,360
Ova boja je jako lepa,kasnije ću možda isprskati malo belom.

130
00:28:42,560 --> 00:28:56,850
Sada ćemo dodati pečate. Koristimo Do Crafts pečate.

131
00:28:57,050 --> 00:29:08,724
Imaju savršen dizajn. Koristićemo ovaj sa motivima biljaka tj trave.

132
00:29:10,788 --> 00:29:16,230
Uzimam ovaj veliki pečat.

133
00:29:26,021 --> 00:29:39,564
Sunđerom nanosimo chalk boju na pečat, tapkanjem.

134
00:29:49,025 --> 00:30:01,020
Svuda tapkamo.

135
00:30:14,390 --> 00:30:28,783
Ravni sunđer je bolji od morskog za ovo.

136
00:30:32,520 --> 00:30:48,417
Tapkam, ali ne predugo jer se boja suši. Pritiskam pečat na podlogu.

137
00:30:48,617 --> 00:31:00,495
Pažljivo pritiskam...brišem sve što mi se ne dopada.

138
00:31:00,695 --> 00:31:06,460
Nastaviću sa pečatom i bež bojom duž cele stranice.

139
00:31:11,615 --> 00:31:14,425
Nije neophodno da otisak bude savršen.

140
00:31:35,421 --> 00:31:40,252
I još do kraja...

141
00:31:50,317 --> 00:32:06,427
Vlažnom maramicom brišemo pečat. Ako se boj osuši, pečat će biti uništen.

142
00:32:22,607 --> 00:32:34,680
Brišemo i ubrusom, da bude suv.

143
00:32:37,646 --> 00:32:49,080
Sada ćemo naneti Archival crni permanentni ink.

144
00:33:01,330 --> 00:33:12,384
Sada pečat stavljamo malo niže nego što smo stavljali bež boju.

145
00:33:12,584 --> 00:33:21,734
Pažljivo pritiskam i evo rezultata.

146
00:33:32,359 --> 00:33:35,222
Ponavljam celom dužinom.

147
00:34:06,291 --> 00:34:14,581
Nanosimo malo crnog inka po ivici.

148
00:34:14,781 --> 00:34:20,037
Da vidimo št još možemo da dodamo...

149
00:34:21,638 --> 00:34:37,321
Dodaću ponegde još pečata.

150
00:34:38,686 --> 00:34:40,614
Ne celom dužinom.

151
00:34:40,814 --> 00:34:43,748
Samo ponegde...

152
00:34:47,391 --> 00:35:00,647
Ovo je takođe zgodan način da se popravi ono što nam se ne sviđa.

153
00:35:05,348 --> 00:35:07,156
Da li treba još nešto da se doda?

154
00:35:13,310 --> 00:35:18,922
Možda samo ovaj detalj.

155
00:35:32,959 --> 00:35:37,599
Ova strana je gotova.

156
00:35:37,799 --> 00:35:49,530
Ovde svakako želim da dodam boju koju sma koristio i na poklopcu.

157
00:36:00,453 --> 00:36:04,814
...da napravim senku...

158
00:36:17,293 --> 00:36:19,975
Vidite kako se dobro uklopilo?

159
00:36:20,175 --> 00:36:25,396
Sada mi treba tvrda četka...

160
00:36:39,413 --> 00:36:56,697
Istom bojom ću ponegde isprskati kutiju...boju mešam sa vodom...

161
00:37:18,794 --> 00:37:27,151
Mogu ta tapkam papirom i na taj način napravim veće fleke.

162
00:37:34,860 --> 00:37:49,292
Vidite ovde ponovo. Dodajemo vodu, prskamo....papirom tapkamo višak vode i boje i dobijemo fleku.

163
00:38:09,157 --> 00:38:12,996
Nastavljamo sa istom bojom.

164
00:38:19,281 --> 00:38:22,962
Dodajemo i malo bele, da posvetlimo kutiju.

165
00:38:28,586 --> 00:38:37,563
Šablon se jedva vidi, ali mi to ne smeta jer mi se dizajn nije baš ni uklopio.

166
00:38:37,763 --> 00:38:42,153
Zato ga nisam ponavljao na ostalim stranicama.

167
00:38:42,353 --> 00:38:51,421
Kutija je gotova. Ja ću dodati pečat i sa druge strane i brzo se vraćam.

168
00:38:52,122 --> 00:38:56,698
Gotov sam sa pečatima. Vidite rezultat.

169
00:38:56,898 --> 00:39:09,271
Braon StazOn Inkom ćemo naneti crackel pečat.

170
00:39:09,471 --> 00:39:19,701
Vidite da ne stavljam pečat svuda, kao i da stavljam malu količinu inka, da deluje izbledelo.

171
00:39:21,170 --> 00:39:33,621
Vodimo računa da otisak pečata ne bude četvrtast.

172
00:39:33,821 --> 00:39:35,596
Ne previše,samo ponegde...

173
00:39:45,291 --> 00:39:48,711
Rukama trljam da još više izbledi.

174
00:39:49,478 --> 00:39:51,706
Još samo ovde.

175
00:39:53,992 --> 00:40:00,365
I gotovi smo! Nadam se da vam se dopada rezultat.

176
00:40:02,199 --> 00:40:07,595
Ovo je današnji projekat. Nadam se da vam je bilo zabavno i da ste uživali.

177
00:40:07,795 --> 00:40:20,681
Zasmejavaju me :))))

178
00:40:20,881 --> 00:40:27,665
Moj način rada je ponekad čudan, ali verujem da to od mene i očekujete.

179
00:40:27,865 --> 00:40:32,654
Dosta koristim vlažne maramice, različite boje..

180
00:40:32,854 --> 00:40:40,696
Koristili smo pečate... nove tehnike...

181
00:40:40,896 --> 00:40:46,578
Boja maline nam je odigrala važnu ulogu u postizanju kontrasta.

182
00:40:46,778 --> 00:40:50,781
Nadam se da ste uzivali, ovo je tek prva radionica drugog meseca www.handmadefantasyworld.com

183
00:40:50,981 --> 00:40:55,551
Slede mix media, canvas....

184
00:40:55,751 --> 00:40:58,167
Vidimo se uskoro!