1
00:00:03,491 --> 00:00:07,461
Good morning from Belgrade. Here we are again at "handmadefantasyworld.com"

2
00:00:07,661 --> 00:00:13,876
Today, we'll make a mix media box. We'll use lots of materials of many companies.

3
00:00:14,076 --> 00:00:21,517
Our main material will be some pages from an old book.

4
00:00:21,717 --> 00:00:26,390
Let's start and see what our creation for today will be.

5
00:00:26,590 --> 00:00:36,273
So here we are. As I told you we'll use pages from an old book. You may use any book you want.

6
00:00:36,473 --> 00:00:48,288
We'll use the Mod Podge, a thick sealer glue which helps us a lot on glueing the papers on the box.

7
00:00:50,117 --> 00:01:01,837
I apply glue on the wooden box surface.I first cover the box with large pieces of pages.

8
00:01:03,022 --> 00:01:08,212
I also apply some glue on the paper. If you think it's not glued well, apply on the wood again.

9
00:01:15,281 --> 00:01:19,786
Take care that the papers do not exceed the box outline, so that we don't  have to cut them.

10
00:01:23,527 --> 00:01:31,327
We don't mind if the papers wrinkle or have bubbles, our final result is not affected.

11
00:01:56,125 --> 00:02:00,517
After finishing with the large pieces, I start glueing some smaller ones,

12
00:02:00,717 --> 00:02:04,812
so as to create the texture I want.

13
00:02:22,692 --> 00:02:29,955
You may use any other glue, surely they don't easily glue with a decoupage glue.  However, using

14
00:02:30,155 --> 00:02:33,446
an atlacol or a hobby glue you might have the same result.

15
00:02:33,646 --> 00:02:40,493
Mod Pogde is easy in use and of good quality. If you can't obtain it, just use something else.

16
00:02:48,254 --> 00:02:57,255
When we are done with the lid, we'll work on the sides, however, in a different way.

17
00:02:57,455 --> 00:03:04,532
I won't cover it all, just this part. Always working, in the same way.

18
00:03:30,134 --> 00:03:34,319
This is a satisfactory result, no more needed. I'll continue with the rest sides.

19
00:04:00,060 --> 00:04:05,833
Since we finished glueing the pages, of course, we dried them.

20
00:04:06,033 --> 00:04:16,541
You can see the way I glued the pages. You may also see that there are some wrinkles on the lid.

21
00:04:16,741 --> 00:04:20,755
I don't mind about it, as it will help me with the technique I 'm gonna use.

22
00:04:20,955 --> 00:04:28,642
After we finish with the paper glueing, we'll apply a water gloss. You may use any gloss you like.

23
00:04:28,842 --> 00:04:34,624
I'll use a gloss by "Schjerning", a danish company. It's of a good quality gloss and dries easily.

24
00:04:34,824 --> 00:04:40,747
We use a soft brush and start applying gloss only on the glued papers.

25
00:05:10,686 --> 00:05:13,576
When we are done with the gloss, we dry it.

26
00:05:27,324 --> 00:05:38,615
Let me remind you that I've use a water gloss by "Schjerning". If you live in Greece, you'll

27
00:05:38,815 --> 00:05:43,701
be able to find the specific gloss either at the "PacoArt center.gr" or at Konstantinart.gr".

28
00:05:43,901 --> 00:05:51,634
Now we'll use chalk paints for the specific technique, to paint the box.

29
00:05:51,834 --> 00:06:02,045
I'll start with a chalk, decor paint soft, color by "PentArt". The color is the olive tree green.

30
00:06:02,245 --> 00:06:20,000
I use a strong brush and start applying color on the pages. I don't want it to be transparent.

31
00:06:20,200 --> 00:06:25,823
You may apply the first coat and like that the pages letters are visible, but we don't want that.

32
00:06:26,023 --> 00:06:31,302
I want you to paint the pages well. So a second coat of paint might be necessary.

33
00:06:49,019 --> 00:06:54,802
It is easy to paint the pages on the lid of the box. However, on the sides we'll  paint the papers

34
00:06:55,002 --> 00:07:02,761
as well as, make extra brushes exceeding the pages outline, just like this.

35
00:07:04,624 --> 00:07:18,125
Not just right on the outline, freestyle... paint the papers and brush a bit more color on the box.

36
00:07:18,325 --> 00:07:19,606
Same thing on the rest sides.

37
00:07:46,562 --> 00:07:53,265
We've applied the color on every side of the box, we dried it and now we'll do the most interesting and

38
00:07:53,465 --> 00:08:04,609
enjoying part of the project. We'll take a baby wipe, ball it in our hand and we'll remove the chalk color,

39
00:08:07,253 --> 00:08:11,325
from that points we want to. As you can see the pages are revealed. Since we've covered them with

40
00:08:11,525 --> 00:08:16,370
gloss, the papers are not affected at all. Besides that's why we've used it.

41
00:08:16,570 --> 00:08:20,762
So as to have the control of the papers and avoid getting them damaged.

42
00:08:20,962 --> 00:08:32,790
I choose some points to remove the chalk paint and have the paper revealed.

43
00:08:32,990 --> 00:08:36,977
As you may also see, using a baby wipe on the chalk color,  gives us a very nice ageing effect.

44
00:08:37,177 --> 00:08:38,729
Generally you can use baby wipes anywhere.

45
00:08:53,453 --> 00:08:58,904
After we have revealed some pages points using the baby wipe, we may also do something else.

46
00:08:59,104 --> 00:09:15,853
That's rubbing with my  fingers and riving to reveal some pages. You might like it or not.

47
00:09:16,053 --> 00:09:23,587
There is no need to do it everywhere. I'll choose a couple of points to practice it.

48
00:09:32,140 --> 00:09:36,347
We don't mind if the papers don't have this look. In general we create a nice result.

49
00:09:36,547 --> 00:09:47,650
Using a clean baby wipe we may clear the point here and let the letters be seen even more.

50
00:09:47,850 --> 00:09:51,420
See? Very nice!

51
00:09:51,620 --> 00:10:01,211
We'll do the same thing on the box sides. I tear a small paper and then rub with my hand.

52
00:10:01,411 --> 00:10:09,597
Not in an abrupt way, cause we could create a result that we wouldn't like.

53
00:10:12,065 --> 00:10:14,470
On the box sides, it would be better if we didn't exaggerate.

54
00:10:23,035 --> 00:10:32,046
This is the result, I hope you like it. It's quite interesting and kind of freestyle. You have

55
00:10:32,246 --> 00:10:35,550
realised that with me you have to let yourself free and do lots of different things.

56
00:10:35,750 --> 00:10:39,401
That's what mixed media and art means.

57
00:10:39,601 --> 00:10:47,435
Now we'll use another chalk color. It's the home decor line by "FolkArt".

58
00:10:47,635 --> 00:10:53,062
The color is the oatmeal and the code is 34166.

59
00:10:55,405 --> 00:11:06,010
With a hard hair brush I pick some paint. When my brush has much paint on, I do the wood surfaces.

60
00:11:06,210 --> 00:11:15,914
When the paint on brush is getting short I paint on the pages edges too, blending the two colors

61
00:11:16,114 --> 00:11:21,327
We either make vertical brushes or  horizontal. I change my brushes direction so as the colors blend.

62
00:11:22,317 --> 00:11:27,468
I paint in the center I might leave some points upainted.

63
00:11:31,869 --> 00:11:41,854
It's something like the dry brush technique. Which means little paint on the brush , almost dry.

64
00:11:43,510 --> 00:11:50,149
See? I might even brush on the blue color and mix it with my hand.

65
00:11:59,201 --> 00:12:04,617
This is how it looks, the colors have been intergrated, to one another.

66
00:12:04,817 --> 00:12:13,452
The same thing will be done for the rest sides. As for the lid we'll add some color at the edges.

67
00:12:13,652 --> 00:12:17,393
Our brushes should be with direction from the outer part to the inner part of the corner. 

68
00:12:18,401 --> 00:12:24,138
Just barely, add some color. Now let's make the rest of the box and we'll be back soon.

69
00:12:44,113 --> 00:12:55,864
Since we finished painting the box, we dried the color. We'll put the box aside 

70
00:12:56,064 --> 00:13:09,797
We are going to do two things . We'll use some paper stones. I've already cut them in squares.

71
00:13:09,997 --> 00:13:20,591
We'll use again a chalk paint by "FolkArt". The color is salmon coral. The code is 34154

72
00:13:22,881 --> 00:13:27,985
Take a brush and paint the stone papers.

73
00:13:28,185 --> 00:13:31,463
We've said before about stone paper by "Stamperia".

74
00:13:31,663 --> 00:13:39,414
It's a very resistant material in everything. It can be made lots of things. It is stone substanced.

75
00:13:39,614 --> 00:13:46,643
It doesn't tear appart. It can be put it in the washing machine. For example if we make a bag,

76
00:13:46,843 --> 00:13:52,360
or something that needs washing. You can sew it, you can do many things.

77
00:13:52,560 --> 00:14:02,220
I use it because it is flexible, it has volume and it gives me lots of different effects.

78
00:14:03,350 --> 00:14:12,162
As you can see, I just paint the stone papers. My brushes are up and down. Not something difficult.

79
00:14:24,430 --> 00:14:31,567
Let's dry them starting from the big one. We won't dry it too much.

80
00:14:35,520 --> 00:14:41,012
Not well dried see? Now I'll crinkle it.

81
00:14:45,968 --> 00:14:47,104
I crinkle it a lot.

82
00:14:52,457 --> 00:14:59,187
And this is the result. At some points, you might see white lines. That's because the color is still fresh. 

83
00:14:59,387 --> 00:15:10,090
We may rub it as well, and create this effects. These stones papers will be used as decoratives on the box.

84
00:15:10,290 --> 00:15:19,087
We create this effect for which I cannot give a name. You see what is like.

85
00:15:19,287 --> 00:15:22,353
The smallest stone papers should be dried well.

86
00:15:39,074 --> 00:15:45,579
As you can see the chalk color , no matter it dries, it always comes away, cause it's like a powder.

87
00:15:48,337 --> 00:15:54,280
Now let's see what we are going to do with the rest stone papers.

88
00:15:54,480 --> 00:16:04,427
On one of these papers we will use a numbered stencil by "Tim Holtz'.

89
00:16:04,627 --> 00:16:13,030
We'll apply transparent gel paste by "Stamperia", from the mixed media line.

90
00:16:13,230 --> 00:16:18,473
We a sptula we fill in the stencil.

91
00:16:18,673 --> 00:16:21,503
We remove the stencil carefully and here is the  number.

92
00:16:21,703 --> 00:16:35,315
Use a baby wipe to clean the stencil, so that you don't have to wash it.

93
00:16:40,190 --> 00:16:47,745
We'll now deal with the other four papers. We'll use these stamps by "Viva Decor". 

94
00:16:47,945 --> 00:16:55,491
They have a motive on, they are actually pattern stamps. Also permanent archival ink, the black one. 

95
00:16:55,691 --> 00:17:06,822
We put ink on the stamps and let's stamp the stone papers.

96
00:17:07,022 --> 00:17:14,209
We put the stamp, we press everywhere and it's ready.

97
00:17:27,715 --> 00:17:35,007
Since we have our decorations ready, we'll go on putting stamps on the box.

98
00:17:37,652 --> 00:17:57,725
Let's begin. We'll use more stamps. I have two pieces of an "Andy Skinner" stamp.

99
00:17:57,925 --> 00:18:02,529
My friend Maria Kamtzali from Thessaloniki gave them to me. I don't have the whole set.

100
00:18:02,729 --> 00:18:11,210
We'll use again the "Viva Decor" stamps. I've also a couple of small stamps by "Finnabair", 

101
00:18:11,410 --> 00:18:23,940
that look like texture. This one which is like a color imprint. And finally the crackle one by "Stamperia"

102
00:18:24,140 --> 00:18:33,039
We put ink and just because I don't want the whole stamp to be applied,

103
00:18:33,239 --> 00:18:42,667
I press only on the part of the stamp I want to be stamped. See? only that part I pressed.

104
00:18:42,867 --> 00:18:53,527
Now let's stamp the box, here and there and create something like a background.

105
00:19:28,475 --> 00:19:38,021
After putting many stamps, it's better to use the one stamp on the other and not cover the spaces.

106
00:19:44,677 --> 00:19:59,588
Now we'll add our decorations. I'll put the big one here and the other one...

107
00:20:14,610 --> 00:20:23,956
We'll use gel paste, we put some on the stone papers and we glue them on the box.

108
00:20:24,156 --> 00:20:29,577
The big one now...

109
00:20:39,153 --> 00:20:42,954
Here, I'll put the stone paper with the number on.

110
00:20:48,493 --> 00:20:53,635
If you want to put them on the corners, avoid glueing them right on the corner outline. 

111
00:21:12,612 --> 00:21:18,542
As soon as we finish, we dry.

112
00:21:23,814 --> 00:21:38,356
After drying, I used these small tacks, they look more vintage though, on the stone papers.

113
00:21:38,556 --> 00:21:41,361
It's quite decorative, helping on the result.

114
00:21:47,117 --> 00:21:56,132
There goes the last one. I have some extra decorations, a few gears by Artemis Vouros.

115
00:21:56,332 --> 00:22:02,987
They are wood carved. If you don't know who  is Artemis Vouros  , you may visit

116
00:22:02,988 --> 00:22:09,221
the handmadefantsyworld. com page and on the right top corner check on the partners list.

117
00:22:09,222 --> 00:22:15,474
There you'll be able to find his site link, so you can check on the materials he has, as well as order.

118
00:22:15,674 --> 00:22:24,106
I'll use some gears to make a composition and decorate the box.

119
00:22:34,118 --> 00:22:47,963
Same way, I'll glue them using the gel paste. I wipe my brush and glue the gears one by one.

120
00:23:01,220 --> 00:23:10,395
I then dab the remaining glue out and thus we also give the gears an extra ageing effect.

121
00:23:10,595 --> 00:23:17,393
Let's glue a small one...

122
00:23:25,849 --> 00:23:32,761
Now we'll dry the gears to make them steady and then we'll apply the oil.

123
00:24:00,352 --> 00:24:17,333
Now it's time for the oil. As always, I'll use the burnt umber oil hue.

124
00:24:17,533 --> 00:24:23,567
I use a hard brush. It's better that you have brushes only for oil as they are cleaned with naphtha.

125
00:24:24,274 --> 00:24:30,462
Pick some oil and start covering the whole project with it.

126
00:24:33,513 --> 00:24:38,609
It is very important to apply enough oil under the papers too.

127
00:24:38,809 --> 00:24:42,770
We have to make it darker there.

128
00:25:09,337 --> 00:25:17,338
At this point we dab well using much oil so as to let it fill in the gear's gaps.

129
00:25:26,580 --> 00:25:45,835
After applying the oil, we take a baby wipe and start cleaning the surface. We'll do the sides too.

130
00:25:46,035 --> 00:25:56,074
Close to the gears, I leave more oil to make it darker, as well as on the papers

131
00:25:56,274 --> 00:25:59,742
where it is supposed to have dirt.

132
00:26:04,566 --> 00:26:07,930
I gently clean the gears too.

133
00:26:20,738 --> 00:26:23,504
I always make it darker at the corners.

134
00:26:32,154 --> 00:26:33,590
You see what's the result.

135
00:26:39,957 --> 00:26:46,305
Now we'll do the sides. If any oil is needed, we just add some.

136
00:26:48,246 --> 00:26:52,371
Very nice! Now side by side, we'll do the same procedure.

137
00:27:31,582 --> 00:27:43,130
You can see the result. Watch the paper here, how it looks.

138
00:27:46,583 --> 00:27:50,123
See all the sides and back.

139
00:27:50,323 --> 00:27:58,653
We'll add one more detail and we are done for today. I'll use an orange oil by "Pebeo".

140
00:28:00,193 --> 00:28:15,368
We'll need just a bit. With my finger I'll apply it on the gears so as to give an orange color.

141
00:28:15,568 --> 00:28:27,329
I put it in a clumsy way, There is no need to be careful. I dab it out with a baby wipe and it's like rust

142
00:28:38,990 --> 00:28:45,105
That's it for today. I hope you enjoyed it. I hope you like it. Thank you for being here.

143
00:28:45,305 --> 00:28:48,774
See you next week...
Bye!