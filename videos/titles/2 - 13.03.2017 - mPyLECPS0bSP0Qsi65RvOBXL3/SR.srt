﻿1
00:00:02,581 --> 00:00:08,520
Zdravo ponovo! Kako ste? Ovo je drugi www.handmadefantasyworld.com video.

2
00:00:08,720 --> 00:00:12,140
Danas ćemo raditi sat.

3
00:00:12,340 --> 00:00:19,470
Koristićemo neke Stamperia boje, Mix Media gel pasti,

4
00:00:19,670 --> 00:00:28,870
a još važnije, koristićemo Tim Holtz papire i distres boje 

5
00:00:29,070 --> 00:00:32,270
kako bismo napravili pačvork na jednoj polovini sata.

6
00:00:34,000 --> 00:00:40,240
Takodje novo je da ćemo koristiti i pečate kompanije Do crafts iz Engleske

7
00:00:40,241 --> 00:00:46,100
Ovo su neki uzorci, pogledajte dizajn

8
00:00:46,300 --> 00:00:51,970
i ovu torbu za odlaganje pečata, olakšava čuvanje pečata,

9
00:00:51,971 --> 00:00:56,110
i bolji pregled.

10
00:00:56,310 --> 00:01:02,440
Ok, hajde da počnemo.Prvo ćemo olovkom iscrtati sat

11
00:01:02,640 --> 00:01:16,330
Uz pomoć lenjira ćemo povući liniju na sredini, kako bi imali dve polovine

12
00:01:16,331 --> 00:01:17,480,
jer je ideja da se na jednoj polovini naoravi pačvork od papira,

13
00:01:17,481 --> 00:01:25,180
pečati i kompozicija od točkiće će biti na drugojpolovini.
 
14
00:01:25,380 --> 00:01:36,840
Povukli smo glavnu liniju, zatim crtamo pravougaonike, za to nema pravila,  

15
00:01:36,841 --> 00:01:39,220
crtate ih kako god želite.

16
00:01:53,420 --> 00:02:03,190
Kao što vidite, potpuno nasumice sam ih nacrta. Možete bilo koji oblik praviti kao i raspored.

17
00:02:03,655 --> 00:02:12,760
Koristićemo makaze, imamo odlične Do krafts makaze, vrlo kvalitetne

18
00:02:12,960 --> 00:02:22,570
Postavimo lenjir tačno na nacrtanu liniju i makazama pravimo udubljenje....

19
00:02:22,571 --> 00:02:30,260
Ok, posekao sam prst....eto dokaza da su makaze vrlo dobre :)

20
00:02:32,460 --> 00:02:37,310
Pažljivo izrezbarite nacrtane linije.

21
00:02:54,000 --> 00:03:03,390
Ok, izrezbarili smo sve linije kako bismo razdvojili polja.

22
00:03:04,590 --> 00:03:10,540
Koristite makaze ili neki drugi alat samo malo širim vrhom.


23
00:03:10,541 --> 00:03:17,420
Ako je vrh jako tanak neće ostaviti adekvatan trag, dobićete dubok ali ne i dovoljno širok rez.

24
00:03:17,620 --> 00:03:29,630
Ovo je prva polovina. Sada koristimo braon Aquacolor Stamperia

25
00:03:30,631 --> 00:03:40,370
kako bismo popunili tj. istakli linije. Tanka četkica, malo boje, bojimo linije.

26
00:03:40,371 --> 00:03:41,470
Nije važno ako nam boja predje

27
00:03:44,670 --> 00:03:48,330
svakako ćemo pokrivati polja, bojom ili papirom, a možete i obrisati višak

28
00:03:48,331 --> 00:03:54,280
papirnim ubrusom, ovako, odmah nakon nanošenja

29
00:03:55,000 --> 00:03:58,130
Drugi sloj....odlično!

30
00:03:58,330 --> 00:04:00,430
Prelazimo bojom sve linije...

31
00:04:26,000 --> 00:04:32,360
Sada bojimo drugu polovinu sata akrilnom bojom

32
00:04:32,361 --> 00:04:35,560
Lično, ja ne koristim prajmer na drvetu niti na MDF podlogama.

33
00:04:35,561 --> 00:04:42,800
Smatram da je prajmer važniji kod glathih površina kao što su staklo, plastika, metal.

34
00:04:42,801 --> 00:04:49,650
Na poroznim površinama nije potreban prajmer. Boja odlično pokriva.

35
00:04:50,850 --> 00:04:55,330
Danas ćemo koristiti Allego akrilne boje firme Stamperia.

36
00:04:55,530 --> 00:05:06,250
Koristićemo torroncino, bež boju, šifra KAL53.

37
00:05:16,000 --> 00:05:22,190
Kada farbamo jako je važno da su nam pokreti ili horizontalni ili vertikalno

38
00:05:22,191 --> 00:05:32,850
i da potez ide pravo od ivice do ivice

39
00:05:32,851 --> 00:05:39,270
kako bi se boja ravnomerno nanele bez tragova, koji bi došli do izražaja nakon nanošenja uljane boje.

40
00:05:40,000 --> 00:05:41,620
Hajde sad da ofarbamo...

41
00:06:15,000 --> 00:06:24,680
Ok, evo me! Jedan važan detalj sam zaboravio da kažem, ne dozvoljavamo da boja udje u udubljenje

42
00:06:24,681 --> 00:06:34,990
i tako nam prekrije prethodni efekat. Isto bi važilo i za drugu stranu,

43
00:06:35,190 --> 00:06:37,980
mada sada tu stranu ne farbamo, nego ćemo koristiti umesto boje scrapbook papire.

44
00:06:37,981 --> 00:06:49,230
Čak i da farbamo istom bojom polja, ne smemo popuniti akrilnom bojom udubljenja.

45
00:06:49,231 --> 00:06:57,720
Kada pravimo ovaj efekat daščica, kako bismo izbegli boju u udubljenju, potrebno je

46
00:06:57,721 --> 00:07:00,720
malom četkicom da pravimo brze pokrete od centra ka ivicama polja

47
00:07:00,721 --> 00:07:06,290
Čim završim farbanje, umočim četkicu u vodu, ne previše

48
00:07:06,291 --> 00:07:16,000
i prelazim površinu ravnim pokretima kako bi  boja bila ravnomerna.

49
00:07:16,200 --> 00:07:36,220
Potrebno nam je malo vode jer se boja delimično osušila i vodom je reaktiviramo....i ponovo dugi potezi.

50
00:07:44,400 --> 00:07:52,780
Sada smo gotovi i sušimo, koristimo topao vazduh sa male razdaljine.

51
00:08:01,280 --> 00:08:09,080
To je to. Sada ćemo da odaberemo papire. Taj izbor nam je jako važan za krajnji efekat.

52
00:08:09,280 --> 00:08:13,140
Motivi koje ćemo odabrati i kombinacija boja su jako važni.

53
00:08:13,141 --> 00:08:17,000
Ne samo za ovaj projekat.

54
00:08:17,200 --> 00:08:23,980
Na radionicama stalno pričam o bojama i njihovom kombinovanju.

55
00:08:24,180 --> 00:08:32,510
Imamo dva Tim Holtz bloka papira, puno motiva i boja.

56
00:08:33,000 --> 00:08:42,720
Znam da nije lako naći američke brendove u Evropi, malo ko ih uvozi

57
00:08:42,725 --> 00:08:49,290
ali to nas neće zaustaviti u kreiranju.

58
00:08:49,490 --> 00:08:59,200
Ove blokove sam kupio u Hobby Shop DecoMagia (Decomagia.gr) u Atini.

59
00:08:59,400 --> 00:09:06,140
Hajde da odaberemo papire koje ćemo koristiti.

60
00:09:11,340 --> 00:09:19,770
Uzećemo ovaj koji ima veze sa mehanizmima....papiri su dvostrani, kao što vidite.

61
00:09:26,970 --> 00:09:33,260
Pošto projekti nastaju na samom snimanju, ja pokušavam sada pred vama da odaberem

62
00:09:33,261 --> 00:09:38,780
koji ću papir koristiti. Nisam ih unapred odabrao.

63
00:09:38,781 --> 00:09:44,030
Zato mi treba malo više vremena, tako da možete da pratite i birate sa mnom.

64
00:09:44,031 --> 00:09:48,980
Tako možete da vidite kako ja razmišljam, kako donosim odluke.

65
00:09:50,990 --> 00:10:01,640
Da, previše papire....ovaj će mi trebati možda kasnije.

66
00:10:28,840 --> 00:10:32,920
Svakako nam trebaju papiri koji će napraviti kontrast.

67
00:10:33,120 --> 00:10:34,490
Ovaj crveni je savršen!

68
00:10:37,690 --> 00:10:40,180
Možda tamno braon...

69
00:10:42,380 --> 00:10:44,950
Ove etikete su lepe...

70
00:10:54,150 --> 00:10:59,670
Ovi papiri su 33cmx33cm

71
00:10:59,672 --> 00:11:01,330
ali isti motiv se ponavlja i u manjoj dimenziji 

72
00:11:01,331 --> 00:11:05,410
što vam daje mogućnost da ih koristite i na manjim površinama,

73
00:11:05,411 --> 00:11:07,320
ipak ću uzeti ovaj, kako ne bih potrošio ovaj veliki.

74
00:11:11,100 --> 00:11:16,150
Hajde da počnemo. Ako nam bude falio neki papir, ponovo ćemo se vratiti na blokove.

75
00:11:18,350 --> 00:11:29,920
Sada ću vam pokazati kako da isečete papir tačno u veličini koja vam je potrebna.

76
00:11:31,600 --> 00:11:44,920
Postavimo papir direktno na pravougaonik 

77
00:11:44,921 --> 00:11:57,310
i linijom ili noktom napravimo udubljenje na papiru. Potrebno je malo veštine.

78
00:11:57,311 --> 00:11:59,920
Kad tako uradimo, olovka upada kao voz u šine :)

79
00:12:01,000 --> 00:12:08,420
i tako prenosimo linije pravougaonika na papir.

80
00:12:08,421 --> 00:12:15,900
Ovo je lako. Pomalo deluje ukoso, ali ni pravougaonik na podlozi nije baš pravilan.

81
00:12:23,000 --> 00:12:25,940
Odlično! Smem da kažem da je baš ravno (Ironic self comment).

82
00:12:28,210 --> 00:12:36,580
Sada sečemo papir, makazama. Sečemo po unutrašnjoj ivici udubljenja, jer je papir na kraju iskrivljen.

83
00:12:36,581 --> 00:12:42,590
Ne želimo da se to vidi, tako da sečemo blago ka unutrašnjosti papira.

84
00:12:42,591 --> 00:12:51,090
AKo se i vidi da je krivo, papir se i pod makazama krivi, ispravićemo ako bude trebalo.

85
00:13:00,100 --> 00:13:08,460
To je to. Hajde da probamo....potrebno je još malo iseći.

86
00:13:15,000 --> 00:13:21,380
Ne smeta nam ako je papir malo manji od ivica. Isto ponavljamo i na rubu sata.

87
00:13:21,381 --> 00:13:29,750
Možemo i sa stranicom olovke. Ostao je jasan trag i lako ćemo iseći.

88
00:13:38,100 --> 00:13:40,850
....imamo prvi komad...

89
00:13:41,900 --> 00:13:47,620
Sada prema papiru, biramo koji dizajn ćemo gde upotrebiti.

90
00:13:47,621 --> 00:13:56,550
Svakako ovaj crveni zbog kontrasta, možda ćemo koristiti i crvenu akrilnu boju.

91
00:13:56,551 --> 00:14:00,640
...hajde da odaberemo dezene i napravimo raspored....to je jako važno.

92
00:14:32,740 --> 00:14:42,070
Odredio sam raspored, isekao papire , i kao što sam već više puta rekao,

93
00:14:42,071 --> 00:14:48,020
pozicija i kontrastu su nam važni. Zato sam uzeo crvenu,

94
00:14:48,220 --> 00:14:52,380
to važi i za zelenu ispod, a ostali su iz iste palete.

95
00:14:53,580 --> 00:14:59,220
Važan je i dizajn papira. Tim Holt blokovi imaju veliki izbor motiva,

96
00:14:59,420 --> 00:15:05,560
svi su vrlo lepi, ali ovaj izbor koji smo napravili je savršen.

97
00:15:05,760 --> 00:15:14,460
Šta dalje? Koristimo makaze da postarimo komade papira.

98
00:15:14,461 --> 00:15:25,770
Kako ćete to uraditi zavisi od vas. Polako povlačimo makaze, negde jače, negde slabije.

99
00:16:05,970 --> 00:16:11,160
Pokazaću vam šta sam uradio na svim papirima.

100
00:16:11,360 --> 00:16:18,780
Koristim Tim Holtz distress ink boju Vintage photo

101
00:16:19,000 --> 00:16:27,530
Potamnjujem papir kako bi izgledao star.

102
00:16:27,730 --> 00:16:39,560
Pored ivice, negad prelazim i na samu površinu dezena i pravim fleke.

103
00:16:39,760 --> 00:16:47,630
U ovome je trik, koristite i svoje prste....kružno takodje....

104
00:16:50,830 --> 00:17:01,150
Sada ću koristiti Tim Holtz Spicy marmelade distress ink....narandžasti ton...

105
00:17:01,350 --> 00:17:06,840
sa idejom da dobijem ponegde i boju rdje.

106
00:17:06,850 --> 00:17:15,630
Tapkamo, ztrljavamo prstima, dok ne dobijemo željeni rezultat.

107
00:17:21,800 --> 00:17:29,220
AKo nemate Tim Holtz ink, možete koristiti bilo koji drugi ink koji imate.

108
00:17:29,221 --> 00:17:36,000
Odličan je i StazOn.On je permanentan.

109
00:17:36,001 --> 00:17:45,340
Stamperia ima inkove na vodenoj bazi. Dakle, šta god možete da nađete.

110
00:17:45,540 --> 00:17:51,860
Možete koristiti razne boje, praviti senke na papiru, šta god želite.

111
00:17:52,060 --> 00:17:56,420
Mašta je važnija od materijala koji koristite.

112
00:17:56,421 --> 00:18:04,810
I na papiru takođe možete potići mnogo efekata.

113
00:18:05,010 --> 00:18:07,500
Sada ću da završim ovaj deo.

114
00:18:23,000 --> 00:18:33,380
Broken China ink ćemo koristiti da dobijema efekat buđi na papiru.

115
00:18:33,580 --> 00:18:43,630
Malo trljamo, malo razmazujemo prstima i evo rezultata. Malo braon na ivice papira, pa narandžaste

116
00:18:43,631 --> 00:18:49,120
koja nam daje efekat rđe i poslednja je opet plava zbog efekta buđi

117
00:18:49,121 --> 00:18:51,510
i pošto se meša sa prethodnom bojom, dobija blago zeleni ton. 

118
00:19:09,870 --> 00:19:20,400
Sada ćemo zalepiti papir Mix Media lepkom, Stamperia. Odličan za papir.

119
00:19:20,600 --> 00:19:32,570
jer bez obzira što je tečan, kada se nanese papir se ne menja.

120
00:19:32,770 --> 00:19:36,040
Sada ćete da vidite kako to izgleda. Nanosimo lepak četkom, ne previše

121
00:19:36,240 --> 00:19:42,750
i pošto se ovaj lepak brzo suši, pozicioniramo papir brzo na podlogu.

122
00:19:42,950 --> 00:19:54,370
Stavljamo komad papira na odgovarajući pravougaonik. Proverite prvo da li ste uzeli odgovarajući komad.

123
00:19:54,570 --> 00:20:08,080
Vidim da nije dovoljno staviti lepak samo na papir, pa ga stavljam i na podlogu.

124
00:20:08,280 --> 00:20:16,280
Nije problem ako ivice nisu potpuno priljubljene.

125
00:20:16,480 --> 00:20:36,420
Ipak ću bolje postaviti i dodati lepka....ovo možete samo odmah da uradite

126
00:20:36,620 --> 00:20:38,000
jer se lepak brzo suši i ne možete pomeriti kasnije papir.

127
00:20:38,200 --> 00:20:43,540
Nije važno ako ivice nisu dobro zalepljene, važan nam je centralni deo papira.

128
00:21:39,520 --> 00:21:49,490
Ok, to je to. Ne brinu nas ivice, baš ovako i treba da izgledaju.

129
00:21:49,690 --> 00:21:54,020
Vidite i rezultate upotrebe inka.

130
00:21:54,220 --> 00:21:58,820
Sada ćemo da odlučimo šta ćemo sa drugom polovinom sata.

131
00:21:59,020 --> 00:22:03,880
Kao što sam već rekao, koristićemo Do crafts pečate.

132
00:22:04,080 --> 00:22:17,210
Ovo je nešto što se po stilu uklapa, zid i ovaj pečat se baš uklapaju u stil sata.

133
00:22:18,410 --> 00:22:27,000
Da vidimo...šta još možemo da upotrebimo....kompas, probaćemo i njega.

134
00:22:30,690 --> 00:22:52,420
Hajde da napravimo pozadinu. Potrebno nam je mastilo za pečate, tamno braon, nanosimo ga na pečat.

135
00:22:52,620 --> 00:23:12,480
Nikad ne vučemo mastilo po silikonskom pečanu, nego tapkamo, dok po HD pečatu možemo i da vučemo.

136
00:23:12,481 --> 00:23:17,360.
Ovde moramo dobro da utapkamo.

137
00:23:23,790 --> 00:23:35,850
Okrećemo pečat, ali ne želim da se i ćoškovi preslikaju pa pritiskam prstima tamo gde želim da ostane trag pečata.

138
00:23:35,851 --> 00:23:46,850
Želim samo pojedine tačke, distress izgled, ne prenosim ceo pečat.

139
00:23:53,851 --> 00:24:08,000
Vidite rezultat. Vlažnim maramicama skidam ako mi se neki deo otiske ne dopada.

140
00:24:08,200 --> 00:24:15,150
Možda sam pritisnuo neku deo koji nisam trebao, jer ne želim ceo pečat da mi se vidi.

141
00:24:19,000 --> 00:24:27,700
Te tačke brišemo. Da je ovo mastilo na bazi vode, pečat bi se ceo razmazao.

142
00:24:27,701 --> 00:24:33,490
Solventino mastilo možemo delimično obrisati bez da upropastimo ceo otisak. 

143
00:24:33,690 --> 00:24:41,200
Koristimo ponovo pečat. Posle upotrebe vlažnih maramica, pečat osupšite ubrusom.

144
00:24:46,400 --> 00:24:47,440
Ponovo nanosimo pečat.

145
00:24:56,000 --> 00:25:03,120
Postavljamo na podlogu i sada pazim gde vršim pritisak.

146
00:25:09,320 --> 00:25:15,250
I evo rezultata. Ovaj pečat je više distress, nego prvi, koji je nekako pravougaonog oblika.

147
00:25:15,251 --> 00:25:17,580
Dodaćemo nešto preko tog otiska da to prikrijemo.

148
00:25:18,000 --> 00:25:22,360
Sada ćemo dodati još pečata sa dezenom zida.

149
00:25:24,070 --> 00:25:37,290
Do crafts firma ima fantastične pečate....naš prijatelj Den nam je poslao uzorke da ih isprobamo.

150
00:25:40,490 --> 00:25:48,760
Ponovo nanosimo pečat i sada se motivi stapaju i postaju jedna distress celina.

151
00:25:50,960 --> 00:25:52,960
Vidite kako to izgleda?

152
00:25:59,160 --> 00:26:04,780
Nanosim još ponegde dezen zida....

153
00:26:23,980 --> 00:26:30,160
Da vidimo šta još od pečata možemo da iskoristimo...

154
00:26:33,360 --> 00:26:46,330
...mehanizmi...koristimo Do crafts akrilnu ploču za pečate, pečati prijanjaju na podlogu.

155
00:26:46,530 --> 00:26:56,230
Tapkamo mastilo i uklapamo pečat u već postojeće....odlično!

156
00:27:01,420 --> 00:27:03,910
Možda pola pečata ovde...

157
00:27:12,110 --> 00:27:44,480
Da vidimo šta još može da se uklopi...ovaj kompas...šta još?

158
00:27:55,680 --> 00:28:02,520
Greškom sam pečatirao i papir, ali lepo izgleda tako da ću dodati još pečata i po papiru.

159
00:28:07,720 --> 00:28:11,000
Kao što vidite nekad mislite da ste pogrešili, a ustvari dođete do odličnog rešenja.

160
00:28:11,200 --> 00:28:14,220
Sada dodajemo pečat kompasa.

161
00:28:34,420 --> 00:28:35,560
Vrlo lepo!

162
00:28:54,760 --> 00:28:58,430
Ja ih sad kombinujem i pravim kompoziciju od pečata.Kompozicija je jako važna.

163
00:28:58,630 --> 00:29:06,000
Nije da moramo da stavimo jedam pečat ovde, drugi tamo, treći vamo...važna je kompozicija.

164
00:29:06,001 --> 00:29:16,890
Sa pečatima pravimo ujednačenu pozadinu .

165
00:29:17,090 --> 00:29:21,950
Koristim velike pečate na odredjenim pozicijama, zatim dezen zida, i tako pravim kompoziciju...

166
00:29:22,150 --> 00:29:30,090
Kombinujem ih i dodajem pečat mehanizma ponegde i sve ih stapam u jednu celinu. 

167
00:29:30,290 --> 00:29:34,630
Možda i ovaj!

168
00:29:48,830 --> 00:29:54,740
Imamo veliki izbor motiva koje možemo iskombinovati.

169
00:29:54,940 --> 00:29:59,640
Pečati su vrlo lepi, nadam se da ih dobro vidite.

170
00:30:01,840 --> 00:30:03,370
Napravili smo odličnu kompoziciju.

171
00:30:04,000 --> 00:30:11,080
Pogledajte detalje, Ovaj ključ smo stavili vertikalno, tako da drugi nećemo postaviti na isti način, nego nakrivljeno.

172
00:30:27,280 --> 00:30:33,840
Mislim da smo završili sa pečatima. Mastilo je permanentno tako da šta god da nanesem preko ono se neće razmazati.

173
00:30:35,040 --> 00:30:46,800
Sada ćemo uklopiti obe polovine koje smo uradili,

174
00:30:47,000 --> 00:30:49,150
i dodaćemo 3D elemente na podlozi.

175
00:30:51,870 --> 00:30:54,320
Saka kada smo završili sa papirima na jednoj polovini i pečatima na drugoj polovini,

176
00:30:54,520 --> 00:30:57,260
proveravamo da li je potrebno dodati još nešto.

177
00:30:57,261 --> 00:31:02,440
Kao što vidite, napravio sam sovu koristeći elemente mehanizma.

178
00:31:02,441 --> 00:31:06,380
Ove elemente sam dobio iz Rusije,

179
00:31:06,381 --> 00:31:09,370
ali ne znam da vam kažem ko je proizvođač.

180
00:31:09,371 --> 00:31:17,240
Koristićemoove elemente i u drugim projektima. Ovo je karton sa različitim motivima

181
00:31:17,241 --> 00:31:24,200
koje ću iskoristi da napravim figuru na satu.

182
00:31:24,400 --> 00:31:30,290
Koristimo mehanizme za figuru kao i za centralni deo sata.

183
00:31:30,490 --> 00:31:38,402
Kada smo napravili kompoziciju, krećemo sa lepljenjem elemenata Stamperijinim Mix media gel pastom

184
00:31:38,602 --> 00:31:55,220
i lepimo sve elemente, komad po komad. Uvek prvo napravim kompoziciju pa onda lepim.

185
00:31:55,420 --> 00:31:58,095
Komad po komad...

186
00:32:06,295 --> 00:32:23,582
Malo gel paste uzimamo četkicom i stavljamo na pozadinu predmeta. Ova paste je odličan lepak

187
00:32:23,583 --> 00:32:31,420
i koristi se u mnogo tehnika, naročito je dobra za šablone!!!

188
00:32:35,780 --> 00:32:39,300
Pritisnemo na podlogu i idemo na sledeći komad.

189
00:32:40,500 --> 00:32:52,190
Lepimo komad po komad pastom.

190
00:33:45,730 --> 00:33:59,510
Završili smo sa lepljenjem. Dodajem pomalo paste i sa gornje strane kako bi učvrstio elemente.

191
00:34:02,150 --> 00:34:07,920
Nije važno ako nam se neki element pokida ili ošteti, svakako ćemo raditi efekte postarivanja.

192
00:34:09,120 --> 00:34:24,600
Sada dodajem i neke metalne elemente,  nešto iz Scrapberrys Auto Vintage kolekcije.

193
00:34:24,800 --> 00:34:35,890
Ponovo koristimo pastu kao lepak. STavim malo na poledjinu i pritisnem metalni element.

194
00:34:36,090 --> 00:34:46,810
Stavićemo i neki točkić...u centar. Ovo je takodje Scrapberrys element.

195
00:34:49,010 --> 00:34:55,170
Ovo mi je potrebno još lepka...

196
00:34:56,370 --> 00:35:02,370
Nije važno ako lepak izlazi sa strane, utapkam ga četkicom.

197
00:35:02,570 --> 00:35:14,890
Stavićemo i ovaj maleni sat na kompoziciju sove.

198
00:35:19,000 --> 00:35:28,930
Isekao sam jedan metalni element koji će nam poslužiti kao sovin kljun.

199
00:35:35,840 --> 00:35:49,600
Ovo je malo teže za lepljenje, pa sam stavio više lepka i četkicom skinuo višak.

200
00:35:56,000 --> 00:36:11,640
Na nekim mestima sam prosuo pastu. Ako pustite da se osuši, ne možete je više skinuti niti prekriti uljanom bojom.

201
00:36:13,660 --> 00:36:20,970
U tom slučaju možete vlažnom maramicom trljati to mesto sve dok ne skinete boju i pojavi se drvena podloga.

202
00:36:21,170 --> 00:36:33,340
Pokrešio sam na par mesta, ne znam da li to možete da vidite....ovde...

203
00:36:33,341 --> 00:36:41,340
Iskoristiću prednoti postarivanja i dodati još ponegde taj efekat tj smkinuti boju.

204
00:36:43,710 --> 00:36:57,284
Ništa važno...napravićemo još jači efekat postarivanja.

205
00:37:06,890 --> 00:37:11,540
Napraviću ih neravnomerno, da ne izgleda kao da je namerno.

206
00:37:13,740 --> 00:37:15,919
Ponegde ćemo skinuti i pečat.

207
00:37:26,000 --> 00:37:27,747
Vrlo,vrlo lepo!

208
00:37:30,720 --> 00:37:39,228
Ne brine nas ako nešto zabrljamo, iskoristićemo sve u efektu postarivanja. Sada sušenje paste.

209
00:37:55,428 --> 00:38:03,490
Sušili smo pastu, na nekim mestima treba još sušenja, ali dok čekamo

210
00:38:03,491 --> 00:38:11,940
ofarbaćemo ivicu sata Cadenca bojom, Style matt vatreno crvena, šifra 9040.

211
00:38:12,140 --> 00:38:24,031
Farbamo rub. Ova boja je vrlo intenzivna, ali kada stavimo braon uljanu boju

212
00:38:24,231 --> 00:38:27,201
postaće tamno crvena skoro bordo, kao što je i ovaj papir.

213
00:38:28,401 --> 00:38:32,194
Pazimo da ne ofarbamo površinu sata,ali ako se to i desi boju brišemo vlažnom maramicom.

214
00:38:48,394 --> 00:38:54,670
Nadam se da se pasta osušila.

215
00:38:56,000 --> 00:39:08,830
Sada pažljivo prelazimo kompoziciju od točkića belim Gessom.

216
00:39:08,831 --> 00:39:18,784
Radimo to pažljivo i ako zamažemo odmah brišemo vlažnim maramicama.

217
00:39:22,984 --> 00:39:29,559
Ne potpuno, može da se ponegde vidi, ali kao senka.

218
00:39:29,560 --> 00:39:44,636
Farbamo i metalni točkić.

219
00:39:52,836 --> 00:39:59,133
Nema veze ako zamažemo okolo, obrusaćemo vlažnom maramicom.

220
00:40:00,333 --> 00:40:07,453
Ne radim to sporo, ne plašim se da li ću isprljati okolo. Centralni deo takodje.

221
00:40:20,653 --> 00:40:25,312
Na papiru morate odmah da brišete višak.

222
00:40:31,512 --> 00:40:41,425
Kada završimo, vlažnim maramicama brišemo ako smo negde zamazali. Nije važno ako negde i ostane senka.

223
00:41:00,625 --> 00:41:12,647
....mleni sat...njega ću očistiti...

224
00:41:16,847 --> 00:41:20,302
Još malo sušenja.

225
00:41:30,000 --> 00:41:40,611
Lepo! Sada farbamo kompoziciju Americana akrilnom bojom - bundeva narandžasta.

226
00:41:40,811 --> 00:41:43,827
Nakon nanošenja uljane boje ona će dobiti ton koji želimo.

227
00:41:44,027 --> 00:41:49,350
Birao sam boju za sovu i centralni deo i ugledao sam komad papira sa slovima

228
00:41:49,550 --> 00:42:00,085
i na njemu ima senke narandžastog inka pa sam se tako i odlučio.

229
00:42:00,285 --> 00:42:17,249
Takodje je dobar kontrast. Boja je intenzivna,ali će se promeniti.

230
00:42:17,449 --> 00:42:30,216.
Ne farbam detaljno, negde ostavljam da se vidi Gesso. 

231
00:42:30,416 --> 00:42:34,519
Želimo da se vidi narandžasti ton.

232
00:42:35,719 --> 00:42:39,933
Nije važno ako negde prekrijemo i pečat. Kao i sa Gessom kad smo prelazili.

233
00:42:58,133 --> 00:43:03,954
Nije neophodno da potpuno prekrijemo kompoziciju i ako negde predjemo prišemo vlažnom maramicom.

234
00:43:08,154 --> 00:43:13,174
Čistim mali sat i element na oku.

235
00:43:25,374 --> 00:43:37,374
Četkicom na dnu kompozicije povlačim boju ka dole, da dobijemo efekat kao da sova leti.

236
00:43:42,574 --> 00:43:47,035
To radimo sa jako malo boje, tek jedva ostaje senka.

237
00:43:49,235 --> 00:44:00,372
Ovo radimo i da bi dobili efekat rdje. Svakako nam treba još nekoliko boja za taj efekat.

238
00:44:00,373 --> 00:44:16,231
Možete vlažnim maramicama razmazati boju i dobiti kao maglu oko motiva.

239
00:44:45,431 --> 00:44:59,219
Boju sam nanosio četkicom i razmazivao vlažnom maramicom.

240
00:45:04,410 --> 00:45:09,953
Kada se pomeša sa braon, dobiće se efekat rdje.

241
00:45:12,153 --> 00:45:14,923
Važno je da ne koristite mnogo boje.

242
00:45:20,123 --> 00:45:25,288
Završili smo sa ovom bojom i višak vraćamo u flašicu.

243
00:45:25,289 --> 00:45:31,101
Gurate boju folijom u flasicu.

244
00:45:35,301 --> 00:45:37,309
Opet sušimo.

245
00:46:14,000 --> 00:46:21,150
Napravio sam neke dodatke od Tim Holtz papira.

246
00:46:21,151 --> 00:46:34,012
Isekao sam ih, postario inkom pre lepljenja i stavljam ih na podlogu.

247
00:46:34,212 --> 00:46:41,646
Možete papir zalepiti Stamperijinim lepkom Extra forte, to je hobby lepak.

248
00:46:41,846 --> 00:46:50,439
Samo malo lepka i stavljamo na podlogu ali pazimo da ne prekrijemo liniju.

249
00:46:50,639 --> 00:47:03,559
Ovaj takodje. Ako stavimo previše lepka, papir će se naborati.

250
00:47:15,759 --> 00:47:20,320
Sada ćemo prelakirati ceo sat. 

251
00:47:21,400 --> 00:47:25,126
To nam omogućava da kasnije nanosimo uljanu boju bez problema.

252
00:47:25,326 --> 00:47:33,350
Kada stavimo lak, uljana boja lepo klizi i takodje je važno da zaštitimo papir od uljane boje kako ne bi nastale fleke.

253
00:47:33,550 --> 00:47:40,400
Nakon laka, uljana boja se lakše skida.

254
00:47:41,600 --> 00:47:50,699
Vodite računa da lak nanosite pravim potezima ravnom mekanom četkicom.

255
00:47:52,899 --> 00:48:01,992
Važno je da lak svuda dobro nanesemo kako kasnije ne bi uljana boja napravila fleke i neuredan izgled sata.

256
00:48:10,900 --> 00:48:11,949
Dobro prelakiramo i papir.

257
00:48:14,000 --> 00:48:22,389
Ako se papir nabora, nakon dan-dva će se ispraviti, kad isparti sva tečnost.

258
00:49:22,000 --> 00:49:27,763
Prelakiramo, osušimo, i nanosimo uljanu boju.

259
00:49:28,963 --> 00:49:37,140
Koristićemo Van Gogh braon, šifra 409, Burnt umber.

260
00:49:38,340 --> 00:49:46,414
Hajde da vidimo šta će se desiti sa bojama nakon nanošenja uljane boje.

261
00:49:46,614 --> 00:49:48,524
Iskreno, ne znam šta kakav će rezultat biti, naročito kod narandžaste boje.

262
00:49:48,724 --> 00:49:50,352
Nadam se da će nam se svideti.

263
00:49:50,552 --> 00:50:09,890
Koristimo četkicu od tvrde svinjeske dlake za nanošenje uljane boje i uzimamo malu količinu boje.

264
00:50:09,891 --> 00:50:22,965
Prekrivamo ceo predmet uljanom bojom. Počinjem od sove i idem dalje.

265
00:50:28,165 --> 00:50:36,202
Razmazujem svu boju iz četkice.

266
00:50:36,402 --> 00:50:43,361
Ne želim debeli sloj uljane boje, potrebno je sve premazati ali ne preterivati.

267
00:50:48,561 --> 00:50:55,241
Za početak prekrivam sovu i centralni element.

268
00:51:11,441 --> 00:51:20,340
DOk nanosim uljanu boju na sovu i centralni deo, mislim da bi trebali da odjednom namažemo celu podlogu

269
00:51:20,540 --> 00:51:28,890
kako ne bi na odredjena mesta nanosili ponovo uljanu.

270
00:51:28,891 --> 00:51:37,060
Ponovno nanošenje i brisanje bi moglo da nam napravi problem, tako da je bolje da odjednom nanesemo na celu površinu.

271
00:52:03,060 --> 00:52:12,376
EVo, naneli smo na celu površinu, uključujući i rub sata.

272
00:52:12,377 --> 00:52:17,230
Nisam lakirao rub, tako da će tu ostati više uljane boje.

273
00:52:17,231 --> 00:52:21,839
Crvena će postati tamnija i približne boje kao tamno-crveni komad papira.

274
00:52:22,039 --> 00:52:32,493
Sada brišemo uljanu boju, malo po malo.

275
00:52:32,693 --> 00:52:41,517
Ovde koristim jači pritisak kako bi metal došao do izražaja.

276
00:52:41,717 --> 00:53:00,142
a lakšim pokretima brišem ostatak.

277
00:53:08,342 --> 00:53:14,117
Možete posvetleti boju koliko želite. Lično, želim da ostane tamnije.

278
00:53:14,317 --> 00:53:18,436
Ne želim velike razlike na narandžastoj boji.

279
00:53:18,636 --> 00:53:27,493
Sada ostatak površine. Okrećem maramicu i pravim nežne vertikalne pokrete.

280
00:53:27,600 --> 00:53:32,886
Blizu mehanizma želim da ostane tamnije.

281
00:53:42,086 --> 00:53:46,370
Brišem vertikalnim pokretim i okrećem povremeno vlažnu maramicu u ruci.

282
00:53:50,000 --> 00:54:00,812
AKo negde želim da dodam uljanu boju, prvo ubrusom osušim taj deo i onda tek nanesem uljanu boju.

283
00:54:13,800 --> 00:54:19,026
Narandžasta mi se ne dopada naročito, videću još do kraja šta ću sa njom.

284
00:54:27,226 --> 00:54:31,644
Nanosimo uljanu boju na papir i videćemo šta ćemo dobiti.

285
00:54:36,844 --> 00:54:42,170
Na isti način nanosim uljanu boju na polovinu sa papirima.

286
00:55:01,171 --> 00:55:06,160
Ovo je previše narandžasto, moram nešto da promenim.

287
00:55:06,360 --> 00:55:17,710 
Možda ću dodati malo tirkizne preko narandžaste.

288
00:55:17,711 --> 00:55:28,235
Improvizovaćemo nešto, nisam siguran šta ćemo dobiti, samo vidim da je narandžasta prejaka.

289
00:55:28,435 --> 00:55:35,514
Dodaćemo drugu boju, preko uljane, da vidimo šta ćemo dobiti.

290
00:55:36,714 --> 00:55:38,618
Hajde da vidimo...

291
00:55:48,619 --> 00:55:54,633
uzimamo malo boje i tapkam...

292
00:56:04,000 --> 00:56:09,773
Znam da akrilna ne ide preko uljane, ali ipak ću probati.

293
00:56:09,973 --> 00:56:16,220 
Rezultat je odličan. Iako smo do njega došli greškom.

294
00:56:16,420 --> 00:56:24,400 
Kao što sam i ranije rekao, eksperimentišite, probajte nove stvari,drugačije načine....

295
00:56:26,401 --> 00:56:29,970
Skupite materijal koji imate i igrajte se sa njim... 

296
00:56:31,170 --> 00:56:36,834
Vidite, nešto radim prstima, nešto četkicom,koristim vlažne maramice.Nema pravila.

297
00:56:42,034 --> 00:56:48,789
Nisam planirao tirkiz boju uopšte za ovaj projekat ali smo je ipak iskoristili.

298
00:56:56,720 --> 00:57:04,436
Sa jako malo boje, pravim senku oko sove.

299
00:57:17,636 --> 00:57:20,270
Senka se pravi kada skoro uopše nema boje na četkici.

300
00:57:23,470 --> 00:57:27,035
Papirom brišemo satić i oko.

301
00:57:27,235 --> 00:57:53,859
Pokušavam sada sve efekte da uklopim.

302
00:58:15,600 --> 00:58:31,423
Dodajem malo tirkizne, nanosim je i četkicom i rukama.

303
00:58:43,623 --> 00:58:57,700 
Dodaću još neke elemente. Možete pronaći razne detalje u lokalnim radnjama.

304
00:58:57,701 --> 00:59:09,284
Sad ću ih zašrafiti i videćemo da li je potrebno dodati još nešto.

305
00:59:41,484 --> 00:59:48,570 
Dodajem dekorativne elemente. Ovaj prvi izgleda kao da sova sedi na njemu.

306
00:59:48,571 --> 00:59:54,679
Drugi je bravica.

307
00:59:54,879 --> 01:00:04,391
Za ekstra efekat, dodaću eksere na krajeve papira.

308
01:00:07,591 --> 01:00:08,050
To su samo detalji, ali prave veliku razliku u krajnjem rezultatu.

309
01:00:20,160 --> 01:00:26,624 
KAd pogledam sada projekat, shvatam da se narandžasta nikako ne bi uklopila.

310
01:00:26,824 --> 01:00:31,365
Bolje bi bilo da sam koristio neku tamniju narandžastu.

311
01:00:33,190 --> 01:00:38,640
Popravio sam sve dodavanjem tirkiz boje.

312
01:00:38,840 --> 01:00:47,560
Nešto mi nedostaje na projektu, nije 100% ono što bih ja želeo.

313
01:00:47,760 --> 01:01:04,625
Probaću da dodam malo bele ili tamno crvene na sovu.

314
01:01:07,220 --> 01:01:14,787
Videte? Detalji naprave veliku promenu.

315
01:01:16,550 --> 01:01:26,898
Završio sam sa šrafljenjem i dodajem još nešto.

316
01:01:26,899 --> 01:01:28,310
Gde su mi kutije?

317
01:01:33,510 --> 01:01:41,423
Dodajem dekorativne eksere. Koristio sam ih i na prvom projektu.

318
01:01:41,623 --> 01:01:54,886
Sad ću ih zakucati. Nadam se da neće probiti drvo! Ipak prolaze!

319
01:02:29,086 --> 01:02:45,565
Dodajem još malo uljane boje.

320
01:03:03,600 --> 01:03:08,353
Želim da posvetlim neke tačke. 

321
01:03:21,900 --> 01:03:26,384
Na četkici ima jako malo ulja.

322
01:04:03,280 --> 01:04:15,278
Belom bojom pravim izmaglicu, tek da popravim ton boje. Ne svuda, samo na nekim tačkama.

323
01:04:22,478 --> 01:04:42,073
Sada dodajemo crnu uljanu boju, jako malu količinu, da naglasimo određene linije.

324
01:04:44,273 --> 01:04:55,376
Ovo je završna faza. 

325
01:05:29,576 --> 01:05:32,066
Dodajemo malo na uglove.

326
01:05:36,266 --> 01:05:37,894
Još malo okolo.

327
01:05:41,094 --> 01:05:53,764
Malo po točkićima.

328
01:06:16,200 --> 01:06:24,657 
I poslednji korak, prskamo belom bojom, može i Gessom. 

329
01:06:27,770 --> 01:06:50,736
Gesso mešamo sa vodom

330
01:07:20,000 --> 01:07:26,022
Napravio sam haos, kao i uvek.

331
01:07:28,080 --> 01:07:32,937
Ovo je današnji projekat! Hvala što ste stvarali sa mnom.

332
01:07:33,137 --> 01:07:35,022
www.handmadefantasyworld.com

333
01:07:35,222 --> 01:07:43,098
Nadam se da vam je bilo interesantno. Scrapbook papiri, Do crafts pečati, Tim Holtz mastila,

334
01:07:43,298 --> 01:07:51,255
rezbarenje drveta, uljen bolje, postarivanje, mnogo boja...

335
01:07:51,455 --> 01:07:58,074
Ne bih propustio da u prvom mesecu napravim neki sat, satovi su  moja strast.

336
01:07:58,274 --> 01:08:04,410
Hvala još jednom. Vidimo se uskoro. Čekam vas za nedelju dana. Ćao!
