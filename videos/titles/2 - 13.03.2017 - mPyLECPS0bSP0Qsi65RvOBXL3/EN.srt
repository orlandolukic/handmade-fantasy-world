1
00:00:02,581 --> 00:00:08,520
Good day again, how are you doing? We are here for the handmadefantasyworld.com second video

2
00:00:08,720 --> 00:00:12,140
Today's project is a 50 inches clock

3
00:00:12,340 --> 00:00:19,470
We are going to use some materials and colours of Stamperia, Mix Media Gloss gen paste

4
00:00:19,670 --> 00:00:28,870
much important, we will use  papers from "Tim Holtz" and some inks that it has

5
00:00:29,070 --> 00:00:32,270
in order to create a half side patchwork on the clock

6
00:00:34,000 --> 00:00:40,240
also very important, we will use some stamps by "docraft" in England

7
00:00:40,241 --> 00:00:46,100
I've been given some samples to work on, you may see here the designs

8
00:00:46,300 --> 00:00:51,970
and this beautiful stamp case where you may keep your stamps, so you avoid the risk of losing them

9
00:00:51,971 --> 00:00:56,110
and have the ability to check easily on a design

10
00:00:56,310 --> 00:01:02,440
Well, let's start. The first thing we are going to do, using a pencil, is to draw the clock

11
00:01:02,640 --> 00:01:16,330
in seminars, I've shown how we do it. Using a ruler we draw a line separating the clock in two halfs

12
00:01:16,331 --> 00:01:17,480
as the idea here is to create a patchwork on the one half of the clock,

13
00:01:17,481 --> 00:01:25,180
stamps and a composition of gears on the other half.

14
00:01:25,380 --> 00:01:36,840
We draw the main line and then we design squares the way we like them, there is no rule on that

15
00:01:36,841 --> 00:01:39,220
You may draw whatever you like.

16
00:01:53,420 --> 00:02:03,190
Now regarding the lines, as you can see they are quite random. You may draw any shape you want.

17
00:02:03,655 --> 00:02:12,760
We will use α scissors, here we have a "Durcraft" line of scissors, quite of a good quality ones

18
00:02:12,960 --> 00:02:22,570
We put the ruler right on the line drawn and using the scissors we start engraving ...

19
00:02:22,571 --> 00:02:30,260
ok I cut my finger, this things happen...that's a proof that the scissors are very good.

20
00:02:32,460 --> 00:02:37,310
You carefully engrave the pencil lines.

21
00:02:54,000 --> 00:03:03,390
Well, as you see, we engraved all the lines and we created the effect of separate pieces

22
00:03:04,590 --> 00:03:10,540
You need to do this using a scissors or any other tool with a wide nib

23
00:03:10,541 --> 00:03:17,420
cause if you use something thin the lines will be egraved in depth and little squares won't distinguish

24
00:03:17,620 --> 00:03:29,630
This is the first part. Now you have to take a dark brown aqua colour by "Stamperia"

25
00:03:30,631 --> 00:03:40,370
to emphasize the lines. A small brush, a bit color and paint the lines.

26
00:03:40,371 --> 00:03:41,470
We don't mind if it goes off lines

27
00:03:44,670 --> 00:03:48,330
one way or another it will be covered by the rest materials, however you may wipe it with

28
00:03:48,331 --> 00:03:54,280
a paper towel, like this, as soon as you paint a line

29
00:03:55,000 --> 00:03:58,130
A second coat ... good!

30
00:03:58,330 --> 00:04:00,430
Let's paint  the rest lines τοο...

31
00:04:26,000 --> 00:04:32,360
As long as we have finished with the lines, we will paint the other half side using an acrylic paint

32
00:04:32,361 --> 00:04:35,560
Personally, I don't use primers neither on MTF nor on wood

33
00:04:35,561 --> 00:04:42,800
I consider that primer is for smooth surfaces like glass, plastic or metal

34
00:04:42,801 --> 00:04:49,650
On porous surfaces like MTF and wood we do not need a primer. The paint takes effect right away.

35
00:04:50,850 --> 00:04:55,330
Today we are going to use color paints by "Stamperia", the allegros

36
00:04:55,530 --> 00:05:06,250
we will use the torroncino, a light ivory color, beige, the code is KAL53

37
00:05:16,000 --> 00:05:22,190
When we paint, it is very important that our movements are either vertical or horizontal

38
00:05:22,191 --> 00:05:32,850
and at the end we make an one straight brush from side to side

39
00:05:32,851 --> 00:05:39,270
making the color look equable and without inperfections after we apply the oil.

40
00:05:40,000 --> 00:05:41,620
Let's paint it ...

41
00:06:15,000 --> 00:06:24,680
So, we are back! An important detail that I forgot to mention earlier is that we don't let paint

42
00:06:24,681 --> 00:06:34,990
get into the engraved line and undo our previous effect. Same thing if we painted this side

43
00:06:35,190 --> 00:06:37,980
now we will not color them because we are going to use scrapbooking papers instead

44
00:06:37,981 --> 00:06:49,230
Even if we painted them in the same color, we shouldn't let the paint get into the engraved lines

45
00:06:49,231 --> 00:06:57,720
About the wood sides and in order to avoid smear them with paint, you just need to make

46
00:06:57,721 --> 00:07:00,720
small airy brushes moving your hand from the inside to the outside points

47
00:07:00,721 --> 00:07:06,290
As soon as I finish painting, Ι soak my brush with water, not too much ...

48
00:07:06,291 --> 00:07:16,000
αnd start making straight brushes from one end to the other, to make the color look equable

49
00:07:16,200 --> 00:07:36,220
We need water cause our color got already dry and we need to "reactivate" it... and again long brushes

50
00:07:44,400 --> 00:07:52,780
We are ready now, let's dry it, we use hot air keeping a small distance

51
00:08:01,280 --> 00:08:09,080
That's it. We are going to choose our papers, Our choise is very important for the result.

52
00:08:09,280 --> 00:08:13,140
The papers we choose and the combination of colors are so much of importance,

53
00:08:13,141 --> 00:08:17,000
apart from the whole project idea.

54
00:08:17,200 --> 00:08:23,980
I keep on saying, during seminars, about the colors and of course their combination

55
00:08:24,180 --> 00:08:32,510
Here we have two blocks from "Tim Holtz", as you can see there are lots of papers

56
00:08:33,000 --> 00:08:42,720
I know it is difficult to find some American products here in Greece since they are not imported

57
00:08:42,725 --> 00:08:49,290
however, we can't stop imagine creating with products that don't exist in Greece

58
00:08:49,490 --> 00:08:59,200
These blocks have been bought from the "Hobby Show DecoMagia" (Decomagia.gr) in Athens

59
00:08:59,400 --> 00:09:06,140
I think you may find these papers there. Let's choose the papers we are going to use

60
00:09:11,340 --> 00:09:19,770
We may keep this one which is relevant to gears... the papers as you see, are double-side

61
00:09:26,970 --> 00:09:33,260
In the meantime, I'd like to tell you that it's because our videos are live, I'm trying now to choose

62
00:09:33,261 --> 00:09:38,780
what papers to use. It is not designated what I'm going to do

63
00:09:38,781 --> 00:09:44,030
So I need a little bit more time, furthermore it's better to think about it together.

64
00:09:44,031 --> 00:09:48,980
You will also be able to see, how my thinking works and under what circumstances I take my decisions

65
00:09:50,990 --> 00:10:01,640
Well, too many papers... maybe this one will be needed later

66
00:10:28,840 --> 00:10:32,920
Surely we are going use a paper that will make a contrast with our colors

67
00:10:33,120 --> 00:10:34,490
I think the red one is ideal!

68
00:10:37,690 --> 00:10:40,180
Maybe dark brown....

69
00:10:42,380 --> 00:10:44,950
These labels are nice ...

70
00:10:54,150 --> 00:10:59,670
As you can see the designs are in 33X33cm paper dimensions

71
00:10:59,672 --> 00:11:01,330
but the same motives are found in smaller size too

72
00:11:01,331 --> 00:11:05,410
which means that if someone wants to use a paper on a small surface,

73
00:11:05,411 --> 00:11:07,320
will use those instead, so as not to waste the large ones.

74
00:11:11,100 --> 00:11:16,150
Let's begin and if we find anything we need missing, we will check the blocks again.

75
00:11:18,350 --> 00:11:29,920
Well, I'm gonna show you how our papers will be cut exactly in the size of the squares.

76
00:11:31,600 --> 00:11:44,920
We put the paper right on the square we want to cover and straight with the bottom side of the clock

77
00:11:44,921 --> 00:11:57,310
and with a pencil and our nail, we find the engraved lines. It needs some skill though.

78
00:11:57,311 --> 00:11:59,920
As soon as we do that, the pencil rails like a train in the lines

79
00:12:01,000 --> 00:12:08,420
and thus we have, the outline of the square, now on the paper.

80
00:12:08,421 --> 00:12:15,900
It is quite easy. However if it seems a bit slanting, it's because my original drawing isn't straight

81
00:12:23,000 --> 00:12:25,940
Great! .... I dare say that mine is nothing but straight (Ironic self comment).

82
00:12:28,210 --> 00:12:36,580
Now we'll scissor the paper, however, the inner side of the outline because the outline was carved

83
00:12:36,581 --> 00:12:42,590
right on the engraved lines and we want them to be seen. So we cut a little more within.

84
00:12:42,591 --> 00:12:51,090
Even if it looks oblique, still, scissor on the paper carves and then if needed correct it

85
00:13:00,100 --> 00:13:08,460
That's all with it ! We try it out to see ...well it needs some trimming off.

86
00:13:15,000 --> 00:13:21,380
I don't mind if the paper is a bit within the engraved line. We do the same on the edge.

87
00:13:21,381 --> 00:13:29,750
Maybe with the pencil side,  see? Steady, rub and the outline is ready for us to cut.

88
00:13:38,100 --> 00:13:40,850
... and we have our first piece.

89
00:13:41,900 --> 00:13:47,620
Now according to the papers we have, we will chose the designs we are going to use.

90
00:13:47,621 --> 00:13:56,550
We will surely use a red paper for contrast, we may even paint red the clock sides for a tint.

91
00:13:56,551 --> 00:14:00,640
...and let's go for the designs ! it's very important ok?

92
00:14:32,740 --> 00:14:42,070
We are back again, we have chosen and cut the papers and as I have already said, the colors

93
00:14:42,071 --> 00:14:48,020
and the position of the papers are very important too, that's why the red for, to make a contrast

94
00:14:48,220 --> 00:14:52,380
That's also valid for the green underneath, the rest are about of the same coloration.

95
00:14:53,580 --> 00:14:59,220
The subjects among the designs should be relevant too. In "Tim Holtz" blocks, there are lots of them.

96
00:14:59,420 --> 00:15:05,560
They are all very nice but those we choose should be congenial.

97
00:15:05,760 --> 00:15:14,460
Now what? We take a small scissors and give our papers the effect of ageing

98
00:15:14,461 --> 00:15:25,770
How it can be done depends on you. We sligtly rub or it may be more at some points and less to others.

99
00:16:05,970 --> 00:16:11,160
After rubbing all the papers, Ι made a process. I'm gonna show you what I did.

100
00:16:11,360 --> 00:16:18,780
Using the "vintage photo" tint from the "Tim Holtz distress ink" line

101
00:16:19,000 --> 00:16:27,530
I actually blacken the papers sides so as to make them look old.

102
00:16:27,730 --> 00:16:39,560
Sometimes I put it down and I blacken even the paper surface. You may also dab it.

103
00:16:39,760 --> 00:16:47,630
It does the tricks! and a little with your fingers....   perimetrically too ...

104
00:16:50,830 --> 00:17:01,150
And then I'm going to use the  Tim Holtzs' "Spicy marmalade" ... actually an orange hue

105
00:17:01,350 --> 00:17:06,840
in order to give, at some points, the rusty effect.

106
00:17:06,850 --> 00:17:15,630
Dabbing a little, with fingers as well, until I have the desirable result

107
00:17:21,800 --> 00:17:29,220
of course if you don't have the Tim Holtz's "distress ink" line, you may use any other kind of ink

108
00:17:29,221 --> 00:17:36,000
In the market you may easily find the "StazOn" inks, they are also permanent.

109
00:17:36,001 --> 00:17:45,340
"Stamperia" water inks, as well ! Anyway, it doesn't matter, any ink you find, will do.

110
00:17:45,540 --> 00:17:51,860
you may apply various colors on the papers, different shadings on the squares, anything!

111
00:17:52,060 --> 00:17:56,420
imagination is what you need, this isn't the only material you may use.

112
00:17:56,421 --> 00:18:04,810
You may create lots of effects on any surface as well as on the papers. Ok?

113
00:18:05,010 --> 00:18:07,500
Let's go and finish this part and we will be back soon.

114
00:18:23,000 --> 00:18:33,380
So ! with the "Broken China", a light blue colored ink, we may give a mouldy effect.

115
00:18:33,580 --> 00:18:43,630
little dabbing, fingers again and we have this result. A bit brown on the paper edges, then is orange

116
00:18:43,631 --> 00:18:49,120
which gives us the rust effect and the last one is light blue for the mould effect beacause

117
00:18:49,121 --> 00:18:51,510
it mingles with the former ones and takes this greenish color of mould

118
00:19:09,870 --> 00:19:20,400
Now it's the time to glue the papers using "Mix media glue" by "Stamperia", really good for papers

119
00:19:20,600 --> 00:19:32,570
cause no matter the fact that it's liquid, you apply it and the papers stay completely unaffected

120
00:19:32,770 --> 00:19:36,040
You will be able to see it on practise.We apply it with a brush, not too much of a quantity

121
00:19:36,240 --> 00:19:42,750
and since it's a fast dry glue, apply it quickly

122
00:19:42,950 --> 00:19:54,370
Then we glue our paper on the right square. Be sure first that you have managed the correct size.

123
00:19:54,570 --> 00:20:08,080
I noticed that it's not enough applying glue only on the paper, so I put some on the wood too

124
00:20:08,280 --> 00:20:16,280
Not a problem if the paper arises a little. Ι misplaced it , see? it is so well glued that It doesn't come away

125
00:20:16,480 --> 00:20:36,420
I'm gonna place it now just right, some extra glue ... this is something you may do only at that time

126
00:20:36,620 --> 00:20:38,000
not later as the paper won't come away

127
00:20:38,200 --> 00:20:43,540
We don't care if the edges are not well glued, we are mainly interested about the center of the paper.

128
00:21:39,520 --> 00:21:49,490
We are ready. We do not care about the edges, we want them this way.

129
00:21:49,690 --> 00:21:54,020
You also see the difference in the result after using the inks

130
00:21:54,220 --> 00:21:58,820
Now we are going to decide what we will be doing on the rest half clock

131
00:21:59,020 --> 00:22:03,880
As I've already mentioned, I'm thinking of using "docrafts" stamps

132
00:22:04,080 --> 00:22:17,210
Here, there is something of this style, maybe the wall and this one which is exactly the clock's style

133
00:22:18,410 --> 00:22:27,000
Let's see ... is there anything else? ... here, maybe some compasses, will see.

134
00:22:30,690 --> 00:22:52,420
Let's create a background,we need ink, dark brown,we dab with the ink the back side of the stamp

135
00:22:52,620 --> 00:23:12,480
We never drag the ink on the silicon stamps, we dab, while on HD we may do it.

136
00:23:12,481 --> 00:23:17,360
Here we need to dab it very well.

137
00:23:23,790 --> 00:23:35,850
We turn over the stamp buτ Ι don't want the corners to be stamped, so with my fingers I press

138
00:23:35,851 --> 00:23:46,850
only the points I want, so as to be "distress", I don't stamp it all, it needs some extra force.

139
00:23:53,851 --> 00:24:08,000
You can see the result. I'll take a baby wipe and will wipe out some points of the stamp I don't like

140
00:24:08,200 --> 00:24:15,150
Maybe, I pressed some points I shouldn't.I dont' want the whole stamp to be seen.

141
00:24:19,000 --> 00:24:27,700
So we wipe those points out. If this was a water ink the whole stamp would be in a mess.

142
00:24:27,701 --> 00:24:33,490
Since it's a permanent one we may interfere with a baby wipe without destroying the stamp effect.

143
00:24:33,690 --> 00:24:41,200
We will use the stamp again. Don't forget! After using a baby wipe, dry, with a paper towel.

144
00:24:46,400 --> 00:24:47,440
Let's stamp again

145
00:24:56,000 --> 00:25:03,120
we put the stamp on the surface. Now I'm gonna be careful on how much I press.

146
00:25:09,320 --> 00:25:15,250
And here is the result.This stamp is more "distress" than the first one which looks square shaped,

147
00:25:15,251 --> 00:25:17,580
but we will add more effects to cover it.

148
00:25:18,000 --> 00:25:22,360
Now we will add more stamps of the wall design.

149
00:25:24,070 --> 00:25:37,290
I remind you that these stamps are by "docraft" ... our friend Den sent us some samples to work with.

150
00:25:40,490 --> 00:25:48,760
Stamp again and by blending the wall design stamp between them I will bring all the pieces together.

151
00:25:50,960 --> 00:25:52,960
See what's like eh?

152
00:25:59,160 --> 00:26:04,780
I apply the wall design stamp on other point

153
00:26:23,980 --> 00:26:30,160
Let's see what other stamp designs we may use...

154
00:26:33,360 --> 00:26:46,330
... the gears... we use this base by "docraft", silicon stamps gum up on it

155
00:26:46,530 --> 00:26:56,230
We dab ink on the stamp and then we adjust it on the project... Great!

156
00:27:01,420 --> 00:27:03,910
Maybe a half of a stamp here ...

157
00:27:12,110 --> 00:27:44,480
Let's check what else we've got here... the compass ... this one... what else? .....

158
00:27:55,680 --> 00:28:02,520
I stamped by mistake on the paper too but I see that It looks very nice, so I'm gonna do it again.

159
00:28:07,720 --> 00:28:11,000
As you can see we did something by mistake but at the same time we created something beautiful.

160
00:28:11,200 --> 00:28:14,220
Now we are going to use the compass design stamp

161
00:28:34,420 --> 00:28:35,560
Very nice!

162
00:28:54,760 --> 00:28:58,430
Actually, what I do is a combination of them.The composition is of great importance too.

163
00:28:58,630 --> 00:29:06,000
It's not that we have to put a stamp here... another one here... and a third one here.

164
00:29:06,001 --> 00:29:16,890
With our stamps we have to make an equable background and of use them more than once.

165
00:29:17,090 --> 00:29:21,950
I used the bigger stamps at two points then the wall design here and here, a little bit there...

166
00:29:22,150 --> 00:29:30,090
so as to combine to each other and the the gear design to piece them all together.

167
00:29:30,290 --> 00:29:34,630
Perhaps ... this one too!

168
00:29:48,830 --> 00:29:54,740
There are a lot of designs we may use

169
00:29:54,940 --> 00:29:59,640
Stamps are very nice, I think you understand that too.

170
00:30:01,840 --> 00:30:03,370
We've done a great job.

171
00:30:04,000 --> 00:30:11,080
Ckeck on a detail! Look this key is vertical so we won't put this one on the same way but a bit aslant.

172
00:30:27,280 --> 00:30:33,840
I think now we are ready. This is a permanent ink, so whatever I do, nothing happens.

173
00:30:35,040 --> 00:30:46,800
Now it's time to create a synthesis over the background that we created,

174
00:30:47,000 --> 00:30:49,150
so as to give a 3D view on this half of the clock

175
00:30:51,870 --> 00:30:54,320
As soon as we have finished our effects with the papers the background and the stamps,

176
00:30:54,520 --> 00:30:57,260
we are checking if there is anything else we may add.

177
00:30:57,261 --> 00:31:02,440
As you see I already have an owl like, gear synthesis.

178
00:31:02,441 --> 00:31:06,380
The truth is that these have been sent to me by Russia.

179
00:31:06,381 --> 00:31:09,370
I don't know how to explain you what's on the product label.

180
00:31:09,371 --> 00:31:17,240
We will use this technique on other projects. They are paper cards with various designs

181
00:31:17,241 --> 00:31:24,200
which I used to create a decorative figure to put on the clock.

182
00:31:24,400 --> 00:31:30,290
Some gears, that's the main subject of our project, plus the clock in the center.

183
00:31:30,490 --> 00:31:38,402
Since we have concluded with our synthesis, we are going to use the"mix media gel paste"

184
00:31:38,602 --> 00:31:55,220
to glue the gears piece by piece. Personally I do first the setting and then I start glueing.

185
00:31:55,420 --> 00:31:58,095
Piece by piece....

186
00:32:06,295 --> 00:32:23,582
Some gel paste and using a brush, dab and apply glue on the surface. That's a very good glue

187
00:32:23,583 --> 00:32:31,420
it is used in many techniques, in many stencils, it is very good!!!.

188
00:32:35,780 --> 00:32:39,300
We press it onto the wood and we go on with the bigger one.

189
00:32:40,500 --> 00:32:52,190
We will glue piece by piece, all the chipboards on the clock.

190
00:33:45,730 --> 00:33:59,510
So we finished glueing. If you happened to see I put some glue on the top too for better bonding.

191
00:34:02,150 --> 00:34:07,920
We don't mind if something does not glue well or is cut or broken cause will give us the effect of old.

192
00:34:09,120 --> 00:34:24,600
Now let's add some metal pieces too. Like these metal caps by the "Scrapberry's" Auto Vintage line.

193
00:34:24,800 --> 00:34:35,890
Again using the gel paste glue. I apply some and then press the metal cap onto the clock.

194
00:34:36,090 --> 00:34:46,810
Let's put this gear too .... here in the center. The gear as well, is by "scrapberry's".

195
00:34:49,010 --> 00:34:55,170
Maybe there is a need of some extra gel around it....

196
00:34:56,370 --> 00:35:02,370
I dont' mind if I put a lot of glue, I then dab it to make it right.

197
00:35:02,570 --> 00:35:14,890
We will also put this tiny clock on the center of the owl synthesis.

198
00:35:19,000 --> 00:35:28,930
Ah! we have cut a metal piece which will be used as a beak for the owl.

199
00:35:35,840 --> 00:35:49,600
This will be more difficult to glue, so I put some extra glue and brush out the remainings.

200
00:35:56,000 --> 00:36:11,640
At some points,I spilt some pasta. If it dries, there is no way to undo it, even if it is covered with oil.

201
00:36:13,660 --> 00:36:20,970
In that case, we can only use the baby wipe and try to take the color off until the wood is revealed.

202
00:36:21,170 --> 00:36:33,340
Making this mistake, I 'm not sure if you are able to see it via camera. Look right here...

203
00:36:33,341 --> 00:36:41,340
I'm going to take advantage of it and use it as an ageing effect. So I'll add more of such an effect.

204
00:36:43,710 --> 00:36:57,284
It's not a big deal... we will create even more ageing effects as if it was part of the original project

205
00:37:06,890 --> 00:37:11,540
I'll make them distressed, so as not look setup.

206
00:37:13,740 --> 00:37:15,919
We mess with the stamp too but it cano't be done otherwise.

207
00:37:26,000 --> 00:37:27,747
Very, very nice!

208
00:37:30,720 --> 00:37:39,228
That's what we do, so we don't mind about this little mess anymore. Let's dry and we will be back soon.

209
00:37:55,428 --> 00:38:03,490
We, ve dried the gel paste, the eye some more time but until it does,

210
00:38:03,491 --> 00:38:11,940
we are going to use a Style Matt  by "CADENCE" paint, code 9040 fire red.

211
00:38:12,140 --> 00:38:24,031
We will paint the clock sides. It looks very intense but as sson as we use the brown oil

212
00:38:24,231 --> 00:38:27,201
it will take the same shading with this paper.

213
00:38:28,401 --> 00:38:32,194
We are careful not to spoil the clock surface but if it happens we wipe it out

214
00:38:48,394 --> 00:38:54,670
After finishing the sides, I hope the paste has dried, it looks better.

215
00:38:56,000 --> 00:39:08,830
We will now carefully cover all the gears with "Gesso", white gesso, so as to paint them later on.

216
00:39:08,831 --> 00:39:18,784
We must be careful, it i may slop, but we will use a baby wipe, to wipe it anytime it happens.

217
00:39:22,984 --> 00:39:29,559
Not completely, as much as not it looks bad. Or let it visible but fade.

218
00:39:29,560 --> 00:39:44,636
We don't want nasty cuts. we paint the metal gear, as well.

219
00:39:52,836 --> 00:39:59,133
I don't mind if it slops,  I'll wipe it afterwards.

220
00:40:00,333 --> 00:40:07,453
I dont' take it slow, I'm not afraid to move my brush out. In the center too

221
00:40:20,653 --> 00:40:25,312
On the contrary, on the papers you have to wipe it out quickly

222
00:40:31,512 --> 00:40:41,425
When we finish, it's time for the baby wipe to clean any slops.We don't mind if we leave some behind.

223
00:41:00,625 --> 00:41:12,647
... the tiny clock, and here in between using a cotton swab to clear it

224
00:41:16,847 --> 00:41:20,302
We dry again, just for a little.

225
00:41:30,000 --> 00:41:40,611
Nice! let's paint using acrylic color Americana. The spicy pumpkin color. Actually a light orange,

226
00:41:40,811 --> 00:41:43,827
so that after using the oil it takes the hue we want.

227
00:41:44,027 --> 00:41:49,350
The reason for using orange is that I was looking for a color to paint the own and the central piece

228
00:41:49,550 --> 00:42:00,085
and then I saw this paper here with the letters and the orange hue of the ink I used on the papers.

229
00:42:00,285 --> 00:42:17,249
To make a contrast. I start painting the gears. Of course, the color is intense but later this will change.

230
00:42:17,449 --> 00:42:30,216
...with the oil. I don't paint it thoroughly, I leave some whites, we don't mind that.

231
00:42:30,416 --> 00:42:34,519
we just want an orange hue.

232
00:42:35,719 --> 00:42:39,933
I take care not to spoil the stamp effect. The same as previously with the gesso.

233
00:42:58,133 --> 00:43:03,954
it is not necessary to fully cover it with paint, we follow the same procedure we use a baby wipe.

234
00:43:08,154 --> 00:43:13,174
I clean both the tiny clock and the eye which says not glueing.

235
00:43:25,374 --> 00:43:37,374
Using a small quantity of thinned paint I brush the paint away to the bottom, like if it's flowing

236
00:43:42,574 --> 00:43:47,035
A very small quantity of painting , almost fading.

237
00:43:49,235 --> 00:44:00,372
We do this in order to create the flowing rust effect. However you need just a few paint.

238
00:44:00,373 --> 00:44:16,231
you may use the baby wipe to blur it or maybe around it

239
00:44:45,431 --> 00:44:59,210
you see that I make a shade around the owl. I brush and the I dab with the baby wipe.

240
00:45:04,410 --> 00:45:09,953
As soon as it blends with dark brown, it will give us the rust effect.

241
00:45:12,153 --> 00:45:14,923
Don't use much paint, that's the most significant.

242
00:45:20,123 --> 00:45:25,288
We finished with this color. A good way to put the remaining paint back in the bottle, it's this one.

243
00:45:25,289 --> 00:45:31,101
You sweep the paint against the foil with the bottle

244
00:45:35,301 --> 00:45:37,309
Let's dry it.

245
00:46:14,000 --> 00:46:21,150
Here we are again. I've made some exrea paper decorations from the "Tim Holtz" lline.

246
00:46:21,151 --> 00:46:34,012
I scraped them, used the ink to make them look old and before glossing, let's put them on their spot

247
00:46:34,212 --> 00:46:41,646
We may glue them using Extra Forte by "Stamperia" it's something like hobby glue

248
00:46:41,846 --> 00:46:50,439
Just a little, and attach them on the spot we want. we are careful not to cover the line here.

249
00:46:50,639 --> 00:47:03,559
these too. if we apply much glue the paper will crinkle.

250
00:47:15,759 --> 00:47:20,320
Let's apply water gloss to the whole project.

251
00:47:21,400 --> 00:47:25,126
This will allow us to use the oils without any further problems

252
00:47:25,326 --> 00:47:33,350
We apply gloss so as to let the oil glide easily plus the fact that the oil will be absorbed by the papers

253
00:47:33,550 --> 00:47:40,400
The gloss help us to wipe the oil out using baby wipes and leavy nothing on the papers.

254
00:47:41,600 --> 00:47:50,699
Take care to apply gloss with straight brushes of the same direction as the motive.

255
00:47:52,899 --> 00:48:01,992
The gloss should be closely and carefully applied for the oil to follow and thus avoid a nasty look.

256
00:48:10,900 --> 00:48:11,949
I apply over the paper too.

257
00:48:14,000 --> 00:48:22,389
However if the paper crinkles, it needs a day or two to rest and it returns to its original form.

258
00:49:22,000 --> 00:49:27,763
We glossed it, we dried it, now we will apply the oil and see what happens.

259
00:49:28,963 --> 00:49:37,140
We will use "Van Gogh" oil  code 409. Classic, I dare say burnt umber.

260
00:49:38,340 --> 00:49:46,414
And now let's see in what color our project will end up.I hope the result will be good.

261
00:49:46,614 --> 00:49:48,524
The truth is that I don't know what result to expect, after using this orange color.

262
00:49:48,724 --> 00:49:50,352
I hope that it lets us satisfied.

263
00:49:50,552 --> 00:50:09,890
Using a strong pig hair brush take a small quantity of oil, just on the brush tip

264
00:50:09,891 --> 00:50:22,965
and start covering the project with oil. I start from the owl and continue to the rest.

265
00:50:28,165 --> 00:50:36,202
I just apply the oil thoroughly  "draining " the oil out of the brush.

266
00:50:36,402 --> 00:50:43,361
I don't want it to be thick, I want to cover it all but not "overdose" it.

267
00:50:48,561 --> 00:50:55,241
for the beginning I'll cover the own and the central design.

268
00:51:11,441 --> 00:51:20,340
During the time I was oil painting the owl and the center, I thought, it would be better

269
00:51:20,540 --> 00:51:28,890
to apply the oil to the whole surface. Thus we will avoid cleaning and then re applying oil

270
00:51:28,891 --> 00:51:37,060
and clean again possibly causing a mess. So I start covering with oil all the surface.

271
00:52:03,060 --> 00:52:12,376
Well, we applied oil to the whole surface, as well as , the clock sides

272
00:52:12,377 --> 00:52:17,230
I hadn't gloss the sides, I wanted the oil to be more penetrating there,

273
00:52:17,231 --> 00:52:21,839
to become darker and chromatically closer to the red paper.

274
00:52:22,039 --> 00:52:32,493
Let's start cleaning but little by little. We don't want to create an acute color.

275
00:52:32,693 --> 00:52:41,517
Here I will wipe stronger, cause I want the metal of the beak to reveal.

276
00:52:41,717 --> 00:53:00,142
And then I will smoothly clear the rest.

277
00:53:08,342 --> 00:53:14,117
Softly, you may lighten it as much as you want. Personally I don't want too much light.

278
00:53:14,317 --> 00:53:18,436
I don't want great differences between oranges.

279
00:53:18,636 --> 00:53:27,493
Now the rest surface. I reel the baby wipe in my hand and soft vertical moves I wipe out.

280
00:53:27,600 --> 00:53:32,886
Close to the nicks I leave it darker.

281
00:53:42,086 --> 00:53:46,370
I wipe it moving up and down only. I alternate the baby wipe side an continue.

282
00:53:50,000 --> 00:54:00,812
If at some point I want to add oil, I use a paper towel first to dry it and then I apply the oil.

283
00:54:13,800 --> 00:54:19,026
The orange color doesn't give me much of impression, we'll see in the end

284
00:54:27,226 --> 00:54:31,644
Let's oil the papers and see next , what changes can be made.

285
00:54:36,844 --> 00:54:42,170
Under the same reasoning I apply the oil on the papers, as we did for the other half of the clock

286
00:55:01,171 --> 00:55:06,160
Well this one here is very orange.We need to find a way to change it.

287
00:55:06,360 --> 00:55:17,710
Maybe... we will use a bit of turquoise and see what's next. 

288
00:55:17,711 --> 00:55:28,235
We will improvise,I don't know what the result will be. I only see that the orange is too intense.

289
00:55:28,435 --> 00:55:35,514
We are going to mix another color to check what we can do, even over the oil.

290
00:55:36,714 --> 00:55:38,618
I don't mind let's see!

291
00:55:48,619 --> 00:55:54,633
pick some color and dab...

292
00:56:04,000 --> 00:56:09,773
I know that I've put oil and I can't paint with acrylic over it but why not

293
00:56:09,973 --> 00:56:16,220
The result looks very nice. By accident though. 

294
00:56:16,420 --> 00:56:24,400
I've told you before, to experiment, try new things,don't get stuck. 

295
00:56:26,401 --> 00:56:29,970
Take materials and use them on your projects

296
00:56:31,170 --> 00:56:36,834
You see, one with my hand, one with my brush, using the baby wipe. There are no rules.

297
00:56:42,034 --> 00:56:48,789
The sure thing is that using the turquoise color hadn't been programmed but we had to.

298
00:56:56,720 --> 00:57:04,436
With almost lack of color I create a similar shade around the owl.

299
00:57:17,636 --> 00:57:20,270
The shades are created when there is no colour on the brush.

300
00:57:23,470 --> 00:57:27,035
Also a piece of paper right here for the eye.

301
00:57:27,235 --> 00:57:53,859
I'm looking for a way to combine the effects on the clock, maybe by re adjusting them.

302
00:58:15,600 --> 00:58:31,423
Using the turquoise color, always small quantity, I mix and match the project with my brush and hand.

303
00:58:43,623 --> 00:58:57,700
 will add some decorations. I've found them in various stores. 

304
00:58:57,701 --> 00:59:09,284
Let's screw them and see what else can be done

305
00:59:41,484 --> 00:59:48,570
I put the decorations. The first one is like the owl is sitting on it. 

306
00:59:48,571 --> 00:59:54,679
and the second is as if the two pieces are latched..

307
00:59:54,879 --> 01:00:04,391
For extra decoration, let's add some small screws on the paper edges .

308
01:00:07,591 --> 01:00:08,050
They are all just ideas, but those ideas make the difference on our project.

309
01:00:20,160 --> 01:00:26,624
As I look on the project right now, I realise that the orange color was very intense 

310
01:00:26,824 --> 01:00:31,365
and it needed one or two tones darker orange color.

311
01:00:33,190 --> 01:00:38,640
We somehow corrected it with the turqoise, we applied turqoise to some other points as well

312
01:00:38,840 --> 01:00:47,560
Totally seing the project, it's not a hundred percent of what I really want

313
01:00:47,760 --> 01:01:04,625
So I'll try by adding some white or dark red color on the owl to correct it.

314
01:01:07,220 --> 01:01:14,787
See? A small detail make the difference

315
01:01:16,550 --> 01:01:26,898
We finished with the screws, now we are going to add something else

316
01:01:26,899 --> 01:01:28,310
Where are our boxes?

317
01:01:33,510 --> 01:01:41,423
We will add decorative copper nails. We used them on our very first project

318
01:01:41,623 --> 01:01:54,886
We wiil add them now. Let's hope they don't penetrate the wood. They do !!!

319
01:02:29,086 --> 01:02:45,565
I cover here, I pick some oil.

320
01:03:03,600 --> 01:03:08,353
I want to lighten some points, it might help with the combinations.

321
01:03:21,900 --> 01:03:26,384
Almost without any oil on my brush

322
01:04:03,280 --> 01:04:15,278
We used white oil almost fading just to correct the colors.Not everywhere, at some points.

323
01:04:22,478 --> 01:04:42,073
Now using black oil, a very small ammount, and the same brush, we will emphasize some lines

324
01:04:44,273 --> 01:04:55,376
This is the final stage, we distinguish some lines, I  continuously pick some oil

325
01:05:29,576 --> 01:05:32,066
Put a little at the joints.

326
01:05:36,266 --> 01:05:37,894
A little bit around

327
01:05:41,094 --> 01:05:53,764
And the last one, a little emphasis at some points of the gears.

328
01:06:16,200 --> 01:06:24,657
For the final effect, some white splashes, I could do them with gesso too. 

329
01:06:27,770 --> 01:06:50,736
Some quantity of gesso and some water

330
01:07:20,000 --> 01:07:26,022
Well, we have a mess here, as always. 

331
01:07:28,080 --> 01:07:32,937
This is our project for today. I'd like to thank you, once more, for being here, ﻿

332
01:07:33,137 --> 01:07:35,022
the <b>handmadefantasyworld.com.</b> 

333
01:07:35,222 --> 01:07:43,098
I hope you had a good time. Scrabooking papers, stamps by "docraft", "Tim Holtz" inks

334
01:07:43,298 --> 01:07:51,255
woodcarving,oils. ageing, lots of colors mixes,

335
01:07:51,455 --> 01:07:58,074
I wouldn't miss to show you a clock  project from the first month, clocks are my fetish.

336
01:07:58,274 --> 01:08:04,410
thank you very much, see you soon. I'll be waiting you till next time. Bye

