1
00:00:09,475 --> 00:00:13,490
Dobro veče!

2
00:00:13,492 --> 00:00:18,670
Ne znam koje je vreme kod vas.

3
00:00:18,671 --> 00:00:19,743
Kod mene je sada veče!

4
00:00:19,943 --> 00:00:28,570
Napravićemo steampunk -  vintage projekat.

5
00:00:28,571 --> 00:00:32,104
Koristićemo Cadence šablon.

6
00:00:32,304 --> 00:00:40,570
Radićemo na ovoj kutiji…dobio sam je u Turskoj.

7
00:00:42,000 --> 00:00:51,790
Od prijatelja Filiza I Ohrana.

8
00:00:51,990 --> 00:01:00,760
Ovu kutiju sam dobio kada sam bio gost u njihovoj prodavnici.

9
00:01:00,761 --> 00:01:05,210
Hajde da počnemo.


10
00:01:10,350 --> 00:01:20,100
Koristićemo Cadence šablon sa steampunk leptirom, mehanizmima I satom. 

11
00:01:22,300 --> 00:01:29,900
Prekrićemo šablonom deo poklopca, ali ćemo ostaviti mesto I za još neke detalje.

12
00:01:29,901 --> 00:01:39,130
Koristićemo Heavy body pastu Stamperia preko šablona.

13
00:01:39,131 --> 00:01:55,470
Ona je čvršća I daje bolji 3D efekat.

14
00:01:55,471 --> 00:02:04,880
Pastu uzimamo špatulom I nanosimo držeći špatulu pod uglom. Kratkim I brzim pokretima.

15
00:02:05,080 --> 00:02:09,040
Lično ne koristim lepak za šablone.

16
00:02:09,041 --> 00:02:13,020
Pastu sad nanosim čak I bez držanja šablona. 

17
00:02:13,220 --> 00:02:15,040
Držite špatulu pod uglom.

18
00:02:15,041 --> 00:02:19,130
Šablon se ne pomera, nije problem naneti pastu.

19
00:02:19,330 --> 00:02:27,570
Nastavljam sa nanošenjem, menjajuči pravac nanošenja.

20
00:02:27,571 --> 00:02:31,230
To radim kako bi bio siguran da sam svuda popunio šablon.

21
00:02:32,430 --> 00:02:39,290
Preskočiću ovaj deo jer mi se ne uklapa u projekat.

22
00:02:39,490 --> 00:02:52,990
Kad predjemo šablon, dodajemo još paste d apojačamo 3D efekat.

23
00:03:02,000 --> 00:03:05,400
Svuda sam naneo pastu.

24
00:03:05,401 --> 00:03:15,900
Skidamo šablon….vrlo lepo!

25
00:03:16,100 --> 00:03:33,590
Sa šablona možemo skupiti višak paste I vratiti u kutiju. I sada sušimo…

26
00:03:33,591 --> 00:03:39,100
Koristim dopao vazduh sa umerene razdaljine.

27
00:03:39,101 --> 00:03:45,890
Ostavite pastu malo da se ohladi na sobnoj temperature, hlađenjem se pasta steže.

28
00:05:23,020 --> 00:05:31,270
Sada je suvo.

29
00:05:31,271 --> 00:05:38,600
U medjuvremenu sam pripremio neke točkiće I detalje.

30
00:05:38,601 --> 00:05:42,200
Doneo sam ih iz Rusije, izradjeni su od kartona.

31
00:05:42,400 --> 00:05:50,580
Ne znam naziv firme.

32
00:05:50,581 --> 00:06:01,570
Koristim motiv sata I neke točkiće tačno gde su oni pozicionirani I na šablonu.

33
00:06:01,571 --> 00:06:10,990
Na centar stavljam sat, a na mehanizme od paste stavljam mehanizme od kartona.

34
00:06:11,190 --> 00:06:18,730
Na radionicama sam video da žene nekad nasumice ređaju točkiće.

35
00:06:18,930 --> 00:06:23,730
Mislim da je važno gde će točkići biti postavljeni I insistiram da se prati šablon,

36
00:06:23,930 --> 00:06:32,280
Jer je to važno zbog kompozicije.

37
00:06:32,281 --> 00:06:38,110
Kada sam odredio poziciju

38
00:06:38,310 --> 00:06:46,670
Koristim Gloss  gel pastu Stamperia da zalepim elemente.

39
00:06:48,340 --> 00:06:58,120
Ova providna pasta je odličan lepak, takođe.

40
00:06:58,320 --> 00:07:13,620
Stavljamo malu količinu na elemente I lepimo ih.

41
00:08:12,500 --> 00:08:21,860
Nakon lepljenja, tvrdom četkicom utapkamo višak lepka.

42
00:08:21,861 --> 00:08:34,020
Tapkamo smao po elementima kako bi postigli teksturu.

43
00:08:34,021 --> 00:08:46,320
Isto radimo sa ostalim elementima.

44
00:08:48,520 --> 00:08:51,870
Negde manje, negde više.

45
00:09:05,070 --> 00:09:08,280
Lepo! Stavljamo četkicu u vodu.

46
00:09:13,700 --> 00:09:18,930
Ipak će mi trebati ponovo četkica…

47
00:09:19,810 --> 00:09:38,580
Dodajemo metalni lančić…trebaće nam klešta. Izmerimo koliko nam je potrebno.

48
00:09:50,780 --> 00:09:58,800
Sečemo koliko nam lanca treba.

49
00:09:59,000 --> 00:10:12,620
Na isti način lepimo I lanac. Nanosimo gel pastu prateći motiv sa šablona.

50
00:10:16,000 --> 00:10:34,590
Ovde ima previse paste I izgubiće se lepota lanca.

51
00:10:34,591 --> 00:10:42,350
Lanac svakako farbamo akrilnom bojom. 

52
00:10:42,550 --> 00:10:53,100
Dodaćemo još paste.

53
00:10:53,300 --> 00:10:57,900
Na kraju se skoro neće ni videte….

54
00:11:07,100 --> 00:11:20,000
Dodajem pastu kako bi se lanac zalepio.

55
00:11:25,200 --> 00:11:31,260
Na isti način lepimo I lančić iznad.

56
00:11:46,000 --> 00:11:49,880
Ovde ćemo iseći lanac.

57
00:11:53,000 --> 00:12:00,760
Nanosimo pastu.

58
00:12:12,000 --> 00:12:15,130
Dobro je!

59
00:12:17,330 --> 00:12:24,700
Sada stavljamo četkicu u vodu I čistimo ruke.

60
00:12:24,900 --> 00:12:33,900
Sada sušimo, sa umerene razdaljine toplim vazduhom. 

61
00:12:50,800 --> 00:13:00,185
Sada smo sve zalepili I osušili.

62
00:13:00,186 --> 00:13:07,749
Koristimo Paco art Prajmer na lanac.

63
00:13:08,274 --> 00:13:12,837
Paco art je razvio svoju liniju hobby proizvoda.

64
00:13:13,037 --> 00:13:17,276
Koristićemo ih u projektima, takodje.

65
00:13:17,476 --> 00:13:22,562
Nanosimo samo na lančić.

66
00:13:22,762 --> 00:13:28,880
Nema potrebe nanositi na elemente od kartona.

67
00:13:29,080 --> 00:13:30,541
Te elemente ćemo svakako bojite akrilnom bojom.

68
00:13:33,334 --> 00:13:41,112
Samo malo prajmera kako bi se metal lakše ofarbao.

69
00:13:41,312 --> 00:13:46,528
Malo prosušimo.

70
00:14:06,868 --> 00:14:21,577
Gornji deo kutije bojimo Amber matt style akrilnom bojom firme Cadence.

71
00:14:28,136 --> 00:14:34,275
Kada koristim njihove boje, uvek malo pokvasim četkicu jer je boja dosta gusta .

72
00:14:36,301 --> 00:14:48,699
Sve farbamo ovom bojom, šablon, točkiće, lanac.

73
00:14:48,899 --> 00:14:52,751
Pokreti četkom su nam u jednom pravcu, povlačimo gore-dole.

74
00:14:52,951 --> 00:15:07,013
Svaka  greška u nanošenju boje će doći do izražaja kada nanesemo uljanu boju.

75
00:15:07,213 --> 00:15:09,540
Uljana boja sve otkriva. Moramo biti pažljivi.

76
00:15:09,553 --> 00:15:17,365
Tapkamo samo u udubljenjima. Na ostalim delovima su nam dugi potezi.

77
00:15:17,565 --> 00:15:20,351
Dobro obojite sve delove.

78
00:15:43,000 --> 00:15:50,322
Ovde se vidi tekstura paste. Bojimo I metalne uglove.

79
00:15:51,522 --> 00:15:56,477
Nije važno da li ćemo uglove dobro obojiti

80
00:15:56,677 --> 00:16:01,046
Scakako želimo da kutija izgleda staro.

81
00:16:07,246 --> 00:16:09,355
Ovde dobro utapkamo.

82
00:17:16,589 --> 00:17:22,083
Obojili smo poklopac I sada nastavljamo sa ostatkom kutije.

83
00:17:22,084 --> 00:17:25,131
Bojimo sve metalne delove takodje.

84
00:17:26,331 --> 00:17:34,026
Sada ćemo koristiti Stamperia Allegro akrilnu boju.

85
00:17:34,027 --> 00:17:40,577
Blue Avio KAL42

86
00:17:40,777 --> 00:17:47,834
Bojimo ostatak.

87
00:18:23,224 --> 00:18:30,087
Završili smo sa bojenjem. Sušimo I nastavljamo sa uljanim bojama.

88
00:18:54,465 --> 00:19:04,051
Sada treba da odlučimo kako ćemo naneti ulje. Postoje 2 načina.

89
00:19:04,251 --> 00:19:09,814
Možemo naneti direktno na akrilnu boju, bez lakiranja.

90
00:19:10,014 --> 00:19:16,748
Predmet u tom slučaju ostaje tamniji I boja se teže skida.

91
00:19:16,948 --> 00:19:28,268
Drugi način je da prvo dobro prelakiramo kutiju.

92
00:19:28,468 --> 00:19:33,397
Kada se lakira, void se računa jer će se svaki potez  videte.
93
00:19:33,597 --> 00:19:38,543
Posle lakiranja, uljana boja se lakše skida I može se postići svetliji efekat.

94
00:19:38,544 --> 00:19:44,983
Mi ćemo lakirati. Koristimo sjajni lak.

95
00:19:44,984 --> 00:19:52,580
Ovo je danski Sherwing lak.

96
00:19:52,780 --> 00:20:00,418
Možete ga poručiti preko sajta www. Pacoartcenter.gr

97
00:20:00,618 --> 00:20:15,080
Koristimo meku četkicu I nanosimo sve u istom pravcu.

98
00:20:15,280 --> 00:20:20,674
Jako je važno – dugi potezi, od ivice do ivice.

99
00:21:38,204 --> 00:21:44,192
Lakiramo I sušimo. Ovo je lak na vodenoj bazi. 

100
00:21:44,392 --> 00:21:49,044
I dobra je osnova za uljanu boju.

101
00:21:49,244 --> 00:21:59,105
Dobro osušimo lak I nanosimo uljanu boju. Kao što već znate,

102
00:21:59,305 --> 00:22:06,832
Najviše volim "Van Gogh" uljane boje... 

103
00:22:10,032 --> 00:22:20,034
Ovaj put koristimo "Van gogh" Raw Umber uljanu boju. Nanosimo tvrdom četkicom.

104
00:22:20,536 --> 00:22:36,226
Uzimamo malu količinu I nanosimu na celu  kutiju. Gde je potrebno, tapkamo četkicom boju. 

105
00:22:36,426 --> 00:22:43,106
Nigde ne sme da se vidi žuta boja. Koristimo tj razmazujemo svu boju koja je u četkici.

106
00:22:43,306 --> 00:22:45,700
Lakše se uljana boja skida kada je nema previše na površini predmeta.

107
00:22:45,900 --> 00:22:47,495
Brzo ću…

108
00:23:25,500 --> 00:23:34,801
Gornja strana je gotova. Uglavnom radim stranu po stranu.

109
00:23:35,001 --> 00:23:46,305
Sam sada naneo uljanu boju na dve strane.  Obratite pažnju kako skidam boju sa točkića.

110
00:23:46,306 --> 00:23:48,807
Kao I kako skidam sa ravne površine.

111
00:23:48,809 --> 00:23:54,331
Kada nanosimo uljanu boju, nisu nam važni potezi četke.

112
00:23:54,531 --> 00:23:56,836
Nanosimo kako nam je zgodno. Skidanje uljane boje je mnogo važnije.

113
00:23:57,036 --> 00:24:03,298
Za to koristim moj omiljeni alat – vlažne maramice. Prvu uglavnom bacim,

114
00:24:03,299 --> 00:24:05,750
najčešće bude suva.

115
00:24:05,950 --> 00:24:12,897
Uzimam više maramica I pravim lopticu od njih I tako počinjem sa skidanjem uljane boje.

116
00:24:16,485 --> 00:24:21,103
Skidamo uljanu boju potezima u istom pravcu.

117
00:24:23,178 --> 00:24:26,865
Gore-dole, nikako drugačije.

118
00:24:27,065 --> 00:24:38,472
Kako se približavam uglovima, sa idejom da kutija izgleda što starije, 

119
00:24:38,672 --> 00:24:44,123
Potezi su mi nežniji I na kutiji ostaje više uljane boje.

120
00:24:44,776 --> 00:24:50,375
Kao što vidite, potezi su samo gore-dole, nežno.

121
00:24:50,376 --> 00:24:53,311
Ovo je deo u kom treba posebno da uživate.

122
00:24:53,312 --> 00:25:02,212
Pazite, leptir I točkići treba da ostanu tamniji.

123
00:25:04,412 --> 00:25:11,761
Videite, kratki potezi u uvek pratite pravac.

124
00:25:24,000 --> 00:25:30,972
Trudim se da se vidi što manje linija kada skidam uljanu boju.

125
00:25:35,500 --> 00:25:42,609
Kod točkića menjan način skidanja ulja. Pravim manju lopticu od vlažnih maramica.

126
00:25:42,610 --> 00:25:54,994
Skidam malo po malo boje, vodeći računa da ostane negde više boje.

127
00:25:55,194 --> 00:26:05,215
Negde je lakše prstom I maramicom istači neke tačke.

128
00:26:05,415 --> 00:26:16,323
Ne znam da li vidite kako je gel pasta napravila 3D efekat.

129
00:26:16,523 --> 00:26:23,882
Ovde je bolje da tapkamo kako očistili ove uvučene delove.


130
00:26:24,082 --> 00:26:28,584
Deo po deo, čistimo sve.

131
00:26:28,784 --> 00:26:33,455
Pažljivo, ne želimo potpuno da skinemo uljanu boju. Iako je možemo ponovo naneti,

132
00:26:33,456 --> 00:26:39,851
Bolje je bez ponavljanja, dobije se prirodniji efekat postarivanja.

133
00:26:39,852 --> 00:26:43,877
Dodavanjem I skidanjem ulja efekat se menja.

134
00:26:44,077 --> 00:26:54,974
Neke delove ostavljam da budu tamniji. Isto ću uraditi I ovde.

135
00:27:02,035 --> 00:27:07,931
Jedan ugao sam očistio više od drugog, da ne izgedaju isto.

136
00:27:13,508 --> 00:27:18,121
Lepo. SKidamo uljanu boju sa druge strane.

137
00:27:18,321 --> 00:27:25,147
Na stranama radimo drugačije, pravimo veću loptu od vlažnih maramica, zbog veličine površine.

138
00:27:25,347 --> 00:27:33,255
Što je veća površina, to je potrebna veća loptica. Brišemo sa leva na desno I obrnuto.

139
00:27:33,256 --> 00:27:40,660
Krećemo od centra I idemo ka jednoj pa ka drugoj strani.

140
00:27:40,860 --> 00:27:48,937
Centar  će biti svetliji a uglovi tamniji.

141
00:27:48,938 --> 00:27:52,701
Sada ćemo malo da ubrzamo.

142
00:29:04,730 --> 00:29:11,552
Gotov sam sa uljanom bojom I možete videte rezultat.

143
00:29:11,752 --> 00:29:20,060
Pažljivo diramo predmet dok je uljana boja sveža, jer ostaju tragovi prstiju.

144
00:29:20,260 --> 00:29:29,801
Kao što vidite, uglovi su tamniji, udubljenja I metalni elementi.

145
00:29:30,001 --> 00:29:36,301
Sada ćemo drugom uljanom bojom napraviti rust efekat.

146
00:29:36,501 --> 00:29:44,395
Proverite da li su ostali tragovi prstiju po kutiji prvo.

147
00:29:45,029 --> 00:30:02,304
Malom tvrdom četkicom dodajemo Pebeo uljanu boju Venetian Zellow Orange . Stavljamo jako malo.

148
00:30:05,340 --> 00:30:23,548
Nanosimo uljanu boju četkicom I tapkamo vlažnom maramicom, tako da boja izgleda kao puder.

149
00:30:23,748 --> 00:30:32,798
Ne znam da li dobro videte.

150
00:30:32,998 --> 00:30:45,503
Naročito kada dve boje izgledaju isto. Ne možete dobro da vidite.

151
00:30:45,504 --> 00:30:53,499
Potreban nam je Super zoom da dobro vidite.

152
00:30:53,699 --> 00:31:01,925
Malo uljane boje I tapkamo maramicom.

153
00:31:01,926 --> 00:31:16,180
To je detalj koji daje efekat rdje.

154
00:31:16,380 --> 00:31:22,558
Prva uljana boja je dosta tamna I narandžasta nije tako upadljiva.

155
00:31:22,758 --> 00:31:29,499
Na nekoj drugoj boji bi se bolje videla.

156
00:31:29,699 --> 00:31:34,498
Ne previse, tek ponegde.

157
00:31:35,698 --> 00:31:49,168
Tapkajte, ne plašite se. Vlažnom maramicom sve možemo da skinemo.

158
00:31:58,815 --> 00:32:08,569
Dodajemo na ivice, na isti način.

159
00:32:11,822 --> 00:32:14,196
Još malo….

160
00:32:31,439 --> 00:32:39,829
Voleo bih da ste ovde da bolje vidite..jako lep efekat.

161
00:32:40,029 --> 00:32:44,326
Mislim da preko ekrana to ne možete dobro da vidite.

162
00:32:44,526 --> 00:32:47,694
Da vidimo šta smo napravili….dodaćemo I na plavu…

163
00:32:48,666 --> 00:32:59,381
Isti postupak. Vidite razliku?

164
00:33:15,821 --> 00:33:26,887
Vidite kako ovo izgleda?

165
00:33:27,087 --> 00:33:37,936
Ovde pravimo efekat kao da je rdja curela. Čistimo maramicama.

166
00:33:43,895 --> 00:33:47,929
Malo rdje sa leve strane….

167
00:33:51,129 --> 00:33:52,456
Malo na drugoj strain…

168
00:34:24,024 --> 00:34:32,551
Moji prijatelji sa kojima ovo snimam mi se sad smeju…nadam se da su dobro snimili ovaj efekat!

169
00:34:32,751 --> 00:34:41,573
Ovo je steampunk ali na moj način. Ja ne volim metalik boje,iako ih većina koristi.

170
00:34:41,773 --> 00:34:47,728
Ja koristrim mat boje.

171
00:34:47,928 --> 00:34:55,719
Iako je lak bio sjajan, posle uljane boje je I on postao mat.

172
00:34:55,919 --> 00:35:00,551
Jako mi se sviđa I ovaj šablon.

173
00:35:00,751 --> 00:35:07,057
Koristili smo razne brendove, Stamperia, Cadence, Paco,Van Gogh,Pebeo…

174
00:35:07,257 --> 00:35:09,785
To je to za danas. Nadam se da Vam se dopalo.

175
00:35:09,985 --> 00:35:14,314
Nadam se da se zabavljate sa nama. Hvala Vam!

176
00:35:14,514 --> 00:35:17,349
Vidimo se uskoro!
www.handmadefantasyworld.com