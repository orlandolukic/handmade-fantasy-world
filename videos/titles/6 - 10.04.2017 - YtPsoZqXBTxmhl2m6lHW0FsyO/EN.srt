1
00:00:05,932 --> 00:00:11,379
Good morning again, from Belgrade. We are at handmadefantasyworld.com

2
00:00:11,579 --> 00:00:17,397
This is the 2nd project of month April.

3
00:00:17,597 --> 00:00:25,013
We will use an MTF base for our mix media project, it will have the use of a canvas.

4
00:00:25,213 --> 00:00:29,996
The truth is, I don't know what the exact result will be. There are many materials that may be used.

5
00:00:30,196 --> 00:00:44,501
I have some stencils and inks and distress stains by "Tim Holtz", some mediums, pastes, stamps

6
00:00:44,701 --> 00:00:50,310
of different companies. Which means that I cannot determine right now the materials of the project.

7
00:00:50,510 --> 00:00:56,830
We'll decide that on the process. We take an idea and create together and check on the final result.

8
00:00:57,030 --> 00:01:04,875
We'll also use some pages from, my friend's Boca, an old Serbian book.

9
00:01:05,075 --> 00:01:08,776
And we'll see what we can do with it. Right? Let's begin!

10
00:01:09,699 --> 00:01:16,878
Now, here we go!  We'll paint our MTF surface. I personally don't use a primer

11
00:01:17,078 --> 00:01:26,097
We'll use two acrylic paints from the "Americana" line by "DecoArt". The victorian blue and the plum.

12
00:01:26,297 --> 00:01:30,883
Let's mix the paints to create our background.

13
00:01:34,083 --> 00:01:36,493
I use a soft painting brush.

14
00:01:39,452 --> 00:01:41,320
So let's paint the surface.

15
00:01:53,645 --> 00:01:57,887
You may scrub your brush on the surface so as let the color spread

16
00:01:58,087 --> 00:02:02,474
and then keep on applying it with up and down brushings.

17
00:02:14,714 --> 00:02:18,130
Now, I'm going paint the lower half side, using the second color.

18
00:02:32,498 --> 00:02:55,845
In the center, we will use a well wet baby wipe to make the two color blending.

19
00:02:56,045 --> 00:03:03,944
Then we'll pick some extra plum color and brush it to the bottom.

20
00:03:11,207 --> 00:03:21,262
With our hand, making gentle moves up and down we blend the color paints.

21
00:03:22,462 --> 00:03:27,414
There many ways to blend colors, today, I show you this one.

22
00:03:32,485 --> 00:03:38,967
Now, I remove the blue color that is way too much to the upper half side.

23
00:03:39,167 --> 00:03:44,532
If it happens that any paint is removed, I re apply some using the brush.

24
00:03:57,183 --> 00:03:59,080
We need to wipe out some paint down here too.

25
00:04:05,730 --> 00:04:23,556
Nice. Well, I think that I should increase the blue color on this side and spread it like this.

26
00:04:34,615 --> 00:04:35,868
Let's dry it now!

27
00:04:46,114 --> 00:04:53,206
Our background is ready. We don't mind if there are some points not well painted.

28
00:04:53,406 --> 00:04:59,788
We create this as a starting point and much of this surface will be covered by other materials.

29
00:04:59,988 --> 00:05:06,874
We will use some stencil and an interesting technique. We won't create anything in the center,

30
00:05:07,074 --> 00:05:09,175
because, here, we are going to use some papers.

31
00:05:09,375 --> 00:05:21,790
We'll use these two stencils by "Tim Holtz". The numbers and these stains of paint.

32
00:05:21,990 --> 00:05:29,266
We'll use a light modeling paste by "Liquitex"

33
00:05:29,466 --> 00:05:42,735
I'll just decide the point where these stencils will be used at. Here, I choose these two digits.

34
00:06:55,878 --> 00:07:01,584
As you can see, I put the stencil right on the paste which is still fresh without the need of drying it.

35
00:07:01,784 --> 00:07:07,128
If you let it gently, you won't face any problem. Let alone, the technique we are going to apply now

36
00:07:10,039 --> 00:07:14,707
See? it hasn't been destroyed. Maybe some paste right here,

37
00:07:14,907 --> 00:07:19,191
but especially regarding the today's project, we don't mind at all.

38
00:07:28,977 --> 00:07:30,758
Now this one...

39
00:07:51,753 --> 00:07:52,667
Very nice!

40
00:08:22,716 --> 00:08:25,503
... and we dry.

41
00:08:32,678 --> 00:08:45,012
Now we'll paint all the surface using the ice blue color from the "Americana" line by "DecoArt"

42
00:08:53,069 --> 00:08:56,831
We pick up some paint and we paint the whole surface.

43
00:09:10,943 --> 00:09:15,688
As you can see the acrylic paints are of incredible quality.

44
00:09:16,291 --> 00:09:20,213
Using a small quantity and making long brushes, we are able to paint with great ease.

45
00:09:33,692 --> 00:09:39,965
After we are done painting the surface, we dry.

46
00:09:48,352 --> 00:09:52,633
Now, I'm gonna show you the technique I mentioned about, before.

47
00:09:52,833 --> 00:09:56,574
We've applied the paste, we created the background as well,

48
00:09:56,774 --> 00:09:59,851
it would be better the background color to contrast with the the top coat one,

49
00:10:00,051 --> 00:10:03,859
using a spatula, we'll remove the paste.

50
00:10:04,059 --> 00:10:20,400
The stencil paste. The background color will be revealed and an ageing effect hue as well.

51
00:10:31,889 --> 00:10:39,214
The stencil is neither 3D (paste effect), nor smooth (paint effect). It's as if it is engraved.

52
00:11:19,705 --> 00:11:27,731
Well, this is the result. If you were able to see it from close, you would see that it looks aged,

53
00:11:27,931 --> 00:11:34,327
it is also repousse and the color jumps out.

54
00:11:36,026 --> 00:11:40,211
Now it's time for us to create the main theme.

55
00:11:40,411 --> 00:11:50,022
We'll use the old book, I told you about at the beggining. It is old paged too.

56
00:11:50,222 --> 00:12:06,096
We'll cut some pages off and we'll scissor the embossed side.

57
00:12:19,903 --> 00:12:24,816
We'll attach the pages so as to create a background.

58
00:12:24,817 --> 00:12:34,595
We'll use the "Dina Wakley Media Gel Medium" by "Ranger", to glue the pages on the wood.

59
00:12:43,576 --> 00:12:57,045
I apply it on the wood and after attaching the page, I re apply gel on the page too.

60
00:12:58,700 --> 00:13:02,443
We'll create a page patchwork.

61
00:14:01,201 --> 00:14:07,547
After drying we'll create a background by adding some stamps.

62
00:14:07,747 --> 00:14:14,612
We'll use a permanent, black, archival ink by "Ranger"

63
00:14:14,812 --> 00:14:17,774
Let's put some stamps...

64
00:14:25,733 --> 00:14:34,746
This is a "Stamperia" stamp. It's a key design. We'll see what's the background will be like.

65
00:15:04,384 --> 00:15:13,494
I'll use some stamps and I'll add some designs on my project. This is a german company stamp,

66
00:15:13,694 --> 00:15:19,877
it's a bit difficult for me to pronounce, it's by "Heindesign"

67
00:15:40,766 --> 00:15:50,417
Should I spoil it a little? The edges are too intense. Using a baby wipe I'll make it more distress.

68
00:15:59,909 --> 00:16:01,827
This stamp is of the same company too.

69
00:16:17,613 --> 00:16:24,299
I'll use many and different stamps, some of which will be lost in the end

70
00:16:24,499 --> 00:16:35,956
but on the other hand, still I don't know what kind of result I expect, I'll add many to keep me satisfied.

71
00:16:36,156 --> 00:16:39,856
And see what the progress of the project will be.

72
00:17:03,105 --> 00:17:12,559
These stamps are the three out of the five, totally, designs of a "Tim Holtz" stamp line.

73
00:17:35,308 --> 00:17:39,802
We'll add some more designs.

74
00:17:42,048 --> 00:17:46,064
A stamp by "Prima marketing", it's been used in a previous project already.

75
00:17:54,922 --> 00:18:01,290
I'm not afraid to put one stamp over another. Actually it gives us a very nice result.

76
00:18:27,726 --> 00:18:31,535
And the crackeled one by "Stamperia".

77
00:18:48,832 --> 00:19:04,523
What else? This is a stamp that I don't remember where I got it from, but it's a very common design.

78
00:19:32,337 --> 00:19:37,209
I'll use a stamp with stripes. It's from the "Finnabair" line by "Prima marketing"

79
00:20:37,931 --> 00:20:44,387
We've created our background and put the stamps. I know that they look quite stirred,

80
00:20:44,587 --> 00:20:50,934
but as I've already said. We don't know yet, what the result will be so we add some things,

81
00:20:51,134 --> 00:20:53,159
which are possible to change during the procedure.

82
00:20:53,359 --> 00:21:01,583
We've used permanent ink stamps, so we won't face any problems, even if we apply fluid materials.

83
00:21:01,783 --> 00:21:08,254
So we are going to apply some paints on the old book pages.

84
00:21:08,454 --> 00:21:15,078
We'll use these three distress stain color paints by "Tim Holtz"

85
00:21:15,278 --> 00:21:19,621
I'll read you details, even though, you'll be able to find this information listed bellow this video.

86
00:21:19,821 --> 00:21:28,123
It's the Evergreen Bough, the Seedless Preserves and the Walnut Stain

87
00:21:28,323 --> 00:21:30,157
Excuse me about my pronunciation.

88
00:21:30,357 --> 00:21:51,948
We'll also need a water spray bottle.We spill some color, we water spray and use the hair dryer.

89
00:21:55,404 --> 00:22:00,806
We use the air to spread the paint.

90
00:22:02,164 --> 00:22:07,756
With a paper towel, you may dab any paint effect you don't like.

91
00:22:17,031 --> 00:22:24,858
Any time you use the water spray, you reactivate the color paint, so that you can control it better.

92
00:22:36,604 --> 00:22:39,575
We'll add some to another point as well.

93
00:22:56,524 --> 00:23:02,005
Now, we'll mix it with the purple color.

94
00:23:41,539 --> 00:23:44,302
We'll add some brown at the corners.

95
00:23:53,946 --> 00:23:59,155
With a paper towel I wipe any extra color, so as the stamps can be seen better.

96
00:24:06,609 --> 00:24:11,544
I use the brown color at some other points too, but mostly, at the corners.

97
00:24:52,268 --> 00:24:54,195
We collect any extra color paints.

98
00:25:16,168 --> 00:25:17,770
So, we are on to the next step.

99
00:25:28,906 --> 00:25:38,818
Since it's dry now, we'll add two more book papers, but in a different way.

100
00:25:39,018 --> 00:26:07,866
We'll apply some gel medium. Then we 'll attach the paper. Finally some gel medium on the paper too.

101
00:26:12,724 --> 00:26:23,828
Same thing here. But be careful as the stain colors are water based and as soon as we apply,

102
00:26:24,028 --> 00:26:30,580
the gel medium, the paint is reactivated and we might spoil our design.

103
00:26:30,780 --> 00:26:39,797
We pill off the paper edges a bit. We don't want this paper to be glued well.

104
00:26:40,674 --> 00:26:50,730
You see, the edges are not glued... then we dry!

105
00:26:54,667 --> 00:26:59,233
Now, we'll use the crackeled stencil by "Tim Holtz".

106
00:27:07,751 --> 00:27:13,848
And we'll adjust some crackeled stencil on the paper.

107
00:27:17,462 --> 00:27:22,273
Once again, we'll use the light modeling paste by "Liquitex"

108
00:27:22,473 --> 00:27:34,464
I cover the stencil, the design is not necessary to be perfect. As much paste as that.

109
00:27:43,664 --> 00:27:46,047
The same here as well.

110
00:28:00,350 --> 00:28:01,912
And... we dry...!

111
00:28:11,266 --> 00:28:18,288
We'll use again the plum acrylic color paint we used for the background.

112
00:28:20,531 --> 00:28:33,581
We'll take a small brush and we'll paint the papers and the stencil. Try not to spoil the rest.

113
00:28:48,720 --> 00:28:53,715
I don't fill the crackeled stencil gaps with paint. I want the paper to be seen within the cracks.

114
00:29:18,378 --> 00:29:20,950
And ... we dry!

115
00:29:28,310 --> 00:29:49,481
Let's make the paper look even more vintage. By peeling off and tearing the edges.

116
00:29:57,555 --> 00:30:07,354
Even if the paste goes off, just rub it with your hand and let the paper get revealed.

117
00:30:07,554 --> 00:30:09,517
Maybe with a baby wipe too.

118
00:30:22,857 --> 00:30:26,418
Let's unmake some other points as well.

119
00:30:35,610 --> 00:30:41,208
We might not be able to see some of these papers, as they wil be covered by other materials.

120
00:30:53,742 --> 00:31:02,052
As you see, I've lready prepared a patchwork involving some scrabooking papers by "Tim Holtz"

121
00:31:02,252 --> 00:31:10,171
There are very nice papers. I've scissored some of them and made this patchwork which I'll glue now.

122
00:31:10,371 --> 00:31:22,619
I'll remove the papers and I'll try to remember their position. I'll remove the top ones alltogether.

123
00:31:25,921 --> 00:31:42,312
We'll use the gel medium again to glue the pieces, applying glue on the paper.

124
00:31:42,512 --> 00:31:50,017
We don't mind, If the paper edges are upward. It's gives our project a 3D effect.

125
00:32:18,775 --> 00:32:25,734
I don't care about any gel medium found on the paoer, as I'll apply some over the papers anyway.

126
00:32:45,595 --> 00:32:52,095
As long as the gel medium is wet, we may readjust the positon of the papers.

127
00:32:53,867 --> 00:33:00,488
After doing that, we cover the papers with gel medium.

128
00:33:11,037 --> 00:33:19,979
After drying the gel medium, we'll apply some oil on the project.

129
00:33:20,179 --> 00:33:27,327
If we had used a water based gloss on the papers, we would be able to protect them more,

130
00:33:27,527 --> 00:33:33,401
after wiping the dark brown oil with baby wipes the papers would be more distinct.

131
00:33:33,601 --> 00:33:39,986
This time I used gel medium. After applying the oil, I've seen that the paper gets darker.

132
00:33:40,186 --> 00:33:49,396
I don't bother if the designs are lost, what I want is to create a leatherish effect.

133
00:33:49,596 --> 00:34:00,041
We'll use oil. As always, it will be the burnt umber by "Van Gogh".

134
00:34:06,408 --> 00:34:08,398
The oil will be used thick.

135
00:34:08,598 --> 00:34:18,070
We pick some oil and start covering the whole surface... over the papers and very well...

136
00:34:22,767 --> 00:34:24,428
... underneath the papers

137
00:34:34,201 --> 00:34:40,914
We don't need much of a quantity of oil, but we need to cover all the papers well.

138
00:34:56,320 --> 00:35:07,696
At this point I apply oil to the scrabooking papers and not the book pages we used before.

139
00:35:11,950 --> 00:35:15,104
Using baby wipe we'll remove the oil.

140
00:35:18,753 --> 00:35:23,630
The papers won't be totally cleaned. I don't mind that.

141
00:35:35,695 --> 00:35:39,343
As you can see the papers edges are upwards, which is something I like.

142
00:35:44,118 --> 00:35:49,323
Using a paper towel we collect the baby wipes' fluid.

143
00:35:52,801 --> 00:36:01,184
With my finger I apply some oil on the wood edges and create some shades. Thus I avoid,

144
00:36:01,384 --> 00:36:10,892
to darken the rest project. With my finger I spread the oil well so as to piece them all together.

145
00:36:16,948 --> 00:36:23,440
On the purple point, I apply some oil making it less intense.

146
00:36:23,640 --> 00:36:28,373
Using a baby wipe we can always change the hue of the paint.

147
00:36:48,308 --> 00:36:52,669
At some points I add more oil, at some other  even less.

148
00:36:56,539 --> 00:37:03,467
See? I add some here, and use the wide side of my finger to spread it.

149
00:37:18,088 --> 00:37:23,233
Now let's make some of the papers look even darker.

150
00:37:29,309 --> 00:37:37,220
It's good to create contrasts. They are helpful on highlighting the project layers.

151
00:37:37,420 --> 00:37:41,277
And always remember, the points underneath , the upward paper edges, should be dark.

152
00:37:53,711 --> 00:37:57,964
For the moment, this is the result. Let's go on and see what's next.

153
00:37:58,164 --> 00:38:12,200
it's time for the project's main theme. I'll use the back side of the project  as a desktop.

154
00:38:13,748 --> 00:38:20,751
I'll use stone paper by "Stamperia". I'll cut a piece.

155
00:38:20,951 --> 00:38:30,650
And I'll use a stencil scissored by me. Actually it's an Andy Skinner  stencil copy.

156
00:38:30,850 --> 00:38:37,047
I don't support such ideas. It would be better if we bought the original stencils.

157
00:38:37,247 --> 00:38:44,341
I just couldn't wait until he sends me the stencil and I wanted to use it. I made an irregularity.

158
00:38:53,712 --> 00:39:06,080
So since this stencil matches perfectly , let's do it. We'll use the light modeling paste by "Liquitex".

159
00:39:06,280 --> 00:39:10,325
Spatula... and go on covering the stencil.

160
00:39:12,785 --> 00:39:16,849
I repeat this stencil is from the "Andy Skinner" line.

161
00:39:20,890 --> 00:39:23,979
I try to use a thick layer.

162
00:39:28,331 --> 00:39:37,498
Before I remove the stencil I'm gonna cut the stone paper right on the stencil outline.

163
00:39:51,380 --> 00:39:55,824
This is something you could do at the beginning, I just forgot it.

164
00:39:59,697 --> 00:40:07,063
By the way, this is a scissors by "Docraft". They have an incredible scissors line. Good cutters.

165
00:40:07,263 --> 00:40:12,053
There are many categories, for different kinds of work.

166
00:40:12,253 --> 00:40:20,172
Then we remove the stencil and our design is ready.

167
00:40:23,109 --> 00:40:25,927
...  let's dry it.

168
00:40:28,640 --> 00:40:33,716
We've done drying it. Now, it's time to paint it. We'll use the victorian blue color by "Americana"

169
00:40:33,916 --> 00:40:42,292
since it's the color with which we created the background stencils.

170
00:40:42,492 --> 00:40:50,040
In order to have a sequence.

171
00:40:55,556 --> 00:41:03,458
Some color and let's gently paint it, as I'm not sure if the paste is well dried.

172
00:41:03,658 --> 00:41:13,603
I said it before again. These paints are incredible. The are "flowing" even on a paper.

173
00:41:14,857 --> 00:41:18,674
I squeese my brush, but I'm careful at the same time not to spoil the stencil,

174
00:41:18,675 --> 00:41:21,014
letting the paint cover the designs.

175
00:41:23,864 --> 00:41:27,098
I don't know what the result will be. We'll see it now.

176
00:41:27,298 --> 00:41:31,055
I hope, what I have in mind to turn out good.

177
00:41:33,943 --> 00:41:37,441
Well, when we finish painting it, we dry...

178
00:41:44,697 --> 00:41:51,986
Then we use an aqua color spray by "Stamperia". It's the leather color

179
00:41:52,186 --> 00:41:57,012
and spray the stencil to change its color.

180
00:41:59,173 --> 00:42:01,881
I spray some points and then I dry.

181
00:42:06,821 --> 00:42:20,210
Not completely dried, so as by using a baby wipe we create paint stains. If you can see it on camera

182
00:42:20,410 --> 00:42:28,383
the points which are still wet ,are easy to work on using a baby wipe.

183
00:42:28,583 --> 00:42:34,148
Better not  use a paper towel cause it's too harsh for the stencil paste.

184
00:42:37,050 --> 00:42:43,865
I'm not sure, whether you can see the result. You may use this material  in many projects. See the difference.

185
00:42:44,065 --> 00:42:46,230
Let's dry again.

186
00:42:50,982 --> 00:42:57,751
After that, we are going to use a new material which will make our stencil look more rusty.

187
00:42:57,951 --> 00:43:15,823
We'll use the Real Rust by "TEXCO". This packing consists of three bottles. The base coat, the spray bottle

188
00:43:16,023 --> 00:43:20,712
which gives us that yellowish color of rust and the greenish one which gives us the mold like effect.

189
00:43:20,912 --> 00:43:38,380
They are easy to use. We just spray the base coat and right away we use the rust color spray.

190
00:43:38,580 --> 00:43:46,456
We dry it. Even though it's best to let it self dry, we use the hairdryer to hasten the procedure.

191
00:43:46,656 --> 00:43:52,871
The more it dries the better the result. Tomorrow the result will be much more better.

192
00:43:53,723 --> 00:44:05,180
After drying, I decide to spray the base coat again and then the second color spray, where needed.

193
00:44:05,380 --> 00:44:09,647
What if I dried it in this way and let the rust leak?

194
00:44:09,847 --> 00:44:12,235
We have to be a bit patient with this, we'll be back soon.

195
00:44:13,939 --> 00:44:25,503
As you may see, quite a few rust stains have been created. I chose this material cause it's fast and easy.

196
00:44:25,703 --> 00:44:34,823
And I'm gonna use it in the future too. You just spray the base coat, then you spray the rust color, you dry

197
00:44:35,023 --> 00:44:43,239
and repeat the same procedure if needed. It gives us nice effects which I like a lot.

198
00:44:43,439 --> 00:44:58,431
We turn our project over and before adjusting the stencil on, we scratch its sides to make it look old.

199
00:45:06,185 --> 00:45:10,421
See? I scratch the paper sides.

200
00:45:28,382 --> 00:45:40,788
There are some points that the paper edges are white. I may add some oil and cover it. It's not a big deal.

201
00:45:52,356 --> 00:45:59,023
What about this. I thought of using two decorations for the eyes.

202
00:45:59,223 --> 00:46:06,335
We'll use these two metallic decorative elements by, "Scrabbery's", the Auto vintage craft line.

203
00:46:06,535 --> 00:46:10,244
They will be put here.

204
00:46:10,444 --> 00:46:17,495
And this... with the clock on, we'll be put right here.

205
00:46:17,695 --> 00:46:26,750
Press well, they have a sticker. Otherwise you'll have to use a medium gel paste to glue them.

206
00:46:26,950 --> 00:46:39,565
We slightly darken them with oil. So let's adjust it on our project.

207
00:46:40,496 --> 00:46:44,370
I have some ideas about how we are going to do that.

208
00:46:44,570 --> 00:46:51,431
Firstly, we need something to be put underneath and give some volume.

209
00:46:51,631 --> 00:47:28,302
Look what I'm going to do. I'll cut some small sized stone papers and I'll glue them together using the paste.

210
00:47:28,502 --> 00:47:37,444
I'm mostly interested in putting paste in the center. You see what I'm doing.

211
00:47:37,644 --> 00:47:43,474
I glue the one paper over the other to add volume.

212
00:47:43,674 --> 00:47:53,287
So as to put the stencil over the papers like this. I need to add some extra volume, by adding more papers.

213
00:47:54,595 --> 00:48:03,567
I found some paper remainings. I glue this one, that too.

214
00:48:06,272 --> 00:48:16,395
There are many ways you can get that volume. I thought of this one, so as not look for another material.

215
00:48:22,367 --> 00:48:29,638
Well, this will be glued here. Use again gel medium paste.

216
00:48:35,525 --> 00:49:23,449
Let's glue it. I've thought to use some cooper nails. We'll stitch the corners and then we'll slightly nail them.

217
00:49:28,279 --> 00:49:42,119
Not deep down. Because we want the paper to stay stretched and three-dimensional.

218
00:49:42,319 --> 00:49:55,124
We've put our main theme. I have an idea. I've been thinking about it during the whole time. 

219
00:49:55,324 --> 00:49:59,418
I thought of putting this hinge right here. It will look as if he has a nose ring.

220
00:50:01,738 --> 00:50:13,428
Here, it will be a bit difficult. There are the papers underneath. So I'll screw it.

221
00:50:13,628 --> 00:50:17,228
... on the papers, I don't mind at all.

222
00:50:21,434 --> 00:50:27,790
Very nice, I'll apply some oil here.

223
00:50:38,663 --> 00:50:52,536
I noticed, that there are some paper corners that they turn upwards a lot. So I'll add some extra gel paste.

224
00:50:54,806 --> 00:50:59,671
I keep it down. Nice!

225
00:51:12,694 --> 00:51:20,093
We've finished our main theme and now we are going to check on the rest project decoration.

226
00:51:20,293 --> 00:51:32,513
I have some ideas. I have some buttons and some hangings. We'll see how we are going to form it.

227
00:51:32,713 --> 00:52:11,881
I'll use some staples, with my the pliers I cut the nail foot and then I paste the nail head on the project.

228
00:52:12,081 --> 00:52:17,304
You may either use a medium gel paste, or silicon to glue them. I use the gel paste.

229
00:52:26,024 --> 00:52:28,385
They are slippery, it's logical.

230
00:52:32,908 --> 00:52:37,953
Quite of medium gel quantity is needed. Don't be afraid, it gets trasparent.

231
00:52:38,153 --> 00:52:42,093
Just before it dries, you may clear the remaining glue.

232
00:52:49,856 --> 00:53:01,864
Very nice. Let's go on with the decoration, as soon as  we dry the glue of  these nails. We'll be back soon.

233
00:53:03,783 --> 00:53:14,909
We are back. I've put some extra decorations on the project. Here are the buttons which are almost dried.

234
00:53:15,109 --> 00:53:21,429
I glued the buttons on the stencil circles. It was a good idea, eventually.

235
00:53:21,629 --> 00:53:28,437
I used some strings which I passed through these bead ornaments.

236
00:53:28,637 --> 00:53:33,504
Then I created some kind of tying and nailed the string down here. I added some nails here as well.

237
00:53:33,704 --> 00:53:56,150
Let's add some rust but on the whole project. I'll add some base coat at the top of it and then the rust color. 

238
00:53:56,350 --> 00:54:00,699
Using, now, the hair dryer, we make it leak.

239
00:54:04,380 --> 00:54:07,853
I might use some here on the buttons.

240
00:54:28,135 --> 00:54:32,829
I repeat this procedure as many times as I want. Until I get the desired result.

241
00:54:37,642 --> 00:54:41,403
Let's spray on the metallc elements too.

242
00:55:01,166 --> 00:55:05,174
So we dry again.

243
00:55:29,131 --> 00:55:38,373
This is the today's project, mix media on wood. I hope you liked the result. We did lots of things. I like it a lot.

244
00:55:38,573 --> 00:55:43,160
Thank you very much, once more, for being here at handmadefandasyworld.com.

245
00:55:43,360 --> 00:55:45,820
See you next Monday. 
Bye!