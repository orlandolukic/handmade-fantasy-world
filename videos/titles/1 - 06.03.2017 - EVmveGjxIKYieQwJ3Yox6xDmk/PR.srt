1
00:00:08,716 --> 00:00:14,990
Olá a todos! Bom dia, este é o nosso primairo vídeo no site www.handmadefantasyworld.com.

2
00:00:15,190 --> 00:00:22,930
A verdade é que para alguns de vocês pode ser bom dia, boa tarde o ainda boa noite. De fato, para vocês talvez seja de manhã ou à noite.

3
00:00:23,130 --> 00:00:24,789
De qualquer forma,

4
00:00:24,990 --> 00:00:26,920
obrigado por subscrever a nossa oficina.

5
00:00:27,120 --> 00:00:29,120
Este é o primeiro projeto e vou apresentar-lhes algo muito interesante.

6
00:00:29,320 --> 00:00:34,720
Vamos trabalhar em dois objetos. A primeira parte do projeto é trabalhar na argila.

7
00:00:34,920 --> 00:00:37,120
Mais tarde vou dizer-lhes algo mais sobre esta peça de argila.

8
00:00:37,320 --> 00:00:41,600
Este é realmente www.handmadefantasyworld.com. logotipo.

9
00:00:41,800 --> 00:00:47,450
Depois de trabalhar na argila, vamos fazer o efeito da pele com tintas acrílicas y tintas a óleo.

10
00:00:47,650 --> 00:00:49,140
Vamos começar com o envelhecimento da argila.

11
00:00:49,340 --> 00:00:58,720
Só para dizer que este logotipo é feito da argila.

12
00:00:58,920 --> 00:01:05,430
Um presente da minha amiga Maria Kantzali de Salónica.

13
00:01:05,690 --> 00:01:10,670
Não posso descrever-lhes tudo o que pode ser feito de argila.

14
00:01:10,780 --> 00:01:17,401
Se lhes gosta podem ver mais em seu perfil no Facebook Maria Kantzali.

15
00:01:17,601 --> 00:01:23,643
Obrigado novamente a Maria para o presente.

16
00:01:23,843 --> 00:01:30,400
Espero que lhe vai gostar do que vamos conseguir com argila.

17
00:01:30,401 --> 00:01:38,730
A argila está pronta para trabalhar. Espero que terá uma oportunidad do que María se junta com nós

18
00:01:38,731 --> 00:01:42,160
e para mostra-nos como trabalhar com a argila.

19
00:01:42,161 --> 00:01:53,620
Vamos começar com o efeito da pele. Este rompi ao longo do caminho, mas não importa.

20
00:01:53,621 --> 00:02:00,180
Temos também um pedaço de madeira, mas isso pode ser qualquer tipo de apoio, dependendo do que vocês têm ao seu lado.

21
00:02:00,181 --> 00:02:03,680
Vamos agora começar com o efeito. Preparei várias peças as quales vou utilizar no projeto.

22
00:02:03,681 --> 00:02:19,400
Mas vou mostrar-lhes na peça não tratada todo o proceso. Por exemplo, nesta peça.

23
00:02:19,411 --> 00:02:26,332
O que é isto? Isto é o papel da pedra da empresa Stamperia de Itália.

24
00:02:26,771 --> 00:02:31,380
O papel tem dois lados, um bom e um pouco áspero.

25
00:02:31,381 --> 00:02:40,120
Depedendo do qué lado usamos, vamos ter diferentes efeitos. Eu prefiro mais o que pode alcançarse no lado áspero.

26
00:02:40,532 --> 00:02:46,630
Precisamos do três copos de plástico e três cores aquacolor também da empresa Stamperia.

27
00:02:46,631 --> 00:03:01,400
Usaremos as seguintes cores: amarela (pino), castanha (castanio) e cor da pele (cuoio).

28
00:03:01,401 --> 00:03:04,590
Eu não falo italiano e por isso me resulta difícil os nomes em italiano.

29
00:03:04,591 --> 00:03:15,440
Tomem uma escova macia, girem o lado mais áspero e vamos começar com amarelo. Vou pintar toda a superfície igualmente.

30
00:03:15,441 --> 00:03:28,880
É muito importante que os movimentos são em linha reta, de borda a borda, e que não há linhas visíveis.

31
00:03:29,080 --> 00:03:36,310
Se começo com os movimentos verticais, começarei a partir do lado esquerdo e puxo a escova até o final nessa direção.

32
00:03:36,311 --> 00:03:45,740
Quando aplicam uma boa camada de tinta, comecem a secar. Secámo-lo brevemente.

33
00:03:54,600 --> 00:03:56,650
Repetirei otra vez este procedimento.

34
00:03:56,651 --> 00:04:01,680
Lado áspero do papel o pintámos com a tinta amarela Aquacolor.

35
00:04:01,681 --> 00:04:07,090
Apliquei-a igualmente, sequei-a e estamos prontos para o próximo passo.

36
00:04:07,091 --> 00:04:14,420
Com a mesma escova, só sequem-lo com o pedaço de papel, aplicamos a próxima cor, castanha aquacolor.

37
00:04:14,421 --> 00:04:22,300
O procedimento é o mesmo. Passamos pela primeira cor que não tem que estar perfeitamente seca, apenas um pouco.

38
00:04:22,500 --> 00:04:27,300
Os movimentos largos da escova, de borda a borda. Isso é muito importante.

39
00:04:34,500 --> 00:04:38,940
Bom…os movimentos largos da escova, de borda a borda.

40
00:04:38,941 --> 00:04:42,350
Ponham a escova em agua e sequem o papel com o secador de pelo.

41
00:04:59,550 --> 00:05:11,410
Agora aplicamos a terceira cor, a cor da pele, e repetimos o processo.

42
00:05:12,000 --> 00:05:17,440
Não importa se têm mais cor, só pintem-no bem.

43
00:05:17,640 --> 00:05:23,380
Bom, pusemos os jornais debaixo para que não sujamos tudo.

44
00:05:31,000 --> 00:05:33,580
A única diferença com o uso da terceira cor é que será aplicada duas vezes.

45
00:05:33,581 --> 00:05:38,000
Isto não é necessário, mas eu prefiro mais o resultado quando aplicamos duas camadas.

46
00:06:07,200 --> 00:06:09,870
Uma camada mais…

47
00:06:09,871 --> 00:06:14,990
Queria dizer-lhes algo mais. Vocês podem vé-me no ecrã, mas em torno de mim há muitas pessoas

48
00:06:14,991 --> 00:06:18,800
e podem ouvi-los falar ou como eu dirijo-los.

49
00:06:19,000 --> 00:06:20,320
Eles estão aqui para ajudar quando seja necessário. :)

50
00:06:24,000 --> 00:06:30,900
Se podem ouvi-los, isso não é um erro, nós estamos aqui para socializar e relaxar, e para oferecemo-lhes o melhor oficina possível.

51
00:06:30,901 --> 00:06:37,600
O relaxamento e a arte, isso realmente reduz o estresse.

52
00:06:38,601 --> 00:06:43,600
Eu acabei com a pele e otra vez secamo-la

53
00:07:13,000 --> 00:07:19,610
Bom. Isto é o resultado da pintura. Temos um passo mais. Um pequeno detalhe, mais preciso.

54
00:07:19,611 --> 00:07:23,600
Olhem a diferença …ou vamos dar o próximo passo para ver melhor.

55
00:07:23,602 --> 00:07:33,270
Esta é a pele que tinhamos feito, a única coisa necessária é o papel amarrotado.

56
00:07:35,271 --> 00:07:38,850
Este papel é excelente e vamos usá-lo em projetos. Excelente papel da pedra, a produção de Stamperia.

57
00:07:38,851 --> 00:07:43,390
E aqui está o resultado final - fizemos a imitação da pele.

58
00:07:43,391 --> 00:07:47,850
Se olharmos para este pedaço de papel, veem que quando pintamos o outro lado,

59
00:07:47,851 --> 00:07:51,230
que é lisa, uma cor diferente é obtida.

60
00:07:51,231 --> 00:07:56,820
Isto acontece porque o lado liso é menos absorvente e isso muda o resultado.

61
00:07:57,020 --> 00:08:01,210
Isto aconteceu. Nós vamos usar um monte de peças para cobrir a placa de madeira.

62
00:08:01,211 --> 00:08:08,080
É a sua decisão do qué lado vão usar. E eu gosto mais quando trabalho no lado áspero.

63
00:08:08,081 --> 00:08:13,630
Estamos prontos para o próximo passo. Começamos com um pedaço de madeira.

64
00:08:13,631 --> 00:08:21,100
Um pouco de café, diz que os gregos isso nunca iria perder.

65
00:08:22,102 --> 00:08:25,210
Agora a minha mãe diria: o meu filho está a beber o café, isso não é possível!

66
00:08:25,211 --> 00:08:31,848
Devido a isso, temos feitos muitas pausas em oficinas, as pausas para o café.

67
00:08:32,048 --> 00:08:43,219
Com os pedaços de papel que são imitação da pele, vamos fazer um trabalho com retalho em uma base de madeira.

68
00:08:43,220 --> 00:08:49,210
Isto será a base para o logotipo da argila.

69
00:08:49,211 --> 00:08:55,370
O papel não vamos colocar à borda da placa de madeira.

70
00:08:55,371 --> 00:08:59,390
Vamos deixar alguns milímetros livre.

71
00:08:59,395 --> 00:09:03,660
Bom, para isto vou usar a Extra Forte cola de Stamperia.

72
00:09:03,661 --> 00:09:06,130
Usaremos muitos productos de Stamperia.

73
00:09:06,131 --> 00:09:11,230
Estes productos, apesar do facto de que, em geral, eu gosto de seus productos, são excelente para o nosso primeiro projeto.

74
00:09:11,231 --> 00:09:15,270
Esto fizemos com a Extra Forte cola.

75
00:09:16,271 --> 00:09:18,520
Ahá ...Tenho que empurrar com mais força.

76
00:09:18,521 --> 00:09:30,970
Colocamos nas bordas e um pouco para o centro.

77
00:09:30,971 --> 00:09:38,280
Depois colamos o papel sobre a madeira.

78
00:09:45,281 --> 00:09:49,580
Devem ser paciente e cuidadoso com o papel porque agora é enrolado e por isso não se adere fácilmente.

79
00:09:49,581 --> 00:09:53,320
E se não tivéssemos enrolado o papel e tivéssemos feito uma espiral, não teríamos sido capazes de obter o efeito da pele.

80
00:09:53,321 --> 00:10:00,590
Estas protuberâncias são necessárias. Aqui vemos um pouco de cola, mas isso não importa.

81
00:10:03,591 --> 00:10:05,250
É um pouco difícil de colá-los.

82
00:10:07,250 --> 00:10:10,260
Sempre usem os lençinhos perfumados, que são um instrumento muito importante.

83
00:10:17,700 --> 00:10:21,080
Vamos agora fazer o trabalho com retalho.

84
00:10:23,900 --> 00:10:27,370
Pressionem o papel com mais força para que pode ser aderido mais fácil.

85
00:10:42,570 --> 00:10:48,120
Lembrem que o seu material não seja submetido ao frio, se eles congelam, já não podem ser utilizados.

86
00:10:48,121 --> 00:10:50,080
Isso aconteceu a mim hoje.

87
00:10:57,081 --> 00:11:01,710
A posição de papel é muito importante para nós.

88
00:11:01,711 --> 00:11:08,470
É necessário que os papéis sobrepõem-se.

89
00:11:08,670 --> 00:11:17,710
Fazemos a composição e é importante onde vamos colocar o que pedaço.

90
00:11:17,910 --> 00:11:24,639
Sempre limpem o excesso de cola.

91
00:11:31,839 --> 00:11:42,370
Um monte de tutoriais serão feitos, muitas coisas serão melhoradas.

92
00:11:42,371 --> 00:11:48,590
Isto é apenas o começo.

93
00:11:48,591 --> 00:12:02,490
Espero que gostem de tutoriais e que ficarão satisfeitos com o conteúdo e a qualidade do vídeo.

94
00:12:02,491 --> 00:12:10,700
Primeiro usei os papéis maiores e cobri as bordas de as peças maiores.

95
00:12:46,900 --> 00:12:53,850
O nosso trabalho com retalho é feito e o vídeo foi acelerado para economizar tempo.

96
00:12:54,050 --> 00:13:01,080
Como vocês podem ver, não só é da técnica, mas também a posição do papel é importante.

97
00:13:01,081 --> 00:13:05,750
Colocamos os papéis cruzados e evitamos que estão próximos uns dos outros.

98
00:13:05,751 --> 00:13:08,690
Não importa que algunas bordas são um pouco elevadas.

99
00:13:08,691 --> 00:13:11,840
Vamos utilizar os pregos decorativos para baixo e também vamos decorar o trabalho.

100
00:13:11,841 --> 00:13:16,580
Isso dá um efeito final muito agradável mas nem todos os papéissão bem ligados.

101
00:13:16,581 --> 00:13:19,010
Agora não vou processar as bordas do painel de madeira.

102
00:13:19,011 --> 00:13:22,950
Depois vou colocar a pintura de óleo, provavelmente preta, ou vou pensar em outra coisa.

103
00:13:22,951 --> 00:13:26,600
Vamos colocar isso de lado e agora vamos começar a trabalhar na argila.

104
00:13:34,060 --> 00:13:41,420
Pintaremos a argila, ou seja, todas as partes.

105
00:13:41,421 --> 00:13:55,730
Vamos usar Stamperia Allegro turquesa cor acrílica, o código KAL 109

106
00:14:00,000 --> 00:14:04,600
e começamos pintar com a escova macia.

107
00:14:06,601 --> 00:14:09,360
As asas serão turquesa,

108
00:14:09,361 --> 00:14:11,790
mas os que me conhecem, sabem que este é a minha favorita.

109
00:14:17,791 --> 00:14:23,400
É muito importante que a cor cobre tudo, especialmente os buracos nas asas.

110
00:14:50,090 --> 00:14:55,350
Agora vamos mudar a cor com a qual pintaremos o relógio que é a parte central e as minhas iniciais.

111
00:14:55,351 --> 00:15:10,170
Novamente Stamperia Allegro pintura acrílica, amarela. Albicoca, o código KAL 60.

112
00:15:10,171 --> 00:15:20,700
É importante combinar as cores, mas também é importante e o seu contraste.

113
00:15:20,701 --> 00:15:27,250
Agora precisamos de uma cor que é contraste da cor anterior, mas que será o contraste e do fundo da pele.

114
00:15:27,251 --> 00:15:33,920
Às vezes cores e contrastes podem parecer demasiado fortes,

115
00:15:33,921 --> 00:15:38,080
mas não esqueçam de que vamos passar por cima delas com pintura de óleo, o que nos dará um tom completamente diferente, o que realmente desejamos alcançar.

116
00:15:38,280 --> 00:15:43,851
Portanto, as cores vão mudar, e não tenham medo de usar cores intensas.

117
00:15:55,000 --> 00:16:05,520
Nós terminamos com a pintura, e agora vamos colocar os pedaços de argila e seca-los.

118
00:16:39,710 --> 00:16:53,610
Então, vamos tomar uma esponja sintética com buracos, que é muito mais barata do que uma esponja do mar real. Pode-se comprar nas farmácias.

119
00:16:55,611 --> 00:16:59,530
E Stamperia Allegro cor acrílica, Terra di Sienna.

120
00:16:59,531 --> 00:17:07,440
O código KAL74, tom alaranjado.

121
00:17:07,441 --> 00:17:13,000
Tenha em mente que cada projeto que está sendo gravado pela primeira vez,

122
00:17:13,001 --> 00:17:18,810
o que nunca teria sido feita, e que é por isso que durante a gravação creamos e reagimos.

123
00:17:21,000 --> 00:17:29,720
Eu não quero te mostrar-lhes os projetos acabados, para reproduzir-os, corrigir os erros e outros semelhantes.

124
00:17:29,920 --> 00:17:39,300
O que quero fazer com a esponja...tomar uma pequena quantidade de cor

125
00:17:39,301 --> 00:17:43,470
para fazer as marcas de laranja na agrila.

126
00:17:53,471 --> 00:18:11,560
Com a esponja, eu toco suave a argila para que não queda demasiada cor, quero obter o efeito da ferrugem em alguns lugares.

127
00:18:15,561 --> 00:18:23,100
Eu uso esta esponja porque deixa uma trilha irregular por causa dos buracos.

128
00:18:30,101 --> 00:18:37,880
Como podem ver, escolho onda vou aplicar a cor mas definitivamente não em todos os lugares.

129
00:18:37,881 --> 00:18:54,870
Se queremos que a cor sai onde a esponja não pode, vamos usar escova.

130
00:19:00,020 --> 00:19:03,050
Sempre deem pancadinhas...não puxem escova ou esponja.

131
00:19:03,051 --> 00:19:06,980
Com a esponja podemos corrigir tudo do que não gostamos.

132
00:19:07,981 --> 00:19:11,430
Ponham em alguns lugares

133
00:19:15,431 --> 00:19:18,990
e não tenham medo, depois da pintura da óleo, o tom vai mudar.

134
00:19:18,991 --> 00:19:28,080
É muito importante que nós relaxamos. Sejam legal, tudo isto fazemos para que se sintamos relaxados.

135
00:19:31,480 --> 00:19:40,200
Bom, com a tinta acrílica temos conseguido o efeito da ferrugem, sem produtos químicos. Lavem a escova e secem a cor.

136
00:20:14,400 --> 00:20:26,060
Agora é momento para pintura a óleo. Nós precisamos de um pedaço de papel alumínio que vocês usam nas suas casas.

137
00:20:26,061 --> 00:20:27,940
Na verdad, vamos usar um monte de tinta a óleo,

138
00:20:27,941 --> 00:20:34,470
mas hoje usamos Burnt Umber tinta a óleo.

139
00:20:34,471 --> 00:20:43,150
Gosto de Burnt Umber porque é uma pintura a óleo clássico.

140
00:20:43,151 --> 00:20:54,060
Eu gosto Van Gogh pinturas a óleo. Vamos agora empezar com a aplicação de óleo.

141
00:20:54,061 --> 00:21:08,000
Ao contrário da Maria que dilui tinta a óleo e borra-a com um tecido,

142
00:21:08,001 --> 00:21:16,450
eu prefiro pintura a óleo não diluída.

143
00:21:16,451 --> 00:21:28,980
Escolhem aquela forma da escova que mais lhes convir.

144
00:21:28,981 --> 00:21:36,610
Tomem uma pequena quantidade da tinta a óleo, caso contrário vocês terão problemas com a remoção da tinta.

145
00:21:36,611 --> 00:21:37,940
Cobrimos todo o objeto, olham?

146
00:21:37,941 --> 00:21:44,210
Especialmente os buracos, mas não apliquem demasiada cor.

147
00:21:44,410 --> 00:21:55,080
Todo que resta, borrão com a escova.

148
00:21:56,000 --> 00:21:59,480
Agora vamos acelerar...e até breve...

149
00:22:26,500 --> 00:22:39,120
Aqui, nós cobrimos toda a peça da argila com pintura de óleo, as ambas asas e o relogio.

150
00:22:39,121 --> 00:22:44,130
Agora com toalhetes húmidos, o instrumento principal, temos de eliminar a pintura a óleo.

151
00:22:44,131 --> 00:22:59,800
Eliminamos pouco a pouco, fazemos uma bola de toalhetes húmidos e removemos tinta a óleo.

152
00:23:08,801 --> 00:23:19,100
Como podem ver, mais cor fica nos buracos
porque ali não pressionmos e a cor fica

153
00:23:19,101 --> 00:23:23,570
Quando querem eliminar mais cor, e necessário pressionar um pouco forte o toalhete mentras limpam a superfície.

154
00:23:23,571 --> 00:23:33,350
Vejam que em certos lugares aparece cor de laranja, a cor de ferrugem.

155
00:23:37,351 --> 00:23:40,020
Como vocês podem ver, o resultado é fantástico!

156
00:23:45,130 --> 00:23:51,440
Depois de aplicar tinta a óleo sobre todo o objeto da argila, suavemente removemos a tinta.

157
00:23:51,441 --> 00:23:55,490
Isto é o que sempre dizemos nas oficinas, que esta é a parte onde vocês devem aproveitar.

158
00:23:55,491 --> 00:23:42,120
Esta parte temos esperado. Se não gostamos de alguma coisa,

159
00:24:11,900 --> 00:24:31,090
ainda podemos aplicar mais tinta de óleo e com os dedos corrigir a camada da tinta.

160
00:24:31,091 --> 00:24:36,500
A cor laranja realmente não é feito um forte contraste.

161
00:24:36,501 --> 00:24:43,820
A diferença certamente existe, mas não é muito grande. Como eu queria destacar os números e deixá-los ser mais escuros,

162
00:24:43,821 --> 00:24:56,980
os meus movimentos nessa parte foram mais suave e ali a cor fica mais escura.

163
00:24:56,981 --> 00:25:02,950
Estou jogando com pintura a óleo. Não importa se sobra nas asas.

164
00:25:02,951 --> 00:25:08,070
Na verdade, isso é o que eu quis conseguir. Olhem o resultado! Passo agora para a segunda asa.

165
00:25:08,071 --> 00:25:20,199
Olhem como estou a eliminar a pintura a óleo. Os movimentos não são para trás e para a frente, mas a partir do interior para o exterior.

166
00:25:20,200 --> 00:25:24,400
Diminuo a pressão onde quero mais tinta a óleo

167
00:25:24,401 --> 00:25:27,720
e aumento a pressão onde quero remover a pintura a óleo.

168
00:25:27,721 --> 00:25:31,560
Como podem ver, usei apenas dois toalhetes porque sempre dobrar-as e usei só a parte limpa do pano.

169
00:25:31,561 --> 00:25:39,380
Dois são suficientes para o inteiro projeto.

170
00:26:08,619 --> 00:26:15,400
Agora tenho que juntar as peças da argila e verificar se há peças que ficaram claras.

171
00:26:15,401 --> 00:26:24,630
As bordas das asas devem ser claras.

172
00:26:43,500 --> 00:26:46,880
Bem, uma pequena parte é...

173
00:27:03,200 --> 00:27:04,280
E como vocês podem ver, temos o resultado final.

174
00:27:04,281 --> 00:27:12,650
Se há algumas partes azuis, basta só adicionar mais pintura a óleo.

175
00:27:12,651 --> 00:27:15,049
Existe uma possibilidade de que em algumas partes da argila aparece a cor branca como um resultado de eliminação.

176
00:27:17,361 --> 00:27:29,110
Podemos deixá-lo, ou adicionar mais óleo nas partes com os dedos.

177
00:27:32,321 --> 00:27:37,230
Eu pessoalmente adoro o efeito branco de "aqui e ali".

178
00:27:42,361 --> 00:27:46,210
Otra vez, com os dedos podem adicionar o óleo e escurecer as partes.

179
00:27:48,100 --> 00:27:52,150
Eu aqui tinha removido o cor e a argila pode-se ver.

180
00:28:01,000 --> 00:28:09,480
Se depois de limpar com lenços umedecidos quisermos adicionar um pouco mais de pintura a óleo, 

181
00:28:09,481 --> 00:28:12,250
temos que limpar a superfície com toalha de papel.

182
00:28:34,251 --> 00:28:42,900
Agora vamos ver o que temos feito e vamos colocar a nossa placa de madeira coberta com a pele.

183
00:28:49,000 --> 00:28:56,310
O primeiro projeto é muito grande, mas isso era o meu desejo, que Maria faz um grande pedaço tal.

184
00:28:58,600 --> 00:29:03,790
Maria, se estás a mirar isto, muito obrigado, tem cuidado e nós vemos pronto.

185
00:29:22,000 --> 00:29:27,700
Vamos juntar todas as peças.

186
00:29:28,701 --> 00:29:34,240
Claro, isso não deve ser quebrado, mas temos encontrado uma solução e temos resolvido a coisa.

187
00:29:38,000 --> 00:29:47,580
Como podem ver, a parte central-o relogio, apesar de que estava claro, adaptou-se na cor da pele. 

188
00:29:47,581 --> 00:29:49,251
Agora podemos ver a cor laranja.

189
00:29:51,252 --> 00:29:57,311
Apesar das cores são de tons semelhantes, o tom vermelho está visível.

190
00:29:57,312 --> 00:30:00,790
 E esta é a cor que temos utilizado inicialmente para as asas.

191
00:30:04,000 --> 00:30:15,220
A argila vamos colocar no centro e vamos aderi-la.

192
00:30:31,000 --> 00:30:34,880
Vamos transladar com atenção para não enrugar os pedaços de pele.

193
00:30:36,000 --> 00:30:55,890
Nas bordas vamos aplicar a cola Extra Forte de Stamperia, uma quantidade maior.

194
00:30:58,489 --> 00:30:58,289
Em algumas peças vai ser necessári adicionar tinta a óleo para alcançar intensidade.

195
00:31:15,000 --> 00:31:22,690
A que coisa é que movemos um pouco a argila para que cola debaixo pode lubrificar e,

196
00:31:22,691 --> 00:31:27,970
portanto, vai secar-se mais rápido.

197
00:31:27,971 --> 00:31:35,900
Apesar de que a cola torna-se transparente,

198
00:31:35,901 --> 00:31:38,260
com os toalhetes húmidos podem eliminar o que sobra de cola debaixo da argila.

199
00:31:38,261 --> 00:31:45,880
E com a escova podem eliminar ainda melhor. 
Não se move já.

200
00:31:51,881 --> 00:32:01,980
Agora outra peça. Temos de limpar completamente este lado, porque a cola não funciona bem na pintura a óleo.

201
00:32:03,990 --> 00:32:20,040
Limpem com o tecido e ponham a cola.

202
00:32:28,360 --> 00:32:30,390
Antes da utilização sacudam a cola.

203
00:32:33,391 --> 00:32:38,170
Com certeza seria mais fácil se o meu objeto de argila não foi quebrado.

204
00:32:38,171 --> 00:32:44,520
Outra vez com o toalhete húmido eliminam o excesso de cola.

205
00:33:00,000 --> 00:33:03,860
Novamente usamos a cola Extra Forte Stamperia para pequenas peças.

206
00:33:03,861 --> 00:33:11,780
A cola extra Stamperia Forte é bom, mas eu acho que

207
00:33:11,781 --> 00:33:21,580
a cola hobby ou a cola scrapbook aderem mais ou menos igualmente.

208
00:33:23,000 --> 00:33:30,010
Maria para a argila usa a cola Atalco.

209
00:33:38,570 --> 00:33:41,400
Este pequeno pedaço vai ficar mais difícil.

210
00:33:41,401 --> 00:33:44,210
Não importa pequenas rachaduras que aparecem. Aplicamos a pintura de óleo,

211
00:33:44,211 --> 00:33:47,150
limpamos e rachaduras desaparecem.

212
00:33:58,130 --> 00:34:10,440
Agora vamos proceder a borda da placa de madeira para que pode ajustar-se a cor da pele.

213
00:34:14,460 --> 00:34:20,230
O mesmo fazemos com as bordas do papel.

214
00:34:20,231 --> 00:34:34,650
Não muita pintura a óleo, só um pouco. Apliquem e esfreguem com os dedos.

215
00:34:48,700 --> 00:35:04,290
Not all sides at once,little by little.Depending on which surface I put oil,
Nem todos os lados ao mesmo tempo, mas gradualmente. Dependendo da superfície na cual coloco a pintura a óleo.

216
00:35:05,710 --> 00:35:08,080
Desvaneço com os dedos ou com os movimentos circulados ou movimentos para cima e para baixo.

217
00:35:39,489 --> 00:35:47,760
Aqui está, estamos quase terminando. Verifico os detalhes e talvez em algumas partes adicionaré mais pintura a óleo.

218
00:35:47,761 --> 00:35:54,030
Primeiro limpem as mãos com toalhetes e, em seguida, o lavem-os com o sabão e a água.

219
00:35:57,031 --> 00:35:58,490
Ou ainda melhor, usem o creme para as mãos para limpar a óleo de mãos.

220
00:35:58,500 --> 00:36:07,930
Olhem o que vamos fazer agora. Vamos adicionar os pregos decorativos...

221
00:36:28,489 --> 00:36:39,870
Vamos pregar cada borda do papel. Eu começo a pregar.

222
00:36:39,871 --> 00:36:43,150
Agora vamos acelerar mas estarei de volta em breve.

223
00:37:11,000 --> 00:37:15,240
Aqui estou,  :) Com a cor, aspergi o pedaço de argila,

224
00:37:15,241 --> 00:37:24,800
usando a cor de marfim, não exatamente branco, mas diluído com água.

225
00:37:39,489 --> 00:37:42,830
Tomo uma pequena quantidade de cor para não aspergir a pele.

226
00:37:42,831 --> 00:37:47,540
Com os dedos, lentamente e com cuidado, estou a aspergir a argila.

227
00:37:47,541 --> 00:38:00,660
Vou aspergir mais no centro.

228
00:38:08,000 --> 00:38:19,980
E as asas só um pouco.

229
00:38:39,170 --> 00:38:45,770
Se acontece que aspergem a pele, rapidamente limpem com toalhete húmido e tudo vai ficar bem.

230
00:39:13,489 --> 00:39:23,050
E aqui estão os resultados. Eu gosto disso.

231
00:39:25,051 --> 00:39:29,460
Isso é tudo por hoje. Obrigado por assistir a oficina.

232
00:39:29,461 --> 00:39:34,280
Espero que tenham gostado. Este é a primeira oficina.
233
00:39:34,281 --> 00:39:44,930
Vou tentar dar-lhes mais informações e detalhes para que vocês podem facilmente repetir o projeto da oficina

234
00:39:44,931 --> 00:39:49,631
ou fazer algo usando a sua imaginação.

235
00:39:49,632 --> 00:40:00,200
Até a próxima semana. Esperamos que os amigos ao redor do mundo se juntam a nós.

236
00:40:00,201 --> 00:40:03,280
E obrigado a Maria outra vez pelo logotipo feito em argila.

237
00:40:03,281 --> 00:40:06,190
Muitos beijos e até a próxima.
