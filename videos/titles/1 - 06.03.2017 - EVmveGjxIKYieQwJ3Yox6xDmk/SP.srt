1
00:00:08,716 --> 00:00:14,990
¡Hola a todos! Buenos días, este es nuestro primer vídeo en la página web www.handmadefantasyworld.com.

2
00:00:15,190 --> 00:00:22,930
La verdad es que, para algunos de vosotros puede ser buenos días, buenas tardes o incluso buenas noches. 
De hecho, para vosotros tal vez sea mañana o noche

3
00:00:23,130 --> 00:00:24,789
De todos modos,

4
00:00:24,990 --> 00:00:26,920
gracias por suscribirse a nuestro taller.

5
00:00:27,120 --> 00:00:29,120
Esto es nuestro primer proyecto y os voy a presentar algo muy interesante.

6
00:00:29,320 --> 00:00:34,720
Vamos a trabajar en dos objetos. La primera parte del proyecto es trabajar con la arcilla.

7
00:00:34,920 --> 00:00:37,120
Más tarde os diré algo más sobre este pedazo de arcilla.

8
00:00:37,320 --> 00:00:41,600
Esto es en realidad www.handmadefantasyworld.com. logo.

9
00:00:41,800 --> 00:00:47,450
Después de haber trabajado con la arcilla vamos a hacer el efecto de la piel con pinturas acrílicas y al óleo.

10
00:00:47,650 --> 00:00:49,140
Vamos a empezar con el envejecimiento de la arcilla.

11
00:00:49,340 --> 00:00:58,720
Dejadme decir que este logo está hecho de arcilla.

12
00:00:58,920 --> 00:01:05,430
Un regalo de mi amiga María Kantzali de Tesalónica.

13
00:01:05,690 --> 00:01:10,670
No os puedo ni contar todo lo que se puede hacer con la arcilla.

14
00:01:10,780 --> 00:01:17,401
Si os gusta, podéis ver más en su perfil de Facebook María Kantzali

15
00:01:17,601 --> 00:01:23,643
Gracias otra vez a María por el regalo.

16
00:01:23,843 --> 00:01:30,400
Espero que le guste lo que vamos a lograr con la arcilla.

17
00:01:30,401 --> 00:01:38,730
La arcilla está lista para trabajar. Espero que habrá oportunidad para que María se une con nosotros 

18
00:01:38,731 --> 00:01:42,160
y para que nos muestra cómo se trabaja con la arcilla.

19
00:01:42,161 --> 00:01:53,620
Vamos a empezar con el efecto de la piel. Esto rompí a lo largo del camino, pero no importa.

20
00:01:53,621 --> 00:02:00,180
También tenemos un trozo de madera, pero puede ser cualquier soporte, dependiendo de lo que tenéis a su lado.

21
00:02:00,181 --> 00:02:03,680
Vamos a empezar ahora con el efecto. He preparado varias piezas las cuales voy a utilizar en el proyecto.

22
00:02:03,681 --> 00:02:19,400
Pero os voy a mostrar todo el proceso en la pieza no procesada. Por ejemplo en esta pieza.

23
00:02:19,411 --> 00:02:26,332
¿Qué es esto? Esto es el papel de piedra de la empresa Stamperia de Italia.

24
00:02:26,771 --> 00:02:31,380
Tiene dos caras, una lisa y una ligeramente áspera.

25
00:02:31,381 --> 00:02:40,120
Dependiendo de qué lado usamos, vamos a tener diferentes efectos. Yo prefiero lo que se consigue en el lado áspero.

26
00:02:40,532 --> 00:02:46,630
Necesitamos tres vasos de plástico y tres colores aquacolor también de la empresa Stamperia.

27
00:02:46,631 --> 00:03:01,400
Vamos a utilizar los siguiente colores: amarillo (pino), castaño (castanio) y color de piel (cuoio).

28
00:03:01,401 --> 00:03:04,590
No hablo italiano y por eso me resulta difícil pronunciar los nombres en italiano.

29
00:03:04,591 --> 00:03:15,440
Tomad un pincel suave, girad al lado más áspero y vamos a empezar con el color amarillo. Voy a pintar toda la superficie igualmente.

30
00:03:15,441 --> 00:03:28,880
Es muy importante que los movimientos son llanos, de borde a borde, y para que no hay líneas visibles.

31
00:03:29,080 --> 00:03:36,310
Si empiezo con los movimientos verticales, empezaré desde el lado ezquierdo, y hasta el final tiro el pincel en esa dirección.

32
00:03:36,311 --> 00:03:45,740
Cuando aplicáis una buena capa de pintura, comenzad con secar. Lo secamos muy brevemente.

33
00:03:54,600 --> 00:03:56,650
Voy a repetir este procedimiento otra vez.

34
00:03:56,651 --> 00:04:01,680
El lado áspero del papel lo pintamos con el amarillo aquacolor.

35
00:04:01,681 --> 00:04:07,090
Lo apliqué igualmente, lo sequé y estamos listos para el siguiente paso.

36
00:04:07,091 --> 00:04:14,420
Con el mismo pincel, solamente secadlo con el pedazo de papel, aplicamos el siguiente color, castaño aquacolor.

37
00:04:14,421 --> 00:04:22,300
El procedimiento es mismo. Cruzamos sobre el primer color que no tiene que estar completamente secado, solo un poco.

38
00:04:22,500 --> 00:04:27,300
Los movimientos largos de pincel, de borde a borde. Eso es muy importante.

39
00:04:34,500 --> 00:04:38,940
Bien…los movimientos largos de pincel, de borde a borde.

40
00:04:38,941 --> 00:04:42,350
Poned el pincel en el agua y secad el papel con el secador.

41
00:04:59,550 --> 00:05:11,410
Ahora aplicamos el tercer color, el color de la piel y repetimos el procedimiento.

42
00:05:12,000 --> 00:05:17,440
No importa si sobra el color, solo pintadlo bien.

43
00:05:17,640 --> 00:05:23,380
Bien, pusimos los periódicos bajo, para que no manchemos todo.

44
00:05:31,000 --> 00:05:33,580
La única diferencia en el uso del tercer color, es que vamos a aplicarlo dos veces.

45
00:05:33,581 --> 00:05:38,000
No es necesario, pero a mí me gusta más el resultado cuando aplicamos dos capas.

46
00:06:07,200 --> 00:06:09,870
Una capa más…

47
00:06:09,871 --> 00:06:14,990
Quería deciros una cosa más. Vosotros podéis verme en la pantalla, pero a mi alrededor hay muchas personas

48
00:06:14,991 --> 00:06:18,800
y podéis oírlas hablar o cómo yo les estoy dirigiendo.

49
00:06:19,000 --> 00:06:20,320
Están aquí para ayudar cuando sea necesario. :)

50
00:06:24,000 --> 00:06:30,900
Si podéis oírlas, eso no es un error, estamos aquí para socializarnos y para relajarnos, y para ofreceros el mejor taller posible.

51
00:06:30,901 --> 00:06:37,600
La relajación y el arte, eso de verdad disminuye el estrés.

52
00:06:38,601 --> 00:06:43,600
He terminado con la piel y otra vez la secamos.

53
00:07:13,000 --> 00:07:19,610
Bien. Este es el resultado de la pintura. Tenemos un paso más. Un pequeño detalle, más preciso.

54
00:07:19,611 --> 00:07:23,600
Mirad la diferencia…o vamos a hacer el siguiente paso para que podáis ver mejor.

55
00:07:23,602 --> 00:07:33,270
Esta es la piel que hemos hecho, lo único que es necesario es que estrujamos el papel.

56
00:07:35,271 --> 00:07:38,850
Este papel es excelente y lo vamos a utilizar en los proyectos. Excelente papel de piedra producido por Stamperia.

57
00:07:38,851 --> 00:07:43,390
Y aquí está el resultado final-hicimos la piel artificial.

58
00:07:43,391 --> 00:07:47,850
Si nos fijamos en este papel, veréis que cuando se pinta en el otro lado,

59
00:07:47,851 --> 00:07:51,230
que es liso, se obtiene un color diferente.

60
00:07:51,231 --> 00:07:56,820
Eso es porque el lado liso absorbe menos y eso cambia el resultado.

61
00:07:57,020 --> 00:08:01,210
Esto sucedió. Vamos a usar muchos pedazos para que podamos cubrir la tabla de madera.

62
00:08:01,211 --> 00:08:08,080
Es vuestra decisión qué lado vais a utilizar. Y me gusta más cuando trabajo en el lado áspero.

63
00:08:08,081 --> 00:08:13,630
Estamos listos para el siguiente paso. Vamos a empezar con el pedazo de madera.

64
00:08:13,631 --> 00:08:21,100
Un poco de café, verdaderos griegos nunca lo perderían.

65
00:08:22,102 --> 00:08:25,210
Ahora mi mamá dirá: mi niño está bebiendo el café, ¡no puede ser!

66
00:08:25,211 --> 00:08:31,848
Por eso hemos hecho muchas pausas en los talleres, pausas de café.

67
00:08:32,048 --> 00:08:43,219
Con los pedazos de papel que son la imitación de la piel vamos a hacer una almazuela sobre una base de madera.

68
00:08:43,220 --> 00:08:49,210
Esto será la base para un logo de arcilla.

69
00:08:49,211 --> 00:08:55,370
El papel no lo vamos a adherir hasta al borde de la tabla de madera.

70
00:08:55,371 --> 00:08:59,390
Dejaremos unos pocos milímetros libre.

71
00:08:59,395 --> 00:09:03,660
Bien, para esto voy a utilizar el Extra Forte pegamento de Stamperia.

72
00:09:03,661 --> 00:09:06,130
Utilizaremos muchos productos de Stamperia.

73
00:09:06,131 --> 00:09:11,230
Estos productos, a pesar de que generalmente me encantan, son excelententes para nuestro primer proyecto.

74
00:09:11,231 --> 00:09:15,270
Esto lo hicimos con el pegamento Extra Forte.

75
00:09:16,271 --> 00:09:18,520
Ajá...tengo que presionar con más fuerza.

76
00:09:18,521 --> 00:09:30,970
Ponemos en los bordes y un poco hacia el centro.

77
00:09:30,971 --> 00:09:38,280
Entonces pegamos el papel sobre la madera.

78
00:09:45,281 --> 00:09:49,580
Tenéis que ser pacientes y cuidado con el papel porque ahora está enrollado y por eso no se adhiere con facilidad.

79
00:09:49,581 --> 00:09:53,320
Y si no hubiéramos enrollado el papel y no hubiéramos hecho los espirales, no habríamos podido conseguir el efecto de la piel.

80
00:09:53,321 --> 00:10:00,590
Estos abultamientos son necesarios. Aquí se puede ver un poquito de pegamento, pero eso no nos importa.

81
00:10:03,591 --> 00:10:05,250
Es un poco difícil pegarlos.

82
00:10:07,250 --> 00:10:10,260
Siempre usad toallitas húmedas para los bebés, que son el instrumento muy importante.

83
00:10:17,700 --> 00:10:21,080
Vamos ahora a hacer una almazuela.

84
00:10:23,900 --> 00:10:27,370
Presionad con más fuerza el papel para que se pueda adherir más fácil.

85
00:10:42,570 --> 00:10:48,120
Recordad que no sometéis al frío su material, porque si se congelan no podréis usarlo más.

86
00:10:48,121 --> 00:10:50,080
Me pasó eso hoy.

87
00:10:57,081 --> 00:11:01,710
La posición del papel es muy importante para nosotros.

88
00:11:01,711 --> 00:11:08,470
Es necesario que los papeles se superponen.

89
00:11:08,670 --> 00:11:17,710
Hacemos la posición y es importante dónde ponemos cual pieza.

90
00:11:17,910 --> 00:11:24,639
Siempre limpiad el exceso de pegamento.

91
00:11:31,839 --> 00:11:42,370
Más tutoriales se van a hacer, muchas cosas van a mejorarse.

92
00:11:42,371 --> 00:11:48,590
Esto es sólo el principio.

93
00:11:48,591 --> 00:12:02,490
Espero que los tutoriales os gusten y que vayan a estar contentos con el contenido y la calidad del vídeo.

94
00:12:02,491 --> 00:12:10,700
Primero he utilizado los papeles más grandes y he cubierto los bordes de las piezas más grandes.

95
00:12:46,900 --> 00:12:53,850
Nuestra almazuela está terminada y el vídeo se ha acelerado para ahorrar el tiempo.

96
00:12:54,050 --> 00:13:01,080
Como podéis ver, no es solo con la técnica, sino también es imporante la posición del papel.

97
00:13:01,081 --> 00:13:05,750
Los papeles ponemos cruzados y evitamos que se encuentren uno junto al otro.

98
00:13:05,751 --> 00:13:08,690
Me da igual que algunos bordes estén un poco elevados.

99
00:13:08,691 --> 00:13:11,840
Vamos a utilizar los clavos decorativos para bajarlos y también para decorar lo hecho.

100
00:13:11,841 --> 00:13:16,580
Le da un muy buen efecto final aunque no todos los papeles están bien adheridos.

101
00:13:16,581 --> 00:13:19,010
Ahora no voy a procesar los bordes de la tabla de madera.

102
00:13:19,011 --> 00:13:22,950
Más tarde voy a poner la pintura al óleo, probablemente negra, o se me ocurrirá otra cosa.

103
00:13:22,951 --> 00:13:26,600
Ahora vamos a poner esto al lado y vamos a empezar trabajar con la arcilla.

104
00:13:34,060 --> 00:13:41,420
Pintaremos la arcilla, es decir, por todas la partes.

105
00:13:41,421 --> 00:13:55,730
Utilizaremos el color turquesa acrílica de Stamperia Allegro, el código KAL 109

106
00:14:00,000 --> 00:14:04,600
y comenzamos a pintar con el pincel suave.

107
00:14:06,601 --> 00:14:09,360
Las alas serán de color turquesa,

108
00:14:09,361 --> 00:14:11,790
y los que me conocen, saben que es mi favorito color.

109
00:14:17,791 --> 00:14:23,400
Es muy importante que el color cubre todo, especialmente huecos en las alas.

110
00:14:50,090 --> 00:14:55,350
Ahora vamos a cambiar el color con la cual pintaremos el reloj, que es el parte central, y mis iniciales.

111
00:14:55,351 --> 00:15:10,170
Otra vez el color amarilla acrílica de Stamperia Allegro. Albicoca, el código KAL 60.

112
00:15:10,171 --> 00:15:20,700
Es muy importante combinar los colores pero también es importante y su contraste.

113
00:15:20,701 --> 00:15:27,250
Ahora necesitamos un color que es el contraste del anterior color, pero que también será el contraste para el fondo que es de color de la piel.

114
00:15:27,251 --> 00:15:33,920
A veces los colores y los contrastes pueden parecer demasiado fuertes,

115
00:15:33,921 --> 00:15:38,080
pero no olvidéis que vamos a pasar por encima de ellos con pintura al óleo, lo que nos dará un tono completamente diferente, que realmente queremos conseguir.

116
00:15:38,280 --> 00:15:43,851
Por lo tanto, los colores van a cambiarse y no tengáis miedo de usar colores intensos.

117
00:15:55,000 --> 00:16:05,520
Terminamos con pintar, y ahora vamos a juntar las piezas de arcilla y secarlas.

118
00:16:39,710 --> 00:16:53,610
Entonces vamos a tomar una esponja sintética con agujeros, que es mucho más barato que una verdadera esponja de mar. Se puede comprar en las farmacias.

119
00:16:55,611 --> 00:16:59,530
Y el color acrílica Terra di Sienna de Stamperia Allegro.

120
00:16:59,531 --> 00:17:07,440
El código KAL74, tono de color naranja.

121
00:17:07,441 --> 00:17:13,000
Tened en la cuenta que cada proyecto que se está grabando se hace por la primera vez,

122
00:17:13,001 --> 00:17:18,810
que jamás habrían sido hechos, y por eso durante la grabación creamos y reaccionamos

123
00:17:21,000 --> 00:17:29,720
No quiero mostraros los proyectos terminados , que los reproducimos, corregimos los errores y cosas similares.

124
00:17:29,920 --> 00:17:39,300
Qué quiero hacer con la esponja…tomo una pequeña cantidad de color,

125
00:17:39,301 --> 00:17:43,470
para poder hacer las marcas de color naranja sobre la arcilla.

126
00:17:53,471 --> 00:18:11,560
Suavemente toco la arcilla con esponja para que no queda demasiado color, quiero conseguir el efecto de herrumbre en algunos lugares.

127
00:18:15,561 --> 00:18:23,100
Utilizo esta esponja porque deja un rastro desigual debido a los agujeros.

128
00:18:30,101 --> 00:18:37,880
Como podéis ver, elijo dónde pongo el color, pero ciertamente no lo pongo en todas las partes.

129
00:18:37,881 --> 00:18:54,870
Si queremos que el color sale dónde con la esponja no se puede, utilizaremos el pincel.

130
00:19:00,020 --> 00:19:03,050
Siempre dad palmaditas…no arrastréis pincel o esponja.

131
00:19:03,051 --> 00:19:06,980
Con la esponja podemos corregir todo lo que no nos gusta.

132
00:19:07,981 --> 00:19:11,430
Poned en algunas partes,

133
00:19:15,431 --> 00:19:18,990
y no tengáis miedo, después de la pintura al óleo el tono cambiará.

134
00:19:18,991 --> 00:19:28,080
Es muy importante que os relajáis. Sed cool, todo esto hacemos para sentirnos relajados.

135
00:19:31,480 --> 00:19:40,200
Bien, con el color acrílico hemos conseguido la herrumbre, sin productos químicos. Lavad el pincel y secad el color.

136
00:20:14,400 --> 00:20:26,060
Ahora es el momento para la pintura al óleo. Necesitamos un trozo de papel de aluminio que usáis en su hogar.

137
00:20:26,061 --> 00:20:27,940
Básicamente vamos a utilizar muchas pinturas al óleo,

138
00:20:27,941 --> 00:20:34,470
pero hoy usamos Burnt Umber pintura al óleo.

139
00:20:34,471 --> 00:20:43,150
Me gusta porque es un clásico entre las pinturas al óleo.

140
00:20:43,151 --> 00:20:54,060
Me gustan Van Gogh pinturas al óleo. Empezamos ya con la aplicación de óleo.

141
00:20:54,061 --> 00:21:08,000
Al contrario de María que diluye las pinturas al óleo y lo borra con un paño de tela,

142
00:21:08,001 --> 00:21:16,450
yo prefiero la pintura al óleo no diluida. Necesitamos un pincel duro de pelo porcino.

143
00:21:16,451 --> 00:21:28,980
Elegid la forma de pincel que más os convenga.

144
00:21:28,981 --> 00:21:36,610
Tomad una pequeña cantidad de la pintura al óleo porque de otra manera vais a tener problema con quitar el color.

145
00:21:36,611 --> 00:21:37,940
Cubrimos todo el objeto, ¿veréis?

146
00:21:37,941 --> 00:21:44,210
Especialmente los huecos, pero no pongáis demasiado color.

147
00:21:44,410 --> 00:21:55,080
Todo lo que sobra manchadlo con el pincel.

148
00:21:56,000 --> 00:21:59,480
Ahora vamos a acelerar…y nos vemos pronto…

149
00:22:26,500 --> 00:22:39,120
Ahí, hemos cubierto todo el trozo de arcilla con la pintura al óleo, las dos alas y el reloj.

150
00:22:39,121 --> 00:22:44,130
Ahora con las toallitas húmedas, el instrumento principal, borramos la pintura al óleo.

151
00:22:44,131 --> 00:22:59,800
Borramos poco a poco, hacemos una bola de toallitas húmedas y quitamos la pintura al óleo.

152
00:23:08,801 --> 00:23:19,100
Como podéis ver, en los huecos queda más color,
porque ahí no prensamos y el color queda.

153
00:23:19,101 --> 00:23:23,570
Cuando queréis quitar más color, es necesario presionar un poco fuerte la toallita mientras lo borráis.

154
00:23:23,571 --> 00:23:33,350
Ya veréis que en algunos lugares aparece el color naranja, color de herrumbre.

155
00:23:37,351 --> 00:23:40,020
Como podéis ver, ¡el resultado es fantástico!

156
00:23:45,130 --> 00:23:51,440
Después de aplicar la pintura al óleo por todas las partes de la arcilla, suavemente quitamos el color.

157
00:23:51,441 --> 00:23:55,490
Eso es lo que siempre recordamos en los talleres, esta es la parte en que debíais disfrutar.

158
00:23:55,491 --> 00:23:42,120
Esta parte estábamos esperando. Si algo no nos gusta,

159
00:24:11,900 --> 00:24:31,090
podemos aplicar más pintura al óleo y corregirlo con los dedos o cambiar la capa.

160
00:24:31,091 --> 00:24:36,500
Básicamente el color naranja no ha hecho un contraste fuerte.

161
00:24:36,501 --> 00:24:43,820
La diferencia existe sin duda, pero no es muy grande. Como quería destacar los números ,

162
00:24:43,821 --> 00:24:56,980
y dejarlos a ser más oscuros.

163
00:24:56,981 --> 00:25:02,950
Estoy jugando con la pintura al óleo. No importa si sobra en las alas.

164
00:25:02,951 --> 00:25:08,070
En realidad, eso es lo que quería conseguir. ¡Mirad el resultado! Ahora paso a la segunda ala.

165
00:25:08,071 --> 00:25:20,199
Mirad como quito la pintura al óleo. Los movimientos no son hacia atrás y adelante, sino desde el interior hacia el exterior.

166
00:25:20,200 --> 00:25:24,400
Disminuyo la presión a donde quiero más pintura al óleo,

167
00:25:24,401 --> 00:25:27,720
y aumento la presión donde quiero quitar la pintura al óleo.

168
00:25:27,721 --> 00:25:31,560
Como podéis ver, he usado solamente dos toallitas húmedas porque siempre las doblaba y usaba el parte limpio de la toallita.

169
00:25:31,561 --> 00:25:39,380
Dos son suficiente para este proyecto.

170
00:26:08,619 --> 00:26:15,400
Ahora tengo que juntar los trozos de arcilla y comprobar si hay partes que quedaron claros.

171
00:26:15,401 --> 00:26:24,630
Los bordes de alas deben quedar claros.

172
00:26:43,500 --> 00:26:46,880
Bien, una pequeña parte es…

173
00:27:03,200 --> 00:27:04,280
Y como podéis ver tenemos el resultado final.

174
00:27:04,281 --> 00:27:12,650
Si vemos unos partes azules, solo añadimos más pintura al óleo.

175
00:27:12,651 --> 00:27:15,049
Existe la posibilidad que en algunas partes aparece el color blanco de arcilla como consecuencia de borrado.

176
00:27:17,361 --> 00:27:29,110
Podemos dejarlo así, o añadir en esos partes más óleo con los dedos.

177
00:27:32,321 --> 00:27:37,230
Yo personalmente amo "aquí y allá" el efecto de blanco.

178
00:27:42,361 --> 00:27:46,210
De nuevo, con los dedos podéis añadir el óleo y ensombrecer los partes.

179
00:27:48,100 --> 00:27:52,150
Yo por aquí quité el color y la arcilla asoma.

180
00:28:01,000 --> 00:28:09,480
Si queremos después de borrar con las toallitas húmedas añadir un poco más de la pintura al óleo,

181
00:28:09,481 --> 00:28:12,250
tendremos que limpiar la superficie con la toallita de papel.

182
00:28:34,251 --> 00:28:42,900
Ahora vamos a ver qué hemos hecho y vamos a colocar nuestra tabla de madera cubierta de piel.

183
00:28:49,000 --> 00:28:56,310
El primer proyecto es bastante grande, pero eso fue mi deseo, que María haga un pedazo tan grande.

184
00:28:58,600 --> 00:29:03,790
María, si estas mirando esto, muchas gracias, cuídate y nos vemos pronto.

185
00:29:22,000 --> 00:29:27,700
Vamos ya a juntar todas las piezas.

186
00:29:28,701 --> 00:29:34,240
Claro, esto no debía ser roto, pero hemos encontrado una solución y lo hemos resuelto.

187
00:29:38,000 --> 00:29:47,580
Como podéis ver, el parte central-el reloj, aunque estaba claro se adaptó en el color de la piel.

188
00:29:47,581 --> 00:29:49,251
Ahora podemos ver y el color naranja.

189
00:29:51,252 --> 00:29:57,311
Aunque son de tonos similares, el tono rojo está visible.

190
00:29:57,312 --> 00:30:00,790
 Y esto es el color que inicialmente hemos usado para las alas.

191
00:30:04,000 --> 00:30:15,220
La arcilla vamos a poner en el centro y vamos a adherirla.

192
00:30:31,000 --> 00:30:34,880
Vamos a transladarla con cuidado para no arrugar las piezas de piel.

193
00:30:36,000 --> 00:30:55,890
Aplicamos en los bordes el pegamento extra forte de Stamperia, una mayor cantidad.

194
00:30:58,489 --> 00:30:58,289
En ciertas partes será necesario que añadimos la pintura de óleo, para lograr la intensidad.

195
00:31:15,000 --> 00:31:22,690
La cosa es que movemos un poco la arcilla para que el pegamento de abajo puede lubricar,

196
00:31:22,691 --> 00:31:27,970
por lo tanto se secará más rápido.

197
00:31:27,971 --> 00:31:35,900
Aunque el pegamento se vuelve transparente,

198
00:31:35,901 --> 00:31:38,260
con las toallitas húmedas podéis borrar lo que sobra del pegamento que está debajo de la arcilla.

199
00:31:38,261 --> 00:31:45,880
Con el pincel podéis eliminarlo incluso mejor. 
Ya no se mueve.

200
00:31:51,881 --> 00:32:01,980
Ahora otra pieza. Debemos limpiar bien este lado porque el pegamento no funcionará bien sobre la pintura al óleo.

201
00:32:03,990 --> 00:32:20,040
Limpiad con el pañal y poned el pegamento.

202
00:32:28,360 --> 00:32:30,390
Antes del uso sacudid el pegamento.

203
00:32:33,391 --> 00:32:38,170
Sin duda sería más fácil si mi objeto de arcilla no fuera roto.

204
00:32:38,171 --> 00:32:44,520
Otra vez con la toallita húmeda eliminad el exceso de pegamento.

205
00:33:00,000 --> 00:33:03,860
Otra vez usamos el pegamento Extra Forte de Stamperia para los trozos pequeños.

206
00:33:03,861 --> 00:33:11,780
El pegamento Extra Forte de Stamperia es bastante bueno pero pienso que

207
00:33:11,781 --> 00:33:21,580
el hobby pegamento o el pegamento scrapbook adherirían más o menos igualmente.

208
00:33:23,000 --> 00:33:30,010
María para arcilla usa el pegamento Atlaco.

209
00:33:38,570 --> 00:33:41,400
Este pedazo pequeño se adherirá más difícil.

210
00:33:41,401 --> 00:33:44,210
No importan las pequeñas grietas que aparecen. Aplicamos pintura al óleo,

211
00:33:44,211 --> 00:33:47,150
limpiamos y las grietas desaparecen.

212
00:33:58,130 --> 00:34:10,440
Ahora vamos a proceder el borde de la tabla de madera para que pueda adaptarse en el color de la piel.

213
00:34:14,460 --> 00:34:20,230
Lo mismo haremos con los bordes de papel.

214
00:34:20,231 --> 00:34:34,650
No mucha pintura al óleo, solamente un poquito. Aplicad y frotad con los dedos.

215
00:34:48,700 --> 00:35:04,290
No todos los lados a la vez, sino poco a poco. Dependiendo en que superficie pongo la pintura al óleo.

216
00:35:05,710 --> 00:35:08,080
Lo descoloro con los dedos o con los movimientos circulados o con los movimientos de arriba y abajo.

217
00:35:39,489 --> 00:35:47,760
Aquí está, casi hemos terminado. Compruebo los detalles y tal vez en algunas partes añadiré más pintura al óleo.

218
00:35:47,761 --> 00:35:54,030
Primero limpiad con las toallitas húmedas las manos y después las lavad con el jabón y el agua.

219
00:35:57,031 --> 00:35:58,490
O aún mejor, usad crema de manos para limpiar el óleo de las manos.

220
00:35:58,500 --> 00:36:07,930
Ved lo que vamos a hacer ahora. Añadiremos los clavos decorativos…

221
00:36:28,489 --> 00:36:39,870
Vamos a clavar cada borde del papel. Empiezo a clavar.

222
00:36:39,871 --> 00:36:43,150
Ahora vamos a acelerar pero vuelvo muy pronto.

223
00:37:11,000 --> 00:37:15,240
Aquí estoy,  :) Con el color voy a espolvorear el trozo de arcilla,

224
00:37:15,241 --> 00:37:24,800
usando color marfil, no precisamente blanco, sino diluido con el agua.

225
00:37:39,489 --> 00:37:42,830
Tomo una pequeña cantidad de color para no espolvorear la piel.

226
00:37:42,831 --> 00:37:47,540
Con los dedos, despacio y con cuidado, estoy espolvoreado la arcilla.

227
00:37:47,541 --> 00:38:00,660
Voy a espolvorear más en el centro.

228
00:38:08,000 --> 00:38:19,980
Y las alas un poco.

229
00:38:39,170 --> 00:38:45,770
Si sucede que espolvoreáis la piel, limpiad rápidamente con las toallitas húmedas y todo estará bien.

230
00:39:13,489 --> 00:39:23,050
Y aquí están los resultados. A mí me gusta.

231
00:39:25,051 --> 00:39:29,460
Eso es todo por hoy. Gracias por ver el taller.

232
00:39:29,461 --> 00:39:34,280
Espero que hayan disfrutado. Este es el primer taller.

233
00:39:34,281 --> 00:39:44,930
Trataré de daros más informaciones y detalles para que podáis repetir fácilmente el proyecto de taller

234
00:39:44,931 --> 00:39:49,631
o hacer algo usando su imaginación.

235
00:39:49,632 --> 00:40:00,200
Hasta la próxima semana. Esperamos que los amigos de todo el mundo se unen a nosotros.

236
00:40:00,201 --> 00:40:03,280
Y gracias a María de nuevo por el logo hecho en arcilla.

237
00:40:03,281 --> 00:40:06,190
Os beso y hasta pronto.
