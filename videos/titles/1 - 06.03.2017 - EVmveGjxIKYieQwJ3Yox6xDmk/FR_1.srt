﻿1
00:03:05,716 --> 00:03:11,990
Salut tout le monde ! Bonjour , c'est notre première vidéo sur la page www.handmadefantasyworld.com

2
00:03:12,190 --> 00:03:19,930
C’est vrai que pour certains d’entre vous c’est bonjour ou bonsoir. En fait , chez vous c’est peut-être le matin ou le soir.

3
00:03:20,130 --> 00:03:21,789
Peu importe

4
00:03:21,990 --> 00:03:23,920
merci de vous être inscrits à nos ateliers.

5
00:03:24,120 --> 00:03:26,120
C’est notre premier projet et je vais vous présenter quelque chose de très interesent.

6
00:03:26,320 --> 00:03:31,720
Nous allons travailler sur deux objets. La première partie du projet est le travail sur l’argile.

7
00:03:31,920 --> 00:03:34,120
Plus tard, je vais vous dire quelque chose de plus sur cette pièce en argile.

8
00:03:34,320 --> 00:03:38,600
C’est le logo du site www.handmadefantasyworld.com

9
00:03:38,800 --> 00:03:44,450
Après le travail de l’argile nous allons travailler l’effet du cuir avec les peintures acryliques et d’huile.

10
00:03:44,650 --> 00:03:46,140
Nous allons commencer par poser l’argile.

11
00:03:46,340 --> 00:03:55,720
Je voudrais seulement dire que ce logo est fait en argile.

12
00:03:55,920 --> 00:04:02,430
C’est le cadeau de mon amie Maria Kantzali de Tessalonique.

13
00:04:02,690 --> 00:04:07,670
Je ne peux pas vous décrire tout ce qu’on peut faire en argile.

14
00:04:07,780 --> 00:04:14,401
Si ça vous plaît, vous pouvez regarder encore sur son profil de FacebookMaria Kantzali.

15
00:04:14,601 --> 00:04:20,643
Merci encore une fois à Maria pour son cadeau.

16
00:04:20,843 --> 00:04:27,400
J’espère qu’elle va aimer ce que nous allons réussir à faire sur l’argile.

17
00:04:27,401 --> 00:04:35,730
L’argile se tient prête pour le travail. J’espère que Maria aura l’occasion de nous rejoindre et

18
00:04:35,731 --> 00:04:39,160
de nous montrer comment on travaille avec l’argile.

19
00:04:39,161 --> 00:04:50,620
 Commençons par l’effet du cuir. J’ai cassé cela, mais ça ne va pas nous déranger.

20
00:04:50,621 --> 00:04:57,180
Nous avons aussi un morceau de bois, mais ça peut être n’importe quelle surface, ça dépend de ce que vous avez à la portée de main.

21
00:04:57,181 --> 00:05:00,680
Commençons maintenant avec l’effet. J’ai préparé quelques morceaux et je vais en utiliser dans le projet.

22
00:05:00,681 --> 00:05:16,400
Je vais vous montrer tout le processus sur un morceau brut. Par example sur celui-ci.

23
00:05:16,411 --> 00:05:23,332
Qu’est-ce que c’est? C’est stone paper (papier de pierre) de l’entreprise Stamperia d’Italie.

24
00:05:23,771 --> 00:05:28,380
Il a deux côté, un côté  lisse et un côté un peu rugueux.

25
00:05:28,381 --> 00:05:37,120
En fonction du côté qu’on utilise nous allons obtenir des effets variés. Je préfère ce qu’on obtient sur le côté rugueux.

26
00:05:37,532 --> 00:05:43,630
Nous avons besoin de trois gobelets et trois couleurs aquacolor aussi de l’entreprise Stamperia.

27
00:05:43,631 --> 00:05:58,400
Nous allons utiliser les couleurs suivantes: le jaune (pino), le marron (castanio), et la couleur de peau (cuoio).

28
00:05:58,401 --> 00:06:01,590
Je ne parle pas l’italien alors c’est difficile pour moi de prononcer les noms de couleurs en Italien.

29
00:06:01,591 --> 00:06:12,440
Prenez une molle brosse, tournez le côté rugueux et nous allons commencer par la couleur jaune. Nous allons enduire également toute la surface.

30
00:06:12,441 --> 00:06:25,880
C’est très important que les gestes soient égaux, d’un bord à l’autre et qu’on ne fasse pas de lignes visibles.

31
00:06:26,080 --> 00:06:33,310
Si je commence par les gestes verticaux, je commence du côté gauche et je tire la brosse jusqu’à la fin dans cette direction.

32
00:06:33,311 --> 00:06:42,740
Quand vous enduisez une grande couche  de couleur commencez avec le séchage . On sèche tout très peu.

33
00:06:51,600 --> 00:06:53,650
Je vais répéter ce procédé encore une fois.

34
00:06:53,651 --> 00:06:58,680
La côté rugueux de papier est enduit par la couleur jaune (Aquacolor).

35
00:06:58,681 --> 00:07:04,090
Je l’ai fait également je l’ai séché et nous sommes prêts pour le pas suivant.

36
00:07:04,091 --> 00:07:11,420
Avec la même brosse un peu essuyée avec un bout de papier nous enduisons la couleur suivante , le marron Aquacolor.

37
00:07:11,421 --> 00:07:19,300
Le procédé est le même. Nous passons sur la première couleur qui ne doit pas être totalement séche, seulement un peu.

38
00:07:19,500 --> 00:07:24,300
Un long trait avec la brosse d’un bord à l’autre. C’est très important.

39
00:07:31,500 --> 00:07:35,940
Bien…de longs traits d’un bord à l’autre.

40
00:07:35,941 --> 00:07:39,350
Mettez la brosse dans l’eau et séchez le papier avec un sèche-cheveux.

41
00:07:56,550 --> 00:08:08,410
Maintenant on enduit la troisième peinture la couleur du cuir et on répète le procédé.

42
00:08:09,000 --> 00:08:14,440
Ce n’est pas important si vous avez trop de peinture vous n’avez qu’à bien l’étaler.

43
00:08:14,640 --> 00:08:20,380
Ok… nous avons mis les journaux au-dessous pour ne pas tout salir.

44
00:08:28,000 --> 00:08:30,580
La seule différence dans l’utilisation de la troisième peinture c’est que nous allons mettre deux fois.

45
00:08:30,581 --> 00:08:35,000
Ce n’est pas nécessaire, mais je préfère le résultat après deux couches de peinture.

46
00:09:04,200 --> 00:09:06,870
Encore , une couche…

47
00:09:06,871 --> 00:09:11,990
Je voudrais vous dire encore quelque chose , vous ne voyez que moi sur l’écran mais il y a beaucoup gens autour de moi.

48
00:09:11,991 --> 00:09:15,800
si bien que vous pouvez les entendre parler ou m’entendre, m’adresser à vous.

49
00:09:16,000 --> 00:09:17,320
Ils sont là pour aider s’il y en a besoin.

50
00:09:21,000 --> 00:09:27,900
Si vous les entendez ce n’est pas une erreur nous nous sommes réunis pour nous détendre dans une ambiance conviviale et pour vous offrir le meilleur atelier possible.

51
00:09:27,901 --> 00:09:34,600
La détente et l’qrt ça diminue vraiment le stress.	

52
00:09:35,601 --> 00:09:40,600
J’ai fini avec le cuir et on sèche de nouveau .

53
00:10:10,000 --> 00:10:16,610
C’est beau, voici le résultat de la peinture. Nous avons encore un pas à faire. Ou pour être précis un petit detail.

54
00:10:16,611 --> 00:10:20,600
 Regardez la différence ou faisons le pas suivant pour voir encore mieux.

55
00:10:20,602 --> 00:10:30,270
C’est le cuir que nous avons fait. Reste à froisser le papier.

56
00:10:32,271 --> 00:10:35,850
Ce papier est excellent et nous allons l’utiliser dans les projets. Le papier de pierre excellent produit par Stamperia.

57
00:10:35,851 --> 00:10:40,390
Et voici le résultat final. Nous avons fait une imitation du cuir.

58
00:10:40,391 --> 00:10:44,850
Si on observe ce papier vous verrez que, si on le peint de l’autre côté ,

59
00:10:44,851 --> 00:10:48,230
qui est lisse, on obtient une couleur différente.

60
00:10:48,231 --> 00:10:53,820
C’est parce que le côté lisse est moins absorbant et ça change le résultat.

61
00:10:54,020 --> 00:10:58,210
Voilà ce qui s’est passé. On va utiliser plusieurs morceaux pour couvrir la planche en bois.

62
00:10:58,211 --> 00:11:05,080
C’est vous de décider quel côté vous allez utiliser. Moi je préfère travailler le côté rugueux.

63
00:11:05,081 --> 00:11:10,630
Nous sommes prêts pour le pas suivant. On commence par un morceau de bois.

64
00:11:10,631 --> 00:11:18,100
Un peu de café de vrais Grecs ne s’en passeraient jamais.

65
00:11:19,102 --> 00:11:22,210
Maintenant maman va dire « mon petit garçon boit du café , c’est pas possible ! »

66
00:11:22,211 --> 00:11:28,848
C’est pour le café que nous avons fait beaucopu de pauses aux ateliers, pauses café.

67
00:11:29,048 --> 00:11:40,219
Nous allons faire un patchwork sur la surface en bois avec les bouts de papier qui imitent le cuir.

68
00:11:40,220 --> 00:11:46,210
Ce sera la base pour le logo en argile.

69
00:11:46,211 --> 00:11:52,370
Nous n’allons pas coller le papier jusqu’au bord de la planche en bois.

70
00:11:52,371 --> 00:11:56,390
Nous allons laisser quelques millimètres libres.

71
00:11:56,395 --> 00:12:00,660
Nous allons laisser quelques millimètres libres.

72
00:12:00,661 --> 00:12:03,130
Nous allons utiliser beaucoup de produits de Stamperia.

73
00:12:03,131 --> 00:12:08,230
à part que moi personnellement j’aime leurs produits , ils sont excellents pour notre premier projet.

74
00:12:08,231 --> 00:12:12,270
Nous avons fait ça avec la colle Extra Forte.

75
00:12:13,271 --> 00:12:15,520
D’accord,  je dois appuyer plus fort.

76
00:12:15,521 --> 00:12:27,970
Nous en mettons un peu sur les bords et au centre.

77
00:12:27,971 --> 00:12:35,280
Puis nous collons le papier sur le bois.

78
00:12:42,281 --> 00:12:46,580
Vous devez être patients avec le papier parce qu’il est un peu roulé et alors on ne le colle pas facilement.

79
00:12:46,581 --> 00:12:50,320
Si nous n’avions pas froissé le papier et si nous n’avions pas fait ces lignes sinueuses nous n’aurions pas obtenu l’effet de cuir.

80
00:12:50,321 --> 00:12:57,590
Cet aspect rugueux est nécessaire. On peut voir un peu de colle ici, mais ça ne nous dérange pas.

81
00:13:00,591 --> 00:13:02,250
C’est un peu compliqué à coller.

82
00:13:04,250 --> 00:13:07,260
Utilisez toujours des lingettes bébé c’est un outil très important pour nous.

83
00:13:14,700 --> 00:13:18,080
Faisons le patchwork maintenant.

84
00:13:20,900 --> 00:13:24,370
Pressez plus fort le papier pour qu’il colle plus vite.

85
00:13:39,570 --> 00:13:45,120
N’exposez pas votre matériel au froid, s’il gèle, vous ne pouvez plus l’utiliser.

86
00:13:45,121 --> 00:13:47,080
C’est ce qui m’est arrivé aujourd ‘hui.

87
00:13:54,081 --> 00:13:58,710
La position du papier est extrêmement importante.

88
00:13:58,711 --> 00:14:05,470
Il faut que les papiers soient superposés.

89
00:14:05,670 --> 00:14:14,710
Nous faisons une composition et il est important où nous allons mettre le papier.

90
00:14:14,910 --> 00:14:21,639
Essuyez toujours la colle en trop.

91
00:14:28,839 --> 00:14:39,370
Il vont faire encore beaucoup de tutoriels et mettre au point beaucoup de chose.

92
00:14:39,371 --> 00:14:45,590
Ce n’est qu’un début.

93
00:14:45,591 --> 00:14:59,490
J’espère que ces tutoriels vont vous plaire et que vous serez content du contenu et de la qualité de cette vidéo.

94
00:14:59,491 --> 00:15:07,700
Tout d’abord j’ai utilisé les papiers plus grands et j’ai couvert les bords des pièces plus grandes.

95
00:15:43,900 --> 00:15:50,850
Notre patchwork est fini et nous avons accéléré la vidéo pour économiser le temps.

96
00:15:51,050 --> 00:15:58,080
Comme vous pouvez voir ce n’est pas seulement une question de technique, la position du papier compte aussi.

97
00:15:58,081 --> 00:16:02,750
On met les papiers en croix et on évite de mettre les deux l’un à côté de l’autre.

98
00:16:02,751 --> 00:16:05,690
Je ne m’inquiète pas que quelques bords soient un peu relevés.

99
00:16:05,691 --> 00:16:08,840
Nous allons utiliser des clous décoratifs pour les baisser et pour décorer le travail en même temps.

100
00:16:08,841 --> 00:16:13,580
ça donne un effet final très beau même si tous les papiers ne sont pas bien collés.

101
00:16:13,581 --> 00:16:16,010
Je ne vais pas travailler les bords de la planche en bois pour le moment.

102
00:16:16,011 --> 00:16:19,950
Plus tard je vais mettre la peinture d’huile , noire probablement ou je vais inviter autre chose.

103
00:16:19,951 --> 00:16:23,600
On va mettre ça de côté et on va commencer à travailler l’argile.

104
00:16:31,060 --> 00:16:38,420
Nous allons repeindre l’argile entier.

105
00:16:38,421 --> 00:16:52,730
Nous allons utiliser la peinture acrylique turquoise Stamperia Allegro , chiffre KAL109 .

106
00:16:57,000 --> 00:17:01,600
Nous commençons la peinture avec la brosse douce.

107
00:17:03,601 --> 00:17:06,360
Les ailes seront de couleur turquoise.

108
00:17:06,361 --> 00:17:08,790
Ceux qui me connaissent savent que c’est ma couleur préférée.

109
00:17:14,791 --> 00:17:20,400
Il est très important que la peinture couvre tout surtout les parties sous les ailes.

110
00:17:47,090 --> 00:17:52,350
Maintenant nous allons changer de peinture qu’on va utiliser pour l’horloge, qui est au centre et mes initiales aussi.

111
00:17:52,351 --> 00:18:07,170
Encore une fois la peinture acrylique Stamperia Allegro mais jaune, Albicoca chiffre KAL60.

112
00:18:07,171 --> 00:18:17,700
Il est important de bien assortir les couleurs mais leur contraste est important aussi.

113
00:18:17,701 --> 00:18:24,250
Maintenant on a besoin d’une couleur qui fera le contraste de la couleur précédente mais aussi le contraste de la base couleur de peau.

114
00:18:24,251 --> 00:18:30,920
Parfois les couleurs et les contrastes ont l’air trop forts.

115
00:18:30,921 --> 00:18:35,080
Mais n’oubliez pas que nous allons les repeindre des peintures d’huile ce qui va produire un effet tout à fait différent, l’effet qu’on veut réellement créer.

116
00:18:35,280 --> 00:18:40,851
Donc , les couleurs vont changer, n’ayez pas peur d’utiliser les nuances intensives.

117
00:18:52,000 --> 00:19:02,520
Nous avons fini de peindre on va coller les morceaux d’argile et on va les sécher.

118
00:19:36,710 --> 00:19:50,610
On va prendre une éponge synthétique avec les trous , il est beaucoup moins cher que la vraie éponge de  mer. On peut l’acheter dans les parfumeries.

119
00:19:52,611 --> 00:19:56,530
Et la couleur acrylique Stamperia Allegro terra di Sienna.

120
00:19:56,531 --> 00:20:04,440
Chiffre KAL74 le ton orange.

121
00:20:04,441 --> 00:20:10,000
Ayez à l’esprit que tous les projets filmés sont faits pour la première fois.

122
00:20:10,001 --> 00:20:15,810
Qu’ils n’ont jamais été faits avant si bien que pendant le tournage, on crée et on réagit.

123
00:20:18,000 --> 00:20:26,720
Je ne veux pas vous montrer des projets finis, nous n’allons pas les reproduire ni corriger les erreurs.

124
00:20:26,920 --> 00:20:36,300
Qu’est-ce que je veux faire avec l’éponge….. je prends une petite quantité d’eau.

125
00:20:36,301 --> 00:20:40,470
pour faire des traces orange sur l’argile

126
00:20:50,471 --> 00:21:08,560
Je touche doucement l’argile avec l’éponge pour ne pas laisser trop de peinture dessus, je veux faire l’effet de rouille à certains endroits.

127
00:21:12,561 --> 00:21:20,100
Je me sers de cette éponge parce qu’elle laisse une trace irrégulière à cause de ses trous.

128
00:21:27,101 --> 00:21:34,880
Comme vous pouvez voir, je choisis où je vais mettre de la peinture, mais bien sûr, pas partout.

129
00:21:34,881 --> 00:21:51,870
Si on veut mettre de la peinture aux endroits que l’éponge ne peut pas atteindre, on va utiliser la brosse.

130
00:21:57,020 --> 00:22:00,050
Tapotez toujours, ne tirez pas la brosse ou l’éponge.

131
00:22:00,051 --> 00:22:03,980
Avec l’éponge on peut corriger tout ce qui ne nous plaît pas.

132
00:22:04,981 --> 00:22:08,430
Mettez-en par-ci par-là,

133
00:22:12,431 --> 00:22:15,990
et n’ayez pas peur, après la peinture d’huile, le ton va changer.

134
00:22:15,991 --> 00:22:25,080
Il est très important de vous détendre. Soyez cool, on fait tout ça pour nous relaxer.

135
00:22:28,480 --> 00:22:37,200
Bien, à l’aide de la peinture acrylique, on a obtenu l’effet de rouille, sans les produits chimiques. Lavez la brosse et séchez la peinture.

136
00:23:11,400 --> 00:23:23,060
Maintenant, il est temps d’utiliser la peinture d’huile. Nous avons besoin d’une feuille d’aluminium que vous utilisez à la maison.

137
00:23:23,061 --> 00:23:24,940
En fait, on va utiliser beaucoup de peintures d’huile,

138
00:23:24,941 --> 00:23:31,470
mais aujourd’hui on utilise la peinture d’huile Burnt Umber.

139
00:23:31,471 --> 00:23:40,150
J’aime la peinture d’huile Burnt Umber, c’est l’un des classique parmi ces peintures.

140
00:23:40,151 --> 00:23:51,060
J’aime les peintures d’huile Van Gogh. Commençons par enduire les peintures.

141
00:23:51,061 --> 00:24:05,000
A la différence de Maria qui dilue les peintures d’huile à l’aide des morceaux de tissus,

142
00:24:05,001 --> 00:24:13,450
moi, je préfère la peinture d’huile non diluée. Nous avons besoin d’une brosse dure, en poil de cochon.

143
00:24:13,451 --> 00:24:25,980
Choisissez la forme de la brosse qui vous convient le mieux.

144
00:24:25,981 --> 00:24:33,610
Prenez une petite quantité de peinture d’huile, autrement, vous aurez du mal à l’enlever.

145
00:24:33,611 --> 00:24:34,940
On couvre tout l’objet, vous voyez ?

146
00:24:34,941 --> 00:24:41,210
Surtout les enfoncements, mais ne mettez pas trop de peinture.

147
00:24:41,410 --> 00:24:52,080
Etalez la peinture en trop avec la brosse.

148
00:24:53,000 --> 00:24:56,480
Maintenant, on va accélérer la vidéo…. A bientôt…

149
00:25:23,500 --> 00:25:36,120
Voilà, nous avons couvert tout le morceau d’argile de peinture d’huile, les deux ailes et l’horloge.

150
00:25:36,121 --> 00:25:41,130
Maintenant, on utilise les lingettes bébé, notre outil principal, pour essuyer la peinture d’huile.

151
00:25:41,131 --> 00:25:56,800
On essuie une partie après l’autre, on fait une petite boule de lingettes bébé et on retire la peinture d’huile.

152
00:26:05,801 --> 00:26:16,100
Comme vous pouvez voir, il reste plus de peinture dans les enfoncements, parce qu’on n’y appuie pas trop et que la peinture se maintient.

153
00:26:16,101 --> 00:26:20,570
Quand vous voulez retirer plus de peinture, il faut appuyer plus fort la lingette pendant que vous essuyez la surface.

154
00:26:20,571 --> 00:26:30,350
Vous voyez qu’à certains endroits, il y a la couleur orange, la couleur de rouille.

155
00:26:34,351 --> 00:26:37,020
Comme vous pouvez voir, le résultat est fantastique !

156
00:26:42,130 --> 00:26:48,440
Après avoir enduit de la peinture d’huile sur tout l’objet en argile, on retire la peinture doucement

157
00:26:48,441 --> 00:26:52,490
C’est ce sur quoi j’attire votre attention à tous les ateliers, c’est la partie pendant laquelle vous devez prendre plaisir.

158
00:26:52,491 --> 00:26:39,120
C’est la partie que nous attendions. S’il y a quelque chose qui ne nous plaît pas,

159
00:27:08,900 --> 00:27:28,090
nous pouvons mettre encore de peinture d’huile des doigts et corriger ou changer la couche de peinture d’huile.

160
00:27:28,091 --> 00:27:33,500
En réalité, la couleur orange n’a pas fait un grand contraste.

161
00:27:33,501 --> 00:27:40,820
En tout cas, il y a une différence, mais pas si grande que ça. Comme je voulais accentuer les numéros et les laisser plus foncés

162
00:27:40,821 --> 00:27:53,980
mes gestes sont plus doux dans cette étape et c’est là que la couleur reste plus foncée.

163
00:27:53,981 --> 00:27:59,950
Je joue avec de la peinture d’huile. Ça ne me dérange pas s’il y en a trop sur les ailes.

164
00:27:59,951 --> 00:28:05,070
En fait, c’est ce que je voulais obtenir. Regardez le résultat ! Maintenant on passe à l’aile suivante.

165
00:28:05,071 --> 00:28:17,199
Regardez comment je retire la peinture d’huile. Je ne fais pas de mouvement en avant et en arrière, mais du centre vers l’extérieur.

166
00:28:17,200 --> 00:28:21,400
J’appuie moins fort là où je veux plus de peinture.

167
00:28:21,401 --> 00:28:24,720
et j’appuie plus fort là où je veux retirer plus de peinture d’huile.

168
00:28:24,721 --> 00:28:28,560
Comme vous pouvez voir, je n’ai utilisé que deux lingettes bébé mais je les pliais tout le temps et j’utilisais que la partie propre de la lingette.

169
00:28:28,561 --> 00:28:36,380
Les deux suffisent pour tout le projet.

170
00:29:05,619 --> 00:29:12,400
Maintenant, je dois mettre ensemble toutes les parties d’argile et vérifier s’il y a des parties qui sont restées claires.

171
00:29:12,401 --> 00:29:21,630
Les bords des ailes doivent rester plus clairs.

172
00:29:40,500 --> 00:29:43,880
Ok, une petite partie est ….

173
00:30:00,200 --> 00:30:01,280
Et comme vous pouvez voir, nous avons le résultat final.

174
00:30:01,281 --> 00:30:09,650
Si on voit quelques parties qui sont bleues, on ne fait qu’ajouter de la peinture d’huile.

175
00:30:09,651 --> 00:30:12,049
Il y a une possibilité que  la couleur blanche de l’argile apparaisse comme résultat de l’essuie.

176
00:30:14,361 --> 00:30:26,110
On peut la laisser telle qu’elle est ou mettre de l’huile des doigts sur ces parties.

177
00:30:29,321 --> 00:30:34,230
Je préfère l’effet blanc « par-ci par-là »

178
00:30:39,361 --> 00:30:43,210
Et de nouveau, vous pouvez mettre de la peinture d’huile des doigts et rendre ces parties plus foncées.

179
00:30:45,100 --> 00:30:49,150
J’ai retiré la couleur ici et on peut deviner l’argile.

180
00:30:58,000 --> 00:31:06,480
Si on veut enduire encore de peinture d’huile après avoir essuyé avec des lingettes

181
00:31:06,481 --> 00:31:09,250
on doit en effacer la surface avec une serviette.

182
00:31:31,251 --> 00:31:39,900
Maintenant, on verra ce qu’on a fait et on mettra notre produit sur le socle en bois couvert de cuir.

183
00:31:46,000 --> 00:31:53,310
Le premier projet est assez grand, mais c’était ce que je voulais, je voulais que Marie fasse un morceau aussi grand.

184
00:31:55,600 --> 00:32:00,790
Marie, si tu nous regarde maintenant, merci beaucoup, prends soin de toi et on se voit bientôt.

185
00:32:19,000 --> 00:32:24,700
Maintenant, on va mettre tous les morceaux ensemble.

186
00:32:25,701 --> 00:32:31,240
Bien sûr, ça ne devait pas être cassé, mais nous nous sommes débrouillé et nous avons réglé le problème.

187
00:32:35,000 --> 00:32:44,580
Comme vous pouvez voir, la partie centrale – l’horloge, même si elle était de couleur claire va très bien avec la couleur de cuir.

188
00:32:44,581 --> 00:32:46,251
Maintenant, vous pouvez voir aussi la couleur orange.

189
00:32:48,252 --> 00:32:54,311
Même si les couleurs ont des nuances similaires, le ton rouge est assez visible.

190
00:32:54,312 --> 00:32:57,790
Et c’est la peinture que nous avons utilisée pour les ailes.

191
00:33:01,000 --> 00:33:12,220
Et c’est la peinture que nous avons utilisée pour les ailes.

192
00:33:28,000 --> 00:33:31,880
On va le bouger attentivement pour ne pas chiffonner les parties de cuir.

193
00:33:33,000 --> 00:33:52,890
On met de la colle Extra Forte de Stamperia aux bords, on en met une grande quantité.

194
00:33:55,489 --> 00:33:55,289
Sur certaines parties, il va falloir ajouter de la peinture d’huile pour avoir un effet plus intensif.

195
00:34:12,000 --> 00:34:19,690
Je vous donne une petite astuce – il faut bouger l’argile pour que la colle sous le produit soit bien étalée.

196
00:34:19,691 --> 00:34:24,970
Du coup, elle va sécher plus vite.

197
00:34:24,971 --> 00:34:32,900
Bien que la colle devienne plus transparente,

198
00:34:32,901 --> 00:34:35,260
essuyez la colle en trop qui déborde sous l’argile avec les lingettes

199
00:34:35,261 --> 00:34:42,880
vous allez encore mieux retirer la colle en trop à l’aide d’une brosse.
Ça ne bouge plus.

200
00:34:48,881 --> 00:34:58,980
Maintenant, on passe au deuxième morceau. On doit bien essuyer ce côté car la colle ne va pas bien coller par-dessus la peinture d’huile.

201
00:35:00,990 --> 00:35:17,040
Essuyez avec une serviette et mettez de la colle.

202
00:35:25,360 --> 00:35:27,390
Secouez la colle avant de l’utiliser.

203
00:35:30,391 --> 00:35:35,170
Ce serait beaucoup plus facile si mon produit en argile n’était pas cassé.

204
00:35:35,171 --> 00:35:41,520
Essuyez de nouveau la colle en trop avec une lingette.

205
00:35:57,000 --> 00:36:00,860
On utilise de nouveau la colle pour les petits morceaux Extra Forte de Stamperia.

206
00:36:00,861 --> 00:36:08,780
La colle Extra Forte de Stamperia est très bonne, mais je pense aussi que ….

207
00:36:08,781 --> 00:36:18,580
que la colle hobby ou scrapbook colleraient assez bien.

208
00:36:20,000 --> 00:36:27,010
Maria utilise la colle Atlacol pour l’argile.

209
00:36:35,570 --> 00:36:38,400
Ce petit morceau va coller plus difficilement.

210
00:36:38,401 --> 00:36:41,210
Je ne m’inquiète pas pour les petites fissures qui apparaissent. On met de la peinture d’huile….

211
00:36:41,211 --> 00:36:44,150
on essuie et les fissures disparaissent.

212
00:36:55,130 --> 00:37:07,440
Maintenant, on va travailler le bord de la planche en bois pour qu’elle se marie bien avec la couleur de cuir.

213
00:37:11,460 --> 00:37:17,230
On va faire de même avec les bords du papier.

214
00:37:17,231 --> 00:37:31,650
Pas trop de peinture d’huile, juste un petit peu. Mettez-en et frottez des doigts.

215
00:37:45,700 --> 00:38:01,290
Pas toute la quantité en même temps. En fonction de la surface sur laquelle je mets de la peinture d’huile

216
00:38:02,710 --> 00:38:05,080
Je la rends plus claire des doigts ou en faisant des gestes circulaires ou des mouvements du haut en bas

217
00:38:36,489 --> 00:38:44,760
Voilà, nous avons presque fini. On vérifie les détails, je vais peut-être ajouter de la peinture d’huile à certains endroits.

218
00:38:44,761 --> 00:38:51,030
Essuyez-vous les mains avec une lingette, puis lavez-vous avec du savon et de l’eau.

219
00:38:54,031 --> 00:38:55,490
Ou encore mieux, utilisez une crème pour les mains afin d’enlever la peinture de l’huile.

220
00:38:55,500 --> 00:39:04,930
Regardez ce que nous allons faire maintenant. Nous allons mettre des clous décoratifs.

221
00:39:25,489 --> 00:39:36,870
On va enfoncer des clous sur chaque coin du papier. Je commence à enfoncer…

222
00:39:36,871 --> 00:39:40,150
Maintenant, on va accélérer, mais je reviens dans quelques secondes.

223
00:40:08,000 --> 00:40:12,240
Me voilà. Je vais arroser de la peinture sur le morceau d’argile.

224
00:40:12,241 --> 00:40:21,800
en utilisant la couleur d’ivoire, et pas la couleur blanche, diluée avec de l’eau.

225
00:40:36,489 --> 00:40:39,830
Je prends une petite quantité d’eau pour ne pas tacher le cuir.

226
00:40:39,831 --> 00:40:44,540
J’arrose l’argile des doigts, doucement et attentivement.

227
00:40:44,541 --> 00:40:57,660
Je vais mieux arroser le centre du morceau en argile.

228
00:41:05,000 --> 00:41:16,980
Alors que les ailes, je les arrose tout un petit peu.

229
00:41:36,170 --> 00:41:42,770
S’il vous arrive de tacher le cuir, essuyez-le vite avec des lingettes et tout ira bien.

230
00:42:10,489 --> 00:42:20,050
Et voici le résultat. Ça me plaît à moi.

231
00:42:22,051 --> 00:42:26,460
C’est tout pour aujourd’hui. Merci d’avoir regardé cet atelier.

232
00:42:26,461 --> 00:42:31,280
J’espère que vous en avez profité. C’est le premier atelier.

233
00:42:31,281 --> 00:42:41,930
Je vais m’efforcer à vous donner le plus d’informations et de détails possible afin que vous répétiez plus facilement le projet de notre atelier 

234
00:42:41,931 --> 00:42:46,631
ou que vous fassiez quelque chose grâce à votre imagination.

235
00:42:46,632 --> 00:42:57,200
On se voit la semaine prochaine. Nous attendons des amis du monde entier pour qu’ils nous rejoignent.

236
00:42:57,201 --> 00:43:00,280
Et merci à Maria encore une fois pour le logo fait en argile.

237
00:43:00,281 --> 00:43:03,190
Je vous embrasse, à bientôt !
