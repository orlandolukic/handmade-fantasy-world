1
00:00:08,716 --> 00:00:14,990
Zdravo, svima! Dobar dan, ovo je naš prvi video na stranici www.handmadefantasyworld.com

2
00:00:15,190 --> 00:00:22,930
Ustvari, možda je kod vas jutro ili veče.

3
00:00:23,130 --> 00:00:24,789
Bez obzira,

4
00:00:24,990 --> 00:00:26,920
hvala vam što ste se registrovali za naše radionice.

5
00:00:27,120 --> 00:00:29,120
Ovo je prvi projekat i predstaviću vam nešto vrlo zanimljivo.

6
00:00:29,320 --> 00:00:34,720
Radićemo na dva premeta. Prvi deo projekta je rad na glini.

7
00:00:34,920 --> 00:00:37,120
Kasnije ću vam više reći o ovom komadu od gline.

8
00:00:37,320 --> 00:00:41,600
Ovo je ustvari www.handmadefantasyworld.com. logo.

9
00:00:41,800 --> 00:00:47,450
Nakok rada na glini ćemo raditi efekat kože sa akrilnim i uljanim bojama.

10
00:00:47,650 --> 00:00:49,140
Počećemo sa postarivanjem gline.

11
00:00:49,340 --> 00:00:58,720
Samo da kažem da je ovaj logo izradjen u glini

12
00:00:58,920 --> 00:01:05,430
Poklon moje prijateljice Marie Kantzali iz Soluna.

13
00:01:05,690 --> 00:01:10,670
Ne mogu da vm opišem šta sve može da napravi od gline.

14
00:01:10,780 --> 00:01:17,401
Ako vam se dopada, možete pogledati više na njenom Facebook profilu Maria Kantzali

15
00:01:17,601 --> 00:01:23,643
Hvala još jednom Mariji na poklonu.

16
00:01:23,843 --> 00:01:30,400
Nadam se da će se i njoj svideti ono što ćemo postići na glini.

17
00:01:30,401 --> 00:01:38,730
Glina je sprenma za rad. Nadam se da će biti prilike da nam se Maria pridruži

18
00:01:38,731 --> 00:01:42,160
i pokaže nam kako radi sa glinom.

19
00:01:42,161 --> 00:01:53,620
Hajde da počnemo sa efektom kože. Ovo sam polomio usput, ali to neće smetati.

20
00:01:53,621 --> 00:02:00,180
Imamo i komad drveta, ali to može biti bilo koja podloga, zavisno šta vam se na dohvat ruke.

21
00:02:00,181 --> 00:02:03,680
Hajde sad da krenemo sa efektom. Nekoliko komada sam pripremio i njih ću koristiti u projektu

22
00:02:03,681 --> 00:02:19,400
Ali ću vam pokazati na neobradjenom komadu ceo proces. Evo na primer na ovome parčetu.

23
00:02:19,411 --> 00:02:26,332
Šta je ovo? Ovo je stone paper (kameni papir) firme Stamperia iz Italije.

24
00:02:26,771 --> 00:02:31,380
On ima dve strane, glatku stranu i blago rapavu stranu.

25
00:02:31,381 --> 00:02:40,120
Zavisno koju stranu koristimo, dobićemo različite efekte. Meni se više dopada ono što se postigne na rapavoj strani.

26
00:02:40,532 --> 00:02:46,630
Potrebne su nam tri plastične čaše i tri boje aquacolor takodje firme Stamperia.

27
00:02:46,631 --> 00:03:01,400
Koristićemo sledeće boje: žutu (pino), kesten (castanio) i boju kože (cuoio).

28
00:03:01,401 --> 00:03:04,590
Ne govorim italijanski pa mi je teško da izgovorim imena na italijanskom.

29
00:03:04,591 --> 00:03:15,440
Uzmite meku četku, okrenite grublju stranu i počećemo sa žutom bojom.Premazaću celu površinu ravnomenro.

30
00:03:15,441 --> 00:03:28,880
Jako je vazno da pokreti budu ravni, od ivice do ivice i da se ne prave vidljive linije.

31
00:03:29,080 --> 00:03:36,310
Ako krenem sa vertikalnim potezima, krećem od leve strane i do kraja povlačim četku u tom pravcu.

32
00:03:36,311 --> 00:03:45,740
Kad nanesede dobar sloj boje, počnite sa sušenjem. Sušimo vrlo kratko.

33
00:03:54,600 --> 00:03:56,650
Ponoviću još jednom ovaj postupak.

34
00:03:56,651 --> 00:04:01,680
Rapavu stranu papira smo premazali žutom Aquacolor bojom.

35
00:04:01,681 --> 00:04:07,090
Naneo sam je ravnomerno, prosušio i spremni smo za sledeći korak.

36
00:04:07,091 --> 00:04:14,420
Istom četkicom, samo je malo obrišite komadom papira, nanosimo sledeću boju, kesten Aquacolor.

37
00:04:14,421 --> 00:04:22,300
Postupak je isti. Prelazimo preko prve boje koja ne mora da bude savršeno suva, samo prosušena.

38
00:04:22,500 --> 00:04:27,300
Dugi potez četkicom, od ivice do ivice. To je jako važno.

39
00:04:34,500 --> 00:04:38,940
Lepo...dugi potezi četkice, od ivice do ivice.

40
00:04:38,941 --> 00:04:42,350
Stavite četkicu u vodu i prosušite fenom papir.

41
00:04:59,550 --> 00:05:11,410
Sada nanosimo treću boju, boju kože i radimo ponavljamo postupak.

42
00:05:12,000 --> 00:05:17,440
Nije važno ako imate više boje, samo je dobro razmažite.

43
00:05:17,640 --> 00:05:23,380
Ok, stavili smo novine ispod kako ne bi sve isprljali.

44
00:05:31,000 --> 00:05:33,580
Jedina razlika kod upotrebe treće boje jeste da ćemo je naneti dva puta.

45
00:05:33,581 --> 00:05:38,000
Nije neophodno, ali mi se više dopada rezultat kada se nanesu dva sloja.

46
00:06:07,200 --> 00:06:09,870
Još jedan sloj...

47
00:06:09,871 --> 00:06:14,990
Hteo bih još nešto da vam kažem. Vi vidite na ekranu samo mene, alo oko mene je puno ljudi,

48
00:06:14,991 --> 00:06:18,800
pa ih možete čuti kako pričaju ili kako se ja obraćam njima.

49
00:06:19,000 --> 00:06:20,320
Tu su da pomognu kad zatreba :)

50
00:06:24,000 --> 00:06:30,900
AKo ih čujete, to nije greška, skupili smo se da se družimo i opustimo, i da vama pružimo što bolju radionicu.

51
00:06:30,901 --> 00:06:37,600
Relaksacija i umetnost,to stvarno smanjuje stres. 

52
00:06:38,601 --> 00:06:43,600
Završio sam sa kožom i ponovo sušimo. 

53
00:07:13,000 --> 00:07:19,610
Lepo. Ovo je rezultat farbanja. Imamo još jedan koram. Mali detalj, tačnije.

54
00:07:19,611 --> 00:07:23,600
Pogledajte razliku...ili hajde da uradimo sledeći korak da još bolje vidite.

55
00:07:23,602 --> 00:07:33,270
Ovo je koža koju smo napravili, jedino što je potrebno jeste da zgužvamo papir.

56
00:07:35,271 --> 00:07:38,850
Ovaj papir je odličan i koristićemo ga u projektima. Odličan stonepaper koji proizvodi Stamperia.

57
00:07:38,851 --> 00:07:43,390
I evo konačnog rezultata - napravili smo imitaciju kožu.

58
00:07:43,391 --> 00:07:47,850
Ako pogledamo ovaj papir, videćete da kada se boji na drugoj strani,

59
00:07:47,851 --> 00:07:51,230
koja je glatka, dobija se drugačija boja.

60
00:07:51,231 --> 00:07:56,820
To je zato što glatka strana manje upija i to menja rezultat.

61
00:07:57,020 --> 00:08:01,210
Ovo se desilo. Koristićemo mnogo komada da pokrijemo drvenu ploču.

62
00:08:01,211 --> 00:08:08,080
Na vama je da odlučito koju ćete stranu koristiti. I više volim kada radim na rapavoj strani.

63
00:08:08,081 --> 00:08:13,630
Spremni smo za sledeći korak. Krećemo sa komadom drveta. 

64
00:08:13,631 --> 00:08:21,100
Malo kafe, pravi Grci to nikad ne bi propustili.

65
00:08:22,102 --> 00:08:25,210
Sad će moja mama da kaže: Moj dečko pije kafu, ma nije moguće!

66
00:08:25,211 --> 00:08:31,848
Zbog ovoga smo pravili mnoge pauze na radionicama, pauza za kafu.

67
00:08:32,048 --> 00:08:43,219
Sa komadima papira koji imatiraju kožu ćemo napraviti pačvork na drvenoj podlozi.

68
00:08:43,220 --> 00:08:49,210
Ovo će biti podloga za logo od gline.

69
00:08:49,211 --> 00:08:55,370
Papir nećemo lepiti do ivice drvene ploče.

70
00:08:55,371 --> 00:08:59,390
Ostavićemo koji milimetar slobodan.

71
00:08:59,395 --> 00:09:03,660
Ok, za ovo ću koristiti lepak, Extra Forte lepak od Stamperie.

72
00:09:03,661 --> 00:09:06,130
Koristićemo dosta STamperijinih proizvoda.

73
00:09:06,131 --> 00:09:11,230
Ovi proizvodi su, poret toga što generano volim njihove proizvode, odlični za naš prvi projekat.

74
00:09:11,231 --> 00:09:15,270
Ovo smo uradili sa Extra Forte lepkom.

75
00:09:16,271 --> 00:09:18,520
Aha...moram da pritisnem jače.

76
00:09:18,521 --> 00:09:30,970
Stavljamo po ivicama i malo u centar.

77
00:09:30,971 --> 00:09:38,280
Zatim lepimo papir na drvo.

78
00:09:45,281 --> 00:09:49,580
Morate biti strpljivi i pazljivi sa papirom jer je sad izuvijan pa se ne lepi lako.

79
00:09:49,581 --> 00:09:53,320
A da nismo zgužvali papir i napravili ove vijuge, ne bismo imali efekat kože.

80
00:09:53,321 --> 00:10:00,590
Ove neravnine su neophodne. Ovde se malo vidi lepak, ali to nam ne smeta.

81
00:10:03,591 --> 00:10:05,250
Pomalo je komplikovano lepiti ih.

82
00:10:07,250 --> 00:10:10,260
Uvek koristite bebi vlažne maramice, one su nam jako važan alat.

83
00:10:17,700 --> 00:10:21,080
Hajde da napravimo sad pačvork.

84
00:10:23,900 --> 00:10:27,370
Pritisnite papir jače kako bi se lakše zalepio.

85
00:10:42,570 --> 00:10:48,120
Zapamtite da svoj materijal ne izlažete hladnoći, ukoliko se zamrznu više ih ne možete koristiti.

86
00:10:48,121 --> 00:10:50,080
To se meni danas desilo.

87
00:10:57,081 --> 00:11:01,710
Pozicija papira nam je jako važna.

88
00:11:01,711 --> 00:11:08,470
Potrebno je da se papiri preklapaju.

89
00:11:08,670 --> 00:11:17,710
Pravimo kompoziciju i važno je gde ćemo koji komad gde staviti.

90
00:11:17,910 --> 00:11:24,639
Uvek brišite višak lepka.

91
00:11:31,839 --> 00:11:42,370
Snimiće još mnogo tutorijala, mnoge stvari će se usavršavati.

92
00:11:42,371 --> 00:11:48,590
Ovo je tek početak.

93
00:11:48,591 --> 00:12:02,490
Nadam se da će vam se tutorijali svideti i da ćete biti zadovoljni sadržajem i kavlitetom videa.

94
00:12:02,491 --> 00:12:10,700
Prvo sam koritio veće papire i prekrivao ivice većih komada.

95
00:12:46,900 --> 00:12:53,850
Naš pačvork je gotov a video je ubrzan kako bismo uštedeli na vremenu.

96
00:12:54,050 --> 00:13:01,080
Kao što vidite, nije samo do tehnike, važna je i pozicija papira.

97
00:13:01,081 --> 00:13:05,750
Stavljamo papire unakrsno i izbegavamo da stavimo dva jedan do drugog.

98
00:13:05,751 --> 00:13:08,690
Ne brine me što su neke ivice izdignute.

99
00:13:08,691 --> 00:13:11,840
Koristićemo ukrasne eksere da ih spustimo i ujedno ukrasimo rad.

100
00:13:11,841 --> 00:13:16,580
To daje jako lep finalni efekat, iako nisu svi papiri dobro zalepljeni.

101
00:13:16,581 --> 00:13:19,010
Neću sada obradjivati ivice drvene ploče,

102
00:13:19,011 --> 00:13:22,950
Kasnije ću staviti uljanu boju, verovatno crnu,ili ću smisliti nešto drugo.

103
00:13:22,951 --> 00:13:26,600
Stavićemo ovo sad sa strane i počećemo da radimo na glini.

104
00:13:34,060 --> 00:13:41,420
Ofarbaćemo glinu, tj sve delove.

105
00:13:41,421 --> 00:13:55,730
Koristićemo Stamperia Allegro akrilnu boju tirkiz, šifra KAL 109

106
00:14:00,000 --> 00:14:04,600
i počinjemo farbanje mekom četkicom.

107
00:14:06,601 --> 00:14:09,360
Krila će biti tirkiz,

108
00:14:09,361 --> 00:14:11,790
a oni koji me poznaju, znaju da je ovo moja omiljena

109
00:14:17,791 --> 00:14:23,400
Jako je važno da boja sve pokrije, naročito udubljenja u krilima.

110
00:14:50,090 --> 00:14:55,350
Sada ćemo promeniti boju kojom ćemo farbati sat koji je centralni deo i moje unicijale.

111
00:14:55,351 --> 00:15:10,170
Ponovo Stamperia Allegro akrilna boja, žuta. Albicoca, šifra KAL 60.

112
00:15:10,171 --> 00:15:20,700
Važno je uklopiti boje, ali je važan i njihov kontrast.

113
00:15:20,701 --> 00:15:27,250
Sada nam je potrebna boja koja je kontrast prethodnoj boji, ali koja će biti kontrast i podlozi u boji kože.

114
00:15:27,251 --> 00:15:33,920
Nekada vam boje i kontrasti deluju prejaki,

115
00:15:33,921 --> 00:15:38,080
ali nemojte zaboraviti da ćemo preko njih preći uljanim bojama, što će nam dati potpuno drugačiji ton, koji ustvari i želimo da postignemo.

116
00:15:38,280 --> 00:15:43,851
Dakle, boje će se promeniti i nemojte se plašiti da koristite intenzivne boje.

117
00:15:55,000 --> 00:16:05,520
Završili smo sa farbanjem, sada ćemo sastaviti komade gline i osušiti. 

118
00:16:39,710 --> 00:16:53,610
Sada ćemo uzeti sintetički sindjer sa rupama, on je daleko jeftiniji od pravog morskog sundjera. Može se kupiti u drogerijama.

119
00:16:55,611 --> 00:16:59,530
i Stamperia Allegro akrilnu boju, Terra di Sienna

120
00:16:59,531 --> 00:17:07,440
Šifra KAL74, narandžasti ton.

121
00:17:07,441 --> 00:17:13,000
Imajte u vidu da se svaki projekat koji se snima radi prvi put, 

122
00:17:13,001 --> 00:17:18,810
nikada ranije nije radjen, tako da u toku samog snimanja stvaramo i reagujemo.

123
00:17:21,000 --> 00:17:29,720
Ne želim da vam pokazujem gotove projekte, da ih reprodukujemo, ispravljamo greške i slično.

124
00:17:29,920 --> 00:17:39,300
Šta želim da uradim sa sundjerom....uzimam malu količinu boje

125
00:17:39,301 --> 00:17:43,470
kako bi napravio narandjaste tragove na glini.

126
00:17:53,471 --> 00:18:11,560
Nešno dodirnem sundjerom glinu kako ne bi ostalo previše boje, želim da postignem efekat rdje ponegde.

127
00:18:15,561 --> 00:18:23,100
Koristim ovaj sundjer jer on ostavlja neravnomeran trak zbog rupica.

128
00:18:30,101 --> 00:18:37,880
Kao što vidite biram gde ću naneti boju, ali svakako ne svuda.

129
00:18:37,881 --> 00:18:54,870
Ukoliko želimo da boja zadje negde gde sundjer ne može, koristićemo četku.

130
00:19:00,020 --> 00:19:03,050
Uvek tapkajte...nemojte povlačiti četku ili sundjer.

131
00:19:03,051 --> 00:19:06,980
SUndjerom možemo ispraviti sve što nam se ne dopada.

132
00:19:07,981 --> 00:19:11,430
Stavite ponegde,

133
00:19:15,431 --> 00:19:18,990
i nemojte se plašiti, nakon uljane boje ton će se promeniti.

134
00:19:18,991 --> 00:19:28,080
Jako je važno da se opustite. Budite cool, ovo sve radimo da bi se opustili.

135
00:19:31,480 --> 00:19:40,200
Lepo, akrilnom bojom smo postigli rdju, bez hemikalija. Operite četkicu i osušite boju.

136
00:20:14,400 --> 00:20:26,060
Sada je vreme vreme za uljanu boju. Potreban nam je komad aluminijumske folije koju koristite u domaćinstvu.

137
00:20:26,061 --> 00:20:27,940
U sustini ćemo koristiti dosta uljanih boja,

138
00:20:27,941 --> 00:20:34,470
ali danas koristimo Burnt Umber uljanu boju.

139
00:20:34,471 --> 00:20:43,150
Volim Burnt Umber uljanu, to je klasik medju uljanim bojama.

140
00:20:43,151 --> 00:20:54,060
Volim Van Gogh uljane boje. Hajde da krenemo sa nanošenjem ulja.

141
00:20:54,061 --> 00:21:08,000
Za razliku od Marie koja razredjuje uljane boje i briše komadima tkanine,

142
00:21:08,001 --> 00:21:16,450
I više volim nerazredjenu uljanu boju.Potrebna nam je tvrda četka, od svinjske dlake. 

143
00:21:16,451 --> 00:21:28,980
Odaberite oblik četkice koji vama odgovara.

144
00:21:28,981 --> 00:21:36,610
Uzmite malu količinu uljane boje, u suprotnom ćete imati problema sa skidanjem boje.

145
00:21:36,611 --> 00:21:37,940
Pokrivamo ceo predmet, vidite?

146
00:21:37,941 --> 00:21:44,210
Naročito udubljenja, ali nemojte stavljati previše boje.

147
00:21:44,410 --> 00:21:55,080
Razmažite sav višak četkicom.

148
00:21:56,000 --> 00:21:59,480
Sada ćemo ubrzati....i vidimo se uskoro...

149
00:22:26,500 --> 00:22:39,120
Evo, prekrili smo ceo komad gline uljanom bojom, oba krila i sat.

150
00:22:39,121 --> 00:22:44,130
Sada bebi maramicama, glavnim alatom, brišemo uljanu boju.

151
00:22:44,131 --> 00:22:59,800
Deo po deo brišemo, napravimo lopticu od vlažnih maramicama i skidamo uljanu boju.

152
00:23:08,801 --> 00:23:19,100
Kao što vidite, u udubljenjima ostaje više boje,
jer tu ne pritiskamo i boja se zadržava.

153
00:23:19,101 --> 00:23:23,570
Kada želite da skinete više boje, potrebno je da jače pritisnete maramicu dok brišete površinu.

154
00:23:23,571 --> 00:23:33,350
Vidite da se na odredjenim mestima pojavljuje i narandjasta boja, boja rdje.

155
00:23:37,351 --> 00:23:40,020
Kao što vidite, rezultat je fantastičan!

156
00:23:45,130 --> 00:23:51,440
Nakon nanošenja uljane boje na ceo predmet og gline, nežno skidamo boju.

157
00:23:51,441 --> 00:23:55,490
To je ono na šta stalno na radionicama skrećem pažnju, ovo je deo u kom treba da uživate.

158
00:23:55,491 --> 00:23:42,120
Ovaj deo smo i čekali. Ukoliko nam se nešto ne dopada,

159
00:24:11,900 --> 00:24:31,090
možemo naneti još uljane boje i prstima korigovati ili promeniti sloj uljane boje.

160
00:24:31,091 --> 00:24:36,500
Narandžasta boja u suštini i  nije napravila jak kontarst.

161
00:24:36,501 --> 00:24:43,820
Svakako postoji razlika, ali ne velika. Pošto sam želeo da istaknem brojeve i ostavim ih da budu tamniji,

162
00:24:43,821 --> 00:24:56,980
moji pokreti su u tom delu nežniji i tu boja ostaje tamnija.

163
00:24:56,981 --> 00:25:02,950
Igram se sa uljanom bojom. Ne smeta mi ako ostane više na krilima.

164
00:25:02,951 --> 00:25:08,070
Ustvari, to sam i želeo da postignem. Vidite rezultat! Sad prelazimo na drugo krilo.

165
00:25:08,071 --> 00:25:20,199
Gledajte kako skidam uljanu boju. Pokreti nisu napred-nazad, nego od unutrašnjosti ka spolja.

166
00:25:20,200 --> 00:25:24,400
Smanjujem pritisak gde želim da ostane više uljane boje.

167
00:25:24,401 --> 00:25:27,720
i pojačavam pritisak gde želim da skinem više uljane boje.

168
00:25:27,721 --> 00:25:31,560
Kao što vidite, iskoristio sam smao 2 vlažne maramice, ali sam stalno savijao i koristio čisti deo maramice.

169
00:25:31,561 --> 00:25:39,380
Dve su dovoljne za ceo projekat.

170
00:26:08,619 --> 00:26:15,400
Sada treba da uklopim sve delove gline i proverim da li ima delova koji su ostali svetli.

171
00:26:15,401 --> 00:26:24,630
Ivice krila treba da ostanu svetlije.

172
00:26:43,500 --> 00:26:46,880
Ok, mali deo je......

173
00:27:03,200 --> 00:27:04,280
I kao što vidite, imamo konačni rezultat.

174
00:27:04,281 --> 00:27:12,650
Ukoliko vidimo neke plave delove, samo dodajemo uljane boje.

175
00:27:12,651 --> 00:27:15,049
Postoji mogućnost da se pojavi negde i bela boja gline kao rezultat brisanja.

176
00:27:17,361 --> 00:27:29,110
Možemo tako da ostavimo, ili prstima nanesemo ulje na te delove.

177
00:27:32,321 --> 00:27:37,230
Ja lično volim "tu i tamo" beli efekat.

178
00:27:42,361 --> 00:27:46,210
I ponovo, prstima možete naneti uljanu boju i potamneti delove.

179
00:27:48,100 --> 00:27:52,150
Ja sam ovde skinuo boju i nazire se glina.

180
00:28:01,000 --> 00:28:09,480
AKo želimo da nanesemo još uljane boje nakon brisanja vlažnim maramicama,

181
00:28:09,481 --> 00:28:12,250
površinu prvo moramo obrisati papirnim ubrusom.

182
00:28:34,251 --> 00:28:42,900
Sad ćemo da vidimo šta smo napravili i staviti naše delo na drvenu podlogu prekrivenu kožom.

183
00:28:49,000 --> 00:28:56,310
Prvi projekat je prilično veliki, ali to je bila moja želja, da Marija napravi ovako veliki komad.

184
00:28:58,600 --> 00:29:03,790
Maria, ako sada gledaš, hvala ti puno, čuvaj se i vidimo se uskoro.

185
00:29:22,000 --> 00:29:27,700
Hajde sad da uklopimo sve delove.

186
00:29:28,701 --> 00:29:34,240
Naravno, ovo nije trebalo da bude polomljeno, ali smo se snašli i rešili to.

187
00:29:38,000 --> 00:29:47,580
Kao što vidite, centralni deo - sat, iako je bio svetao, uklopio se u boju kože.

188
00:29:47,581 --> 00:29:49,251
Sada možemo videti i narandžastu boju.

189
00:29:51,252 --> 00:29:57,311
Iako su boje sličnih tonova, vidljiv je crveni ton.

190
00:29:57,312 --> 00:30:00,790
A ovo je i boja koju smo prvobitno koristili za krila.

191
00:30:04,000 --> 00:30:15,220
Centriraćemo glinu na ploču i zalepiti.

192
00:30:31,000 --> 00:30:34,880
Pomeraćemo pažljivo kako ne bi zguzvali komade kože. 

193
00:30:36,000 --> 00:30:55,890
Nanosimo Stamperia extra forte lepak po ivicama, veću količinu.

194
00:30:58,489 --> 00:30:58,289
Na pojedinim delovima će biti potrebno dodati uljane boje kako bi postigli intenzitet.

195
00:31:15,000 --> 00:31:22,690
Caka je da se glina malo pomeri kako bi se lepak ispod predmeta razmazao, 

196
00:31:22,691 --> 00:31:27,970
samim tim će se brže i osušiti.

197
00:31:27,971 --> 00:31:35,900
Iako lepak postaje transparentan,

198
00:31:35,901 --> 00:31:38,260
vlažnim maramicama obrišite višak lepka koji izlazi ispod gline.

199
00:31:38,261 --> 00:31:45,880
Još bolje ćete četkicom ukloniti višak...
Više se ne pomera.

200
00:31:51,881 --> 00:32:01,980
Sada drugi komad.Moramo dobro obrisati ovu stranu jer lepak neće dobro lepiti preko uljane boje.

201
00:32:03,990 --> 00:32:20,040
Obrišite ubrusom i stavite lepak.

202
00:32:28,360 --> 00:32:30,390
Promućkajte lepak pre upotrebe.

203
00:32:33,391 --> 00:32:38,170
Svakako bi bilo lakše da moj predmed od gline nije polomljen.

204
00:32:38,171 --> 00:32:44,520
Opet vlažnom maramicom obrišite višak lepka.

205
00:33:00,000 --> 00:33:03,860
Ponovo koristimo Stamperia Extra Forte lepak za male komade.

206
00:33:03,861 --> 00:33:11,780
Stamperia Extra forte lepak je jako odbar, ali mislim da bi manje-više 

207
00:33:11,781 --> 00:33:21,580
hobi lepak ili scrapbook lepak isto lepili.

208
00:33:23,000 --> 00:33:30,010
Maria koristi Atlacol lepak za glinu.

209
00:33:38,570 --> 00:33:41,400
Ovaj manji komad će se teže zalepiti. 

210
00:33:41,401 --> 00:33:44,210
Ne brinu me male pukotine koje se pojavljuju. Nanesemo uljanu boju,

211
00:33:44,211 --> 00:33:47,150
obrišemo i pukotine nestanu.

212
00:33:58,130 --> 00:34:10,440
Sada ćemo obraditi ivicu drvene table kako bi se uklopila u boju kože.

213
00:34:14,460 --> 00:34:20,230
Isto ćemo uraditi na ivicama papira.

214
00:34:20,231 --> 00:34:34,650
Ne previše uljane boje, samo mrvicu. Nanesite i utrljajte prstima.

215
00:34:48,700 --> 00:35:04,290
Ne sve odjednom, malo po malo. Zavisno na koju podlogu stavljam uljanu boju,

216
00:35:05,710 --> 00:35:08,080
izbledjujem je prstima ili pokretima u krug ili pokretima gore-dole.

217
00:35:39,489 --> 00:35:47,760
Evo, skoro da smo gotovi. Proveravam detalje, možda ću negde dodati još uljane boje.

218
00:35:47,761 --> 00:35:54,030
Ruke očistite prvo vlažnom maramicom, zatim ih operite sapunom i vodom

219
00:35:57,031 --> 00:35:58,490
ili još bolje koristite kremu za ruke kako biste očistili uljanu boju sa ruku.

220
00:35:58,500 --> 00:36:07,930
Pogledajte šta ćemo sad uraditi. Dodaćemo ukrasne eksere....

221
00:36:28,489 --> 00:36:39,870
Zakucaćemo svaki ćošak papira.Krećem sa kucanjem.

222
00:36:39,871 --> 00:36:43,150
Sada ćemo ubrzati,ali se vraćam vrlo brzo.

223
00:37:11,000 --> 00:37:15,240
Evo me :) Bojom ćemo isprskati komad gline,

224
00:37:15,241 --> 00:37:24,800
koristeći boju slonove kosti (ivory), ne čisto belu, razredjenu sa vodom.

225
00:37:39,489 --> 00:37:42,830
Uzimam malu količinu boje kako ne bih isprskao i kožu.

226
00:37:42,831 --> 00:37:47,540
Prstima, polako i pažljivo, prskam glinu.

227
00:37:47,541 --> 00:38:00,660
Više ću isprskati centar glinenog komada.

228
00:38:08,000 --> 00:38:19,980
Krila ću tek malo isprskati.

229
00:38:39,170 --> 00:38:45,770
Ukoliko se desi da isprskate kožu, brzo obrišite vlažnim maramicama i sve će biti ok.

230
00:39:13,489 --> 00:39:23,050
I evo rezultata. Meni se dopada.

231
00:39:25,051 --> 00:39:29,460
To je sve za danas. Hvala što ste gledali radionicu.

232
00:39:29,461 --> 00:39:34,280
Nadam se da ste uživali. Ovo je prva radionica.

233
00:39:34,281 --> 00:39:44,930
Trudiću se da vam dam što više informacija i detalja kako biste lakše radili ponovili projekat sa radionice,

234
00:39:44,931 --> 00:39:49,631
ili uradili nešto koristeći vašu maštu.

235
00:39:49,632 --> 00:40:00,200
Vidimo se sledeće nedelje. Očekujemo prijatelje širom sveta da nam se pridruže.

236
00:40:00,201 --> 00:40:03,280
I hvala Mariji još jednom za logo izradjen od gline.

237
00:40:03,281 --> 00:40:06,190
Ljubim vas, vidimo se uskoro.
