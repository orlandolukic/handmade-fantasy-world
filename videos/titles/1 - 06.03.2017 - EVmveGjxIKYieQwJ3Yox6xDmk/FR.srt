1
00:00:08,716 --> 00:00:14,990
Salut tout le monde ! Bonjour , c’est notre première vidéo sur la page www.handmadefantasyworld.com

2
00:00:15,190 --> 00:00:22,930
C’est vrai que pour certains d’entre vous c’est bonjour ou bonsoir. En fait , chez vous c’est peut-être le matin ou le soir.

3
00:00:23,130 --> 00:00:24,789
Peu importe

4
00:00:24,990 --> 00:00:26,920
 merci de vous être inscrits à nos ateliers.

5
00:00:27,120 --> 00:00:29,120
C’est notre premier projet et je vais vous présenter quelque chose de très interesent.

6
00:00:29,320 --> 00:00:34,720
Nous allons travailler sur deux objets. La première partie du projet est le travail sur l’argile.

7
00:00:34,920 --> 00:00:37,120
Plus tard, je vais vous dire quelque chose de plus sur cette pièce en argile.

8
00:00:37,320 --> 00:00:41,600
C’est le logo du site www.handmadefantasyworld.com

9
00:00:41,800 --> 00:00:47,450
Après le travail de l’argile nous allons travailler l’effet du cuir avec les peintures acryliques et d’huile.

10
00:00:47,650 --> 00:00:49,140
Nous allons commencer par poser l’argile.

11
00:00:49,340 --> 00:00:58,720
Je voudrais seulement dire que ce logo est fait en argile.

12
00:00:58,920 --> 00:01:05,430
C’est le cadeau de mon amie Maria Kantzali de Tessalonique.

13
00:01:05,690 --> 00:01:10,670
Je ne peux pas vous décrire tout ce qu’on peut faire en argile.

14
00:01:10,780 --> 00:01:17,401
Si ça vous plaît, vous pouvez regarder encore sur son profil de FacebookMaria Kantzali.

15
00:01:17,601 --> 00:01:23,643
Merci encore une fois à Maria pour son cadeau.

16
00:01:23,843 --> 00:01:30,400
J’espère qu’elle va aimer ce que nous allons réussir à faire sur l’argile.

17
00:01:30,401 --> 00:01:38,730
L’argile se tient prête pour le travail. J’espère que Maria aura l’occasion de nous rejoindre et

18
00:01:38,731 --> 00:01:42,160
de nous montrer comment on travaille avec l’argile.

19
00:01:42,161 --> 00:01:53,620
 Commençons par l’effet du cuir. J’ai cassé cela, mais ça ne va pas nous déranger.

20
00:01:53,621 --> 00:02:00,180
Nous avons aussi un morceau de bois, mais ça peut être n’importe quelle surface, ça dépend de ce que vous avez à la portée de main.

21
00:02:00,181 --> 00:02:03,680
Commençons maintenant avec l’effet. J’ai préparé quelques morceaux et je vais en utiliser dans le projet.

22
00:02:03,681 --> 00:02:19,400
Je vais vous montrer tout le processus sur un morceau brut. Par example sur celui-ci.

23
00:02:19,411 --> 00:02:26,332
Qu’est-ce que c’est? C’est stone paper (papier de pierre) de l’entreprise Stamperia d’Italie.

24
00:02:26,771 --> 00:02:31,380
Il a deux côté, un côté  lisse et un côté un peu rugueux.

25
00:02:31,381 --> 00:02:40,120
En fonction du côté qu’on utilise nous allons obtenir des effets variés. Je préfère ce qu’on obtient sur le côté rugueux.

26
00:02:40,532 --> 00:02:46,630
Nous avons besoin de trois gobelets et trois couleurs aquacolor aussi de l’entreprise Stamperia.

27
00:02:46,631 --> 00:03:01,400
Nous allons utiliser les couleurs suivantes: le jaune (pino), le marron (castanio), et la couleur de peau (cuoio).

28
00:03:01,401 --> 00:03:04,590
Je ne parle pas l’italien alors c’est difficile pour moi de prononcer les noms de couleurs en Italien.

29
00:03:04,591 --> 00:03:15,440
Prenez une molle brosse, tournez le côté rugueux et nous allons commencer par la couleur jaune. Nous allons enduire également toute la surface.

30
00:03:15,441 --> 00:03:28,880
C’est très important que les gestes soient égaux, d’un bord à l’autre et qu’on ne fasse pas de lignes visibles.

31
00:03:29,080 --> 00:03:36,310
Si je commence par les gestes verticaux, je commence du côté gauche et je tire la brosse jusqu’à la fin dans cette direction.

32
00:03:36,311 --> 00:03:45,740
Quand vous enduisez une grande couche  de couleur commencez avec le séchage . On sèche tout très peu.

33
00:03:54,600 --> 00:03:56,650
Je vais répéter ce procédé encore une fois.

34
00:03:56,651 --> 00:04:01,680
La côté rugueux de papier est enduit par la couleur jaune (Aquacolor).

35
00:04:01,681 --> 00:04:07,090
Je l’ai fait également je l’ai séché et nous sommes prêts pour le pas suivant.

36
00:04:07,091 --> 00:04:14,420
Avec la même brosse un peu essuyée avec un bout de papier nous enduisons la couleur suivante , le marron Aquacolor.

37
00:04:14,421 --> 00:04:22,300
Le procédé est le même. Nous passons sur la première couleur qui ne doit pas être totalement séche, seulement un peu.

38
00:04:22,500 --> 00:04:27,300
Un long trait avec la brosse d’un bord à l’autre. C’est très important.

39
00:04:34,500 --> 00:04:38,940
Bien…de longs traits d’un bord à l’autre.

40
00:04:38,941 --> 00:04:42,350
Mettez la brosse dans l’eau et séchez le papier avec un sèche-cheveux.

41
00:04:59,550 --> 00:05:11,410
Maintenant on enduit la troisième peinture la couleur du cuir et on répète le procédé.

42
00:05:12,000 --> 00:05:17,440
Ce n’est pas important si vous avez trop de peinture vous n’avez qu’à bien l’étaler.

43
00:05:17,640 --> 00:05:23,380
Ok… nous avons mis les journaux au-dessous pour ne pas tout salir.

44
00:05:31,000 --> 00:05:33,580
La seule différence dans l’utilisation de la troisième peinture c’est que nous allons mettre deux fois.

45
00:05:33,581 --> 00:05:38,000
Ce n’est pas nécessaire, mais je préfère le résultat après deux couches de peinture.

46
00:06:07,200 --> 00:06:09,870
Encore , une couche…

47
00:06:09,871 --> 00:06:14,990
Je voudrais vous dire encore quelque chose , vous ne voyez que moi sur l’écran mais il y a beaucoup gens autour de moi.

48
00:06:14,991 --> 00:06:18,800
si bien que vous pouvez les entendre parler ou m’entendre, m’adresser à vous.

49
00:06:19,000 --> 00:06:20,320
Ils sont là pour aider s’il y en a besoin.

50
00:06:24,000 --> 00:06:30,900
Si vous les entendez ce n’est pas une erreur nous nous sommes réunis pour nous détendre dans une ambiance conviviale et pour vous offrir le meilleur atelier possible.

51
00:06:30,901 --> 00:06:37,600
La détente et l’qrt ça diminue vraiment le stress.	

52
00:06:38,601 --> 00:06:43,600
J’ai fini avec le cuir et on sèche de nouveau .

53
00:07:13,000 --> 00:07:19,610
C’est beau, voici le résultat de la peinture. Nous avons encore un pas à faire. Ou pour être précis un petit detail.

54
00:07:19,611 --> 00:07:23,600
 Regardez la différence ou faisons le pas suivant pour voir encore mieux.

55
00:07:23,602 --> 00:07:33,270
C’est le cuir que nous avons fait. Reste à froisser le papier.

56
00:07:35,271 --> 00:07:38,850
Ce papier est excellent et nous allons l’utiliser dans les projets. Le papier de pierre excellent produit par Stamperia.

57
00:07:38,851 --> 00:07:43,390
Et voici le résultat final. Nous avons fait une imitation du cuir.

58
00:07:43,391 --> 00:07:47,850
Si on observe ce papier vous verrez que, si on le peint de l’autre côté ,

59
00:07:47,851 --> 00:07:51,230
qui est lisse, on obtient une couleur différente.

60
00:07:51,231 --> 00:07:56,820
C’est parce que le côté lisse est moins absorbant et ça change le résultat.

61
00:07:57,020 --> 00:08:01,210
Voilà ce qui s’est passé. On va utiliser plusieurs morceaux pour couvrir la planche en bois.

62
00:08:01,211 --> 00:08:08,080
C’est vous de décider quel côté vous allez utiliser. Moi je préfère travailler le côté rugueux.

63
00:08:08,081 --> 00:08:13,630
Nous sommes prêts pour le pas suivant. On commence par un morceau de bois.

64
00:08:13,631 --> 00:08:21,100
Un peu de café de vrais Grecs ne s’en passeraient jamais.

65
00:08:22,102 --> 00:08:25,210
Maintenant maman va dire « mon petit garçon boit du café , c’est pas possible ! »

66
00:08:25,211 --> 00:08:31,848
C’est pour le café que nous avons fait beaucopu de pauses aux ateliers, pauses café.

67
00:08:32,048 --> 00:08:43,219
Nous allons faire un patchwork sur la surface en bois avec les bouts de papier qui imitent le cuir.

68
00:08:43,220 --> 00:08:49,210
Ce sera la base pour le logo en argile.

69
00:08:49,211 --> 00:08:55,370
Nous n’allons pas coller le papier jusqu’au bord de la planche en bois.

70
00:08:55,371 --> 00:08:59,390
Nous allons laisser quelques millimètres libres.

71
00:08:59,395 --> 00:09:03,660
Nous allons laisser quelques millimètres libres.

72
00:09:03,661 --> 00:09:06,130
Nous allons utiliser beaucoup de produits de Stamperia.

73
00:09:06,131 --> 00:09:11,230
à part que moi personnellement j’aime leurs produits , ils sont excellents pour notre premier projet.

74
00:09:11,231 --> 00:09:15,270
Nous avons fait ça avec la colle Extra Forte.

75
00:09:16,271 --> 00:09:18,520
D’accord,  je dois appuyer plus fort.

76
00:09:18,521 --> 00:09:30,970
Nous en mettons un peu sur les bords et au centre.

77
00:09:30,971 --> 00:09:38,280
Puis nous collons le papier sur le bois.

78
00:09:45,281 --> 00:09:49,580

Vous devez être patients avec le papier parce qu’il est un peu roulé et alors on ne le colle pas facilement.

79
00:09:49,581 --> 00:09:53,320
Si nous n’avions pas froissé le papier et si nous n’avions pas fait ces lignes sinueuses nous n’aurions pas obtenu l’effet de cuir.

80
00:09:53,321 --> 00:10:00,590
Cet aspect rugueux est nécessaire. On peut voir un peu de colle ici, mais ça ne nous dérange pas.

81
00:10:03,591 --> 00:10:05,250
C’est un peu compliqué à coller.

82
00:10:07,250 --> 00:10:10,260
Utilisez toujours des lingettes bébé c’est un outil très important pour nous.

83
00:10:17,700 --> 00:10:21,080
Faisons le patchwork maintenant.

84
00:10:23,900 --> 00:10:27,370
Pressez plus fort le papier pour qu’il colle plus vite.

85
00:10:42,570 --> 00:10:48,120
N’exposez pas votre matériel au froid, s’il gèle, vous ne pouvez plus l’utiliser.

86
00:10:48,121 --> 00:10:50,080
C’est ce qui m’est arrivé aujourd ‘hui.

87
00:10:57,081 --> 00:11:01,710
La position du papier est extrêmement importante.

88
00:11:01,711 --> 00:11:08,470
Il faut que les papiers soient superposés.

89
00:11:08,670 --> 00:11:17,710
Nous faisons une composition et il est important où nous allons mettre le papier.

90
00:11:17,910 --> 00:11:24,639
Essuyez toujours la colle en trop.

91
00:11:31,839 --> 00:11:42,370
Il vont faire encore beaucoup de tutoriels et mettre au point beaucoup de chose.

92
00:11:42,371 --> 00:11:48,590
Ce n’est qu’un début.

93
00:11:48,591 --> 00:12:02,490
J’espère que ces tutoriels vont vous plaire et que vous serez content du contenu et de la qualité de cette vidéo.

94
00:12:02,491 --> 00:12:10,700
Tout d’abord j’ai utilisé les papiers plus grands et j’ai couvert les bords des pièces plus grandes.

95
00:12:46,900 --> 00:12:53,850
Notre patchwork est fini et nous avons accéléré la vidéo pour économiser le temps.

96
00:12:54,050 --> 00:13:01,080
Comme vous pouvez voir ce n’est pas seulement une question de technique, la position du papier compte aussi.

97
00:13:01,081 --> 00:13:05,750
On met les papiers en croix et on évite de mettre les deux l’un à côté de l’autre.

98
00:13:05,751 --> 00:13:08,690
Je ne m’inquiète pas que quelques bords soient un peu relevés.

99
00:13:08,691 --> 00:13:11,840
Nous allons utiliser des clous décoratifs pour les baisser et pour décorer le travail en même temps.

100
00:13:11,841 --> 00:13:16,580
ça donne un effet final très beau même si tous les papiers ne sont pas bien collés.

101
00:13:16,581 --> 00:13:19,010
Je ne vais pas travailler les bords de la planche en bois pour le moment.

102
00:13:19,011 --> 00:13:22,950
Plus tard je vais mettre la peinture d’huile , noire probablement ou je vais inviter autre chose.

103
00:13:22,951 --> 00:13:26,600
On va mettre ça de côté et on va commencer à travailler l’argile.

104
00:13:34,060 --> 00:13:41,420
Nous allons repeindre l’argile entier.

105
00:13:41,421 --> 00:13:55,730
Nous allons utiliser la peinture acrylique turquoise Stamperia Allegro , chiffre KAL109 .

106
00:14:00,000 --> 00:14:04,600
Nous commençons la peinture avec la brosse douce.

107
00:14:06,601 --> 00:14:09,360
Les ailes seront de couleur turquoise.

108
00:14:09,361 --> 00:14:11,790
Ceux qui me connaissent savent que c’est ma couleur préférée.

109
00:14:17,791 --> 00:14:23,400
Il est très important que la peinture couvre tout surtout les parties sous les ailes.

110
00:14:50,090 --> 00:14:55,350
Maintenant nous allons changer de peinture qu’on va utiliser pour l’horloge, qui est au centre et mes initiales aussi.

111
00:14:55,351 --> 00:15:10,170
Encore une fois la peinture acrylique Stamperia Allegro mais jaune, Albicoca chiffre KAL60.

112
00:15:10,171 --> 00:15:20,700
Il est important de bien assortir les couleurs mais leur contraste est important aussi.

113
00:15:20,701 --> 00:15:27,250
Maintenant on a besoin d’une couleur qui fera le contraste de la couleur précédente mais aussi le contraste de la base couleur de peau.

114
00:15:27,251 --> 00:15:33,920
Parfois les couleurs et les contrastes ont l’air trop forts.

115
00:15:33,921 --> 00:15:38,080
Mais n’oubliez pas que nous allons les repeindre des peintures d’huile ce qui va produire un effet tout à fait différent, l’effet qu’on veut réellement créer.

116
00:15:38,280 --> 00:15:43,851
Donc , les couleurs vont changer, n’ayez pas peur d’utiliser les nuances intensives.

117
00:15:55,000 --> 00:16:05,520
Nous avons fini de peindre on va coller les morceaux d’argile et on va les sécher.

118
00:16:39,710 --> 00:16:53,610
On va prendre une éponge synthétique avec les trous , il est beaucoup moins cher que la vraie éponge de  mer. On peut l’acheter dans les parfumeries.

119
00:16:55,611 --> 00:16:59,530
Et la couleur acrylique Stamperia Allegro terra di Sienna.

120
00:16:59,531 --> 00:17:07,440
Chiffre KAL74 le ton orange.

121
00:17:07,441 --> 00:17:13,000
Ayez à l’esprit que tous les projets filmés sont faits pour la première fois.

122
00:17:13,001 --> 00:17:18,810
Qu’ils n’ont jamais été faits avant si bien que pendant le tournage, on crée et on réagit.

123
00:17:21,000 --> 00:17:29,720
Je ne veux pas vous montrer des projets finis, nous n’allons pas les reproduire ni corriger les erreurs.

124
00:17:29,920 --> 00:17:39,300
Qu’est-ce que je veux faire avec l’éponge….. je prends une petite quantité d’eau.

125
00:17:39,301 --> 00:17:43,470
pour faire des traces orange sur l’argile

126
00:17:53,471 --> 00:18:11,560
Je touche doucement l’argile avec l’éponge pour ne pas laisser trop de peinture dessus, je veux faire l’effet de rouille à certains endroits.

127
00:18:15,561 --> 00:18:23,100
Je me sers de cette éponge parce qu’elle laisse une trace irrégulière à cause de ses trous.

128
00:18:30,101 --> 00:18:37,880
Comme vous pouvez voir, je choisis où je vais mettre de la peinture, mais bien sûr, pas partout.

129
00:18:37,881 --> 00:18:54,870
Si on veut mettre de la peinture aux endroits que l’éponge ne peut pas atteindre, on va utiliser la brosse.

130
00:19:00,020 --> 00:19:03,050
Tapotez toujours, ne tirez pas la brosse ou l’éponge.

131
00:19:03,051 --> 00:19:06,980
Avec l’éponge on peut corriger tout ce qui ne nous plaît pas.

132
00:19:07,981 --> 00:19:11,430
Mettez-en par-ci par-là,

133
00:19:15,431 --> 00:19:18,990
et n’ayez pas peur, après la peinture d’huile, le ton va changer.

134
00:19:18,991 --> 00:19:28,080
Il est très important de vous détendre. Soyez cool, on fait tout ça pour nous relaxer.

135
00:19:31,480 --> 00:19:40,200
Bien, à l’aide de la peinture acrylique, on a obtenu l’effet de rouille, sans les produits chimiques. Lavez la brosse et séchez la peinture.

136
00:20:14,400 --> 00:20:26,060
Maintenant, il est temps d’utiliser la peinture d’huile. Nous avons besoin d’une feuille d’aluminium que vous utilisez à la maison.

137
00:20:26,061 --> 00:20:27,940
En fait, on va utiliser beaucoup de peintures d’huile,

138
00:20:27,941 --> 00:20:34,470
mais aujourd’hui on utilise la peinture d’huile Burnt Umber.

139
00:20:34,471 --> 00:20:43,150
J’aime la peinture d’huile Burnt Umber, c’est l’un des classique parmi ces peintures.

140
00:20:43,151 --> 00:20:54,060
J’aime les peintures d’huile Van Gogh. Commençons par enduire les peintures.

141
00:20:54,061 --> 00:21:08,000
A la différence de Maria qui dilue les peintures d’huile à l’aide des morceaux de tissus,

142
00:21:08,001 --> 00:21:16,450
moi, je préfère la peinture d’huile non diluée. Nous avons besoin d’une brosse dure, en poil de cochon.

143
00:21:16,451 --> 00:21:28,980
Choisissez la forme de la brosse qui vous convient le mieux.

144
00:21:28,981 --> 00:21:36,610
Prenez une petite quantité de peinture d’huile, autrement, vous aurez du mal à l’enlever.

145
00:21:36,611 --> 00:21:37,940
On couvre tout l’objet, vous voyez ?

146
00:21:37,941 --> 00:21:44,210
Surtout les enfoncements, mais ne mettez pas trop de peinture.

147
00:21:44,410 --> 00:21:55,080
Etalez la peinture en trop avec la brosse.

148
00:21:56,000 --> 00:21:59,480
Maintenant, on va accélérer la vidéo…. A bientôt…

149
00:22:26,500 --> 00:22:39,120
Voilà, nous avons couvert tout le morceau d’argile de peinture d’huile, les deux ailes et l’horloge.

150
00:22:39,121 --> 00:22:44,130
Maintenant, on utilise les lingettes bébé, notre outil principal, pour essuyer la peinture d’huile.

151
00:22:44,131 --> 00:22:59,800
On essuie une partie après l’autre, on fait une petite boule de lingettes bébé et on retire la peinture d’huile.

152
00:23:08,801 --> 00:23:19,100
Comme vous pouvez voir, il reste plus de peinture dans les enfoncements, parce qu’on n’y appuie pas trop et que la peinture se maintient.

153
00:23:19,101 --> 00:23:23,570
Quand vous voulez retirer plus de peinture, il faut appuyer plus fort la lingette pendant que vous essuyez la surface.

154
00:23:23,571 --> 00:23:33,350
Vous voyez qu’à certains endroits, il y a la couleur orange, la couleur de rouille.

155
00:23:37,351 --> 00:23:40,020
Comme vous pouvez voir, le résultat est fantastique !

156
00:23:45,130 --> 00:23:51,440
Après avoir enduit de la peinture d’huile sur tout l’objet en argile, on retire la peinture doucement

157
00:23:51,441 --> 00:23:55,490
C’est ce sur quoi j’attire votre attention à tous les ateliers, c’est la partie pendant laquelle vous devez prendre plaisir.

158
00:23:55,491 --> 00:23:42,120
C’est la partie que nous attendions. S’il y a quelque chose qui ne nous plaît pas,

159
00:24:11,900 --> 00:24:31,090
nous pouvons mettre encore de peinture d’huile des doigts et corriger ou changer la couche de peinture d’huile.

160
00:24:31,091 --> 00:24:36,500
En réalité, la couleur orange n’a pas fait un grand contraste.

161
00:24:36,501 --> 00:24:43,820
En tout cas, il y a une différence, mais pas si grande que ça. Comme je voulais accentuer les numéros et les laisser plus foncés

162
00:24:43,821 --> 00:24:56,980
mes gestes sont plus doux dans cette étape et c’est là que la couleur reste plus foncée.

163
00:24:56,981 --> 00:25:02,950
Je joue avec de la peinture d’huile. Ça ne me dérange pas s’il y en a trop sur les ailes.

164
00:25:02,951 --> 00:25:08,070
En fait, c’est ce que je voulais obtenir. Regardez le résultat ! Maintenant on passe à l’aile suivante.

165
00:25:08,071 --> 00:25:20,199
Regardez comment je retire la peinture d’huile. Je ne fais pas de mouvement en avant et en arrière, mais du centre vers l’extérieur.

166
00:25:20,200 --> 00:25:24,400
J’appuie moins fort là où je veux plus de peinture.

167
00:25:24,401 --> 00:25:27,720
et j’appuie plus fort là où je veux retirer plus de peinture d’huile.

168
00:25:27,721 --> 00:25:31,560
Comme vous pouvez voir, je n’ai utilisé que deux lingettes bébé mais je les pliais tout le temps et j’utilisais que la partie propre de la lingette.

169
00:25:31,561 --> 00:25:39,380
Les deux suffisent pour tout le projet.

170
00:26:08,619 --> 00:26:15,400
Maintenant, je dois mettre ensemble toutes les parties d’argile et vérifier s’il y a des parties qui sont restées claires.

171
00:26:15,401 --> 00:26:24,630
Les bords des ailes doivent rester plus clairs.

172
00:26:43,500 --> 00:26:46,880
Ok, une petite partie est ….

173
00:27:03,200 --> 00:27:04,280
Et comme vous pouvez voir, nous avons le résultat final.

174
00:27:04,281 --> 00:27:12,650
Si on voit quelques parties qui sont bleues, on ne fait qu’ajouter de la peinture d’huile.

175
00:27:12,651 --> 00:27:15,049
Il y a une possibilité que  la couleur blanche de l’argile apparaisse comme résultat de l’essuie.

176
00:27:17,361 --> 00:27:29,110
On peut la laisser telle qu’elle est ou mettre de l’huile des doigts sur ces parties.

177
00:27:32,321 --> 00:27:37,230
Je préfère l’effet blanc « par-ci par-là »

178
00:27:42,361 --> 00:27:46,210
Et de nouveau, vous pouvez mettre de la peinture d’huile des doigts et rendre ces parties plus foncées.

179
00:27:48,100 --> 00:27:52,150
J’ai retiré la couleur ici et on peut deviner l’argile.

180
00:28:01,000 --> 00:28:09,480
Si on veut enduire encore de peinture d’huile après avoir essuyé avec des lingettes

181
00:28:09,481 --> 00:28:12,250
on doit en effacer la surface avec une serviette.

182
00:28:34,251 --> 00:28:42,900
Maintenant, on verra ce qu’on a fait et on mettra notre produit sur le socle en bois couvert de cuir.

183
00:28:49,000 --> 00:28:56,310
Le premier projet est assez grand, mais c’était ce que je voulais, je voulais que Marie fasse un morceau aussi grand.

184
00:28:58,600 --> 00:29:03,790
Marie, si tu nous regarde maintenant, merci beaucoup, prends soin de toi et on se voit bientôt.

185
00:29:22,000 --> 00:29:27,700
Maintenant, on va mettre tous les morceaux ensemble.

186
00:29:28,701 --> 00:29:34,240
Bien sûr, ça ne devait pas être cassé, mais nous nous sommes débrouillé et nous avons réglé le problème.

187
00:29:38,000 --> 00:29:47,580
Comme vous pouvez voir, la partie centrale – l’horloge, même si elle était de couleur claire va très bien avec la couleur de cuir.

188
00:29:47,581 --> 00:29:49,251
Maintenant, vous pouvez voir aussi la couleur orange.

189
00:29:51,252 --> 00:29:57,311
Même si les couleurs ont des nuances similaires, le ton rouge est assez visible.

190
00:29:57,312 --> 00:30:00,790
Et c’est la peinture que nous avons utilisée pour les ailes.

191
00:30:04,000 --> 00:30:15,220
Et c’est la peinture que nous avons utilisée pour les ailes.

192
00:30:31,000 --> 00:30:34,880
On va le bouger attentivement pour ne pas chiffonner les parties de cuir.

193
00:30:36,000 --> 00:30:55,890
On met de la colle Extra Forte de Stamperia aux bords, on en met une grande quantité.

194
00:30:58,489 --> 00:30:58,289
Sur certaines parties, il va falloir ajouter de la peinture d’huile pour avoir un effet plus intensif.

195
00:31:15,000 --> 00:31:22,690
Je vous donne une petite astuce – il faut bouger l’argile pour que la colle sous le produit soit bien étalée.

196
00:31:22,691 --> 00:31:27,970
Du coup, elle va sécher plus vite.

197
00:31:27,971 --> 00:31:35,900
Bien que la colle devienne plus transparente,

198
00:31:35,901 --> 00:31:38,260
essuyez la colle en trop qui déborde sous l’argile avec les lingettes

199
00:31:38,261 --> 00:31:45,880
vous allez encore mieux retirer la colle en trop à l’aide d’une brosse.
Ça ne bouge plus.

200
00:31:51,881 --> 00:32:01,980
Maintenant, on passe au deuxième morceau. On doit bien essuyer ce côté car la colle ne va pas bien coller par-dessus la peinture d’huile.

201
00:32:03,990 --> 00:32:20,040
Essuyez avec une serviette et mettez de la colle.

202
00:32:28,360 --> 00:32:30,390
Secouez la colle avant de l’utiliser.

203
00:32:33,391 --> 00:32:38,170
Ce serait beaucoup plus facile si mon produit en argile n’était pas cassé.

204
00:32:38,171 --> 00:32:44,520
Essuyez de nouveau la colle en trop avec une lingette.

205
00:33:00,000 --> 00:33:03,860
On utilise de nouveau la colle pour les petits morceaux Extra Forte de Stamperia.

206
00:33:03,861 --> 00:33:11,780
La colle Extra Forte de Stamperia est très bonne, mais je pense aussi que ….

207
00:33:11,781 --> 00:33:21,580
que la colle hobby ou scrapbook colleraient assez bien.

208
00:33:23,000 --> 00:33:30,010
Maria utilise la colle Atlacol pour l’argile.

209
00:33:38,570 --> 00:33:41,400
Ce petit morceau va coller plus difficilement.

210
00:33:41,401 --> 00:33:44,210
Je ne m’inquiète pas pour les petites fissures qui apparaissent. On met de la peinture d’huile….

211
00:33:44,211 --> 00:33:47,150
on essuie et les fissures disparaissent.

212
00:33:58,130 --> 00:34:10,440
Maintenant, on va travailler le bord de la planche en bois pour qu’elle se marie bien avec la couleur de cuir.

213
00:34:14,460 --> 00:34:20,230
On va faire de même avec les bords du papier.

214
00:34:20,231 --> 00:34:34,650
Pas trop de peinture d’huile, juste un petit peu. Mettez-en et frottez des doigts.

215
00:34:48,700 --> 00:35:04,290
Pas toute la quantité en même temps. En fonction de la surface sur laquelle je mets de la peinture d’huile

216
00:35:05,710 --> 00:35:08,080
Je la rends plus claire des doigts ou en faisant des gestes circulaires ou des mouvements du haut en bas

217
00:35:39,489 --> 00:35:47,760
Voilà, nous avons presque fini. On vérifie les détails, je vais peut-être ajouter de la peinture d’huile à certains endroits.

218
00:35:47,761 --> 00:35:54,030
Essuyez-vous les mains avec une lingette, puis lavez-vous avec du savon et de l’eau.

219
00:35:57,031 --> 00:35:58,490
Ou encore mieux, utilisez une crème pour les mains afin d’enlever la peinture de l’huile.

220
00:35:58,500 --> 00:36:07,930
Regardez ce que nous allons faire maintenant. Nous allons mettre des clous décoratifs.

221
00:36:28,489 --> 00:36:39,870
On va enfoncer des clous sur chaque coin du papier. Je commence à enfoncer…

222
00:36:39,871 --> 00:36:43,150
Maintenant, on va accélérer, mais je reviens dans quelques secondes.

223
00:37:11,000 --> 00:37:15,240
Me voilà. Je vais arroser de la peinture sur le morceau d’argile.

224
00:37:15,241 --> 00:37:24,800
en utilisant la couleur d’ivoire, et pas la couleur blanche, diluée avec de l’eau.

225
00:37:39,489 --> 00:37:42,830
Je prends une petite quantité d’eau pour ne pas tacher le cuir.

226
00:37:42,831 --> 00:37:47,540
J’arrose l’argile des doigts, doucement et attentivement.

227
00:37:47,541 --> 00:38:00,660
Je vais mieux arroser le centre du morceau en argile.

228
00:38:08,000 --> 00:38:19,980
Alors que les ailes, je les arrose tout un petit peu.

229
00:38:39,170 --> 00:38:45,770
S’il vous arrive de tacher le cuir, essuyez-le vite avec des lingettes et tout ira bien.

230
00:39:13,489 --> 00:39:23,050
Et voici le résultat. Ça me plaît à moi.

231
00:39:25,051 --> 00:39:29,460
C’est tout pour aujourd’hui. Merci d’avoir regardé cet atelier.

232
00:39:29,461 --> 00:39:34,280
J’espère que vous en avez profité. C’est le premier atelier.

233
00:39:34,281 --> 00:39:44,930
Je vais m’efforcer à vous donner le plus d’informations et de détails possible afin que vous répétiez plus facilement le projet de notre atelier 

234
00:39:44,931 --> 00:39:49,631
ou que vous fassiez quelque chose grâce à votre imagination.

235
00:39:49,632 --> 00:40:00,200
On se voit la semaine prochaine. Nous attendons des amis du monde entier pour qu’ils nous rejoignent.

236
00:40:00,201 --> 00:40:03,280
Et merci à Maria encore une fois pour le logo fait en argile.

237
00:40:03,281 --> 00:40:06,190
Je vous embrasse, à bientôt !
