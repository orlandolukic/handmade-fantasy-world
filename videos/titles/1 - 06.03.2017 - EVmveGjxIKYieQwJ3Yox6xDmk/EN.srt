1
00:00:08,716 --> 00:00:14,990
Hi there!,good evening ,this is our first day for the www.handmadefantasyworld.com.

2
00:00:15,190 --> 00:00:22,930
The truth is that for some of you, it may be good morning good afternoon or even good evening,

3
00:00:23,130 --> 00:00:24,789
Anyway it doesn' matter

4
00:00:24,990 --> 00:00:26,920
I’d like to thank you for being here.

5
00:00:27,120 --> 00:00:29,120
Today is our first day and we are going to show you something very interesting.

6
00:00:29,320 --> 00:00:34,720
Two words about today's project. Our project is about working on clay.

7
00:00:34,920 --> 00:00:37,120
I'll tell you later how this clay came up

8
00:00:37,320 --> 00:00:41,600
It has to do with the www.handmadefantasyworld.com.clay logo

9
00:00:41,800 --> 00:00:47,450
we will create leather effects and acrylic with oils

10
00:00:47,650 --> 00:00:49,140
actually, the ageing process on clay

11
00:00:49,340 --> 00:00:58,720
Firstly, I want to say that this clay is the www.handmadefantasyworld.com. logo

12
00:00:58,920 --> 00:01:05,430
It has been gifted to us by a friend of mine Maria Kantzali from Thessaloniki, Greece.

13
00:01:05,690 --> 00:01:10,670
I find no words to describe all the things she can do with clay.

14
00:01:10,780 --> 00:01:17,401
If you are interested in finding her you may either visit her facebook profile Maria Kantzali in latin,

15
00:01:17,601 --> 00:01:23,643
or her page with her works “Tου Πηλού τα παραμύθια» in Greek. I thank her so much!

16
00:01:23,843 --> 00:01:30,400
I hope she likes the result too! Actually today we are going to see how we will do the rest.

17
00:01:30,401 --> 00:01:38,730
Our logo clay is ready, I truly  hope that we have the opportunity Maria becomes a part of what we are doing here

18
00:01:38,731 --> 00:01:42,160
and show us all the magnificent things she does with clay.

19
00:01:42,161 --> 00:01:53,620
Let's start with leather. Well this clay broke in pieces on my way here.

20
00:01:53,621 --> 00:02:00,180
We have a piece of wood too ,it may be anything depending on your whereabouts.

21
00:02:00,181 --> 00:02:03,680
We’ll start with the leather effect. I made some, we will need them.

22
00:02:03,681 --> 00:02:19,400
Let me find the one undone left... never mind, I'll show you with this one...

23
00:02:19,411 --> 00:02:26,332
.Let’s see how we create the leather effect,what’s this? It’s a stone paper by Stamperia, Italian company.

24
00:02:26,771 --> 00:02:31,380
It’s a two sided paper,a smooth side and a shagreen one.

25
00:02:31,381 --> 00:02:40,120
It gives two different results. We prefer the shagreen surface or at least,  I prefer this side.

26
00:02:40,532 --> 00:02:46,630
We need three plastic cups for equal number of aqua colour paints by Stamperia,

27
00:02:46,631 --> 00:03:01,400
pino aqua in Italian, actually yellow, castanio and leather as I call it

28
00:03:01,401 --> 00:03:04,590
cause it’s in Italian and I find it a bit difficult to pronounce.

29
00:03:04,591 --> 00:03:15,440
Take a brush, starting from on the rough side,pino will be used first, and we cover all the surface,

30
00:03:15,441 --> 00:03:28,880
It's very important to make straight brushes, from side to side,thoroughly.Avoid leaving stripes.

31
00:03:29,080 --> 00:03:36,310
If I start from this point a vertical stripe will be left , so I move from the one end to the other.

32
00:03:36,311 --> 00:03:45,740
We apply our colour well.As soon as we finish with this, we dry!

33
00:03:54,600 --> 00:03:56,650
Well, In case you didn’t see clearly what I did, I’ll explain the steps once more.

34
00:03:56,651 --> 00:04:01,680
On the stone paper shagreen side, we apply
the first colour, pino.

35
00:04:01,681 --> 00:04:07,090
I apply it thoroughly, I dry it a little bit and it's ready.

36
00:04:07,091 --> 00:04:14,420
Using the same brush, wipe it with some roll paper and take Castanio paint.

37
00:04:14,421 --> 00:04:22,300
and I follow the same procedure. I paint over the first colour. It's not necessary to be well dried.

38
00:04:22,500 --> 00:04:27,300
Long brushes again, it is very important.

39
00:04:34,500 --> 00:04:38,940
Very nice... ok? Long brushes. We don't mind if there is a spot not well painted.

40
00:04:38,941 --> 00:04:42,350
We put our brush in the water and use the hair dryer again

41
00:04:59,550 --> 00:05:11,410
We pick up the third colour, which is the colour of leather and do the same thing.

42
00:05:12,000 --> 00:05:17,440
There is no problem if you pick too much paint, just brush it through.

43
00:05:17,640 --> 00:05:23,380
It's ok, that's why we lay newspapers underneath , so as not to spoil everything.

44
00:05:31,000 --> 00:05:33,580
The only difference in this round is that we are going to apply two coats of paint.

45
00:05:33,581 --> 00:05:38,000
It's not necessary, but I've seen that I like it more, after applying two coats of paint.

46
00:06:07,200 --> 00:06:09,870
One more coat...

47
00:06:09,871 --> 00:06:14,990
I'd like also to tell you that, you see only me but there are lots of people around me,

48
00:06:14,991 --> 00:06:18,800
so there is a chance you may hear talking between them or you may see me talking to them.

49
00:06:19,000 --> 00:06:20,320
They are here to help when needed.

50
00:06:24,000 --> 00:06:30,900
It's not by mistake, we gather here to relax and in the way I can to, give you what you need.

51
00:06:30,901 --> 00:06:37,600
Both relaxation and art, as what we really need is to stress down.

52
00:06:38,601 --> 00:06:43,600
We have finished with leather. We dry again,for a last time.

53
00:07:13,000 --> 00:07:19,610
Nice. Well this is the painting part final result.There is one more step. A small detail.

54
00:07:19,611 --> 00:07:23,600
Look the difference... or I guess it's better to do the next step, for you to see it.

55
00:07:23,602 --> 00:07:33,270
This is your leather and you just need to crinkle the stone paper.

56
00:07:35,271 --> 00:07:38,850
We are going to use it from time to time. It's a very good material, by Stamperia.

57
00:07:38,851 --> 00:07:43,390
And here is our leather. This is the result.

58
00:07:43,391 --> 00:07:47,850
If I look my papers here,you will see that the when you paint on the other side,

59
00:07:47,851 --> 00:07:51,230
which is the smooth one, the colour is different

60
00:07:51,231 --> 00:07:56,820
and that’s because the smooth side of the paper is less absorbent. So the result changes.

61
00:07:57,020 --> 00:08:01,210
Now it just happened. We will use lots of them to cloak the wood.

62
00:08:01,211 --> 00:08:08,080
Then it's up to you to decide what you wanna do. I prefer the rough side.

63
00:08:08,081 --> 00:08:13,630
We are ready. I’s time for our piece of wood to deal with.

64
00:08:13,631 --> 00:08:21,100
Some coffee,real Greeks never miss it.

65
00:08:22,102 --> 00:08:25,210
When my mother sees this will think “my boy drinking coffee,no way”!!!

66
00:08:25,211 --> 00:08:31,848
This is why we stopped seminars,too many women ,coffee and smoking.

67
00:08:32,048 --> 00:08:43,219
With our leather effect papers we are going to create a patchwork,

68
00:08:43,220 --> 00:08:49,210
something like a matress on the wood where our piece of clay will be laid on.

69
00:08:49,211 --> 00:08:55,370
The sure thing is that our papers will not be glued right on the wood edge.

70
00:08:55,371 --> 00:08:59,390
I just need a millimeter margin.

71
00:08:59,395 --> 00:09:03,660
Ehh..now I'm gonna use the glue. We have the extra forte glue by Stamperia.

72
00:09:03,661 --> 00:09:06,130
Generally,we are going to use lots of products except from Stamperia ones,

73
00:09:06,131 --> 00:09:11,230
but it happens,apart from Stamperia being a good company ,Stamperia products are more suitable for this our first project.

74
00:09:11,231 --> 00:09:15,270
That's what we do with Extra Forte.

75
00:09:16,271 --> 00:09:18,520
Yeah... I need to squeeze harder

76
00:09:18,521 --> 00:09:30,970
We put some quantity perimetrically and maybe some in the center.

77
00:09:30,971 --> 00:09:38,280
Then glue the paper on the wood

78
00:09:45,281 --> 00:09:49,580
You have to be patient as leather papers are wrinkled, they are not easily attached.

79
00:09:49,581 --> 00:09:53,320
On the other hand if we didnt't wrinkle the paper we wouldn't have this effect.

80
00:09:53,321 --> 00:10:00,590
The wrinkles are necessary. You see here the glue but we don't mind.

81
00:10:03,591 --> 00:10:05,250
It will be a bit difficult to glue them.

82
00:10:07,250 --> 00:10:10,260
Always use baby wipes, they are an important tool.

83
00:10:17,700 --> 00:10:21,080
Let’s create our patchwork.

84
00:10:23,900 --> 00:10:27,370
It's good to press down, a bit, the papers, so as to glue more easily.

85
00:10:42,570 --> 00:10:48,120
Remember not to leave your materials exposed to cold as they get damaged,

86
00:10:48,121 --> 00:10:50,080
it already happened to me today.

87
00:10:57,081 --> 00:11:01,710
You see right? The position of your paper is very important.

88
00:11:01,711 --> 00:11:08,470
We need the one paper covering a part of another as I did mention in our promo video,

89
00:11:08,670 --> 00:11:17,710
is that the composition, (where we put them), of the papers is very important.

90
00:11:17,910 --> 00:11:24,639
Even though more papers will be glued and any mistakes will be covered,it’s better to wipe out glue.

91
00:11:31,839 --> 00:11:42,370
As time goes by,more videos will be filmed,lots of things will be improved,

92
00:11:42,371 --> 00:11:48,590
everything is going be ok, it’s just a new start.

93
00:11:48,591 --> 00:12:02,490
I hope the those filming will do a great job so as to let you satisfied.

94
00:12:02,491 --> 00:12:10,700
For the beginning I use bigger papers in order to cover as many paper edges as I can.

95
00:12:46,900 --> 00:12:53,850
Our patchwork is finished, the video was fast forwarded to save us video time.

96
00:12:54,050 --> 00:13:01,080
You can see that it’s not only the technique but the position of the papers,

97
00:13:01,081 --> 00:13:05,750
crossing each other and not the one right next to the other

98
00:13:05,751 --> 00:13:08,690
I don’t care about the edges protruding upwards,

99
00:13:08,691 --> 00:13:11,840
soon afterwards we are going to use some extra metal decorations to nail them down.

100
00:13:11,841 --> 00:13:16,580
which gives a very nice effect, as many papers have not been glued well.

101
00:13:16,581 --> 00:13:19,010
I haven’t done anything yet regarding the wooden margin,

102
00:13:19,011 --> 00:13:22,950
later on we are going to oil it black or will think about something else.

103
00:13:22,951 --> 00:13:26,600
Let’s put this aside and start working on the clay.

104
00:13:34,060 --> 00:13:41,420
We take our clay as it is in pieces and start painting it.

105
00:13:41,421 --> 00:13:55,730
We will use allegro colour by Stamperia turquoise indiano the code number is kal 109

106
00:14:00,000 --> 00:14:04,600
and using a soft brush we start painting our clay.

107
00:14:06,601 --> 00:14:09,360
Surely the wings will be painted with this colour,

108
00:14:09,361 --> 00:14:11,790
those who know me, also know that is my favourite colour.

109
00:14:17,791 --> 00:14:23,400
What we have to be careful for is that the paint covers everything ,especially the gaps among the wings.

110
00:14:50,090 --> 00:14:55,350
Now it’s time to change paint colour for the main theme of the clay logo ,the clock and my initial.

111
00:14:55,351 --> 00:15:10,170
Again allegro by Stamperia the name is alpicoka ,code number kal 60 for those who want to find the paints.

112
00:15:10,171 --> 00:15:20,700
As I have already mentioned in the promo video ,both the combination and the contrast of the paint colours are important.

113
00:15:20,701 --> 00:15:27,250
At this point I’m looking for two colours that contrast with each other but they contrast with leather too.

114
00:15:27,251 --> 00:15:33,920
Sometimes you see the colours and they seem intense to you,

115
00:15:33,921 --> 00:15:38,080
however don’t forget that we are going to cover them with oil, so we will have the desirable tone.

116
00:15:38,280 --> 00:15:43,851
So the result changes. Don’t be afraid of colours .

117
00:15:55,000 --> 00:16:05,520
Now that we are done with colours, we put the clay pieces together and we dry.

118
00:16:39,710 --> 00:16:53,610
Using a synthetic sponge,not necessarily an original one,I bought it from Jumbo stores,it was quite cheap

119
00:16:55,611 --> 00:16:59,530
we will take an acrylic allegro paint by Stamperia, Terra du Sienna

120
00:16:59,531 --> 00:17:07,440
code number kal 74,an orange colour, we will do something based on an idea I have.

121
00:17:07,441 --> 00:17:13,000
at this point you need to know that every project shown on video is live,

122
00:17:13,001 --> 00:17:18,810
never done it again before,we create,we see,we act.

123
00:17:21,000 --> 00:17:29,720
We don’t want a ready project,we create,we make mistakes,we correct them and so on.

124
00:17:29,920 --> 00:17:39,300
What I want to do with the sponge…I take small quantity of paint …

125
00:17:39,301 --> 00:17:43,470
is to give some parts of our clay an orange hue.

126
00:17:53,471 --> 00:18:11,560
I gently dab,I don’t want too much paint on it and what I mainly want is a rusty look.

127
00:18:15,561 --> 00:18:23,100
We use a sponge because it leaves blanks and the paint is somehow sloppily applied.

128
00:18:30,101 --> 00:18:37,880
As you may see I choose where I dab, but not everywhere.

129
00:18:37,881 --> 00:18:54,870
We could either use a brush and dab some points where our sponge cannot reach.

130
00:19:00,020 --> 00:19:03,050
Always dabbing... not brushing

131
00:19:03,051 --> 00:19:06,980
We correct anything we don't like, with our sponge.

132
00:19:07,981 --> 00:19:11,430
Put some here and maybe here,

133
00:19:15,431 --> 00:19:18,990
don’t be afraid,anyway,it will change look after we apply the oil.

134
00:19:18,991 --> 00:19:28,080
It’s very important not to be afraid and dare.Be cool,the reason for us doing it is to relax.

135
00:19:31,480 --> 00:19:40,200
Nice,we put some acrylic rust,no chemicals at all.Wash your brush and dry again.

136
00:20:14,400 --> 00:20:26,060
Nice,it’s time for the oil,we need some aluminum foil and we will classically use...

137
00:20:26,061 --> 00:20:27,940
In general we are going to use lots of oil kinds,

138
00:20:27,941 --> 00:20:34,470
today since its our first project we will use this one, baked Ombra, for safety.

139
00:20:34,471 --> 00:20:43,150
We like baked Ombra.The most classic painting oil. Most painters use them.

140
00:20:43,151 --> 00:20:54,060
I like Van Gogh colours they are quite good, so let's do the oil part...

141
00:20:54,061 --> 00:21:08,000
Unlike Maria who uses oil diluted with thinner and then wipes it out with a piece of cloth

142
00:21:08,001 --> 00:21:16,450
I prefer oil thick as it is.I need a pig hair brush,there so many brushes we may use,

143
00:21:16,451 --> 00:21:28,980
depending on what you want to do,the cat’s tongue,this soft one,why not use this oil painting brush.

144
00:21:28,981 --> 00:21:36,610
Take a small quantity not too much otherwise you’ll find it difficult to wipe it out.

145
00:21:36,611 --> 00:21:37,940
We do want to cover it all,see?

146
00:21:37,941 --> 00:21:44,210
Especially in the alcoves,but not too much paint,

147
00:21:44,410 --> 00:21:55,080
thus we “drain” the paint found on our brush….

148
00:21:56,000 --> 00:21:59,480
fast forward again....and…we will be back soon…!

149
00:22:26,500 --> 00:22:39,120
Well,here we are,we have covered our clay with oil, both the wings and the clock...

150
00:22:39,121 --> 00:22:44,130
and now we start wiping out the oil using a baby wipe,our main“tool”.

151
00:22:44,131 --> 00:22:59,800
Piece by piece,we make a small ball of the baby wipe in our hand and start removing the oil.

152
00:23:08,801 --> 00:23:19,100
As you may see more oil is left close to the alcoves.
That’s because we don’t put strength there,

153
00:23:19,101 --> 00:23:23,570
on the contrary if we want to remove extra oil we press our baby wipe more onto the surface.

154
00:23:23,571 --> 00:23:33,350
You also see that the orange painted points,come up with a rusty effect.

155
00:23:37,351 --> 00:23:40,020
As you may see the result is magical!

156
00:23:45,130 --> 00:23:51,440
As long as we have covered all the clay surface with oil,we finally remove it gently.

157
00:23:51,441 --> 00:23:55,490
That’s what during seminars I’ve been always crying out,this is the project part that we should enjoy.

158
00:23:55,491 --> 00:23:42,120
The part we were waiting for.Of course if there is anything we don’t like,

159
00:24:11,900 --> 00:24:31,090
we may apply some oil again and with our fingers make changes or corrections in colour brightness.

160
00:24:31,091 --> 00:24:36,500
The truth is that the orange colour does not contrast too much .

161
00:24:36,501 --> 00:24:43,820
There is a difference but not a big one.Since I want the clock numbers to look darker,

162
00:24:43,821 --> 00:24:56,980
my movements removing oil are even gently.At some points I reduce or heighten the light.

163
00:24:56,981 --> 00:25:02,950
I play with the oil.I don’t mind if I left more oil quantity on the wings,

164
00:25:02,951 --> 00:25:08,070
actually that’s what we want.You can see the result.Let’s finish the other wing too.

165
00:25:08,071 --> 00:25:20,199
Watch how I remove the oil.I don’t move back and forth. From the inner parts to the outer ones.

166
00:25:20,200 --> 00:25:24,400
I gently wipe the points where extra oil is preferred to be left,

167
00:25:24,401 --> 00:25:27,720
while I need more pressure in wiping to those points where I want oil to be removed.

168
00:25:27,721 --> 00:25:31,560
As you can see I used only two baby wipes just by alternating their clean sides in my hand.

169
00:25:31,561 --> 00:25:39,380
They are enough for the whole project.

170
00:26:08,619 --> 00:26:15,400
Now I need to adjust all pieces of clay together and check if there are any brightness differences.

171
00:26:15,401 --> 00:26:24,630
The outer parts of the wings should be lighter.

172
00:26:43,500 --> 00:26:46,880
Ok ,the small piece is …………..

173
00:27:03,200 --> 00:27:04,280
and as you can see we have the final result.

174
00:27:04,281 --> 00:27:12,650
If we see any blue odds and ends left we just cover it with oil.

175
00:27:12,651 --> 00:27:15,049
There is a chance that we notice the original white colour of clay,

176
00:27:17,361 --> 00:27:29,110
we may leave it as it or we might apply some oil with our finger.

177
00:27:32,321 --> 00:27:37,230
Personally I like this  "here and there" white effect.

178
00:27:42,361 --> 00:27:46,210
And again use your finger to darken it.

179
00:27:48,100 --> 00:27:52,150
Actually I remove the paint and reveal the clay.

180
00:28:01,000 --> 00:28:09,480
If we decide to re apply oil after we have already used a baby wipe,

181
00:28:09,481 --> 00:28:12,250
we have,first,to clean the surface with a kitchen roll.

182
00:28:34,251 --> 00:28:42,900
Now it’s time to see what we have created and put our piece of work on the wood and leather project.

183
00:28:49,000 --> 00:28:56,310
It’s quite a big,our first,project but that’s what I asked Maria for.

184
00:28:58,600 --> 00:29:03,790
Maria if you are watching this,thank you very much,take care,see you soon.

185
00:29:22,000 --> 00:29:27,700
Now let’s adjust all the logo pieces together.

186
00:29:28,701 --> 00:29:34,240
It shouldn’t have been broken,anyway ,we managed to overcome that.

187
00:29:38,000 --> 00:29:47,580
As you can see the main clock theme even if it was brighter,now it has a leatherish colour.

188
00:29:47,581 --> 00:29:49,251
We are able to see the orange colour.

189
00:29:51,252 --> 00:29:57,311
Although both paints have the same shade base,that redish touch is clear.

190
00:29:57,312 --> 00:30:00,790
And here is the colour originally used for the wings.

191
00:30:04,000 --> 00:30:15,220
We will center our clay on the wood so as to glue it.

192
00:30:31,000 --> 00:30:34,880
We turn,carefully,upside down so as not the leather gets smeared

193
00:30:36,000 --> 00:30:55,890
and apply hobby extra forte glue by Stamperia perimetrically at the edges....much a quantity

194
00:30:58,489 --> 00:30:58,289
Some points will need extra oil,however,we also need not make it very intense.

195
00:31:15,000 --> 00:31:22,690
The tip here is to move our clay like this and let the glue underneath spread.

196
00:31:22,691 --> 00:31:27,970
it will soon get harder to move.

197
00:31:27,971 --> 00:31:35,900
Even though this glue becomes transparent,

198
00:31:35,901 --> 00:31:38,260
Use a baby wipe to clean any extra glue sticking out of the clay.

199
00:31:38,261 --> 00:31:45,880
it’s better to use our brush to clean it. 
.... It doesn’t move any longer.

200
00:31:51,881 --> 00:32:01,980
Now the other piece.We need to clean this side very well as glue won’t work on oil.

201
00:32:03,990 --> 00:32:20,040
Dry with paper towels and put some glue on the top point,in the middle and at the back side too.

202
00:32:28,360 --> 00:32:30,390
Shake it a little bit.

203
00:32:33,391 --> 00:32:38,170
Obviously it would have been easier,if my clay hadn’t been broken.

204
00:32:38,171 --> 00:32:44,520
Again use baby wipe and brush for the remaining glue.

205
00:33:00,000 --> 00:33:03,860
Now some extra forte glue for the smaller piece.

206
00:33:03,861 --> 00:33:11,780
Extra forte is a very good glue but I think it’s more or less

207
00:33:11,781 --> 00:33:21,580
a hobby glue for  scrabooking papers and other materials.Atlacol would be good too.

208
00:33:23,000 --> 00:33:30,010
Maria uses atlacol for clay.

209
00:33:38,570 --> 00:33:41,400
This small piece will be more difficult to glue.

210
00:33:41,401 --> 00:33:44,210
We don’t care about the small crack,Isee,here.We brush with oil,

211
00:33:44,211 --> 00:33:47,150
we wipe it out and the crack disappears.

212
00:33:58,130 --> 00:34:10,440
We are going to create the leatherish effect on the wood edges,straight on the wood.

213
00:34:14,460 --> 00:34:20,230
We are going to do the same thing on the leather papers ends.

214
00:34:20,231 --> 00:34:34,650
Not too much oil,not full black just a touch.Brush and wipe out with your finger.

215
00:34:48,700 --> 00:35:04,290
Not all sides at once,little by little.Depending on which surface I put oil,

216
00:35:05,710 --> 00:35:08,080
I  fade it out either by moving my finger cyclically or up and down.

217
00:35:39,489 --> 00:35:47,760
Well ,we are almost done.Now I m checking some details,extra oil may be necessary.

218
00:35:47,761 --> 00:35:54,030
Firstly clean your hands with a baby wipe and then wash with soap

219
00:35:57,031 --> 00:35:58,490
or even better use some hand crème.

220
00:35:58,500 --> 00:36:07,930
See what we are going to do now.Well we are going to take these nail pins, see here...

221
00:36:28,489 --> 00:36:39,870
not too many and we will nail down almost any free paper corner edge.Let’s start nailing.

222
00:36:39,871 --> 00:36:43,150
The video will be in fast forward mode,we will be back soon as always.

223
00:37:11,000 --> 00:37:15,240
Here we are again.We will add some splashes on our clay project only,

224
00:37:15,241 --> 00:37:24,800
using an ivory colour,not straight white,diluted with very little water

225
00:37:39,489 --> 00:37:42,830
I take a small quantity of paint as I want to avoid messing my leather

226
00:37:42,831 --> 00:37:47,540
and with my finger,slow and gently,splash my clay.

227
00:37:47,541 --> 00:38:00,660
Create some extra splashes in the center of your clay and much more less,

228
00:38:08,000 --> 00:38:19,980
almost fading out as you move to the edges of the wings.

229
00:38:39,170 --> 00:38:45,770
If it happens that you splash your leather,use quickly a baby wipe and you will be ok.

230
00:39:13,489 --> 00:39:23,050
And the result is……let’s see….very nice.

231
00:39:25,051 --> 00:39:29,460
.That’s all for today,thank you for watching,

232
00:39:29,461 --> 00:39:34,280
I hope you enjoyed it.It was the first time.

233
00:39:34,281 --> 00:39:44,930
I’ll do my best for even more detailed videos in order to help you reproduce the projects

234
00:39:44,931 --> 00:39:49,631
or help you create new ones using your imagination.

235
00:39:49,632 --> 00:40:00,200
See you next week.We expect new friends from round the world to become members.

236
00:40:00,201 --> 00:40:03,280
I’d like to thank Maria once again for the logo clay she created….

237
00:40:03,281 --> 00:40:06,190
Kiss you ,bye

