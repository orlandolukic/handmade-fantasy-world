<?php 

	class Language_SR
	{
		public $about                = "O nama",
			   $home                 = "Početna",
			   $title_head			 = "Naslov",
			   $review				 = "Utisak",
			   $popular_workshops    = "Popularne radionice",
			   $workshop             = "Radionica",
			   $workshops            = "Radionice",
			   $recent_Workshops     = "Nedavne radionice",
			   $visit_workshop		 = "Posetite radionicu",
			   $share_workshop		 = "Podelite radionicu",
			   $unlimited_workshop	 = "Neograničene radionice",
			   $news_newWorkshop	 = "Obaveštenja o novim radionicama",
			   $contact              = "Kontakt",
			   $addWishlist          = "Dodaj u <span class=\"strong\">Listu želja</span>",
			   $publishDate          = "Datum objavljivanja",
			   $till                 = "do",
			   $unlimited            = "Neograničeno",
			   $oneMonth             = "1 mesec",
			   $threeMonths          = "3 meseca",
			   $oneYear              = "1 godina",
			   $followUs			 = "Pratite nas",
			   $findUsOnFace		 = "Pronađite nas na Facebook-u",
			   $share_facebook		 = "Podelite na Facebook-u",
			   $findUsOnTwitter		 = "Pronađite nas na Twitter-u",
			   $share_twitter		 = "Podelite na Twitter-u",
			   $findUsOnGoogle		 = "Pronađite nas na Google-u",
			   $findUsOnDribbble	 = "Pronađite nas na Dribbble-u",
			   $findUsOnPinterest	 = "Pronađite nas na Pinterest-u",
			   $findUsOnInstagram    = "Pronađite nas na Instagramu",
			   $facebook             = "Facebook",
			   $instagram            = "Instagram",
			   $sale			     = "Rasprodaja",
			   $wish_list			 = "Lista želja",
			   $alreadyOnList		 = "Već u listi želja",
			   $addToWish			 = "Dodajte u listu želja",
			   $lang				 = "Jezik",
			   $serbian				 = "Srpski",
			   $english				 = "Engleski",
			   $polish				 = "Poljski",
			   $russian				 = "Ruski",
			   $register			 = "Registracija",
			   $login				 = "Prijava",
			   $views                = "Pregledi",
			   $inCart               = "U korpi",
			   $succAddedInCart      = "Uspešno ste dodali proizvod u korpu",
			   $removeProductFromCart= "Brisanje proizvoda iz korpe",
			   $removeAllProductCart = "Obrišite celokupan sadržaj korpe",
			   $succDeletedAll       = "Uspešno ste obrisali sadržaj korpe.",
			   $succDeletedOne       = "Uspešno ste obrisali proizvod iz korpe.",
			   $succAddedCart        = "Uspešno ste dodali proizvod u korpu.",
			   $succChangedEmail     = "Uspešno ste promenili e-mail adresu.",
			   $package_self         = "Paket",
			   $gallery              = "Galerija",
			   $news                 = "Novosti",
			   $predefined           = "Podrazumevano",
			   $privileged           = "Privilegovan korisnik",
			   $rest_links           = "Ostali linkovi",
			   $calendar             = "Kalendar",
			   $accessAllVideos      = "Imate pristup svim radionicama.",
			   $searchByMonth        = "Pretražite radionice po mesecima",
			   $myCalendar           = "Moj kalendar",
			   $more                 = "Još",
			   $currNoWS             = "Trenutno nemamo aktivne radionice.",
			   $currNoWSMonth        = "Trenutno nemamo aktivne radionice u ovom mesecu.",
			   $currNoIngr           = "Trenutno nema sastojaka za ovu radionicu.",
			   $currNoNews           = "Trenutno nema novosti.",
			   $currNoPopWS          = "Trenutno nemamo radionice.",
			   $currNoLink           = "Trenutno nema linka",
			   $selectMonth          = "Odaberite mesec",
			   $no_subsMonth         = "Nema meseci",
			   $quantity             = "Količina",
			   $suppliers            = "Dobavljači",
			   $links                = "Linkovi",
			   $shoppingMaterials    = "Ukoliko želite da kupite materijale, molimo odaberite zemlju.",  
			   $btnEmailTmplNoWork   = "Ukoliko dugme ne radi, prekopirajte ovaj link u vas browser",  
			   $have_coupon          = "Imate kupon za ovu radionicu",
			   $unsubscribe          = "Ne želite više da primate ovakve email-ove?",
			   $unsubscribed         = "Pretplata odjavljena",
			   $succ_unsubscribed    = "Uspešno ste se odjavili sa ove mailing liste.",
			   $redirection_in       = "Preusmeravanje na početnu stranicu za ",
			   $you_can              = "Možete da se odjavite sa ove liste ",
			   $unsubscribe_lst      = "ovde",
			   $e_ws_start_today     = "počinje danas",
			   $lookOnOurWebsite     = "Pogledajte na našem sajtu",
			   
			   $office				 = "Kancelarija",
			   $message              = "Poruka",
			   
			   $comment_a			 = "Komentara",
			   $comment_i			 = "Komentari",
			   $comment_site		 = "Komentarišite...",
			   $comment_delete		 = "Brisanje komentara?",
			   $check_comment		 = "Pogledajte komentare",
			   $send_comment		 = "Pošaljite komentar",
			   $more_comment		 = "Još komentara",
			   $review_delete		 = "Brisanje utiska?",
			   $areYouSureReview	 = "Da li ste sigurni da želite da obrišete ovaj utisak?",
			   $myComment			 = "Moj komentar",
			   $allRightsReserved	 = "Sva prava zadržana",
			   $verifiedUser		 = "Verifikovan korisnik",
			   $delete				 = "Brisanje",
			   $cancel				 = "Odustanite",
			   $onCheck			 	 = "Na proveri",
			   $delete_Review		 = "Obrišite utisak",
			   $change_Review		 = "Izmenite utisak",
			   $allow_Review		 = "Dozvolite utisak",
			   $enter_titleReview	 = "Unesite naslov utiska",
			   $enter_textReview	 = "Unesite tekst utiska",
			   $review_sent			 = "Vaš utisak je poslat na proveru.",
			   $send_Review			 = "Pošaljite utisak",
			   $grade				 = "Ocena",
			   $reply				 = "Odgovorite",
			   $quality				 = "Kvalitet",
			   $enchant_magic		 = "Magija koja opčinjava",
			   $satisfied_customers	 = "Zadovoljni korisnici",
			   $many_news			 = "Puno novosti",
			   $work_relax			 = "Rad + uživanje",
			   $new_stuff			 = "Novo",
			   $top_stuff			 = "Top",
			   $people_whoVisited	 = "Ljudi koji su posetili radionicu",
			   $challenge_areReady	 = "Da li ste spremni da se prepustite izazovu?",
			   $learn_more			 = "Saznajte Više",
			   $pay_plan			 = "Plan pretplate",
			   $beginner			 = "Početnik",
			   $month				 = "Mesec",
			   $subscribe_now		 = "Pretplatite se odmah",
			   $subscribe			 = "Pretplatite se",
			   $subscribed			 = "Pretplaćeni ste",
			   $subscribe_for        = "Dodajte u korpu",
			   $subscription_seccess = "Pretplaćeni ste na ovu radionicu.",
			   $chooseMSUB           = "Izaberite mesec za pretplatu",
			   $access_workValid	 = "Pristup radionici važi",
			   $artist				 = "Umetnik",
			   $master				 = "Maestro",
			   $watch_video			 = "Gledajte Video",
			   $videos               = "Radionice",
			   $package_type 		 = "Tip paketa",
			   $package_price 		 = "Cena paketa",
			   $package_detailHere	 = "Detaljnije ponudu paketa možete pogledati",
			   $here				 = "ovde",
			   $workshop_description = "Opis radionice",
			   $impressions			 = "Utisci",
			   $recent_impressions	 = "Nedavni utisci",
			   $top_impressions		 = "Top utisci",
			   $leave_impression	 = "Ostavite Vaš utisak",
			   $delete_impressionSucc= "Uspešno ste obrisali utisak.",
			   $change_impressionSucc= "Uspešno ste obrisali utisak.",
			   $error_whileProcess	 = "Dogodila se greška prilikom obrade podataka.",
			   $user_a				 = "korisnika",
			   $user_impression		 = "Utisci korisnika",
			   $no_impressionsATM	 = "Trenutno nema utisaka.",
			   $no_commentsATM	 	 = "Trenutno nema komentara.",
			   $yes					 = "Da",
			   $no 					 = "Ne",
			   $didNotFill_RightWay	 = "Niste na pravilan način popunili sva obavezna polja.",
			   $necessary_field		 = "Obavezna polja su označena sa",
			   $talent_alwaysSurprise= "Svi imaju talenat",
			   $workshop_wka         = "Radionice sa umetnikom svetskog glasa",
			   $succAddWishLst       = "Uspešno ste dodali radionicu u listu želja.",
			   $buyWarranty 		 = "When You buy workshop, you have 24/7 access to it's video.",
			   $peopleVisited        = "Ljudi koji su posetili radionicu",
			   $in                   = "u",
			   $emailExample         = "npr. neko@domen.com",
			   $comeBack             = "Vratite se nazad",
			   $backToTop            = "Nazad na vrh",
			   $show_details         = "Detaljnije",
			   $no_events            = "Ne postoje dogadjaji za ovaj mesec.",

			   // Template
			   $new_Message			 = "Nova poruka",
			   $message_Reply		 = "Odgovori",
			   $user_name			 = "Ime",
			   $user_surname		 = "Prezime",
			   $subject				 = "Tema",
			   $request_changePass	 = "Zahtev za promenu lozinke",
			   $dear                 = "Poštovani",
			   $changePass_pushButton= "Promenu lozinke mozete uciniti pritiskom na dugme.",
			   $ifNot_thenHere		 = "U slucaju da dugme ne radi, lozinku mozete promeniti",
			   $change_pass			 = "Promeni",
			   $password_changed	 = "Promenjena lozinka",
			   $pressButton_toConfirm= "Da biste potvrdili aktivaciju vaseg naloga molimo vas da pritisnete dugme.",
			   $request_approve	     = "Potvrda registracije",
			   $ifButtonNot_Link	 = "Ukoliko dugme ne radi, pokušajte da aktivirate nalog klikom na sledeći",
			   $link 				 = "link",
			   $thankYou			 = "Zahvaljujemo vam se",
			   $contact_Us			 = "Kontaktirajte nas",
			   $new_review			 = "Novi utisak",
			   $new_workshop 		 = "Nova radionica",
			   $workshop_forSale	 = "Radionica je na prodaju",
			   $details				 = "Detalji",
			   $add_toCart			 = "Dodaj u korpu",
			   $toCart   			 = "Dodaj u korpu",
			   $user 				 = "Korisnik",
			   $hasLeft_aReview		 = "je ostavio utisak na radionici",
			   $see_ApproveDelete	 = "Mozete pregledati,odobriti ili obrisati ovaj utisak",
			   $review_overview		 = "Pregled utiska",
			   $success_Activation	 = "Uspešno ste aktivirali vaš nalog.",
			   $happy_YoureBack		 = "Radujemo se vašem povratku",
			   $pib 				 = "PIB",
			   $mb 					 = "MB",
			   $email_UP 			 = "EMAIL",
			   $start_price			 = "Osnovica",
			   $pdv_amount			 = "Iznos PDV",
			   $totalUP				 = "UKUPNO",
			   $check_invoice		 = "Vidite fakturu",
			   $handmade_isin		 = "je u sistemu PDV-a, stoga obračunava porez na dodatu vrednost",
			   $formoreinfo			 = "Za sve dodatne informacije obratite se na",
			   $reg_succ             = "Uspešno ste se registrovali na <a href=\"http://www.handmadefantasyworld.com/\">Handmade Fantasy World</a>",
			   $newUser 			 = "Novi korisnik",
			   $newUser_Register	 = "Novi korisnik se upravo registrovao.",
			   $question_WS			 = "Pitanje sa radionice",
			   $ifNot_ButtonRespond	 = "U slucaju da dugme ne radi,možete odgovoriti",
			   $belgrade			 = "Beograd",
			   $serbia 				 = "Srbija",
			   $ifLinkNotWork		 = "Ukoliko dugme ne radi, prekopirajte ovaj link u vas browser ",
			   $subFor 				 = "Pretplata za",
			   $coupon               = "Kupon",

			   $won_coupons			 = "Osvojeni kuponi",
			   $check_coupons		 = "Pogledajte kupone koje ste osvojili",
			   $watch_Now			 = "Pogledajte sada",
			   $we_hopeYoullEnjoyONE = "Nadamo se da ćete uživati u ovoj radionici",
			   $we_hopeYoullEnjoyM   = "Nadamo se da ćete uživati u ovim radionicama",
			   $see_you				 = "Vidimo se",
			   $online_ws 			 = "Online radionica",


			   //about.php
			   $new_WorkshopsUP		 = "NOVE RADIONICE",
			   $new_opportunitiesUP	 = "NOVE PRILIKE",
			   $new_ideasUP			 = "NOVE IDEJE",
			   $highest_ratesUP		 = "NAJVIŠIH OCENA",
			   $workshopUP 			 = "RADIONICA",
			   $active_usersUP		 = "AKTIVNIH KORISNIKA",
			   $meet_ourTeamUP		 = "UPOZNAJTE NAŠ TIM",
			   $artistUP			 = "UMETNIK",
			   $koordinatorUP		 = "KOORDINATOR",
			   $contributorUP		 = "SARADNIK",
			   $finance_directorUP	 = "FINANSIJSKI DIREKTOR",
			   $what_peopleSayUP	 = "ŠTA LJUDI KAŽU",
			   $graduate_economics	 = "Diplomirani ekonomista",
			   $graduate_law		 = "Diplomirani pravnik",
			   $physiotherapist		 = "Fizioterapeut",
			   $director_newBusiness = "Direktor NewBisuness centra",

			   //coming.php
			   $hi_were				 = "Hi, we're",
			   $website_comingSoon	 = "Our Website is Coming Soon",
			   $online_WSAvailable	 = "Online workshops will be available",
			   $very_soon 			 = "very soon",
			   $days				 = "Dani",
			   $hours				 = "Sati",
			   $minutes				 = "Minuti",
			   $seconds				 = "Sekunde",


			   //contact.php
			   $always_forNewContact = "Uvek raspoloženi za nove kontakte",
			   $sayHello			 = "Javite nam se",
			   $wont_postEmail		 = "Nećemo objavljivati Vašu e-mail adresu. Obavezna polja su označena sa *.",
			   $errors_occurred		 = "Dogodile su se greške",
			   $error_email			 = "E-mail adresa nije ispravno uneta.",
			   $error_name			 = "Ime nije ispravno uneto.",
			   $min_tenCharacters	 = "Poruka mora imati najmanje 10 karaktera.",
			   $send_message		 = "Pošaljite poruku",
			   $contactinfo          = "Kontakt informacije",
			   $welcome_message		 = "Dobrodošli na naš website. Drago nam je da ste ovde. Možete nas kontaktirati u svakom trenutku putem e-maila ili nam možete poslati pismo..",
			   $telephone			 = "Telefon",
			   $email				 = "E-mail",
			   $mustAgreeTerms       = "Morate da se složite sa uslovima korišćenjima.",

			   //index.php
			   $newUP 				 = "NOVO",
			   $year 				 = "godina",
			   $scrbForMyWS          = "Pretplatite se na online radionice",

			   //login.php
			   $please_login		 = "Prijavite se",
			   $add_productToCart	 = "Dodajte proizvod u korpu",
			   $login_toBuy			 = "Da biste nastavili kupovinu, molimo Vas da se prijavite.",
			   $error_usernameorPass = "Korisničko ime ili lozinka nisu ispravni.",
			   $or 					 = "ili",
			   $facebook_login		 = "Facebook prijava",
			   $google_login		 = "Google Plus prijava",
			   $not_registered		 = "Niste registrovani?",
			   $become_member		 = "Postanite naš član",
			   $forgot_pass			 = "Zaboravili ste lozinku?",
			   $send_passChange		 = "Pošaljite mi zahtev za promenu lozinke",
			   $reset_pass 			 = "Reset lozinke",
			   $register_email		 = "Molimo Vas da unesete e-mail adresu preko koje ste se registrovali kod nas.",
			   $error_invalidEmail	 = "E-mail adresa koju ste uneli nije ispravna",
			   $no_emailInDatabase	 = "U našoj bazi ne postoji e-mail adresa",
			   $send_request		 = "Pošaljite zahtev",
			   $sec_token            = "Ulogovali ste se na naš sistem sa drugog uređaja. Molimo Vas, prijavite se ponovo.",
			   $sess_expired         = "Vaša sesija je istekla. Molimo Vas da se prijavite ponovo.",
			    $mssg_sentReq 		 = "Zahtev je poslat na",
			    $force_logout         = "Trenutno održavamo Vaš nalog. Molimo pokušajte za par minuta.",

			   //register.php
			   $no_emailInSistem	 = "Vaša email adresa već postoji na našem sistemu.",
			   $request 			 = "Tražite",
			   $request_forPassChange= "zahtev za promenu lozinke",
			   $name_enoughChar		 = "Ime ima nedovoljno karatkera.",
			   $surname_enoughChar	 = "Prezime ima nedovoljno karatkera.",
			   $user_alreadyExist	 = "Korisničko ime već postoji.",
			   $insert_username		 = "Molimo unesite korisničko ime.",
			   $pass_atLeastMiddle	 = "Lozinka mora imati barem 'srednju' jačinu.",
			   $pass_notMatch		 = "Lozinke se ne poklapaju.",
			   $reg_cityErr          = "Polje 'grad' ne sme biti prazno",
			   $reg_stateErr         = "Polje 'država' ne sme biti prazno",
			   $please_insertPass	 = "Molimo unesite lozinku.",
			   $weak                 = "Slabo",
			   $medium               = "Srednje",
			   $strong               = "Jako",
			   $very_strong          = "Veoma jako",
			   $basic_info			 = "Osnovni podaci",
			   $user_info			 = "Korisnički podaci",
			   $pass_Strength		 = "Snaga lozinke",
			   $matching_passwords	 = "Lozinke se podudaraju",
			   $register_please		 = "Registrujte se",
			   $video_mat            = "Video Materijal",
			   $video                = "Video",
			   $email_purp           = "Preko ove e-mail adrese Vam šaljemo informacije",
			   $pass_atLeastChars    = "Lozinka mora imati najmanje 3 karaktera.",
			   $account_reactivation = "Aktiviran nalog",

			   //workshop.php
			   $workshop_forSale1	 = "Radionica se prodaje",
			   $buy_thisWS			 = "Kupite ovu radionicu",
			   $when_youBuyWS		 = "Kada kupite radionicu, možete stalno da je posećujete i gledate video materijale.",
			   $verify_Accout		 = "Verifikujte Vaš nalog",
			   $verify_toComment	 = "Da biste komentarisali radionice potrebno je da verifikujete Vaš nalog.",

			   //workshops.php
			   $online_workshops	 = "Online Radionice",
			   $online_workshop 	 = "Online Radionica",
			   $sort				 = "Sortiranje",
			   $newest				 = "Najnovije",
			   $oldest				 = "Najstarije",
			   $best_rated			 = "Najbolje ocenjeno",
			   $a_toZ				 = "Abecedno A-Z",
			   $z_toa				 = "Abecedno Z-A",
			   $workshop_perPage	 = "Broj radionica po stranici",
			   $searchForWS          = "Pretražite radionice",
			   $sfw_selectedP        = "Molimo Vas da odaberete željeni mesec",
			   $no_WS                = "Nema radionica",
			   $unableToFindWS       = "Nismo uspeli da pronađemo radionice za mesec",

			   //users/cart.php
			   $cart 				 = "Korpa",
			   $payment_amount       = "Ukupno za plaćanje",
			   $nomore_subs			 = "U Vašoj korpi se nalazi više pretplatničkih paketa. Molimo Vas da odaberete željeni paket.",
			   $you_haveSub			 = "Trenutno ste prijavljeni na pretplatnički paket.",
			   $transaction_denied   = "Transakcija odbijena",
			   $error_paymentBank	 = "Dogodila se greška prilikom plaćanja na stranici Banke. Molimo Vas da pokušate ponovo.",
			   $product 			 = "Proizvod",
			   $price 				 = "Cena",
			   $action 				 = "akcije",	
			   $enter_workshopBetween= "Pristup radionicama u periodu od",
			   $will_beAvailable	 = "Pristup video radionicama iz ovog perioda biće dostupan odmah po uspešno obavljenoj transakciji.",
			   $package_detail		 = "Detaljniji pregled paketa možete pogledati",
			   $empty_cart			 = "Ispraznite korpu",
			   $to_continuePlease	 = "Da biste nastavili kupovinu, molimo Vas da",
			   $verify_yourAcc		 = "verifikujete nalog",
			   $after_payment		 = "Posle plaćanja",
			   $if_paymentSuccess	 = "Ukoliko je transakcija uspešna bićete vraćeni na naš sajt sa obaveštenjem da je plaćanje uspešno obavljeno. U slučaju da plaćanje nije uspelo, bićete 					   vraćeni na naš sajt sa informacijom o neuspelom plaćanju i moći ćete da ponovite plaćanje ili da izaberete drugi način plaćanja.
										Nakon uspešno izvršene transkacije, priznanica će vam biti automatski poslata na e-mail",
			   $for_infoGoTo		 = "Za sve informacije obratite se na",								
			   $information			 = "Informacije",
			   $after_order			 = "Nakon potvrđivanja porudžbine, bićete preusmereni na stranicu na kojoj će se naći primer uplatnice sa svim informacijama vezanim za plaćanje.",
			   $buyer_info			 = "Informacije o kupcu",
			   $info_forPayment		 = "Ove informacije nam služe isključivo za izradu fakutra",
			   $add_newInfo			 = "Dodajte nove podatke",
			   $ready 				 = "Spremno",
			   $name_surname		 = "Ime i prezime",
			   $adress				 = "Adresa",
			   $city 				 = "Grad",
			   $state                = "Država",
			   $email_email			 = "Email",
			   $list_oldInfo		 = "Prikaži listu starih podataka",
			   $hide_oldInfo		 = "Sakrij listu starih podataka",
			   $user_info_bef		 = "Podaci koje ste ranije koristili",	
			   $default				 = "Podrazumevano",
			   $approve_choice		 = "Potrvrdite izbor",
			   $error_happened		 = "Dogodile su se određene greške",
			   $atleast_threeLetters = "Vaše ime i prezime moraju imati najmanje 3 slova.",
			   $adress_five			 = "Vaša adresa mora imati najmanje 5 slova.",
			   $city_five			 = "Grad mora imati najmanje 5 slova.",
			   $phone_seven			 = "Broj telefona mora imati više od 7 brojeva.",
			   $phone_signs			 = "Broj telefona sadrži znakove ili slova.",
			   $info_onBuyerOnlyOnce = "Popunjavanje informacija o kupcu popunjavate samo jednom",
			   $insert_info			 = "Dodajte podatke",
			   $iAgree				 = "Slažem se sa",
			   $procedure_andUses	 = "procedurama i uslovima korišćenjima",
			   $termsOfUse      	 = "Uslovi korišćenja",
			   $price_withoutPDV	 = "Cena bez PDV",
			   $pdv   				 = "PDV",	 	
			   $total_buy			 = "Ukupno za uplatu",
			   $continue_withPay	 = "Nastavite sa plaćanjem",
			   $overview 			 = "Pregled",
			   $ready_forPayment	 = "Vaša narudžbina je spremna za plaćanje. Molimo pogledajte još jednom Vašu korpu.",
			   $bank_card			 = "Banka / Podržane kartice",
			   $online_buying		 = "Online plaćanje kreditnim karticama omogućila",
			   $aik_bank			 = "AIK banka",
			   $secure_payment		 = "Sigurna transakcija",
			   $visible_onlyToAIK	 = "Iz sigurnosnih razloga, podaci o vašoj platnoj kartici su vidljivi samo AIK banci kao procesoru kartica. Stranica banke je zaštićena i sigurna za ovaj 						način plaćanja.",	
			   $responsibility		 = "se odriče odgovornosti vezanih za plaćanje putem interneta.",
			   $info_foreignCurrency = "Informacije o plaćanju u stranoj valuti",	
			   $empty_yourcart		 = "Vaša korpa je prazna.",
			   $no_productsInCart	 = "Trenutno ne postoje proizvodi koji su u Vašoj korpi.",
			   $continue_buying		 = "Nastavite kupovinu",
			   $basics               = "Osnovno",	
			   $succSendNewEmail     = "Uspešno smo Vam poslali novi verifikacioni email.",
			   $succSendQuestion     = "Uspešno ste poslali Vaše pitanje. Odgovorićemo Vam čim budemo pročitali.",
			   $customerInfoSave     = "Molimo Vas da sačuvate podatke o kupcu.",
			   $AIK_proc             = "Ako koristite AIK banku kao način plaćanja, molimo Vas da sačekate da se završi transakcija. NE ZATVARAJTE stranicu na kojoj se odigrava uplata, već sačekajte da Vas web browser vrati ponovo na naš sajt.",
			   $note                 = "Napomena",

			   // users/index.php
			   $user_pannel			 = "Korisnički panel",	
			   $notification		 = "Obaveštenje",
			   $workshop_u			 = "Radionicu",
			   $have_alreadyBought	 = "Već ste kupili",
			   $check_workshop		 = "Gledajte video",
			   $inactive_payment	 = "Neaktivna pretplata",
			   $payment_whichis		 = "Postoji pretplata koja je",
			   $currently			 = "trenutno",
			   $inactive 			 = "neaktivna",
			   $name 				 = "Naziv",
			   $date_start			 = "Datum početka",
			   $date_expire			 = "Datum isteka",
			   $activate_sub		 = "Aktivirajte pretplatu",
			   $payment_successfull	 = "Transakcija uspešno obavljena",
			   $cart_emptyCanProceed = "Sadržaj Vaše korpe je sada prazan. Možete pristupati online radionicama.",
			   $payment_details		 = "Detalji transkacije",
			   $payment_ID 			 = "Transakcijski ID",
			   $invoice				 = "Fakutra",
			   $just_date			 = "Datum",	
			   $ammount				 = "Iznos",
			   $currency 			 = "Valuta",
			   $show_invoice		 = "Prikaz fakture",	
			   $sub_info			 = "Podaci o pretplati",
			   $package_name		 = "Naziv paketa",
			   $package_duration	 = "Trajanje paketa",
			   $sub_start			 = "Početak pretplate",
			   $sub_end				 = "Kraj pretplate",
			   $appointment_use		 = "Termin korišćenja",
			   $all_packageProposal  = "Predlog svih paketa",
			   $tosee_onlineWSVerify = "Da biste gledali online radionice ili ostavljali utiske na našem sajtu potrebno je da verifikujete Vaš nalog.",
			   $weve_sentemail		 = "Poslali smo Vam verifikacioni e-mail na",
			   $didnt_recieveEmail	 = "Niste primili e-mail?",
			   $send_newEmail		 = "Pošaljite mi novi e-mail",
			   $ws_detail			 = "Detalji radionice",
			   $addToWishList        = "Dodajte u listu želja",
			   $addedWishList        = "Dodato u listu želja",

			   // users receipt.php
			   $invoice_view		 = "Pregled fakture",	
			   $invoice_individual	 = "Fakturisano fizičkom licu",
			   $contact_phone		 = "Kontakt telefon",
			   $user_nameone		 = "Korisničko ime",
			   $warning				 = "Napomena",	
			   $pdv_sistem			 = "Handmade Fantasy World je u sistemu PDV-a, stoga obračunava porez na dodatu vrednost.",
			   $payment_status		 = "Status transakcije",
			   $paid 				 = "Plaćeno",
			   $in_process           = "U procesu",
			   $cash                 = "Keš",
			   $payment_date		 = "Datum transakcije",
			   $payment_method		 = "Način plaćanja",
			   $id_payment			 = "ID transakcije",
			   $invoice_number		 = "Broj fakture",	
			   $total 				 = "Ukupno",
			   $number 				 = "R.br.",
			   $name_package		 = "Naziv proizvoda",
			   $price_pdvA			 = "Cena bez PDV-a",
			   $package 			 = "Paket",
			   $sub_lasts			 = "Pretplata traje",
			   $ws_lowchar			 = "Radionica",
			   $confirm              = "Potvrdite", 
			   $emailChange          = "Promena e-maila",    

			   // WEBSITE PROPERTY			   
			   $wb_description       = "Online mixed media workshops. Join us and improve your artistic skills and passion to create. Let's have fun together!",               

			   // settings.php
			   $acc_settOpt          = "Podešavanja",
			   $acc_logout           = "Odjavite se",        
			   $acc_settings		 = "Podešavanja naloga",
			   $password_change		 = "Promena lozinke",
			   $confirm_change       = "Potvrdite promenu",
			   $oldPass_notValid	 = "Stara lozinka nije ispravna",
			   $pass_areNotMatching	 = "Lozinke se ne podudaraju",
			   $old_passCantBeEmpty  = "Stara lozinka ne sme biti prazna",
			   $new_passCantBeEmpty	 = "Nova lozinka ne sme biti prazna",
			   $last_timeYouChanged	 = "Poslednji put ste promenili lozinku",
			   $loading              = "Učitava se...",
			   $pleaseWait           = "Molimo sačekajte...",
			   $yearLOW				 = "godine",
			   $new_pass 			 = "Nova lozinka",
			   $enterNewPass         = "Unesite novu lozinku",
			   $enterNewEmail        = "Unesite novu e-adresu",
			   $enterNewUsername     = "Unesite novo korisničko ime",
			   $enterNewUsername_s   = "Novo korisničko ime",
			   $equal_usernames      = "Uneli ste isto korisničko ime koje sada koristite.",
			   $confirmNewPass       = "Potvrdite novu lozinku",
			   $repeat_newPAss		 = "Ponovite novu lozinku",
			   $passwords_areMatching= "Lozinke se poklapaju",
			   $approve_passChange	 = "Potvrdite promenu lozinke",
			   $error_happenedOne	 = "Dogodila se greška",
			   $please_enterEmail	 = "Molimo Vas unesite novu e-mail adresu",
			   $change_email		 = "Promenite e-mail adresu",	
			   $after_emailChange	 = "Nakon promene e-mail adrese poslaćemo Vam informacioni e-mail o promeni na novu adresu koju budete uneli",
			   $deactivate_Acc		 = "Deaktivacija naloga",
			   $areYouSure 			 = "Da li ste sigurni da želite da deaktivirate nalog? Nalog je moguće ponovno aktivirati u bilo kom trenutku",
			   $acc_deactivate		 = "Deaktivirajte nalog",
			   $deact_success		 = "Uspešno ste deaktivirali nalog",
			   $redirect_toMain		 = "Preusmeravanje na početnu stranicu za",
			   $remove_profilePic	 = "Uklonite profilnu sliku",
			   $change_profilePic	 = "Promenite profilnu sliku",
			   $acc_verified		 = "nalog verifikovan",
			   $acc_NotVerified		 = "nalog nije verifikovan",
			   $change_emailAdress	 = "Izmenite e-mail adresu",
			   $your_language 		 = "Jezik",
			   $want_toRecieve		 = "Želim da primam",
			   $newsletter			 = "newsletter",
			   $s 					 = "-e",
			   $maxPicSize			 = "Maksimalna dozvoljena veličina slika je",
			   $pass 				 = "Lozinka",
			   $confPass             = "Potvrdite lozinku",
			   $change_yourPass		 = "Promenite lozinku",
			   $passChange_LastTime	 = "Lozinka poslednji put promenjena",
			   $after_deactivation	 = "Nakon dekativacije, nalog možete ponovno aktivirati u bilo kom trenutku",
			   $passwordStrength     = "<div style='font-size: 120%; margin-top: 5px' class='uppercase text-left strong'>Jačina lozinke</div><div style='margin-top: 5px'><div class='text-left'>Dobra lozinka sadrži:<br><br>1. Mala slova (minimum 5)<br>2. Veliko slovo (minimum 1)<br>3. Neki znak (#,$,%,&,*,.)</div></div>",
			   $email_address        = "E-mail adresa",

			   // subscriptions.php
			   $subs 				 = "Pretplate",
			   $all_subs			 = "Sve pretplate",
			   $allSubs_List		 = "Spisak svih pretplata",
			   $data_product		 = "Podaci o proizvodu",
			   $status 				 = "Status",
			   $sales				 = "Akcije",
			   $noSubs_ATM			 = "Trenutno ne postoje pretplate",
			   $deactivate_sub		 = "Deaktiviranje pretplate",
			   $if_youDeactivateSub	 = "Deaktiviranjem pretplate gubite pravo na pristup aktivnim radionicama",
			   $deact_yourSub		 = "Deaktivirajte pretplatu",
			   $activation_date		 = "Datum aktivacije",
			   $expired				 = "isteklo",
			   $inactive_an 		 = "neaktivan",
			   $active 				 = "aktivan",
			   $credit_card			 = "Kreditna kartica",
			   $paypal 				 = "PayPal Express Checkout",
			   $deactivation 		 = "Deaktivacija",

			   // wishlist.php
			   $my_wishlist			 = "Moja lista želja",
			   $noWS_inYourWishList	 = "Trenutno ne postoje radionice u Vašoj listi želja",
			   $all_workshops		 = "Sve radionice",
			   $others_like			 = "Drugi vole ovo",
			   $subscription 		 = "Pretplata",
			   $select_package		 = "Odaberite željeni paket",
			   $addToCart 			 = "Dodajte u korpu",
			   $delete_fromWishlist	 = "Obrišite iz liste želja",

			   // workshops.php
			   $my_workshops 		 = "Moje radionice",
			   $allowed_WS			 = "Dostupne radionice",
			   $noWS_ATM			 = "Trenutno nema dostupnih radionica",
			   $no_WSOnyourAcc		 = "Na Vašem nalogu ne postoje radionice koje možete gledati.",
			   $pachages_low		 = "paketa",
			   $no_rating            = "nema ocenu",

			   $_403                 = "Nemate dozvolu da pristupate ovoj stranici.",
			   $h403                 = "Zabranjen pristup",
			   $_404                 = "Dogodila se greška prilikom lociranja stranice. Stranica koju ste pokušali da posetite je trenutno nedostupna ili ne postoji.",
			   $h404                 = "Stranica nije pronađena",
			   $_500                 = "Dogodila se greška na serveru. Molimo pokušajte ponovo.",
			   $h500                 = "Unutrašnja greška servera",
			   $error                = "Greška",


			   $receipt              = "Faktura",
			   $payment              = "Plaćanje",
			   $comment              = "Komentar",
			   $comments             = "Komentari",
			   $email_change         = "Promena e-mail adrese",

			   // VIDEO.PHP
			   $passForPass          = "Lozinka za pristup",
			   $inOrderToWatch       = "Da biste gledali video sadržaj, molimo Vas da prekopirate dobijenu lozinku.",
			   $copy                 = "Kopiraj",
			   $copyPass             = "Prekopirajte lozinku",
			   $succCopied           = "Uspešno ste prekopirali lozinku. Možete je nalepiti u polje <span class='strong'>password.</span>",
			   $likeIt               = "Sviđa mi se",
			   $likeItImage          = "Sviđa mi se slika",
			   $dislikeIt            = "Ne sviđa mi se",
			   $dislikeItImage       = "Ne sviđa mi se slika",
			   $like_yand            = "Vi i još",
			   $person               = "osoba",
			   $persons              = "osobe",
			   $like_this            = "volite ovo.",
			   $likes_this           = "volite ovo.",
			   $youLikeThis          = "Vi volite ovo.",
			   $beFirstToLike        = "Budite prvi kome se ovo sviđa.",
			   $love                 = "volite ovo.",
			   $love_e               = "vole ovo.",
			   $loves                = "voli ovo.",
			   $wantToHearOp         = "Želimo da čujemo Vaš utisak",
			   $submitReview         = "Objavljivanje utiska",
			   $text_reviews         = "Prilikom dodavanja utiska moramo da se uverimo da ne postoji uvredljivi sadržaj, pa zbog toga utisak šaljemo na proveru. Provera obično traje od 24 do 48h. Nakon toga, Vaš utisak će biti vidljiv za sve korisnike i posetioce ove radionice.",
			   $controlPanel         = "Kontrolna tabla",
			   $welcomeAdmin         = "Dobrodošli administratore",
			   $soonContact          = "Uspešno ste poslali poruku. Javićemo Vam se uskoro!",
			   $finishedProduct      = "U nastavku je prikazana slika gotovog proizvoda",
			   $finishedProductAcr   = "Gotov proizvod",
			   $qHeading             = "Naslov pitanja",
			   $qText                = "Vaša poruka (minimum 10 slova)",
			   $finished_product	 = "Gotov proizvod",
			   $ingredients			 = "Sastojci",
			   $for_thisWSYouNeed	 = "Za ovu radionicu Vam je potrebno",
			   $ask_aQuestion		 = "Imate pitanje?",
			   $here_postQuestion	 = "Ovde možete postaviti pitanje vezano za ovu radionicu",
			   $send_question		 = "Pošaljite pitanje",
			   $respond_asSoonAs	 = "Odgovor ćemo Vam poslati na email što pre budemo mogli",
			   $email_btnNoWork      = "Ukoliko dugme ne radi, prekopirajte ovaj link u vas browser",
			   $ws_with              = "Radionica sa",


			   // Gallery
			   $no_likes             = "Nema sviđanja",
			   $onSlide              = "Na slajdu",	
			   $noPhotos             = "Trenutno nemamo slike za prikaz.",
			   $moreImages           = "Još slika",

			   // calendar.php
			   $welcomeCalendar      = "Dobrodošli u kalendar događaja",
			   $wc_details           = "Na ovoj stranici možete pogledati raspored događaja",
			   $location             = "Lokacija",
			   $backEvents           = "Nazad na događaje",
			   $prevMonth            = "Prethodni mesec",
			   $nextMonth            = "Sledeći mesec",

			   // News
			   $stay_tunned			 = "Budite u toku sa našim vestima",
			   $topical				 = "Aktuelno",

			   // Index(new translation)
			   $talent_cantBeTaught	 = "Talenat se ne može naučiti, ali se može probuditi.",
			   $join_ourOnline		 = "Pridružite se našim online radionicama i poboljšajte svoje umetničke veštine i strast za stvaranje.",	
			   $lets_haveFun		 = "Hajde da se zabavljamo zajedno",
			   $cooperation			 = "Zaiteresovani ste za saradnju",
			   $emailUS              = "Pošaljite nam e-mail na ",
			   $send_usEmail		 = "Pošaljite nam email",

			   // about(new translation)
			   $my_nameIS			 = "Ćao, moje ime je <span class=\"strong color-theme\">Antonis Tzanidakis",
			   $be_inTouch			 = "Bićemo u kontaktu",
			   $thank_you			 = "Hvala Vam",

			   // packages.php
			   $bestPackage          = "Odaberite paket koji Vam najviše odgovara",

			   // Languages
			   $L_serbian            = "Srpski",
			   $L_english            = "Engleski",
			   $L_russian            = "Ruski",
			   $L_spanish            = "Španski",
			   $L_greek              = "Grčki",

			   // Currencies         
			   $C_RSD                = "Srpski Dinar",
			   $C_EUR                = "Euro",
			   $C_RUB                = "Ruska Rublja",
			   $C_USD                = "Američki Dolar",
			   $C_PLN                = "Poljki Zlot",

			   // AJAX
			   $aj_sucChangeCurr     = "Uspešno ste promenili valutu",
			   $aj_sucChangeLang     = "Uspešno ste promenili jezik",
			   $aj_sucChangePass     = "Uspešno ste promenili lozinku",
			   $aj_sucChangeUsername = "Uspešno ste promenili korisničko ime",
			   $aj_allowedExtensions = "Dozvoljene ekstenzije slika su",
			   $aj_maxUplaodSize     = "Maksimalna veličina slike za upload je",
			   $aj_errorOccured      = "Dogodila se greška prilikom upload-ovanja",
			   $aj_succUploaded      = "Uspešno ste upload-ovali profilnu sliku",

			   // NEW TRANSLATE 04.03.2017.
			   $next_Workshop 		 = "Sledeća radionica",
			   $our_Partners		 = "Naši partneri",
			   $upcoming_event		 = "Predstojeći događaj",
			   $currently_noEvents	 = "Trenutno nemamo predstojeće događaje",
			   $sub_ToMyWS			 = "Pretplatite se za moje online radionice",
			   $subbed_For			 = "Pretplaćen za",
			   $subb_for 			 = "Pretplati se za",
			   $select_StartMonth	 = "Izaberite početni mesec",
			   $could_NotDelete		 = "Nije moguće obrisati poslednji podrazumevani podatak",
			   $video_plsCopy		 = "Šifra za video. Molimo vas da prekopirate i nalepite šifru u polje \"password\" ispod.",
			   $take_aLookSup		 = "Bacite pogled na naše dobavljače",
			   $website 			 = "Vebsajt",

			   // 12.03.2017
			   $other_supp			 = "Ostale zalihe",
			   $only_englishSubs	 = "Video trenutno ima prevode samo na engleskom i srpskom jeziku. Ostali jezici ce biti dostupni za par dana.",
			   $no_access			 = "Nemate dozvolu da gledate video sa ovog urađaja",
			   $no_access_blck       = "Nemate dozvolu da gledate video",
			   $other_user_blck      = "Drugi korisnik koristi ovaj uredjaj za gledanje videa. Molimo Vas da se ulogujete na uredjaj na kome uobičajeno gledate naše radionice.",
			   $max_numDevices		 = "Registrovali ste se na maksimalnom broju uređaja. Molimo vas da se ulogujete na uređaju sa kojeg inače gledate video-zapise.",
			   $for_moreInfo		 = "Za više informacija kontaktirajte nas na",
			   $you_haveCoupon		 = "Imate kupon za ovu radionicu",

			   // 19.06.2017
			   $november 			 = "Novembar",
			   $wS_coventry			 = "Workshops Coventry", 
			   $explore_wS			 = "Istražite radionice",
			   $add_ws_toCart		 = "Dodavanje radionice u korpu",
			   $show_shoppingCart	 = "Prikažite korpu za kupovinu",
			   $visit 				 = "Posetite",
			   $website_sm 			 = "vebsajt",
			   $no_ingredients		 = "Trenutno nemamo listu sastojaka za ovu radionicu.",
			   $enjoy_ourPassion	 = "Uživajte u našoj kreativnosti",
			   $per_ws				 = "po radionici",
			   $per_ws_acr      	 = "r",
			   $your_Cart			 = "Vaša Korpa",
			   $finish_Purchase		 = "Završite kupovinu",
			   $welcome_To			 = "Dobrodošli u",
			   $user_Section		 = "korisnički deo",
			   $site_a				 = "sajta",
			   $news_user			 = "Obaveštenja",
			   $check_News			 = "Pogledajte obaveštenja",
			   $online_workshops_sm  = "Online radionice", 
			   $newest_Ws			 = "Najnovije radionice",
			   $new 				 = "NOVO",
			   $share_ws_facebook	 = "Podelite radionicu na Facebook-u",
			   $share_ws_Twitter	 = "Podelite radionicu na Twitter-u",
			   $listOf_YourWs		 = "Spisak Vaših radionica",
			   $take_a_Look			 = "Pogledajte korpu za kupovinu.",
			   $see_Also			 = "Takođe pogledajte",
			   $new_pricing			 = "novi cenovnik",
			   $continue_shopping	 = "Nastavite sa kupovinom",
			   $pls_wait_Transaction = "Molimo Vas da sačekate da se transakcija završi. NE napuštajte ovu stranicu.",
			   $look_newPriceLst     = "Pogledajte nov cenovnik.",
			   $whatYouGet           = "Nova ponuda",
			   $price_pw             = "Cena po radionici",
			   $trait_1              = "Neograničen pristup kupljenim radionicama",
			   $trait_2              = "Prevod na engleski, grčki i srpski jezik",
			   $trait_3              = "Spisak sastojaka korišćen u radionici",
			   $trait_4              = "<span class=\"strong\">Full HD</span> radionice",
			   $suppEngTitle         = "Prevod na engleski",

			   //newEmail
			   $new_notification	 = "Novo obaveštenje",
			   $we_areGrateful		 = "Zahvalni smo Vam na prethodnim pretplatama i želimo da Vas nagradimo specijalnom ponudom. Radionice su Vam sada dostupne po ceni od",	
			   $explore_newWs		 = "Otkrijte nove radionice",
			   $buy_Ws 				 = "Kupite radionicu",
			   $watch_ws			 = "Gledajte radionice",
			   $thanks_trust		 = "Hvala Vam na poverenju",
			   $sincerely			 = "Srdačno",

			   // Months
			   $c_months             = ["Januar","Februar","Mart","April","Maj","Jun","Jul","Avgust","Septembar","Oktobar","Novembar","Decembar"],
			   $c_days_acr           = ["Ned","Pon","Uto","Sre","Čet","Pet","Sub","Ned"];
	}
	if (!isset($MAKE_LANG) || isset($MAKE_LANG) && $MAKE_LANG === true) $lang = new Language_SR;

	if(isset($page))
	{
		switch($page)
		{
			case "index":
			$INDEX_1					= "Ja sam kreativna duša i u uvek želim da istražim nešto novo i potpuno drugačije! Pozvani ste da podelite moje kreativno putovanje i istražite sa mnom nove tehnike, umetnike, ideje i naravno sastojke potrebne za rad!";

			$INDEX_3					= "Vaš talenat je sakriven? Hajde da ga istražimo zajedno!";
			$INDEX_4					= "Svakog meseca ćemo postavljati nove HD video radionice, sa prevodima na razne jezike, koji će oslikavati naše kreacije, nove alate i materijale.";

			$INDEX_5					= "Zajedno ćemo koristi razne materijale i od njih praviti deliće umetnosti... Zar to nije magija?";
			$INDEX_6					= "Naš kreativni tim će predstaviti mnoge tehnike. Pratićemo našu kreativnost ali i Vaše utiske i sugestije koji će nam pomoći pri stvaranju novih projekata.";

			$INDEX_7					= "Do sada je više od 1000 ljudi bilo na radionicama koje se održavaju uživo. Odlično iskustvo će biti primenjeno na naše online radionice kako bismo Vam pružili znanje i informacije o tehnikama, materijalima, i najvažnije, dali motivaciju da stvarate.";

			$INDEX_8					= "Stvaranje nečeg novog nije samo zabavno već i smanjuje stres i pomaže da se opustite. To nije posao, već užitak i relaksacija! Dok stvarate novo delo, pronaćićete ljude koji slično razmišljaju poput Vas i napraviti dugoročna prijateljstva širom sveta.";

			$INDEX_9					= "Svakog meseca ćemo praviti najmanje 4 različita projekta. Naše radionice su tako koncipirane da budu dostupne za svaki nivo znanja. Ali nemojte da se zavarate - to nisu gomila instrukcija! Naše radionice su zabavne i pune energije! Da li ste spremni da doživite to iskustvo?";
			break;

			case "about":
				$introduction = "Ja sam umetnik mešovite medije iz Grčke.";
				$story_1 = "Zaronio sam u proces umetnosti mešovite medije pre tri godine. Nije bilo planirano, bio je to samo hobi ali je uskoro postao moj život! Proveo sam sate i sate istražujući svoju umetnost, uvek želeći da odem korak unapred, uvek želeći da se igram još malo! To je ubrzo postala moja strast, moja opsesija, tajno mesto u kojem pronalazim sebe!";
				$story_2 = "Napravio sam Facebook stranicu <a class=\"color-theme link-ord\" href=\"".$COMPANY_INFO->facebook."\" target=\"_blank\">“Handmade Fantasy”</a>, i iskusio ljubav i prihvatanje od strane ostalih korisnika širom globusa. U tom trenutku sam shvatio koliko sam dobar u tome što radim i da je ova stranica samo početak - Nisam mogao da prestanem!";
				$story_3 = "Sada sam odlučan da pretvorim Fantasy Around the World u globalni projekat, priznat i podeljen od strane svih mojih prijatelja umetnika i ljudi koji vole umetnost!";
				$story_4 = "Jednoga dana me je drugar pitao da li želim da podučavam ljude, da ih podučavam svojim načinom stvaranja i to je bio moj početak rada u radionicama pod imenom Handmade Fantasy. Počeo sam od ostrva Lefkada, mesto u kojem sam trenutno živeo, i dan za danom, mesec za mesecom, ljudi širom Grčke su počeli da me pitaju da organizujem radionice u njihovim radnjama. Vrlo brzo su počele da mi stižu poruke iz drugih zemalja, pa se moj san zamalo ostvario.. Od početka sam imao ideju da započnem website kao što je ovaj, da bi ljudi sa svih strana sveta mogli da vide kako radim, bez potrebe za putovanjem i trošenjem mnogo novca. Tako sam došao do ovoga i sada svi vi možete da posetite moje mesto koje se zove <a class=\"link-ord color-theme\" href=\"#\">handmadefantasyworld.com</a>.";
				$story_5 = "Tako da će se mnogo stvari dogoditi ovde, imam mnogo ideja.";
			break;

			case "policy":
				$header1   = "Procedure i uslovi korišćenja";
				$para1     = "Ovi uslovi i odredbe se odnose na korišćenje svih servisa pod domenom www.handmadefantasyworld.com. Pristupanjem ovom veb-sajtu i/ili postavljanjem porudžbine, pristajete da budete vezani ovim uslovima i odredbama. Ovaj veb-sajt rukovodi i održava: Handamde fantasy A&amp;B doo. Kompanijski registracioni broj: 21256706.";

				$header2   = "Handmade fantasy A&B d.o.o. Beograd-Zemun";
				$pol_info1 = "Cara Dušana 266e";
				$pol_info2 = "11080 Zemun - Beograd";
				$pol_info3 = "Srbija";
				$pom_info4 = "Poreski broj";
				$pol_info5 = "Kompanijski registracioni broj";
				$para2	   = "Korisničke usluge se mogu kontaktirati preko support@handmadefantasyworld.com. Radno vreme: (CET) Od ponedeljka do četvrtka: 10.00 – 16.00; Friday: 10.000 - 16.00";
				$header3   = "Sadržaj";
				$header4   = "Polisa od otkazivanju i vraćanju";
				$header5   = "Korišćenje kolačića";
				$header6   = "Registracija";
				$header7   = "Poricanje";
				$header8   = "Polisa privatnosti";

				// Cancellation and Returns Policy
				$para3	   = "Ako želite da otkažete svoju narudžbinu:";
				$para4	   = "Možete nas obavestiti preko email-a <a href=\"mailto:support@handmadefantasyworld.com\">(support@handmadefantasyworld.com)</a> preko nego što vam odobrimo pristup video servisu ili";
				$para5	   = "Gde Vam je pristup već odobren, imate pravo da otkažete svoj ugovor do 2 radna dana počevši od dana nakon verifikacije.";
				$para6	   = "Možete da otkažete vaš ugovor i povratite svoj novac tako što ćete nas obavestiti u pismenom email-u do 2 radna dana počevši od dana nakon verifikacije na adresu na vrhu ovih uslova. Novac će biti vraćen na isti račun ili kreditnu karticu sa koje je uzet tokom prethodnog plaćanja.";

				// Use of Cookies
				$para7	   = "Kolačić je mali komad podatka koji se čuva na vašem računaru. Postavljamo ga kada posetite naš veb-sajt i jedino mu mi možemo pristupiti i koristiti ga. Svaki kolačić ima datum isteka koji određuje koliko će kolačić da traje. Kolačići ne sadrže nikakve lične podatke; međutim, kada se registrujete, kolačić se može kombinovati sa ličnim podacima koje ste nam dali tokom registracije. Koristimo kolačiće na nekoliko načina da poboljšamo naše usluge. Molimo vas da budete svesni da koristimo kolačiće samo kada su neophodni i da uvek obezbeđujemo da traju dok god su potrebni.";

				// Registration
				$para8	   = "Na veb-sajtu nema starosnog ograničenja, međutim, roditelji i staratelji su odgovorni za saglasnost sa ovim odredbama od strane dece pod njihovom odgovornošću.";
				$para9	   = "Registracijom vi potvrđujete da su sve informacije koje ste uneli tokom registracije tačne i da pristajete da nećete oličavati nijednu osobu ili entitet i da nećete pogrešno predstavljati odnos ni sa jednom osobom ili entitetom.";
				$para10	   = "Možete ažurirati svoju registraciju u bilo koje vreme na Moj Profil sekciji na našem sajtu.";
				$para11	   = "Svaka registracija je samo za jednog korisnika. Ne dozvoljavamo vam da delite svoj username i šifru ni sa jednom osobom ili grupom ljudi na mreži.";
				$para12	   = "Odgovornost za zaštitu bilo koje izdate šifre pada na vas i ako znate ili sumnjate da neko zna vašu šifru, trebali biste što pre da je promenite i da se osigurate da vam je zaštićena. Vi ste odgovorni za sve što se dešava pod vašim nalogom ili šifrom.";
				$para13	   = "Možemo vam smesta suspendovati ili ukinuti registraciju na našem razumnom nahođenju ili ako prekršite bilo koju obavezu pod ovim odredbama i uslovima.";

				// Disclaimer
				$para14	   = "Dok nastojimo da obezbedimo da su informacije na ovom veb-sajtu tačne, mi ne garantujemo tačnost i potpunost materijala na ovom veb-sajtu. Možemo da pravimo izmene na materijalima na ovom veb-sajtu ili na proizvodima i cenama koji su opisani u njima u bilo koje doba bez prethodne najave. Materijali na ovom veb-sajtu mogu biti zastareli i nećemo se obavezati da ih ažuriramo.";
				$para15	   = "Materijal na ovom veb-sajtu je obezbeđen \"takav kakav je\" bez ikakvih uslova, garancija ili odredbi bilo kakve vrste. Shodno tome, u najvećoj meri dozvoljenoj zakonom, pružamo vam ovaj veb-sajt na bazi toga da izostavljamo sve reprezentacije, garanicije, uslove i druge odredbe koje, za ove uslove i odredbe, mogu imati efekta u odnosu na ovaj veb-sajt.";

				// Privacy Policy
				$para16    = "Privatna polisa Handmade fantasy A&B je da poštuju i štite privatnost svojih posetioca i pretplatnika na našem veb-sajtu. Iskaz ove polise vam govori na koji način prikupljamo informacije od vas i kako ih koristimo. To će vam pomoći pri pravljenju obaveštenih odluka tokom korišćenja našeg veb-sajta i servisa.";
				$para17	   = "Mi zahtevamo lične podatke kao što su vaše ime, email adresa, detalji kompanije itd. koje možete izabrati da obezbedite tokom pretplate na naš veb-sajt. Potrebno je da sakupljamo ove informacije da bi nam pomogle da personalizujemo i dostavimo relevantni sadržaj. Niste u obavezi da obezbedite bilo kakve lične podatke ako ne želite. Takođe koristimo lične podatke za potrebe sprovođenja i širenja naših poslovnih aktivnosti, pružajući korisničku uslugu i postavljajući nove proizvode i servise našim klijentima i potencijalnim kupcima. Povremeno, možemo takođe koristiti informacije koje prikupljamo da vas obavestimo o važnim promenama na našem veb-sajtu, novim servisima i specijalnim ponudama koje mislimo da će vam biti od koristi. Ako se odlučite da odustanete od prijema takvih komunikacija od nas, onda učinite to putem linka unsubscribe koji se nalazi na svakom email-u koji dobijete od nas. Ovaj skup podataka služi za prilagođavanje našeg veb sadržaja da pruži bolji doživljaj za naše korisnike i neće vas, ni na koji način, lično identifikovati.";
				$para18    = "IP adresa je broj koji se automatski dodeljuje vašem računaru kad god surfujete internetom. Veb servisi automatski identifikuju vaš računar po njegovoj IP adresi. Povremeno prikupljamo IP adrese za potrebe administracije sistema, da analiziramo prikupljene podatke i za proveru korišćenja našeg veb-sajta.";
				$para19    = "Zaštita bezbednosti svih ličnih podataka naših gostiju je od najvećeg značaja za nas. Kada primimo vašu isporuku, preduzećemo razumne mere predostrožnosti da bismo osigurali njenu zaštitu na našem sistemu. Na žalost, nema prenosa preko interneta koje se može smatrati 100% sigurnim. Kao rezultat toga, dok preduzimamo sve razumne korake kako bi se osigurali svi lični podaci naših korisnika i čuvala njihova privatnost, ne možemo da garantujemo sigurnost informacija koje nam otkrivate ili prenosite na mreži i ne možemo biti odgovorni za krađu, uništavanje ili slučajno otkrivanje takvih ličnih podataka.";
				$para20	   = "Korišćenjem ovog veb-sajta vi pristajete na pravila polise privatnosti. Ako se ne slažete sa ovom polisom, molimo vas da ne koristite ovaj veb-sajt. Odredbe ove polise privatnosti se mogu menjati s vremena na vreme bez prethodnog obaveštenja, tako da vas molimo da povremeno proveravate ovu stranicu za novosti. Vaše dalje korišćenje ovog veb-sajta nakon objavljivanja promene ovih uslova će značiti da ste saglasni sa promenama.";
			break;

			case "workshops":
				$WS_header1		= "O radionicama";
				$WS_para1		= "Paket onlajn radionica se bazira na mesečnom planu i plaćanju. Svakog meseca ćemo objaviti 4 radionice (od strane Antonisa Tzanidakisa). Radionice će biti na Grčkom sa mogućim titlovima na Engleskom, Ruskom, ex-YU jezicima, Francuskom, Španskom, Portugalskom, Poljskom,....";
				$WS_para2		= "Svaka radionica će trajati oko 40 minuta sa punim pokazanim procesom stvaranja, listom potrošenih materijala i Q&A(Pitanja i Odgovori) sekcijom.";
				$WS_para3		= "Vaš pristup mesečnom paketu koji ste platili nikada neće isteći. Možete ponovo gledati video plaćenih paketa koliko god puta želite.";
				$WS_para4		= "Radionice ne zahtevaju predznanje i pogodne su za sve nivoe iskustva.";
				$WS_para5		= "Materijali koji će se koristiti su uglavnom na bazi vode, bezbedni za korišćenje kod kuće i pristupačni u celom svetu.";
			break;

			case "cart":
				$Cart_para1		= "All payments will be effected in <span class=\"strong\">Serbian currency – dinar (RSD)</span>. The amount your credit card account will be charged for is obtained through the conversion of the price in Euro into Serbian dinar according to the current exchange rate of the Serbian National Bank. When charging your credit card, the same amount is converted into your local currency according to the exchange rate of credit card associations. As a result of this conversion there is a possibility of a slight difference from the original price stated in our web site.";

				$Cart_para1		= "Sva plaćanja će se vršiti u <span class=\"strong\">Srpskoj valuti – dinar (RSD)</span>. Iznos za koji će račun vaše kreditne kartice biti zadužen se dobija konverzijom cene iz Eura i Srpski dinar prema važećem kursu Narodne Banke Srbije. Prilikom naplaćivanja vaše kreditne kartice, ista suma će biti konvertovana u vašu lokalnu valutu u zavisnosti od kursa udruženja kreditnih kartica. Kao rezultat ove konverzije postoji mogućnost blage razlike od originalne cene navedene na našem veb-sajtu.";

				$PI1            = "Uputstva plaćanja";
				$PI2            = "Dobrodošli u instrukcije za plaćanje kreditnim karticama";
				$PI3            = "Bićete vraćeni na naš veb-sajt, nemojte da gasite stranicu za plaćanje";
				$PI4            = "Molimo vas da pogledate <span class=\"strong\">3 jednostavna</span> koraka plaćanja:";
				$PI5            = "Korak";
				$PI6            = "First you have to click on <span class=\"italic\">\"Nastavi plaćanje\"</span>, and than you will be redirected to <a href=\"http://www.aikbanka.rs\" class=\"link-ord\" target=\"_blank\">AIK bank's</a> secure webpage for transactions.";
				$PI6            = "Prvo morate da kliknete na <span class=\"italic\">\"Nastavi plaćanje\"</span>, i onda ćete biti preusmereni na bezbednu stranicu za transakcije <a href=\"http://www.aikbanka.rs\" class=\"link-ord\" target=\"_blank\">AIK banke</a>.";
				$PI7            = "Pogledajte veb-stranicu";
				$PI8            = "Sledeći korak je da popunite informacije o Vašoj kreditnoj kartici. Sledeća polja će biti obavezna:";
				$PI9            = "Formular za plaćanje na sigurnoj stranici AIK banke";
				$PI10           = "Kada ste popunili sva potrebna polja iz <span class=\"strong\">koraka 2</span>, tada kliknite na <span class=\"italic\">\"prosledi\"</span> dugme i <span class=\"strong\">sačekajte otprilike 5-10 sekundi</span>.";
				$PI11           = "Pritisnite ovo dugme kada završite, i <span class=\"strong\">sačekajte 5-10 sekundi</span>";
				$PI12           = "Ne prikazuj ponovo";
				$PI13           = "Zatvori";
				$PI14           = "Nastavi plaćanje";
				$PI15           = "Sigurna veb-stranica AIK banke";
			break;


		}

	}



?>