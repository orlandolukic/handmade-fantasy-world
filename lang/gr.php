<?php

	class Language_GR
	{
		public $about                = "Σχετικά με εμάς",
			   $home                 = "Αρχική",			   
			   $title_head			 = "Title",
			   $review				 = "Επισκόπηση",
			   $popular_workshops    = "Δημοφιλή Εργαστήρια",
			   $workshop             = "Εργαστήριο",
			   $workshops            = "Εργαστήρια",
			   $recent_Workshops     = "Πρόσφατα Εργαστήρια",
			   $visit_workshop		 = "Επισκεφτείτε ",
			   $share_workshop		 = "Μοιραστείτε",
			   $unlimited_workshop	 = "Απεριόριστα Εργαστήρια",
			   $news_newWorkshop	 = "Νέα Εργαστηρίων ",
			   $contact              = "Επικοινωνία",
			   $addWishlist          = "Προσθήκη  <span class=\"strong\">wish list</span>",
			   $publishDate          = "Ημερομηνία έκδοσης",
			   $till                 = "",
			   $unlimited            = "Απεριόριστο",
			   $oneMonth             = "1 Μήνας",
			   $threeMonths          = "3 Μήνες",
			   $oneYear              = "1 Έτος",
			   $followUs			 = "Ακολουθήστε μας",
			   $findUsOnFace		 = "Βρείτε μας στο Facebook",
			   $share_facebook		 = "Μοιραστείτε στο Facebook",
			   $findUsOnTwitter		 = "Βρείτε μας στο Twitter",
			   $share_twitter		 = "Μοιραστείτε στο Twitter",
			   $findUsOnGoogle		 = "Βρείτε μας στο Google",
			   $findUsOnInstagram    = "Βρείτε μας στο Instagram",
			   $findUsOnDribbble	 = "Βρείτε μας στο Dribble",
			   $findUsOnPinterest	 = "Βρείτε μας στο Pinterest",
			   $sale			     = "Πώληση",
			   $wish_list			 = "Αγαπημένα",
			   $alreadyOnList		 = "Ήδη στα αγαπημένα",
			   $addToWish			 = "Προσθήκη στα αγαπημένα",
			   $lang				 = "Γλώσσα",
			   $serbian				 = "Σέρβικα",
			   $english				 = "Αγγλικά",
			   $polish				 = "Πολωνικά",
			   $russian				 = "Ρώσσικα",
			   $register			 = "Εγγραφή",
			   $login				 = "Είσοδος",
			   $views                = "Προβολές",
			   $inCart               = "Προΐόντα στο καλάθι ",
			   $succAddedInCart      = "Έχετε προσθέσει επιτυχώς ένα πακέτο στο καλάθι σας",
			   $removeProductFromCart= "Διαγραφή προΐόντος",
			   $removeAllProductCart = "Καθαρισμός καλαθιού",
			   $succDeletedAll       = "Έχετε επιτυχώς διαγράψει όλα τα προΐόντα από το καλάθι σας.",
			   $succDeletedOne       = "Έχετε επιτυχώς διαγράψει ένα πρΐόν από το καλάθι σας.",
			   $succAddedCart        = "Έχετε επιτυχώς προσθέσει ένα πρΐόν στο καλάθι σας",
			   $succChangedEmail     = "Έχετε επιτυχώς αλλάξει την ηλεκτρονική σας διεύθυνση.",
			   $failAddedCart		 = "Αυτό το πρΐόν υπάρχει ήδη στο καλάθι σας",
			   $package_self         = "Πακέτο",
			   $gallery              = "Έκθεση",
			   $news                 = "Νέα",
			   $predefined           = "Προεπιλογή",
			   $privileged           = "Για προχωρημένους χρήστες",
			   $rest_links           = "Άλλοι σύνδεσμοι",
			   $calendar             = "Ημερολόγιο",
			   $accessAllVideos      = "Έχετε πρόσβαση σε όλα τα εργαστήρια.",
			   $searchByMonth        = "Αναζητήστε εργαστήρια ανά μήνα",
			   $myCalendar           = "Το ημερολόγιο μου",
			   $more                 = "Περισσότερα",
			   $currNoWS             = "Προσωρινά, δεν υπάρχουν ενεργά εργαστήρια.",
			   $currNoWSMonth        = "Προσωρινά, δεν υπάρχουν ενεργά εργαστήρια για αυτο τον μήνα.",
			   $currNoIngr           = "Προσωρινά, δεν υπάρχει λίστα με συστατικά.",
			   $currNoNews           = "Προσωρινά, δεν υπάρχουν νέα.",
			   $currNoLink           = "Currently no link",
			   $currNoPopWS          = "Προσωρινα, δεν υπάρχουν ενεργά εργαστήρια.",
			   $selectMonth          = "Επιλέξτε μήνα",
			   $no_subsMonth         = "Δεν υπάρχουν μήνες",
			   $quantity             = "Ποσότητα",
			   $suppliers            = "Προμηθευτές",
			   $links                = "Σύνδεσμοι",  
			   $shoppingMaterials    = "Για την αγορά αυτών των υλικών,επιλέξτε τη χώρα σας παρακάτω.",   
			   $confirm              = "Επιβεβαίωση",  
			   $emailChange          = "Αλλαγή ηλεκτρονικής διεύθυνσης", 
			   $btnEmailTmplNoWork   = "If the button doesn't work, please copy this link in Your browser.", 
			   $have_coupon          = "You have coupon for this workshop",
			   $unsubscribe          = "You don't want to receive these emails anymore?",
			   $unsubscribed         = "Unsubscribed",
			   $succ_unsubscribed    = "You have successfully unsubscribed from this list.",
			   $redirection_in       = "Redirection on homepage in ",
			   $you_can              = "You can",
			   $unsubscribe_lst      = "unsubscribe from this list", 

			   // WEBSITE PROPERTY			   
			   $wb_description       = "Online mixed media Εργαστήρια. Ελάτε μαζί μας και βελτιώστε τις καλλιτεχνικές σας δεξιότητες και το πάθος για δημιουργία. Ας διασκεδάσουμε μαζί!",

			   $office				 = "Γραφείο",
			   $message              = "Μήνυμα",
			   
			   $comment_a			 = "Σχόλιο",
			   $comment_i			 = "Σχόλια",
			   $comment_site		 = "Προσθέστε σχόλιο...",
			   $comment_delete		 = "Διαγραφή σχόλιου?",
			   $check_comment		 = "Δείτε τα σχόλια",
			   $send_comment		 = "Στείλτε ένα σχόλιο",
			   $more_comment		 = "Περισσότερα σχόλια",
			   $review_delete		 = "Διαγραφή κριτικής ",
			   $areYouSureReview	 = "Είστε σίγουροι ότι θέλε να διαγράφετε αυτή την κριτική;",
			   $myComment			 = "Το σχόλιο μου",
			   $allRightsReserved	 = "Πνευματικά Δικαιώματα ",
			   $verifiedUser		 = "Επαλήθευση χρήστη",
			   $delete				 = "Διαγραφή",
			   $cancel				 = "Ακύρωση",
			   $onCheck			 	 = "Σε έλεγχο",
			   $delete_Review		 = "Διαγραφή κριτικής",
			   $change_Review		 = "Αλλαγή κριτικής",
			   $allow_Review		 = "Αποδοχή κριτικής",
			   $enter_titleReview	 = "Εισαγωγή τίτλου κριτικής",
			   $enter_textReview	 = "Εισαγωγή κειμένου κριτικής",
			   $review_sent			 = "Η κριτική σας ελέγχεται.",
			   $send_Review			 = "Αποστολή κριτικής",
			   $grade				 = "Αστέρια",
			   $reply				 = "Απάντηση",
			   $quality				 = "Ποιότητα",
			   $enchant_magic		 = "Γοητευτική μαγεία",
			   $satisfied_customers	 = "Ικανοποιημένοι χρήστες",
			   $many_news			 = "Το δικό σας σημείο ολοκληρωμένης εξυπηρέτησης",
			   $work_relax			 = "Όχι μόνο εργασία αλλά και πολλά άλλα....",
			   $new_stuff			 = "Νέο",
			   $top_stuff			 = "Κορυφαίο",
			   $people_whoVisited	 = "Άτομα που επισκέφτηκαν το εργαστήριο",
			   $challenge_areReady	 = "Είστε έτοιμοι να δεχτείτε την πρόκληση;",
			   $learn_more			 = "Μάθετε περισσότερα",
			   $pay_plan			 = "Φόρμα εγγραφής",
			   $beginner			 = "Αρχάριοςr",
			   $month				 = "Μήνας",
			   $subscribe_now		 = "Εγγραφείτε τώρα",
			   $subscribe			 = "Εγγραφή",
			   $subscribed			 = "Εγγραφήκατε",
			   $chooseMSUB           = "Επιλέξτε μήνα εγγραφής",
			   $subscribe_for        = "Εγγραφείτε για ",
			   $subscription_seccess = "Εχετε εγγραφεί σε αυτό το εργαστήριο.",
			   $access_workValid	 = "Η πρόσβαση σε αυτό το εργαστήριο επιτρέπεται μέχρι",
			   $artist				 = "Καλλιτέχνης",
			   $master				 = "Δάσκαλος",
			   $watch_video			 = "Παρακολουθείστε το βίντεο",
			   $videos   			 = "Βίντεο",
			   $package_type 		 = "Τύπος πακέτου",
			   $package_price 		 = "Τιμή πακέτου",
			   $package_detailHere	 = "Λεπτομέρειες πακέτου που μπορείτε να δείτε",
			   $here				 = "Εδώ",
			   $workshop_description = "Περιγραφή εργαστηρίου",
			   $impressions			 = "Κριτικές",
			   $recent_impressions	 = "Πρόσφατες κριτικές",
			   $top_impressions		 = "Κορυφαίες κριτικές",
			   $leave_impression	 = "Αφήστε την κριτική σας",
			   $delete_impressionSucc= "Έχετε επιτυχώς διαγράψει την κριτική.",
			   $change_impressionSucc= "Έχετε επιτυχώς αλλάξει την κριτική .",
			   $error_whileProcess	 = "Υπήρξε λάθος στην επεξεργασία δεδομένων.",
			   $user_a				 = "Χρήστης",
			   $user_impression		 = "Κριτικές χρηστών",
			   $no_impressionsATM	 = "Προς το παρόν δεν υπάρχουν κριτικές.",
			   $no_commentsATM	 	 = "Προς το παρόν δεν υπάρχουν σχόλια.",
			   $yes					 = "Ναί",
			   $no 					 = "Όχι",
			   $didNotFill_RightWay	 = "Δεν έχετε συμπληρώσει όλα τα υποχρεωτικά πεδία.",
			   $necessary_field		 = "Τα υποχρεωτικα πεδία επισημαίνονται με",
			   $talent_alwaysSurprise= "Όλοι έχουν ένα ταλέντο",
			   $workshop_wka         = "Online εργαστήρια με παγκοσμίως γνωστό καλλιτέχνη",
			   $succAddWishLst       = "¨Έχετε επιτυχώς προσθέσει το εργαστήριο στη λίστα επιθυμιών .",
			   $peopleVisited        = "Άτομα που επισκέφτηκαν το εργαστήριο",
			   $in                   = "Είσοδος",
			   $emailExample         = "π.χ. somebody@example.com",
			   $comeBack             = "Επιστροφή",
			   $backToTop            = "Επιστροφή στην κορυφή",
			   $show_details         = "Λεπτομέρειες",
			   $no_events            = "Δεν υπάρχουν συμβάντα για τον επιλεγμένο μήνα.",

			   // Template
			   $new_Message			 = "Νέο μήνυμα",
			   $message_Reply		 = "Απάντηση",
			   $user_name			 = "Όνομα",
			   $user_surname		 = "Επίθετο",
			   $subject				 = "Θέμα",
			   $request_changePass	 = "Αίτημα αλλαγής κωδικού ",
			   $dear                 = "Αγαπητέ",
			   $changePass_pushButton= "Πατήστε το κουμπί για να αλλάξετε κωδικό .",
			   $ifNot_thenHere		 = "Εάν το κουμπί δεν λειτουργεί, δεν μπορείτε να αλλάξετε τον κωδικό ",
			   $change_pass			 = "Αλλαγή κωδικού",
			   $password_changed	 = "Ο κωδικός άλλαξε",
			   $pressButton_toConfirm= "παρακαλώ πατήστε αυτό το κουμπί για να επιβεβαιώσετε τον λογαριασμό σας .",
			   $request_approve	     = "Η εγγραφή επιβεβαιώθηκε ",
			   $ifButtonNot_Link	 = "Εάν το κουμπί δεν δουλέυει, προσπαθείστε να ενεργοποιήσετε τον λογαριασμό σας πατώντας σε αυτον τον",
			   $link 				 = "σύνδεσμο",
			   $thankYou			 = "Ευχαριστώ",
			   $contact_Us			 = "επικοινωνείστε μαζί μας",
			   $new_review			 = "Νέα εντύπωση",
			   $new_workshop 		 = "Νέο εργαστήριο",
			   $workshop_forSale	 = "Εργαστήριο προς πώληση ",
			   $details				 = "Λεπτομέρειες",
			   $add_toCart			 = "Προσθήκη στο καλάθι",
			   $toCart   			 = "Στο καλάθι",
			   $user 				 = " Ο Χρήστης",
			   $hasLeft_aReview		 = "έχει αφήσει μια κριτική .",
			   $see_ApproveDelete	 = "Μπορείτε να ελέγξετε, να εγκρίνετε ή να διαγράψετε αυτη την κριτική",
			   $review_overview		 = "Επισκόπηση κριτικής ",
			   $success_Activation	 = "Έχετε επιτυχώς ενεργοποιήσει τον λογαριασμό σας.",
			   $happy_YoureBack		 = "Ανυπομονούμε να σας δούμε ξανά  Αριθμός ταυτότητας",
			   $pib 				 = "Αριθμός ΑΦΜ",
			   $mb 					 = "Αριθμός εταιρείας",
			   $email_UP 			 = "E-mail",
			   $start_price			 = "Υποσύνολο",
			   $pdv_amount			 = "Φόρος",
			   $totalUP				 = "Σύνολο",
			   $check_invoice		 = "Έλεγχος απόδειξης",
			   $handmade_isin		 = "είναι θεωρημένη, οπότε ο φόρος έχει υπολογίστει",
			   $formoreinfo			 = "Για περισσότερες πληροφορίες επικοινωνήστε",
			   $video_mat            = "Βίντεο",
			   $video                = "Βίντεο",
			   $reg_succ             = "Έχετε επιτυχώς εγγραφεί στο <a href=\"http://www.handmadefantasyworld.com/\">Handmade Fantasy World</a>",
			   $newUser 			 = "Νέος χρήστης",
			   $newUser_Register	 = "Νεος χρήστης έχει εγγραφεί στο σύστημα",
			   $question_WS			 = "Ερώτηση για εργαστήριο",
			   $ifNot_ButtonRespond	 = "Σε περίπτωση που το κουμπί δεν δουλεύει, μπορειτε να απευθυνθειτε",
			   $belgrade			 = "Βελιγράδι",
			   $serbia 				 = "Σερβία",
			   $ifLinkNotWork		 = "Σε περίπτωση που το κουμπί δεν δουλεύει, αντιγράψτε αυτο τον σύνδεσμο στο browser σας",
			   $subFor 				 = "Εγγραφή για",


			   //about.php
			   $new_WorkshopsUP		 = "Νέα εργαστήρια",
			   $new_opportunitiesUP	 = "Νέες ευκαιρίες",
			   $new_ideasUP			 = "Νέες ιδέες",
			   $highest_ratesUP		 = "Υψηλότερα ποσοστά",
			   $workshopUP 			 = "Εργαστήριο",
			   $active_usersUP		 = "Ενεργοί χρήστες",
			   $meet_ourTeamUP		 = "Γνωρίστε την ομάδα μας",
			   $artistUP			 = "Καλλιτέχνης",
			   $koordinatorUP		 = "Συντονιστής",
			   $contributorUP		 = "Χορηγός",
			   $what_peopleSayUP	 = "Τι λέει ο κόσμος",
			   $director_newBusiness = "Διευθυντής νέου κέντρου επιχειρήσεων",

			   //coming.php
			   $hi_were				 = "Γειά, είμαστε",
			   $website_comingSoon	 = "Η ιστοσελίδα μας θα είναι σύντομα κοντά σας",
			   $online_WSAvailable	 = "Τα εργαστήρια θα είναι διαθέσιμα online",
			   $very_soon 			 = "πολύ σύντομα",
			   $days				 = "Ημέρες",
			   $hours				 = "Ώρες",
			   $minutes				 = "Λεπτά",
			   $seconds				 = "Δευτερόλεπτα",

			   //contact.php
			   $always_forNewContact = "Ευχαρίστηση μας πάντα να κάνουμε νέες γνωριμίες",
			   $sayHello			 = "πείτε γεια",
			   $wont_postEmail		 = "Το e-mail σας δεν θα είναι ορατό.  Τα υποχρεωτικά πεδία σημειώνονται με *.",
			   $errors_occurred		 = "Έχουν προκύψει λάθη",
			   $error_email			 = "Εσφαλμένη ηλεκτρονική διεύθυνση.",
			   $error_name			 = "Εσφαλμένο όνομα.",
			   $min_tenCharacters	 = "Το μήνυμα πρέπεινα περιέχει τουλάχιστον 10 χαρακτήρες.",
			   $send_message		 = "Αποστολή μηνύματος",
			   $contactinfo          = "Πληροφορίες επικοινωνίας",
			   $welcome_message		 = "Καλως 'ηλθατε στην ιστοσελίδα μας. Μπορείτε να επικοινωνήσετε μαζί μας οποιαδήποτε στιγμή μέσω e-mail,  ή μπορείτε να μας στείλετε επιστολή..",
			   $telephone			 = "Τηλέφωνο",
			   $email				 = "Ηλεκτρονική διεύθυνση",
			   $mustAgreeTerms       = "Πρέπει να συμφωνήσετε με τις διαδικασίες και τους όρους χρήσης.",

			   //index.php
			   $newUP 				 = "Νεο",
			   $year 				 = "'Ετος",
			   $scrbForMyWS          = "Εγγραφείτε σε online εργαστήρια",

			   //login.php
			   $please_login		 = "Συνδεθείτε",
			   $add_productToCart	 = "προσθέστε προΐόν στο καλάθι",
			   $login_toBuy			 = "Για να συνεχίσετε τις αγορές σας παρακαλώ συνδεθείτε.",
			   $error_usernameorPass = "Λανθασμένος κωδικός ή όνομα χρήστη.",
			   $or 					 = "ή",
			   $facebook_login		 = "συνδεθείτε μέσω Facebook",
			   $google_login		 = "Συνδεθείτε μέσω Google +",
			   $not_registered		 = "Δεν εγγραφήκατε;",
			   $become_member		 = "Γίνετε μέλος",
			   $forgot_pass			 = "Ξεχάσατε τον κωδικό σας;",
			   $send_passChange		 = "Στείλτε αίτημα αλλαγής κωδικού",
			   $reset_pass 			 = "Επαναφορά κωδικού",
			   $register_email		 = "Παρακαλώ εισάγετε την ηλεκτρονική σας διεύθυνση.",
			   $error_invalidEmail	 = "Μη έγκυρη ηλεκτρονική διεύθυνση",
			   $no_emailInDatabase	 = "Δεν υπάρχει κανένα e-mail στην βάση δεδομένων μας",
			   $send_request		 = "Στείλτε αίτημα",
			   $sec_token            = "Φαίνεται ότι συνδεθήκατε από άλλη συσκεύη. Παρακαλώ συνδεθείτε πάλι.",
			   $sess_expired         = "Η συνεδρία έληξε. Παρακαλώ συνδεθείτε πάλι.",
			   $mssg_sentReq 		 = "Το αίτημα σας έχει σταλεί στο",
			   $force_logout         = "We are maintaining Your account. Please try again in few minutes.",

			   //register.php
			   $no_emailInSistem	 = "Η ηλεκτρονική σας διεύθυνση υπάρχει ήδη στο σύστημα μας.",
			   $request 			 = "Αίτημα",
			   $request_forPassChange= "Αίτημα αλλαγής κωδικού",
			   $name_enoughChar		 = "Το όνομα δεν περιλαμβάνει αρκετούς χαρακτήρες.",
			   $surname_enoughChar	 = "Το επίθετο δεν περιλαμβάνει αρκετούς χαρακτήρες.",
			   $user_alreadyExist	 = "Το όνομα χρήστη υπάρχει ήδη",
			   $insert_username		 = "Παρακαλώ εισάγετε όνομα χρήστη.",
			   $equal_usernames      = "Παρακαλώ επιλέξτε διαφορετικό όνομα χρήστη από αυτό που είναι ήδη ενεργό.",
			   $pass_atLeastMiddle	 = "Ο κωδικός πρέπει να είναι τουλάχιστον μέσης ισχύος.",
			   $reg_cityErr          = "Το πεδίο Πόλη δεν πρέπει να είναι κενό",
			   $reg_stateErr         = "Το πεδίο Νομός δεν μπορεί να είναι κενό",
			   $pass_notMatch		 = "Οι κωδικοί δεν ταιριάζουν.",
			   $please_insertPass	 = "Παρακαλώ εισάγετε κωδικό.",
			   $basic_info			 = "Βασικές πληροφορίες",
			   $user_info			 = "Πληροφορίες χρηστών",
			   $weak                 = "Αδύναμο",
			   $medium               = "μεσαίο",
			   $strong               = "Δυνατό",
			   $very_strong          = "Πολύ δυνατό",
			   $pass_Strength		 = "Ισχύς κωδικού",
			   $matching_passwords	 = "Οι κωδικοί ταιριάζουν",
			   $register_please		 = "Εγγραφή",
			   $passwordStrength     = "<div style='font-size: 120%; margin-top: 5px' class='uppercase text-left strong'>Password Strength</div><div style='margin-top: 5px'><div class='text-left'>Strong password contain:<br><br>1. Lower chars (minimum 5)<br>2. Upper chars (minimum 1)<br>3. Special chars (#,$,%,&,*,.)</div></div>",
			   $email_address        = "Διεύθυνση e-mail",
			   $email_purp           = "Χρησιμοποιούμε αυτή την ηλεκτρονική διεύθυνση για να σας στείλουμε πληροφορίες",
			   $pass_atLeastChars    = "Ο κωδικός πρέπει να έχει τουλάχιστον 3 χαρακτήρες.",
			   $account_reactivation = "Account reactivation",

			   //workshop.php
			   $workshop_forSale1	 = "Εργαστήριο προς πώληση",
			   $buy_thisWS			 = "Αγοράστε αυτό το εργαστήριο",
			   $when_youBuyWS		 = "Όταν αγοράζετε ένα εργαστήριο,  μπορείτε πάντα να το επισκέπτεστε και να παρακολουθείτε τα βίντεο.",
			   $verify_Accout		 = "Επιβεβαιώστε το λογαριασμό σας",
			   $verify_toComment	 = "Για να μπορέσετε να σχολιάσετε τα εργαστήρια, πρέπει να επιβεβαιώσετε το λογαριασμό σας.",

			   //workshops.php
			   $online_workshops	 = "Online εργαστήρια",
			   $online_workshop 	 = "Online εργαστήριο",
			   $sort				 = "Ταξινόμηση",
			   $newest				 = "Πιο πρόσφατα",
			   $oldest				 = "Παλαιότερα",
			   $best_rated			 = "Με την καλύτερη βαθμολόγηση",
			   $a_toZ				 = "Από το A στο Ω" ,
			   $z_toa				 = "Από το Ω στο Α",
			   $workshop_perPage	 = "Αριθμός εργαστηρίων ανά σελίδα",
			   $searchForWS          = "Αναζήτηση εργαστηρίων",
			   $sfw_selectedP        = "Παρακαλώ επιλέξτε μήνα αναζήτησης εργαστηρίων για την επιλεγχθήσα περίοδο",
			   $no_WS                = "Δεν υπάρχουν εργαστήρια",
			   $unableToFindWS       = "Δεν μπορέσαμε να βρούμε εργαστήρια για τον επιλεγμένο μήνα:",

			   //users/cart.php
			   $cart 				 = "Καλάθι αγορών",
			   $payment_amount       = "Payment amount",
			   $cash                 = "Cash",
			   $nomore_subs			 = "Το καλάθι σας περιέχει πάνω από ένα πακέτο εγγραφής, παρακαλώ επιλέξτε ένα.",
			   $you_haveSub			 = "'Εχετε γραφτεί σε πακέτο. Θυμηθείτε να αφαιρέσετε προΐόντα πακέτου από το καλάθι αγορών.",
			   $transaction_denied   = "Η συναλλαγή απορρίφθηκε",
			   $error_paymentBank	 = "Υπήρξε σφάλμα κατά την διάρκεια της πληρωμής. Παρακαλώ προσπαθήστε ξανά.",
			   $product 			 = "Προΐόντα",
			   $price 				 = "Τιμή",
			   $action 				 = "Ενέργειες",	
			   $enter_workshopBetween= "Μπείτε στα εργαστήρια ανάμεσα",
			   $will_beAvailable	 = "Η πρόσβαση σε όλα τα βίντεο αυτής της περιόδου θα είναι δυνατή αμέσως μετά την συναλλαγή.",
			   $package_detail		 = "Μπορείτε να ελέγξετε τις λεπτομέρειες του πακέτου",
			   $empty_cart			 = "Καθαρισμός καλαθιού",
			   $to_continuePlease	 = "Για να συνεχίσετε τα ψώνια σας παρακαλώ",
			   $verify_yourAcc		 = "επιβεβαιώστε τον λογαριασμό σας",
			   $after_payment		 = "Μετά την πληρωμή",
			   $if_paymentSuccess	 = "αν η συναλλαγή είναι επιτυχής, θα σας έρθει απαντητικό μήνυμα από την ιστοσελίδα μας ότι η πληρωμή ήταν επιτυχής. Σε περίπτωση που απέτυχε θα έρθει απαντητικό μήνυμα της ιστοσελίδας μας ότι η συναλλαγή ήταν ανεπιτυχής, και θα έχετε την δυνατότητα να προσπαθήσετε πάλι ή να διαλέξετε διαφορετική μέθοδο πληρωμής. Μετά από μια επιτυχή συναλλαγή, θα λάβετε αυτόματα μια απόδειξη στο",
			   $for_infoGoTo		 = "Για περαιτέρω πληροφορίες στείλτε μας email στο",								
			   $information			 = "Πληροφορίες",
			   $after_order			 = "Μετά την επιβεβαίωση της παραγγελίας, θα οδηγηθείτε σε μία σελίδα όπου θα δείτε ένα παράδειγμαyou will see an example με όλες τις πληροφορίες σχετικά με την πληρωμή.",
			   $buyer_info			 = "Στοιχεία πελάτη",
			   $info_forPayment		 = "Αυτά τα στοιχεία είναι μόνο για τιμολόγιο",
			   $add_newInfo			 = "Προσθέστε νέα στοιχεία",
			   $ready 				 = "'Ετοιμο",
			   $name_surname		 = "¨Όνομα και επίθετο",
			   $adress				 = "Διεύθυνση",
			   $city 				 = "Πόλη",
			   $state                = "Νομός",
			   $email_email			 = "Email",
			   $list_oldInfo		 = "Εμφάνιση στοιχείων παλαιότερων πληρωμώ",
			   $hide_oldInfo		 = "Απόκρυψη στοιχείων παλαιότερων πληρωμών",
			   $user_info_bef		 = "Πληροφορίες που χρησιμοποιήσατε στο παρελθόν",	
			   $default				 = "Προεπιλογή",
			   $approve_choice		 = "Επιβεβαιώστε επιλογή",
			   $error_happened		 = "Προέκυψε κάποιο σφάλμα",
			   $atleast_threeLetters = "Το όνομα σας και το επίθετο σας πρέπει να έχουν τουλάχιστον 3 χαρακτήρες.",
			   $adress_five			 = "Η διεύθυνση σας πρέπει να έχει τουλάχιστον 5 χαρακτήρες.",
			   $city_five			 = "Η πόλη σας πρέπει να έχει τουλάχιστον 5 χαρακτήρες.",
			   $phone_seven			 = "Ο τηλεφωνικός σας αριθμός πρέπει να περιλαμβάνει τουλάχιστον 7 ψηφία.",
			   $phone_signs			 = "Τηλεφωνικός αριθμός που περιέχει σημεία ή γράμματα.",
			   $info_onBuyerOnlyOnce = "Οι πληροφορίες του αγοραστή συμπληρώνονται μια φορά μόνο",
			   $insert_info			 = "Υποβάλετε τα στοιχεία πληρωμής",
			   $iAgree				 = "Συμφωνώ με",
			   $procedure_andUses	 = "διαδικασίες και όροι χρήσης",
			   $termsOfUse      	 = "¨οροι χρήσης",
			   $price_withoutPDV	 = "Τιμή χωρίς ΦΠΑ",
			   $pdv   				 = "ΦΠΑ",	 	
			   $total_buy			 = "Σύνολο",
			   $continue_withPay	 = "Συνέχεια πληρωμής",
			   $overview 			 = "Επισκόπηση",
			   $ready_forPayment	 = "Η παραγγελία σας είναι έτοιμη προς επεξεργασία. Παρακαλώ ελέγξτε το καλάθι σας άλλη μια φορά.",
			   $bank_card			 = "Τράπεζα/ Αποδεκτές κάρτες",
			   $online_buying		 = " Η Online πληρωμή με πιστωτική παρέχεται από",
			   $aik_bank			 = " την τράπεζα AIK",
			   $secure_payment		 = "Ασφαλή συναλλαγή",
			   $visible_onlyToAIK	 = "Για λόγους ασφαλείας τσα στοιχεία της κάρτας σας είναι ορατά  μόνο στην τράπεζα ΑΙΚ. Η σελίδα της τράπεζας προστατέυεται και είναι ασφαλής για υτό το είδος πληρωμής.",	
			   $responsibility		 = "επιφέρει ευθύνη σε ότι αφορά την online πληρωμή.",
			   $info_foreignCurrency = "Πληροφορίες σχετικά με την μετατροπή νομίσματος",	
			   $empty_yourcart		 = "Το καλάθι σας είναι άδειο.",
			   $no_productsInCart	 = "Δεν υπάρχουν προΐόντα στο καλάθι σας.",
			   $continue_buying		 = "Συνεχίστε τις αγορές σας",	
			   $basics               = "Βασικά",	
			   $succSendNewEmail     = "Σας έχουμε στείλει επιτυχώς ένα νέο email επιβεβαίωσης.",
			   $succSendQuestion     = "Έχετε στείλει επιτυχώς ένα ερώτημα. Θα σας απαντήσουμε μόλις διαβάσουμε το e-mail.",
			   $customerInfoSave     = "Παρακαλώ αποθηκέυστε τα στοιχεάι του αγοραστή και προχωρείστε στην ολοκλήρωση της αγοράς.",
			   $AIK_proc             = "Αν χρησιμοποιείτε την μέθοδο πληρωμής της τράπεζας ΑΙΚ, παρακαλώ περιμένετε μέχρι να ολοκληρωθεί η συναλλαγή. ΜΗΝ κλείνετε την σελίδα πληρωμής",
			   $note                 = "Σημείωση",

			   // users/index.php
			   $user_pannel			 = "Πίνακας",	
			   $notification		 = "Ειδοποίηση",
			   $workshop_u			 = "Εργαστήριο",
			   $have_alreadyBought	 = "Έχετε ήδη αγοράσει",
			   $check_workshop		 = "Παρακολουθείστε βίντεο",
			   $inactive_payment	 = "Μη ενεργή πληρωμή",
			   $payment_whichis		 = "Υπάρχει εγγραφή η οποία είναι",
			   $currently			 = "προσωρινά",
			   $inactive 			 = "ανενεργή",
			   $name 				 = "Όνομα",
			   $date_start			 = "Ημερομηνία έναρξης",
			   $date_expire			 = "Ημερομηνία λήξης",
			   $activate_sub		 = "Ενεργοποιήστε την εγγραφή",
			   $payment_successfull	 = "Επιτυχής συναλλαγή",
			   $cart_emptyCanProceed = "Το καλάθι σας είναι άδειο. Μπορείτε να έχετε πρόσβαση στα online εργαστήρια",
			   $payment_details		 = "Λεπτομέρειες συναλλαγής",
			   $payment_ID 			 = "Ταυτοποίηση συναλλαγής",
			   $invoice				 = "τιμολόγιο",
			   $just_date			 = "Ημερομηνία",	
			   $ammount				 = "Ποσό",
			   $currency 			 = "Νόμισμα",
			   $show_invoice		 = "Εμφάνιση τιμολογίου",	
			   $sub_info			 = "πληροφορίες εγγραφής",
			   $package_name		 = "Όνομα πακέτου",
			   $package_duration	 = "Διάρκεια πακέτου",
			   $sub_start			 = "Έναρξη εγγραφής",
			   $sub_end				 = "Λήξη εγγραφής",
			   $appointment_use		 = "Περίοδος χρήσης",
			   $all_packageProposal  = "Όλα τα πακέτα",
			   $tosee_onlineWSVerify = "Για να παρακολουθήσετε online εργαστήρια, ή να αφήσετε κριτικές για την ιστοσελίδα μας, πρέπει να επιβεβαιώσετε το λογαριασμό σας.",
			   $weve_sentemail		 = "Έχουμε στείλει e-mail επιβεβαίωσης στο",
			   $didnt_recieveEmail	 = "Δεν λάβατε κάποιο e-mail;",
			   $send_newEmail		 = "Στείλτε μου ένα νέο e-mail",
			   $ws_detail			 = "Λεπτομέρειες εργαστηρίου",
			   $addToWishList        = "Προσθήκη στη λίστα επιθυμιών",
			   $addedWishList        = "Προστέθηκε στη λίστα επιθυμιών",

			   // users receipt.php
			   $invoice_view		 = "Εικόνα τιμολογίου",	
			   $invoice_individual	 = "Τιμολόγιο ιδιώτη",
			   $contact_phone		 = "Τηλέφωνο επικοινωνίας",
			   $user_nameone		 = "Όνομα χρήστη",
			   $warning				 = "Προειδοποίηση",	
			   $pdv_sistem			 = "Handmade Fantasy World είναι στην εφορία, γιαυτό ο φόρος χρεώνεται.",
			   $payment_status		 = "Κατάσταση συναλλαγής",
			   $paid 				 = "Εξοφλήθηκε",
			   $in_process           = "In process",
			   $payment_date		 = "Ημερομηνία συναλλαγής",
			   $payment_method		 = "Τρόπος πληρωμής",
			   $id_payment			 = "Αριθμός ταυτοποίησης συναλλαγής",
			   $invoice_number		 = "Αριθμός τιμολογίου",	
			   $total 				 = "Σύνολο",
			   $number 				 = "Αριθμός τιμολογίου.",
			   $name_package		 = "Όνομα προΐόντος",
			   $price_pdvA			 = "Υποσύνολο",
			   $package 			 = "Πακέτο",
			   $sub_lasts			 = "Η εγγραφή διαρκεί",
			   $ws_lowchar			 = "Ερηαστήριο",

			   // settings.php
			   $acc_settOpt          = "Ρυθμίσεις",
			   $acc_logout           = "Αποσύνδεση",
			   $acc_settings		 = "Ρυθμίσεις λογαριασμού",
			   $password_change		 = "Αλλαγή κωδικού",
			   $confirm_change       = "Επιβεβαίωση αλλαγής",
			   $oldPass_notValid	 = "Ο παλιός κωδικός δεν είναι έγκυρος",
			   $pass_areNotMatching	 = "Ο κωδικός δεν ταιριάζει",
			   $old_passCantBeEmpty  = "Ο παλιός κωδικός δεν μπορεί να είναι κενός",
			   $new_passCantBeEmpty	 = "Ο νέος κωδικός δεν μπορεί να είναι κενός",
			   $last_timeYouChanged	 = "Τελευταία φορά που αλλάξατε κωδικό",
			   $loading              = "Φορτώνει...",
			   $yearLOW				 = "",
			   $changeUsername       = "Αλλαγή ονόματος χρήστη",
			   $new_pass 			 = "Νέος κωδικός",
			   $enterNewPass         = "Εισάγετε νέο κωδικό",
			   $enterNewEmail        = "Εισάγετε νέο email",
			   $confirmNewPass       = "Επιβεβαιώστε νέο κωδικό",
			   $repeat_newPAss		 = "Επαναλάβετε κωδικό",
			   $passwords_areMatching= "Οι κωδικοί ταιριάζουν",
			   $approve_passChange	 = "Επιβεβαιώστε αλλαγή κωδικού",
			   $error_happenedOne	 = "Προέκυψε κάποιο σφάλμα",
			   $please_enterEmail	 = "Παρακαλώ εισάγετε νέο email",
			   $change_email		 = "Παρακαλώ εισάγετε νέο e-mail",	
			   $enterNewUsername     = "Παρακαλώ εισάγετε νέο όνομα χρήστη",
			   $enterNewUsername_s   = "Εισάγετε νέο όνομα χρήστη",
			   $after_emailChange	 = "Μετά την αλλαγή e-mail θα λάβετε ένα e-mail με πληροφορίες",
			   $deactivate_Acc		 = "Απενεργοποίηση λογαριασμού",
			   $areYouSure 			 = "Είστε σίγουροι ότι θέλετε να απενεργοποιήσετε το λογαριασμό;. Μπορείτε να επανενεργοποιήσετε το λογαριασμό οποιαδήποτε στιγμή",
			   $acc_deactivate		 = "Απενεργοποίηση λογαριασμου",
			   $deact_success		 = "Επιτυχής απενεργοποίηση λογαριασμού",
			   $redirect_toMain		 = "Οδηγείστε στην αρχική σελίδα για",
			   $remove_profilePic	 = "Αφαίρεση εικόνας προφίλ",
			   $change_profilePic	 = "Αλλαγή εικόνας προφίλ",
			   $acc_verified		 = "Ο λογαριασμός επιβεβαιώθηκε",
			   $acc_NotVerified		 = "Ο λογαριασμός δεν επιβεβαιώθηκε",
			   $change_emailAdress	 = "Αλλαγή ηλεκτρονική διεύθυνσης",
			   $your_language 		 = "Γλώσσα",
			   $want_toRecieve		 = "Θέλω να λαμβάνω",
			   $newsletter			 = "Ενημερωτικά δελτία",
			   $s 					 = "s",
			   $maxPicSize			 = "Το μέγιστο μέγεθος εικόνας είναι",
			   $pass 				 = "Κωδικός",
			   $confPass             = "Επιβεβαίωση κωδικού",
			   $change_yourPass		 = "Αλλάξτε τον κωδικό σας",
			   $passChange_LastTime	 = "Τελευταία αλλαγή κωδικού",
			   $after_deactivation	 = "Μετά την απενεργοποίηση, μπορείτε να ενεργοποιήσετε το λογαριασμό σας οποιαδήποτε στιγμή",

			   // subscriptions.php
			   $subs 				 = "Εγγραφή",
			   $all_subs			 = "Όλες οι εγγραφές",
			   $allSubs_List		 = "Λίστα όλων των εγγραφών",
			   $data_product		 = "Στοιχεία του προΐόντος",
			   $status 				 = "Κατάσταση",
			   $sales				 = "Ενέργείες",
			   $noSubs_ATM			 = "Δεν υπάρχουν συνδρομές προς το παρόν",
			   $deactivate_sub		 = "Απενεργοποίηση συνδρομών",
			   $if_youDeactivateSub	 = "Απενεργοποιώντας τον λογαριασμό χάνετε το δικαίωμα εισόδου στα ενεργά εργαστήρια",
			   $deact_yourSub		 = "Απενεργοποιήστε την συνδρομή ",
			   $activation_date		 = "Ημερομηνία ενεργοποίησης",
			   $expired				 = "έληξε",
			   $inactive_an 		 = "Ανενεργή",
			   $active 				 = "Ενεργή",
			   $credit_card			 = "πιστωτική Κάρτα",
			   $paypal 				 = "Ολοκλήρωση αγοράς με PayPall express",
			   $deactivation 		 = "Απενεργοποίηση",

			   // wishlist.php
			   $my_wishlist			 = "Η λίστα επιθυμιών μου",
			   $noWS_inYourWishList	 = "Δεν υπάρχουν εργαστήρια στη λίστα επιθυμιών σας",
			   $all_workshops		 = "Όλα τα εργαστήρια",
			   $others_like			 = "Άλλα άτομα που τους άρεσε αυτό",
			   $subscription 		 = "Συνδρομή",
			   $select_package		 = " Διαλέξτε πακέτο",
			   $addToCart 			 = "Προσθήκη στο καλάθι",
			   $delete_fromWishlist	 = "Αφαίρεση από την λίστα επιθυμιών",

			   // workshops.php
			   $my_workshops 		 = "Τα εργαστήρια μου",
			   $allowed_WS			 = "Διαθέσιμα εργαστήρια",
			   $noWS_ATM			 = "Δεν υπάρχουν διαθέσιμα εργαστήρια",
			   $no_WSOnyourAcc		 = "Δεν υπάρχουν εργαστήρια στο λογαριασμό σας.",
			   $pachages_low		 = "τα πακέτα μας",
			   $no_rating            = "Χωρίς ταξινόμηση",

			   $_403                 = "Δεν έχετε δικαιώματα πρόσβασης σε αυτή τη σελίδα.",
			   $h403                 = "Απαγορευμένη πρόσβαση",
			   $_404                 = "Υπήρξε σφάλμα κατά τη φόρτωση αυτής της σελίδας. Η ιστοσελίδα που ζητήσατε δεν είναι διαθέσιμη προσωρινά ή δεν υπάρχει.",
			   $h404                 = "Η σελίδα δεν βρέθηκε",
			   $_500                 = "Τυγχάνει να υπάρχει πρόβλημα στον διακομιστή. Παρακαλώ δοκιμάστε αργότερα.",
			   $h500                 = "Εσωτερικό σφάλμα διακομιστή",
			   $error                = "Σφάλμα",

			   $receipt              = "Απόδειξη",
			   $payment              = "Πληρωμή",
			   $comment              = "Σχόλιο",
			   $comments             = "Σχόλια",
			   $email_change         = "Αλλάξτε διεύθυνση ηλεκτρονικού ταχυδρομείου",

			   // VIDEO.PHP
			   $passForPass          = "Κωδικός πρόσβασης",
			   $inOrderToWatch       = "Για να παρακολουθήσετε το βίντεο, παρακαλώ αντιγράψτε τον κωδικό που σας δόθηκε.",
			   $copy                 = "Αντιγραφή",
			   $copyPass             = "Αντιγραφή στο πρόχειρο",
			   $succCopied           = "Έχετε επιτυχώς αντιγράψει τον κωδικό. Μπορείτε να τον επικολλήσετε στο πεδίο του βίντεο <span class='strong'>password.</span>",
			   $likeIt               = "Μου αρέσει",
			   $likeItImage          = "Μου αρέσει η εικόνα",
			   $dislikeIt            = "Δεν μου αρέσει",
			   $dislikeItImage       = "Δεν μου αρέσει η εικόνα",
			   $like_yand            = "Εσείς και σε",
			   $person               = "άτομο",
			   $persons              = "άτομα",
			   $like_this            = "αρέσει αυτό.",
			   $likes_this           = "αρέσει αυτό.",
			   $youLikeThis          = "Σας αρέσει.",
			   $beFirstToLike        = "Γίνετε ο πρώτος που θα σας αρέσει αυτό.",
			   $love                 = "Αγαπάνε",
			   $loves                = "Αγαπάει",
			   $wantToHearOp         = "Θέλουμε να σας ακούσουμε",
			   $submitReview         = "Υποβολή κριτικής",
			   $text_reviews         = "κατά την προσθήκη ( ή την επεξεργασία μιας δημοσιευμένης κριτικής), πρέπει να βεβαιωθούμε πρώτα ότι δεν υπάρχει κάποιο προσβλητικό περιεχόμενο στην δκριτική σας, οπότε στέλνουμε την κριτική για έλεγχο. Ο έλεγχος υποβολής συνληθως διαρκεί λιγότερο από 48 ώρες. Μετά από αυτό, η κριτική σας θα είναι ορατή σε όλους τους χρήστες και επισκέπτες της <span class=\"strong\">Handmade Fantay World.</span>",
			   $controlPanel         = "Πίνακας ελέγχου",
			   $welcomeAdmin         = "Καλωσήλθατε, Διαχειριστής",
			   $soonContact          = "Το μήνυμα σας έχει σταλεί. Θα επικοινωνήσουμε μαζί σας πολύ σύντομα!",
			   $finishedProduct      = "Ορίστε μια φωτογραφία του ολοκληρωμένου προΐόντος",
			   $finishedProductAcr   = "Ολοκληρωμένο προΐόν",
			   $qHeading             = "Τίτλος",
			   $qText                = "Το μήνυμα σας(το λιγότερο 10 χαρακτήρες)",
			   $finished_product	 = "Ολοκληρωμένο προΐόν",
			   $ingredients			 = "Υλικά",
			   $for_thisWSYouNeed	 = "Για αυτό το εργαστήριο θα χρειαστείτε",
			   $ask_aQuestion		 = "Έχετε κάποια ερώτηση;",
			   $here_postQuestion	 = "Εδώ μπορείτε να μας στείλετε ερώτηση σχετική με αυτό το εργαστήριο",
			   $send_question		 = "Στείλτε ερώτηση",
			   $respond_asSoonAs	 = "Θα απαντήσουμε στην ερώτηση σας μέσω email αμέσως μόλις μπορέσουμε",
			   $email_btnNoWork      = "Αν το κουμπί δεν λειτουργεί, αντιγράψτε αυτό το σύνδεσμο στον browser σας",
			   $ws_with              = "Εργαστήριο με",

			   // Gallery
			   $no_likes             = "Δεν υπάρχουν like",	
			   $onSlide              = "Σε διαφάνεια",	
			   $noPhotos             = "Προσωρινά, δεν έχουμε καθόλου φωτογραφίες.",
			   $moreImages           = "περισσότερες εικόνες",

			   // packages.php
			   $bestPackage          = "Διαλέξτε το καλύτερο πακέτο εργαστηρίων για σας",

			   // calendar.php
			   $welcomeCalendar      = "Καλώς ήλθατε στο ημερολόγιο",
			   $wc_details           = "Σε αυτή τη σελίδα μπορείτε να βρείτε όλα τα προηγούμενες και προσεχείς εκδηλώσεις",
			   $location             = "Τοποθεσία",
			   $backEvents           = "Πίσω στις εκδηλώσεις",
			   $prevMonth            = "Προηγούμενος μήνας",
			   $nextMonth            = "επόμενος μήνας",

			   // News
			   $stay_tunned			 = "Μείνετε συντονισμένοι",
			   $topical				 = "Hit",

			   // Index(novi prevod)
			   $talent_cantBeTaught	 = "Το τελέντο δεν διδάσκεται αλλά μπορεί να αφυπνιστεί",
			   $join_ourOnline		 = "Ελάτε στα online εργαστήρια μας και βελτιώστε τις καλλιτεχνικες σας δεξιότητε και το πάθος για δημιουργία",	
			   $lets_haveFun		 = "Ας διασκεδάσουμε παρέα",
			   $cooperation			 = "Ενδιαφέρεστε να συνεργαστούμε;",
			   $emailUS              = "στείλτε μας mail sto",
			   $send_usEmail		 = "Στείλτε μας ένα email",

			   // about(novi prevod)
			   $my_nameIS			 = "Γειά, το όνομα μου είναι <span class=\"strong color-theme\">Αντώνης Τζανιδάκης",
			   $be_inTouch			 = "Να είστε σε επαφή",
			   $thank_you			 = "Σας ευχαριστώ",
			   
			   // Languages
			   $L_serbian            = "Σέρβικα",
			   $L_english            = "Αγγλικά",
			   $L_russian            = "Ρώσσικα",
			   $L_spanish            = "Ισπανικά",
			   $L_greek              = "Ελληνικά",

			   // Currencies         
			   $C_RSD                = "Σερβικό Δηνάριο",
			   $C_EUR                = "Ευρώ",
			   $C_RUB                = "Ρωσσικό Ρούβλι",
			   $C_USD                = "Αμερικάνικο Δολλάριο",
			   $C_PLN                = "Πολωνικό Ζλότι",

			   // 19.06.2017
			   $november 			 = "November",
			   $wS_coventry			 = "Workshops Coventry", 
			   $explore_wS			 = "Explore workshops",
			   $add_ws_toCart		 = "Adding workshops to cart",
			   $show_shoppingCart	 = "Show Shopping Cart",
			   $visit 				 = "Visit",
			   $website_sm 			 = "website",
			   $no_ingredients		 = "Currently, we don't have list of igredients for this workshop.",
			   $enjoy_ourPassion	 = "Enjoy in our passion",
			   $per_ws				 = "per workshop",
			   $per_ws_acr           = "w",
			   $your_Cart			 = "Your cart",
			   $finish_Purchase		 = "Complete your purchase",
			   $welcome_To			 = "Welcome to",
			   $user_Section		 = "user section",
			   $site_a				 = "website",
			   $news_user			 = "Notifications",
			   $check_News			 = "See the notifications",
			   $online_workshops_sm  = "Online workshops", 
			   $newest_Ws			 = "Newest workshops",
			   $new 				 = "NEW",
			   $share_ws_facebook	 = "Share workshop on Facebook",
			   $share_ws_Twitter	 = "Share workshop on Twitter",
			   $listOf_YourWs		 = "List of your workshops",
			   $take_a_Look			 = "Take a look at the Shopping Cart.",
			   $see_Also			 = "See also",
			   $new_pricing			 = "new pricing",
			   $continue_shopping	 = "Continue shopping",
			   $pls_wait_Transaction = "Please wait for transaction to finish. DO NOT leave this page.",
			   $look_newPriceLst     = "Take a look at new Pricelist.",
			   $whatYouGet           = "What You get",
			   $price_pw             = "Price per workshop",
			   $trait_1              = "Unlimited access to bought workshops",
			   $trait_2              = "Subtitles in English, Greek, Serbian",
			   $trait_3              = "Supplies list",
			   $trait_4              = "<span class=\"strong\">Full HD</span> workshops",
			   $suppEngTitle         = "Subtitle in English",

			   // AJAX
			   $aj_sucChangeCurr     = "Εχετε επιτυχώς αλλάξει νόμισμα",
			   $aj_sucChangeLang     = "Έχετε επιτυχώς αλλάξει τη γλώσσα",
			   $aj_sucChangePass     = "Εχετε επιτυχώς αλλάξει κωδικό",
			   $aj_sucChangeUsername = "Έχετε επιτυχώς αλλάξει όνομα χρήστη",
			   $aj_allowedExtensions = "Οι επιτρεπτές καταλήξεις αρχείων είναι",
			   $aj_maxUplaodSize     = "Το μέγιστο μέγεθος εικόνας για ανέβασμα είναι",
			   $aj_errorOccured      = "Προέκυψε κάποιο σφάλμα κατά την φόρτωση. Παρακαλώ δοκιμάστε ξανά",
			   $aj_succUploaded      = "Έχετε επιτυχώς ανεβάσει μία εικόνα προφίλ",

			   $next_Workshop 		 = "Επόμενο εργαστήριο",
			   $our_Partners		 = "Οι συνεργάτες μας",
			   $upcoming_event		 = "Επερχόμενη εκδήλωση",
			   $currently_noEvents	 = "Προσωρινά, δεν έχουμε επερχόμενες εκδηλώσεις",
			   $sub_ToMyWS			 = "Γίνετε συνδρομητές στα online μου εργαστήρια",
			   $subbed_For			 = "Γίνετε συνδρομητές για",
			   $subb_for 			 = "Γίνετε συνδρομητές για",
			   $select_StartMonth	 = "Επιλέξτε μήνα έναρξης",
			   $could_NotDelete		 = "Δεν είναι δυνατόν να διαγράψετε την τελευταία προεπιλεχθήσα πληροφορία",
			   $video_plsCopy		 = "Κωδικός βίντεο. Παρακαλώ αντιγράψτε και επικολλήστε τον κωδικό παρακάτω στο πεδίο \"Κωδικός\".",
			   $take_aLookSup		 = "Ρίξτε μια ματιά στους προμηθευτές μας",
			   $website 			 = "Ιστοσελίδα",

			   // Calendar - opt
			   $c_months             = ["Ιανουάριος","Φεβρουάριος","Μάρτιος","Απρίλιος","Μαΐος","Ιούνιος","Ιούλιος","Άυγουστος","Σεπτέμβριος","Οκτώβριος","Νοέμβριος","Δεκέμβριος"],
			   $c_days_acr           = ["Κυρ","Δευ","Τρι","Τετ","Πεμ","Παρ","ΣΑβ","κυρ"],

			   // 12.03.2017
			   $other_supp			 = "Other supplies",
			   $only_englishSubs	 = "Currently, video has only subtitles in English and Serbian. Other languages will be provided in couple of days.",
			   $no_access			 = "You don't have access to watch video from this device",
			   $max_numDevices		 = "You have registered on maximum amount of devices. Please log in on device where you always watched videos.",
			   $for_moreInfo		 = "For more information contact us at",
			   $you_haveCoupon		 = "You have coupon for this workshop",
			   $coupon               = "Coupon",	

			   $won_coupons			 = "Won coupons",
			   $check_coupons		 = "Check which coupons you've won",
			   $watch_Now			 = "Watch Now",
			   $we_hopeYoullEnjoyONE = "We hope you'll enjoy this workshop",
			   $we_hopeYoullEnjoyM   = "We hope you'll enjoy these workshops",
			   $see_you				 = "See you",
			   $online_ws 			 = "Online workshop",
			   $e_ws_start_today     = "starting today",
			   $lookOnOurWebsite     = "See workshop",
			   $facebook             = "Facebook",
			   $instagram            = "Instagram",
			   $pleaseWait           = "Please wait...",
			   $no_access_blck       = "You don't have access to watch video",
			   $other_user_blck      = "Other user is using this device to watch videos. Please log in on device you ordinary watch videos.";

			   //newEmail
			   $new_notification	 = "Novo obaveštenje",
			   $we_areGrateful		 = "Zahvalni smo Vam na prethodnim pretplatama i želimo da Vas nagradimo specijalnom ponudom. Radionice su Vam sada dostupne po ceni od",	
			   $explore_newWs		 = "Explore new workshops",
			   $buy_Ws 				 = "Kupite radionicu",
			   $watch_ws			 = "Gledajte radionice",
			   $thanks_trust		 = "Hvala Vam na poverenju",
			   $sincerely			 = "Sincerely",

	}
	if (!isset($MAKE_LANG) || isset($MAKE_LANG) && $MAKE_LANG === true) $lang = new Language_GR;

	if(isset($page))
	{
		switch($page)
		{
			case "index":
			$INDEX_1					= "Είμαι ένα δημιουργικό πνέυμα και πάντα θέλω να αναζητώ και να βρίσκω κάτι νέο και καινοτόμο! Σας προσκαλώ να μοιραστείτε μαζί μου το δημιουργικό μου ταξίδι και να ανακαλύψετε μαζί μου νέες τεχνικές, καλλιτέχνες, ιδέες και φυσικά προμήθειες!";
			$INDEX_3					= "Κρυφό ταλέντο? Ας το ξεδιπλώσουμε μαζί!";
			$INDEX_4					= "Κάθε μήνα θα σας παρέχουμε με βίντεο υψηλής ποιότητας, με υπότιτλους σε πολλές γλώσσες, που θα περιγράφουν τις δημιουργίες μας, νέα εργαλεία και υλικά.";
			$INDEX_5					= "Μαζί θα χρησιμοποιήσουμε κοινά υλικά και θα τα μεταμορφώσουμε σε έργα τέχνης... δεν είναι μαγικό?";
			$INDEX_6					= "Η δημιουργική μας ομάδα θα παρουσιάσει πολλές τεχνικές. Θα ακολουθήσουμε την δημιουργικότητα μας, αλλά επίσης τη δική σας ανάδραση για να κάνουμε νέα έργα.";
			$INDEX_7					= "Εώς τώρα, πάνω από 1000 άνθρωποι πήραν μέρος στα κατ' ιδίαν εργαστήρια. Τεράστια εμπειρία που θα εφαρμοστεί στα online εργαστήρια για να σας δώσει γνώση και πληροφορίες για τεχνικές και υλικά, και ,το πιο σημαντικό, να σας ενθαρύνει να δημιουργήσετε.";
			$INDEX_8					= "Το να δημιουργείς κάτι δεν είναι μόνο διασκέδαση, αλλά μειώνει το άγχος και βοηθά να ηρεμήσει το μυαλό. Δεν είναι δουλεία, αλλά χαρά και χαλάρωση επίσης! Ενόσω αναπτύσσετε τη δική σας διαδικασία δημιουργίας, θα βρείτε παρόμοια μυαλα και θα κάνετε διαχρονικές φιλίες απ' όλο τον κόσμο.";
			$INDEX_9					= "Κάθε μήνα, θα δημιουργούμε τουλάχιστον τέσσερα διαφορετικά έργα. Σχεδιάζουμε τα εργαστήρια μας έτσι ώστε να είνα κατάλληλα για όλα τα επίπεδα εξειδίκευσης. Αλλά μην γελιέστε- δεν είναι απλά ένα μάτσο από οδηγίες! Τα εργαστήρια μας είναι τρελά και αστεία καιγεμάτα πάθος! Είσαστε 'ετοιμοι να μοιραστούμε την εμπειρία?";
			break;

			case "gallery":
				$GALLERY_1 = "Pertinax invenire eos, ut vis eius maiestatis temporibus";
			break;

			case "about":
				$introduction = "Είμαι ένας mixed media καλλιτέχνης από την Ελλάδα.";

				$story_1 = "Βούτηξα στη διαδικασία των mixed media πρίν από τρία χρόνια. Δεν το είχα σχεδιάσει, ήταν απλά ένα χόμπι, αλλά πολύ σύντομα έγινε η ζωή μου! ¨εχω περάσει ώρες και ώρες εξερευνώντας την τέχνη μου, πάντα θέλοντας να πάω ένα βήμα παραπέρα, πάντα ήθελα να παίξω λίγο ακόμα! Σύντομα έγινε το πάθος μου, η εμμονή μου,  το μυστικό μέρος για να βρώ τον εαυτό μου!";

				$story_2 = "Δημιούργησα μια σελίδα στο Facebook <a class=\"color-theme link-ord\" href=\"".$COMPANY_INFO->facebook."\" target=\"_blank\">“Handmade Fantasy”</a>, και βίωσα την αγάπη και την αποδοχή χρηστών από όλη την υφήλιο. Σε αυτό το σημείο, συνειδητοποίησα πόσο καλός ήμουνα σε αυτό που έκανα  και πως αυτή η σελίδα ήταν μόνο η αρχή - Δεν μπορούσα να σταματήσω!";	

				$story_3 = "Αφοσιώθηκα στο να κάνω την Fantasy Around the World παγκοσμίου προβολής, αναγνωρίσθηκε και διαμοιράσθηκε από όλους τους συναδέλφους καλλιτέχνες και ανθρώπους που αγαπάνε την τέχνη!";	

				$story_4 = "Κάποια μέρα ένας φίλος με ρώτησε αν θα ήθελα να διδάξω κι άλλους ανθρώπους, να τους διδάξω το δικό μου τρόπο που δημιουργώ και αυτό έγινε η αρχή να κάνω εργαστήρια υπό την ονομασία Handmade Fantasy. Ξεκίνησα από το νησί της Λευκάδας, το μέρος που έμενα εκείνη την εποχή,και μέρα με τη μέρα, μήνα με το μήνα, άνθρωποι απ' όλη την Ελλάδα άρχισαν να μου ζητάνε να οργανώσω εργαστήρια στα καταστήματα τους. Πολύ σύντομα ήρθαν μηνύματα και από άλλες χώρες, οπότε το όνειρο μου έγινε σχεδόν πραγματικότητα. Από την αρχή είχα την ιδέα να ξεκινήσω μία ιστοσελίδα σαν κι αυτή, έτσι ώστε άνθρωποι από κάθε γωνιά του κόσμου να δούν πως δουλέυω, χωρίς να χάνουν χρόνο ταξιδέυοντας και να ξοδεύουν χρήματα. Έτσι μπήκα σε αυτό και τώρα όλοι σας μπορείτε να δείτε και να επισκεφτείτε το μέρος μου που λέγεται<a class=\"link-ord color-theme\" href=\"#\">handmadefantasyworld.com</a>.";	
				$story_5 = "Επομένως πολλά πράγματα πρόκειται να συμβουν εδω μέσα, Έχω πολλές ιδέες.";
			break;

			case "policy":
				$header1   = "Πολιτική και συνθήκες";
				$para1     = "Αυτοί οι όροι και συνθήκες εφαρμόζονται για τη χρήση όλων των υπηρεσιώνervices under the domain www.handmadefantasyworld.com. By accessing this website and/or placing an order, you agree to be bound by these terms and conditions. This site is operated and maintained by: Handamde Fantasy A&amp;B doo. Αριθμός μητρώου εταιρείας: 21256706.";

				$header2   = "Handmade fantasy A&B d.o.o. Beograd-Zemun";
				$pol_info1 = "Cara Dušana 266e";
				$pol_info2 = "11080 Zemun - Belgrade";
				$pol_info3 = "Serbia";
				$pom_info4 = "ΑΦΜ";
				$pol_info5 = "Αριθμός μητρώου εταιρείας";
				$para2	   = "Η επικοινωνία με την εξυπηρέτηση πελατών μπορεί να γίνει μέσω email support@handmadefantasyworld.com. Office hours: (CET) Monday to Thursday: 10.00am – 4.00  pm; Friday: 10.00am - 4.00pm";
				$header3   = "Πίνακας περιεχομένων";
				$header4   = "Πολιτική ακύρωσεων και επιστροφών";
				$header5   = "Χρήση των Cookies";
				$header6   = "Εγγραφή";
				$header7   = "Περί αποποίησης ευθυνών";
				$header8   = "Πολιτική Προστασίας Προσωπικών Δεδομένων";

				// Cancellation and Returns Policy
				$para3	   = "Εάν επιθυμείτε να ακυρώσετε την παραγγελία σας:";
				$para4	   = "μπορείτε να μας ενημερώσετε μέσω mail <a href=\"mailto:support@handmadefantasyworld.com\">(support@handmadefantasyworld.com)</a> before we have approved you access to video servise or";
				$para5	   = "όπου σας έχει ήδη εγκριθεί πρόσβαση, έχετε το δικαίωμα να ακυρώσετε το συμβόλαιο σας μέσα σε 2 ημέρες ξεκινώντας από την ημέρα της επιβεβαίωσης.";
				$para6	   = "Μπορείτε να ακυρώσετε το συμβόλαιο σας και να κάνετε εξαργύρωση ενημερώνοντας μας γραπτώς μέσω mail μέχρι και 2 εργάσιμες ημέρες ξεκινώντας από την ημέρα μετά την επιβεβαίωση στην διεύθυνση στην αρχή αυτών των όρων. Τα χρήματα θα επιστραφούν στον ίδιο λογαριασμό ή την πιστωτική κάρτα που χρησιμοποιήθηκε στην προηγούμενη συναλλαγή.";

				// Use of Cookies
				$para7	   = "Το cookie είναι ένα μικρό κομμάτι πληροφοριών που αποθηκέυεται στον υπολογιστή σας. Ορίζεται από εμαάς όταν επισκέπτεστε την ιστοσελίδα και υπάρχει πρόσβαση και χρήση μόνο από εμάς. Κάθε cookie έχει ημερομηνία λήξης που καθορίζει το πόσο διαρκεί ένα cookie. Τα Cookies δεν περιέχουν καμία πληροφορία προσωπικής ταυτοποίησης όταν κάνετε εγγραφή, παρόλα αυτά, το μπορεί να συνδυάζεται με προσωπικές πληροφορίες τις οποίες μας έχετε δώσει ως μέρος της εγγραφής. Χρησιμοποιούμε cookies με πολλούς τρόπους για να βελτιώσουμε τις υπηρεσίες μας. Παρακαλώ ας είστε γνώστες ότι χρησιμοποιούμε τα cookies μόνο όταν είναι απολύτως απαραίτητα και πάντα διασφαλίζουμε ότι διαρκούν μόνο τόσο όσο είναι χρήσιμα.";

				// Registration
				$para8	   = "Δεν έχει επιβληθεί όριο ηλικίας στην ιστοσελίδα μας, όμως οι γονείς και οι νόμιμοι κηδεμόνες είναι υπέυθυνοι για την συμμόρφωση με αυτούς τους όρους για τα παιδιά υπό την δική τους ευθύνη.";
				$para9	   = "Κάνοντας εγγραφή βεβαιώνετε ότι όλες οι πληροφορίες που παρέχετε κατά την εγγραφή είναι ακριβής και συμφωνείτε να μην υποδύθείτε οποιοδήποτε πρόσωπο ή οντότητα ή κάνετε ψευδή δήλωση κατάστασης ή διαστρέβλώσετε τη σχέσης σας με οποιοδήποτε πρόσωπο ή οντότητα.";
				$para10	   = "Μπορείτε να ενημερώνετε την εγγραφή σας οποιαδήποτε στιγμή στον τομέα το προφίλ μου της ιστοσελίδας μας.";
				$para11	   = "Κάθε εγγραφή είναι για ένα και μόνο χρήστη. Δεν σας επιτρέπουμε να μοιράζεστε το όνομα χρήστη ή τον κωδικό σας με οποιοδήποτε άλλο άτομο ούτε με πολλαπλούς χρήστες στο δίκτυο.";
				$para12	   = "Η ευθύνη για την ασφάλεια των κωδικών που έχουν εκδοθεί επαφίεται σε εσάς και αν γνωρίζετε ή υποψιάζεστε ότι κάποιο άλλος γνωρίζει τον κωδικό σας, θα πρέπει να αλλάξετε τον κωδικό σας αμέσως και να βεβαιώσετε ότι είναι ασφαλής. Είσατε υπέυθυνοι για όλες τις ενέργειες που γίνονται στο λογαριασμό σας ή στον κωδικό.";
				$para13	   = "Μπορούμε να αναστείλλουμε ή να ακυρώσουμε την εγγραφή σς αμέσως κατά τη δική μας έυλογη κρίση είτε αν αθετήσετε οποιαδήποτε από τις υποχρεώσεις σας υπό αυτούς τους όρους και συνθήκες.";

				// Disclaimer
				$para14	   = "Ενώ προσπαθούμε να διασφαλίσουμε ότι οι πληροφορίες στην παρούσα Ιστοσελίδα είναι σωστές, δεν εγγυόμαστε την ακρίβεια και την πληρότητα του υλικού αυτού του δικτυακού τόπου. Μπορούμε να κάνουμε αλλαγές στο υλικό σε αυτή την ιστοσελίδα, ή με τα προϊόντα και τις τιμές που περιγράφονται σε αυτό, οποιαδήποτε στιγμή χωρίς προηγούμενη ειδοποίηση. Το υλικό σε αυτή την ιστοσελίδα μπορεί να είναι ξεπερασμένο, και δεν κάνουμε καμία δέσμευση για την ενημέρωση του εν λόγω υλικόυ.";
				$para15	   = "Το υλικό σε αυτή την ιστοσελίδα παρέχεται \"ως έχει\" χωρίς όρους, εγγυήσεις ή άλλες διατάξεις οποιουδήποτε είδους. Κατά συνέπεια, στο μέγιστο βαθμό που επιτρέπεται από το νόμο, σας παρέχουμε αυτή την ιστοσελίδα με βάση το ότι θα αποκλείσει όλες τις αντιπροσωπέυσεις, εγγυήσεις, προϋποθέσεις και άλλους όρους οι οποίοι, εκτός από αυτούς τους όρους και τις προϋποθέσεις, θα μπορούσε να έχει επιπτώσεις σε σχέση με αυτή την ιστοσελίδα.";


				// Privacy Policy
				$para16    = " Η Πολιτική Προστασίας Προσωπικών Δεδομένων της Handmade fantasy A&B είναι να σέβεται και να προστατεύει την ιδιωτική ζωή των επισκεπτών και των συνδρομητών στην ιστοσελίδα μας. Αυτή η δήλωση πολιτικής σας λέει πώς συλλέγουμε πληροφορίες από εσάς και πώς τις χρησιμοποιούμε. Θα σας βοηθήσει να παίρνετε συνειδητές αποφάσεις όταν χρησιμοποιείτε την ιστοσελίδα μας και τις υπηρεσίες μας .";

				$para17	   = "Πράγματι ζητάμε προσωπικές πληροφορίες όπως το όνομά σας, τη διεύθυνση ηλεκτρονικού ταχυδρομείου, τα στοιχεία της εταιρείας, κλπ τα οποία μπορείτε να επιλέξετε να παρέχετε κατά την εγγραφή σας στην ιστοσελίδα μας. Πρέπει να συλλέξουμε αυτές τις πληροφορίες για να μας βοηθήσουν να προσωποποιήσουμε και να προσφέρουμε το σχετικό περιεχόμενο με γνώμονα το όφελος. Δεν έχετε καμία υποχρέωση να παράσχετε οποιεσδήποτε προσωπικές πληροφορίες, εάν δεν θέλετε. Επίσης, χρησιμοποιούμε τις προσωπικές πληροφορίες για τους σκοπούς της διαχείρισης και την επέκταση των επιχειρηματικών δραστηριοτήτων μας, παρέχοντας εξυπηρέτηση πελατών και τη διάθεση άλλων προϊόντων και υπηρεσιών προς τους πελάτες μας και μελλοντικούς πελάτες. Περιστασιακά, μπορούμε επίσης να χρησιμοποιήσουμε τις πληροφορίες που συλλέγουμε για να σας ενημερώσουμε σχετικά με σημαντικές αλλαγές στην ιστοσελίδα μας, νέες υπηρεσίες και ειδικές προσφορές που νομίζουμε ότι θα βρείτε αξιόλογες. Αν επιλέξετε να εξαιρεθείτε από τη λήψη τέτοιου είδους επικοινωνίες από εμάς, τότε παρακαλώ να το κάνετε μέσω του συνδέσμου unsubcribe σε κάθε email που διανεμουμε. Αυτη η  συγκέντρωση δεδομένων δεδομένα χρησιμοποιείται για να προσαρμόσουμε το περιεχόμενο της ιστοσελίδας μας έτσι ώστε να προσφέρει μια καλύτερη εμπειρία στους χρήστες μας και σε καμία περίπτωση να σας ταυτοποιήσουν προσωπικά.";

				$para18    = "Μια διεύθυνση IP είναι ένας αριθμός που αποδίδεται αυτόματα στον υπολογιστή σας κάθε φορά που είστε συνδεδεμένοι στο διαδίκτυο. Οι διακομιστές διαδικτύου αναγνωρίζουν αυτόματα τον υπολογιστή σας από τη διεύθυνση IP του. Σε ορισμένες περιπτώσεις, συλλέγουμε διευθύνσεις IP για τους σκοπούς της διαχείρισης του συστήματος, για να αναλύσουμε τις συγκεντρωτικές πληροφορίες, και για τον έλεγχο της χρήσης του site μας.";

				$para19    = "Η προστασία της ασφάλειας όλων των προσωπικά αναγνωρίσιμων πληροφοριών που σχετίζονται με τους επισκέπτες μας είναι υψίστης ανησυχίας μας. Μόλις λάβουμε την μετάδοση  σας, θα λάβουμε εύλογα μέτρα για να εξασφαλίσουμε την ασφάλεια της σχετικά στα συστήματα μας. Δυστυχώς, δεν μπορούμε να εγγυηθούμε ότι η μετάδοση δεδομένων μέσω του Διαδικτύου είναι 100% ασφαλής. Ως αποτέλεσμα, ενώ λαμβάνουμε όλα τα εύλογα μέτρα για να εξασφαλίσουμε την ασφάλεια των προσωπικών πληροφοριών των χρηστών μας και να διατηρήσουν την ιδιωτική τους ζωή, δεν μπορούμε να εγγυηθούμε ούτε να βεβαιώσουμε την ασφάλεια των πληροφοριών που αποκαλύπτετε ή μεταδίδετε σε εμάς online και δεν μπορούμε να είμαστε υπέυθυνοι για την κλοπή, καταστροφής, ή ακούσια γνωστοποίηση τέτοιων πληροφοριών προσωπικού χαρακτήρα.";

				$para20	   = "Με τη χρήση αυτής της ιστοσελίδας, δηλώνετε την συγκατάθεσή σας στην Πολιτική Προστασίας Προσωπικών Δεδομένων της docrafts. Αν δεν συμφωνείτε με αυτή την πολιτική, παρακαλούμε μην χρησιμοποιήσετε αυτή την ιστοσελίδα. Οι όροι της παρούσας Πολιτικής Προστασίας Προσωπικών Δεδομένων μπορεί να αλλάξουν από καιρό σε καιρό χωρίς να προηγηθεί ειδοποίηση προς εσάς, έτσι παρακαλούμε να ελέγξετε αυτή τη σελίδα περιοδικά για τυχόν αλλαγές. Η συνέχιση της χρήσης αυτού του site μετά τις όποιες αλλαγές σε αυτούς τους όρους, θα σημαίνει ότι αποδέχεστε αυτές τις αλλαγές.";
			break;

			case "workshops":
				$WS_header1		= "σχετικά με τα εργαστήρια";
				$WS_para1		= "Το πακέτο online εργαστήρια βασίζεται σε μηνιαίο βάση και πληρωμή. Κάθε μήνα θα δημοσιεύσουμε 4 εργαστήρια (του Αντώνη Τζανιδάκη). Τα εργαστήρια θα είναι στα ελληνικά με διαθέσιμους υπότιτλους στα αγγλικά, ρωσικά, γλώσσες ex-YU, Γαλλικά, Ισπανικά, Πορτογαλικά, Πολωνικά....";
				$WS_para2		= "Κάθε εργαστήριο θα διαρκείι περίπου 40 λεπτά με πλήρη επίδειξη της διαδικασίας της δημιουργίας, τον κατάλογο προμηθειών και τμήμα Ερωτήσεις & Απαντήσεις.";
				$WS_para3		= "Your access to the monthly package that you paid will never expire. You can repaet videos of paid package unlimited numbers of times.";
				$WS_para4		= "Τα εργαστήρια δεν απαιτούν προηγούμενη γνώση και είναι κατάλληλα για όλα τα επίπεδα εμπειρίας.";
				$WS_para5		= "Τα υλικά που θα χρησιμοποιηθούν είναι ως επί το πλείστον με βάση το νερό, ασφαλή για χρήση στο σπίτι και οικονομικά προσιτά παγκοσμίως.";
			break;

			case "cart":
				$Cart_para1		= "Όλες οι πληρωμές θα πραγματοποιούνται σε <span class=\"strong\">Σερβικό νόμισμα - δηνάριο (RSD)</span>. Το ποσό που η πιστωτικής σας κάρτα θα χρεώνεται θα επιτυγχάνεται μέσω της μετατροπής των τιμών από ευρώ σε Σερβικό δηνάριο, σύμφωνα με την τρέχουσα συναλλαγματική ισοτιμία της Εθνικής Τράπεζας της Σερβίας. Όταν θα χρεώνεται η πιστωτικής σας κάρτα, το ίδιο ποσό μετατρέπεται σε τοπικό νόμισμα, σύμφωνα με τη συναλλαγματική ισοτιμία των ενώσεων πιστωτικής κάρτας. Ως αποτέλεσμα αυτής της μετατροπής υπάρχει πιθανότητα μια μικρή διαφορά από την αρχική τιμή που αναγράφεται στην ιστοσελίδα μας.";

				$PI1            = "Payment Instructions";
				$PI2            = "Welcome to credit card payment instructions";
				$PI3            = "You will be returned to our website, do not close payment page";
				$PI4            = "Please take a look at <span class=\"strong\">3 simple</span> payment steps:";
				$PI5            = "Step";
				$PI6            = "First you have to click on <span class=\"italic\">\"Proceed to payment page\"</span>, and than you will be redirected to <a href=\"http://www.aikbanka.rs\" class=\"link-ord\" target=\"_blank\">AIK bank's</a> secure webpage for transactions.";
				$PI7            = "See webpage";
				$PI8            = "Next step is to fill your credit card information. The following fields will be required:";
				$PI9            = "Payment form on AIK bank secure webpage";
				$PI10           = "When you filled all of the reqired fields from <span class=\"strong\">step 2</span>, then you have to click on <span class=\"italic\">\"submit\"</span> button and <span class=\"strong\">wait for approx. 5 seconds</span>.";
				$PI11           = "Click on this button when you're finished, and <span class=\"strong\">wait 5-10 seconds</span>";
				$PI12           = "Don't show this again";
				$PI13           = "Close";
				$PI14            = "Proceed to payment page";
				$PI15            = "AIK bank secure webpage";
			break;

		}


	}



?>