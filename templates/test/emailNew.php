<?php 
	
	$prepath = "";
	include "../../functions.php";
	include "../../connect.php";
	include "../../global.php";
	include "../../getDATA.php";
	$domain = "http://localhost/pr/";

	$sql_subscr = mysql_query("SELECT `users`.name, `users`.surname, `users`.currencyID, `users`.lang, `users`.email, `tbl1`.* FROM (SELECT DISTINCT username FROM subscriptions WHERE `subscriptions`.active = 1) tbl1 INNER JOIN `users` ON `tbl1`.username = `users`.username", DBC_STORE);
	while($t = mysql_fetch_object($sql_subscr))
	{
		$SUBSCRIPTION = determine_user_subscription($t->username);	// Get info for subscription months
		$lang         = return_lang($t->lang, "../../");	// Get user's language

		// Get workshops for user that is not subscribed for that month
		$mnths = "";
		for ($i=0, $m = $SUBSCRIPTION->months, $ft = true; $i<count($m); $i++)
		{
			if (!$m[$i])
			{
				$mnths .= (!$ft ? " OR " : "")."MONTH(date_publish) = '".($i+1)."'";
				$ft = false;
			};
		};

		$sql_ws = mysql_query(" SELECT `tbl3`.*, `narrators`.n_name_".$t->lang." AS narrator_name FROM ( SELECT `tbl2`.*, COALESCE(COUNT(`reviews`.workshopID),0) AS reviews, COALESCE(SUM(`reviews`.rating) / COUNT(`reviews`.workshopID), 0) AS rating FROM (SELECT `tbl1`.*, CONCAT(`images`.imageID,'.',`images`.extension) AS image FROM (SELECT heading_".$t->lang." AS heading, subheading_".$t->lang." AS subheading, workshopID, narratorID, f_index FROM workshops WHERE (".$mnths.") AND active = 1 LIMIT 5) tbl1 INNER JOIN images ON `images`.workshopID = `tbl1`.workshopID AND `tbl1`.f_index = `images`.im_index) tbl2 LEFT OUTER JOIN `reviews` ON `reviews`.workshopID = `tbl2`.workshopID GROUP BY `tbl2`.workshopID ) tbl3 LEFT OUTER JOIN `narrators` ON `narrators`.narratorID = `tbl3`.narratorID", DBC_STORE);

		$message = '<!DOCTYPE html>
		<html>
		<head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width">
		</head>
		<body style="background: #FAFCFC; padding: 0; margin: 0; font-family: Arial; font-size: 100%;">
			<div style="width: 70%; margin: 0 auto; height:100%; background: white; border: 1px solid #DBDBDB; margin-top: 50px; border-radius: 3px">
				
				<div style="padding: 20px 20px 5px 20px">
					<table style="width:100%;">
						<tr>
							<td style="width:100%; padding-left: 10px; text-align: center;"> 
								<img src="'.$FILE.'img/logo.png" style="width:300px;"> 
							</td>
						</tr>
						<tr>
							<td style="text-align: center">
								<img src="'.$FILE.'img/email_logo.png" style="width: 80%; max-width: 400px" align="center">
							</td>
						</tr>
					</table>	
				</div>

				<hr style="#131A6E">
				
				<div style="width:100%;">
					<table style="width:100%; text-align:center;">
						<tr>
							<td style="width:45%;" align="center">
								<table>
									
									<td>
											<h1 style="color:#131A6E; "> '.$lang->new_notification.' </h1>
									</td>
									
								</table>
							</td>
						</tr>
					</table>
				</div>

				<hr style="#131A6E">

				<div style="width:100%; padding: 0 20px; margin-top:20px; margin-bottom: 20px; color:#131A6E;">
					<table style="width:95%; border-collapse: collapse">
						<tr>
							<td>
								<div style="margin-top:20px;">
									'.$lang->dear.' <b>('.$lang->name_surname.')</b>, <br><br>'.$lang->we_areGrateful.':
								</div>
								<div style="margin-top: 40px; text-align: center">
									<span style="font-size: 40px; font-weight: bold;">'.($t->currencyID === "RSD" ? "860" : "7").'</span> '.($t->currencyID === "RSD" ? "RSD" : "EUR").'
								</div>
							</td> 
						</tr>';
						
						if (mysql_num_rows($sql_ws)) {
						$g = '
						<tr>
							<td>
								<div style="margin-top: 30px; font-size: 20px; font-weight: bold;">'.$lang->explore_newWs.'</div>
							</td>
						</tr>
						<tr>
							<td>
								<div>
									<table style="width: 100%; background-color: #FCFCFC; border: 1px solid #C5C6C7; margin-top: 10px; margin-bottom:40px; border-collapse: collapse">';
									$tbl = '';
									while($h = mysql_fetch_object($sql_ws))	{
									$tbl .= '
									<tr style="vertical-align: top">
										<td style="width: 160px;">
											<a href="'.$FILE.($t->lang==="EN" ? "" : strtolower($t->lang)."/")."workshop/".$h->workshopID.'?traffic=email" target="_blank">
											<img src="'.$FILE.'img/content/'.$h->image.'" style="width: 140px; height: 120px;" >
											</a>
										</td>
										<td style="width: 50%">
											<div style="margin-top: 5px; font-size: 22px; font-weight: bold;">
											<a href="'.$FILE.($t->lang==="EN" ? "" : strtolower($t->lang)."/")."workshop/".$h->workshopID.'?traffic=email" target="_blank" style="color:#131A6E; "><span>'.$h->heading.'</span></a>
											</div>
											<div style="font-size: 14px; margin-top: 2px;">
											   '.$lang->ws_with.' '.$h->narrator_name.'
											</div>
											<div style="margin-top: 10px; font-size: 15px; color: #2E2E2E">'.$h->subheading.'</div>
										</td>
										<td style="text-align: right;">
											<div style="margin-top: 20px; margin-right: 10px;">
												<div>';
												if ($t->currencyID === "RSD")
												{
													$tbl .= '<b>860,00</b> RSD';
												} else 
												{
													$tbl .= '<b>7,00</b> EUR';
												};
												$tbl .= ' 
												</div>
												<div style="margin-top: 5px;">
												<button style="background-color:#131A6E; border: 2px solid white; border-radius:5px; display:inline-block; height:35px;  color:white; font-size: 14px; padding: 6px 15px; cursor:pointer; outline: none"> '.$lang->buy_Ws.' </button>
												</div>
											</div>
										</td>										
									</tr>
									';
									};
									$g .= $tbl.'
									</table>	 
								</div>
							</td> 
						</tr>';
						$message .= $g;
					} else // Show shopping button, all workshops are bought
					{
					$message .= '
					<tr>
						<td style="text-align: center">
							<div style="margin-top: 10px;">
								<a href="'.$FILE.($t->lang==="EN" ? "" : strtolower($t->lang)."/").'workshops">
								<button style="background-color:#131A6E; border: 2px solid white; border-radius:5px; display:inline-block; height:40px; margin: 0 auto; color:white; font-size: 16px; padding: 6px 25px; cursor:pointer; outline: none"> '.$lang->watch_ws.' </button>
								</a>
							</div>
						</td> 
					</tr>
					';
					}
					$message .= '
					</table>
				</div>
				
				<div>
					<table style="width:100%; text-align:left; padding: 0 20px; margin-bottom:20px; color:#131A6E;">
						<tr>
							<td>
								<div>
									'.$lang->thanks_trust.'.
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div>
									'.$lang->sincerely.', <span style="font-weight: bold"> Handmade Fantasy World </span>
								</div>
							</td>
						</tr>
					</table>
				</div>
				
				<div style="background-color:#F2F2F2; width:100%;">
							<hr style="color:#131A6E;">
								<table style="width:100%; text-align:center;  font-size:70%">
									<tr>
										<td>
											<p style="margin:0;"> Copyright © Handmade Fantasy World Ltd, All rights reserved. </p>
											<p style="margin:0;"> <a href="http://handmadefantasyworld.com" target="_blank"> handmadefantasyworld.com </a> </p>		
										</td>
									</tr>
								</table>
				</div>
				
			</div>
		</body>
		</html>';
		echo $message;
	}
	


?>