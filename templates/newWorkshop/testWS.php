<?php 
	
	$prepath = "";
	include "../../functions.php";
	include "../../connect.php";
	include "../../global.php";
	include "../../getDATA.php";
	$domain = "http://localhost/hfw/";


	//$PAYMENT_INFO = (object) array("paymentID" => $_GET['pid']);
	$SENDER = (object) array("email" => "", "username" => "lidox20", "currencyID" => "RSD");

	$SOCIAL   = (object) array("facebook" => "https://www.facebook.com/handmadefantasybyantony/","twitter" => "", "instagram" => "https://www.instagram.com/antonistzanidakis/");
			
			
	$pom = (object) array("currencyID" => "RSD", "lang" => "EN", "email" => "luka.orlandic77@gmail.com", "username" => "lidox20");	

	$sql_main = mysql_query("SELECT workshopID, heading, subheading, date_publish, date_end,image,price, priceRSD, forsale FROM (SELECT `wsh`.*, CONCAT(`images`.imageID,'.',`images`.`extension`) AS image, `images`.`im_index` FROM (SELECT `workshops`.`workshopID` , `workshops`.`heading_".$pom->lang."` AS heading ,`workshops`.`price_".$pom->currencyID."` AS price, `workshops`.`price_RSD` AS priceRSD ,  `workshops`.`subheading_".$pom->lang."` AS subheading , `workshops`.`date_publish`, `workshops`.forsale , `workshops`.`date_end` FROM workshops WHERE `workshops`.active=1 AND BINARY workshopID = 'tjOmPXxdBvH2iq48B1Sw' AND (`workshops`.`date_end` = '0000-00-00' OR (CURDATE() <= `workshops`.`date_end`))) wsh LEFT OUTER JOIN images ON `images`.`workshopID` = `wsh`.`workshopID` ORDER BY im_index ASC LIMIT 3) tbl");

			$i=0; $workshop = array();
			while($sql_pom = mysql_fetch_object($sql_main)) $workshop[$i++] = $sql_pom;

				$LANGUAGE = return_lang(strtolower($pom->lang),"");

				$to = $pom->email;
				$subject = " - ".$workshop[0]->heading." - Handmade Fantasy World";
				$message = '
				<!DOCTYPE html>
				<html>
				<head>
					<meta charset="utf-8">
					<meta name="viewport" content="width=device-width">
				</head>

				<body style="background: #FAFCFC; padding: 0; margin: 0; font-family: Arial; font-size: 100%;">
					<div style="width: 80%; margin: 0 auto; height:100%; background: white; border: 1px solid #DBDBDB; margin-top: 50px; border-radius: 3px">
					
						<div style="padding: 30px 20px; color:#131A6E; text-align:center;">
							<table style="width:100%;">
								<tr>
									<th>
										<img src="'.$FILE.'img/logo.png" style="width:300px;">
									</th>
								</tr>
								<tr>
									<td style="text-align: center">
										<img src="'.$FILE.'img/email_logo.png" style="max-width: 80%, width: 400px" >
									</td>
								</tr>								
							</table>	
						</div>
						
						<hr style="color:#131A6E;">
						
						<div style="width:100%; color:#26B5B5; padding: 15px 0 25px 0">
							<table style="width:100%;">
								<tr>
									<th>
										<h1 style="margin:10px 0 0 0;">
											 '.$workshop[0]->heading.' 
										</h1>
									</th>
								</tr>																
								<tr>
									<td>
										<div style="text-align: center; padding-bottom: 20px; color:#3D3F40">
											'.$LANGUAGE->new_workshop.', '.$LANGUAGE->e_ws_start_today.'
										</div>
									</td>
								</tr>								
							</table>
						</div>
						
						<div style="width:90%; margin: auto">
							<table style="width:100%; border-collapse: collapse">
								<tr style="vertical-align: top">
									<td style="width: 40%; padding: 10px; border: 2px solid #E0E0E0; border-radius: 3px">
										<a href="'.$domain.'workshop/'.$workshop[0]->workshopID.'" target="_blank">
											<img src="'.$FILE.'img/content/'.$workshop[0]->image.'" style=" width: 100%; max-height:600px;">
										</a>
									</td>
									<td style="padding-left: 25px; padding-top: 10px">
										<table>
											<tr>
												<td>
													<span style="color: #1FADAD; font-weight: bold; font-size: 20px">'.$LANGUAGE->workshop_description.'</span>
												</td>
											</tr>
											<tr>
												<td>
													<div style="padding-top: 10px; font-size: 15px">'.$workshop[0]->subheading.'</div>
												</td>
											</tr>
											<tr>
												<td>
													<div style="padding-top: 10px;">
														<a href="'.$domain.'workshop/'.$workshop[0]->workshopID.'" target="_blank" style="color: #000000">
															<div style="padding: 17px 30px; background: url('.$FILE.'img/burnt-paper.png); background-size: 100% 100%; border-radius: 2px; display: inline-block; font-weight: bold;">'.$LANGUAGE->lookOnOurWebsite.'</div>
														</a>
													</div>
												</td>
											</tr>
										</table>										
									</td>
								</tr>
							</table>
						</div>					
						<div style="background-color:#F2F2F2; margin-top: 35px; width:100%;">
						<hr style="color:#131A6E;">
							<table style="width:100%" align="center">
								<tr>
									<th>
										<div style="text-align:center; margin-top:5px;">
											<a href="'.$SOCIAL->facebook.'" target="blank"><img src="'.$FILE.'img/facebook.png" style="width:30px; margin-right: 5px;"></a>
											<a href="'.$SOCIAL->instagram.'" target="blank"><img src="'.$FILE.'img/instagram.png" style="width:30px; margin-right: 5px;"></a>												
										</div>
									</th>
								</tr>
							</table>
							
							<table style="width:100%; text-align:center;  font-size:70%; padding-bottom:25px">
								<tr>
									<td>
										<p style="margin:0;"> Copyright © Handmade Fantasy World Ltd, All rights reserved. </p>
										<p style="margin:0;"> <a href="http://handmadefantasyworld.com" target="_blank"> handmadefantasyworld.com </a> </p>		

										<br>
										<p style="margin:0;"> '.$LANGUAGE->unsubscribe.' </p>
										<p style="margin:0;"> '.$LANGUAGE->you_can.' <a href="'.$FILE.'user/unsubscribe?username='.$pom->username.'&email='.$pom->email.'" target="blank">'.$LANGUAGE->unsubscribe_lst.'</a> </p>
									</td>
								</tr>
							</table>
						</div>
						
					</div>
				</body>
				</html>
			';

			echo $message;

?>