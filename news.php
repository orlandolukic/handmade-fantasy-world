<?php 
	session_start();
	$prepath = '';
	$ft_mg   = '';
	include $prepath."functions.php";
	include $prepath."connect.php";
	include "global.php";
	include "getDATA.php";
	include $prepath."pages.php";
	include $prepath."lang/func.php";
	include $prepath."lang/write_".strtolower($lang_acr).".php";

	$sql = mysql_query("SELECT `news`.newsID, `news`.`heading_".$lang_acr."` AS heading, `news`.`text_".$lang_acr."` AS text, `news`.date_publish, `news`.valid_to, `news`.highlight, `news`.highlight_from, `news`.highlight_to, `news`.print_event_date, `news`.event_date_start, `news`.event_date_end, `news`.event_location, MONTH(`news`.event_date_start) AS smonth, MONTH(`news`.event_date_end) AS emonth, DAY(`news`.event_date_start) AS sday, DAY(`news`.event_date_end) AS eday, `news`.event_image, `news`.`event_link` FROM news WHERE active = 1 AND (`news`.valid_to='0000-00-00' OR (`news`.valid_to != '0000-00-00' AND `news`.valid_to >= CURDATE())) ORDER BY `news`.highlight DESC, `news`.highlight_from DESC,`news`.date_publish DESC LIMIT 10", DBC_STORE);
	$i = 0; $NEWS = array();
	while ($t = mysql_fetch_object($sql)) $NEWS[$i++] = $t;

?>
<!DOCTYPE html>
<html>
<head>
	<?php print_HTML_data("head","news") ?>
</head>
<body class="<?= $bodyClass ?>">

	<?php echo $mobileMenu; $mobileMenu = NULL; ?>

	<!-- Fixed Menu -->
	<?php printMainMenu(1,5); ?>

	<!-- Header container -->
	<div class="header-container md-dn">
		<?php echo $headerInfo; $headerInfo = NULL; ?>
		<?php printMainMenu(0,6); ?>		
	</div>
	<!-- /Header container -->

	<!-- Main Container -->
	<div class="main-container margin-md-t-60">
		<div class="welcome-note welcome-note-minified">
			<div class="overlay"></div>
			<div id="select_div" class="welcome-optimised-background welcome-optimised-background-news"></div>
		</div>
	</div>
	<!-- /Main Container -->

	<div class="container margin-t-80 margin-b-80">
		<div class="row">
			<div class="col-md-8 col-xs-12 col-sm-12">
				<?php for ($i=0; $i<count($NEWS); $i++) {
				  $highlighted = $NEWS[$i]->highlight && 							     
							     $NEWS[$i]->highlight_from !== "0000-00-00" && 
							     strtotime($NEWS[$i]->highlight_from) <= time() &&
							     (
							     	$NEWS[$i]->highlight_to !== "0000-00-00" && 
							    	strtotime($NEWS[$i]->highlight_to, time()) >= time()
							    	||
							    	$NEWS[$i]->highlight_to === "0000-00-00"
							     );
				?>
				<div class="news <?= ($i>0 ? "margin-t-40 " : "") ?><?= ($highlighted ? "highlighted" : "") ?>">
					<?php if ($highlighted) { ?><div class="news-highlight smaller uppercase strong"><?= $lang->topical ?></div><?php }; ?>					
					<h3 class="color-theme strong margin-t-10"><?= $NEWS[$i]->heading ?></h3>
					<div class="smaller">
						<?php if ($NEWS[$i]->print_event_date) { ?> 

							<?php if ($NEWS[$i]->event_date_start !== $NEWS[$i]->event_date_end) { ?>
								<?php if ($NEWS[$i]->smonth !== $NEWS[$i]->emonth) { ?>
								<i class="fa fa-calendar"></i> <?= make_date(-1, $NEWS[$i]->event_date_start); ?> - <?= make_date(-1, $NEWS[$i]->event_date_end); ?>								
								<?php } else { // Start and end months are the same ?>
								<i class="fa fa-calendar"></i> <?= $NEWS[$i]->sday ?> - <?= make_date(-1, $NEWS[$i]->event_date_end) ?>
								<?php }; ?>
							<?php } else { // Event date end is the same as event date start ?>
							<i class="fa fa-calendar"></i> <?= make_date(-1, $NEWS[$i]->event_date_start); ?>
							<?php }; 
							if ($NEWS[$i]->event_location) { // Print location if exists ?>
							,<span class="padding-l-5 strong"><i class="fa fa-map-marker"></i> <?= $NEWS[$i]->event_location ?></span>
							<?php }; ?>

						<?php } else { // Print news date ?>
							<i class="fa fa-calendar"></i>  <?= make_date(-1, $NEWS[$i]->date_publish); ?>
						<?php }; ?>						
					</div>
					<div class="margin-t-20 margin-b-10 <?= ($highlighted ? "margin-md-t-40" : "") ?>">
						<?php 
						// PRINT IMAGE, if exists
						if ($NEWS[$i]->event_image) { ?>
						<div class="pull-right news-image">
							<div class="image-optimised">
								<?php if ($NEWS[$i]->event_link) { ?>
								<a href="<?= $NEWS[$i]->event_link ?>" target="_blank">
								<?php }; ?>
								<img src="<?= $FILE ?>img/<?= $NEWS[$i]->event_image ?>">
								<?php if ($NEWS[$i]->event_link) { ?></a><?php }; ?>
							</div>
						</div>

						<?php }; ?>
						<?= $NEWS[$i]->text ?>						
					</div>
				</div>
				<?php }; ?>
				<?php if (!count($NEWS)) { ?>				
				<h4 class="strong color-theme"><i class="fa fa-info-circle"></i> <?= $lang->currNoNews ?></h4>
				<?php }; ?>
			</div>
			<div class="col-md-4 col-xs-12 col-sm-12 margin-md-t-30">
				<div class="popular-workshops">
					<div class="padding-10 uppercase padding-l-20 strong pw-heading"><?= $lang->popular_workshops ?></div>
					<?php if (!count($DATA_HEADER)) { ?>
					<div class="row">
						<div class="col-md-12 col-xs-12 col-sm-12">
							<div class="padding-l-20 padding-r-10 padding-t-10 padding-b-10 smaller"><?= $lang->currNoPopWS ?></div>
						</div>
					</div>
					<?php } else { ?>
					<?php for ($i=0; $i<count($DATA_HEADER); $i++) { ?>
					<div class="row">
						<div class="col-md-6 col-xs-12 col-sm-12">
							<div class="image-optimised">
								<a href="<?= $domain ?>workshop/<?= $DATA_HEADER[$i]->workshopID ?>"><img src="<?= make_image_content($DATA_HEADER[$i]->image, $FILE); ?>"></a>
							</div>
						</div>
						<div class="col-md-6 col-xs-12 col-sm-12 padding-l-none padding-md-l-30 margin-md-t-10">							
							<div class="workshop-placeholder">
									<div class="small padding-t-10 strong"><a class="link-ord" href="<?= $domain ?>workshop/<?= $DATA_HEADER[$i]->workshopID ?>"><?= $DATA_HEADER[$i]->heading ?></a></div>
								<div class="padding-t-5 padding-b-10">
									<table class="color-theme" style="margin: auto">
										<tr>
											<td><i class="fa fa-2x fa-eye"></i></td>
											<td><i class="fa fa-2x fa-comments"></i></td>											
										</tr>
										<tr class="text-center strong">
											<td><?= $DATA_HEADER[$i]->views ?></td>
											<td><?= $DATA_HEADER[$i]->reviews ?></td>											
										</tr>
									</table>
								</div>
							</div>							
						</div>
					</div>
					<?php if ($i<count($DATA_HEADER)-1) { ?><div class="divider"></div><?php }; ?>
					<?php }; ?>
					<?php }; // if statement ?>
				</div>
			</div>
		</div>
	</div>


	<?php echo $footer; $footer = NULL; ?>
	<?php echo $upBtn; $upBtn = NULL; ?>

	<?php print_HTML_data("script","news") ?>

</body>
</html>