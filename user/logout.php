<?php 
//	Logout page
	session_start();
	include "../functions.php";
	include "../global.php";
	$REDIRECTION   = $domain;
	$lang          = $_SESSION['lang'];
	$hasRedrection = false;
	if (isset($_GET['type']))
	{
		switch($_GET['type'])
		{
		case "access_token":
			if (!isset($_GET['redirect'])) break;
			$hasRedrection = true;
			$REDIRECTION = urldecode($_GET['redirect']);			
			break;
		case "session_expired":
			if (!isset($_GET['redirect'])) break;
			$hasRedrection = true;
			$REDIRECTION = urldecode($_GET['redirect']);	
			break;
		case "force_logout":
			if (!isset($_GET['redirect'])) break;
			$hasRedrection = true;
			$REDIRECTION = urldecode($_GET['redirect']);	
			break;
		}
	};

	// Destroy all session data
	session_destroy();
	session_start();

	if (isset($_GET['session_name']) && trim($_GET['session_name'])!=="")
	{
		// Initiate session asignment option
		$_SESSION[$_GET['session_name']] = isset($_GET['session_value']) && trim($_GET['session_value'])!=="" ? $_GET['session_value'] : "";
	};

	$_SESSION['token'] = "FGlgtoIzTpfPkyi2UibB";
	$_SESSION['lang']  = $lang;
	header("location: ".$REDIRECTION);

?>