

$(document).ready(function(e) {
	$(".add-to-cart").on("click", function(e) {
		var obj = $(this);
		obj.find("i")[0].className = "fa fa-circle-o-notch fa-spin";
		_SD_DB("cdws", {type: btoa("addWSToShoppingCart"), data: JSON.stringify({wid: obj.parents(".ws-dashboard-preview-placeholder").attr("data-wid")})}, function(v) {
			v = JSON.parse(v);
			if (v.info.response)
			{
				obj.parents(".ws-actions").find(".ws-ajax-loader").addClass("dn");				
				if (v.data.action === "add")
				{
					obj.find("span").html(v.data.d.in_cart);
					obj.find("i")[0].className = "fa fa-check";
				} else
				{
					obj.find("span").html(v.data.d.add_to_cart);
					obj.find("i")[0].className = "fa fa-shopping-cart";
				}	
				update_cart_items_sidebar(v.data.cart_items);
			}
		}, function(x) {}, true);
	});
	_SD_DB("cdws", {type: btoa("checkWorkshopsUserSection"), data: JSON.stringify({})}, function(v) {
		v = JSON.parse(v);
		if (v.info.response)
		{	
			if (!v.info.priviledged)
			{
				var el, btn;

				// Work with workshops in cart
				for (var i=0, b = v.data.ws_cart; i<b.length; i++)
				{
					var parent = $(".ws-dashboard-preview-placeholder[data-wid='"+b[i].workshopID+"'] .ws-actions");					
					if (!parent.length) continue;
					btn = parent.find(".btn-theme");
					btn.find("i")[0].className = "fa fa-check";
					btn.find("span").html(v.data.d.in_cart);
				};

				// Print status for workshops
				for (var i=0, b = v.data.ws_status; i<b.length; i++)
				{					
					var parent = $(".ws-dashboard-preview-placeholder[data-wid='"+b[i].workshopID+"'] .ws-actions");	
					if (!parent.length) continue;	
					el = parent.find(".ws-action-btn");
					el.find("a").attr("href", b[i].url);				
					el.find(".btn-theme").removeClass("add-to-cart").off("click").attr("data-set", true);
					el.find(".btn-theme i")[0].className = "fa fa-play";
					el.find(".btn-theme span").html(v.data.d.watch_video);
				}				
				$(".ws-dashboard-preview-placeholder .ws-actions .ws-action-btn").removeClass("v-dn");
				$(".ws-dashboard-preview-placeholder .ws-actions .ws-ajax-loader").addClass("dn");				
			}
		} else
		{
			if (v.info.redirect) window.location.href = v.info.redirect_url;
		}
	}, function(x) {}, true);
})