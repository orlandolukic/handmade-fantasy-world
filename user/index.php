<?php 
	session_start();
	$prepath  = '../';
	$REDIRECT = false;

	include $prepath."requests/userManagementCheck.php";
	include $prepath."functions.php";
	include $prepath."connect.php";
	include $prepath."global.php";
	include $prepath."getDATA.php";
	include $prepath."requests/cart_management.php";

//	Prep for include
	if (!isset($_SESSION['self']) || (isset($_SESSION['self']) && $_SESSION['self']===false))
	$ACTIONS  = (object) array("redirect"    => true, 
	                           "execute"     => true, 
	                           "valid"       => false,	                           
	                           "redirection" => (object) array("SQL_error"     => $FILE,
	                                                           "GET_error"     => "",
	                                                           "urlToPostData" => true,
	                                                           "error_params"  => $FILE."user/",
	                                                           "success_req"   => $FILE."user/",
	                                                           "SQL_exsist"    => $FILE."user/" 
	                                                           ));

	include $prepath."pages.php";
	include $prepath."lang/func.php";
	include $prepath."lang/user_".strtolower($lang_acr).".php";
	include $prepath."requests/userManagement.php";
	if (!isset($_SESSION['self']) || isset($_SESSION['self']) && $_SESSION['self']===false) include $prepath."requests/actions.php";

	$SUBSCRIPTION = (object) array();
	$sql = mysql_query("SELECT `subscriptions`.*, `packages`.packageID, `packages`.text_".$USER->lang." AS name, `packages`.flag, `packages`.price_".$USER->currencyID." FROM subscriptions INNER JOIN `packages` ON type=`packages`.packageID WHERE username='".$USER->username."' AND CURDATE()<=date_end AND CURDATE()>=date_start AND `subscriptions`.active=1 ORDER BY date_end DESC LIMIT 1", DBC_STORE);
	if (mysql_num_rows($sql)) 
	{ 
		$SUBSCRIPTION->found = true; 
		while($t = mysql_fetch_object($sql)) $SUBSCRIPTION->data = $t;
	} else $SUBSCRIPTION->found = false;

//	Show sucess payment
	if (isset($_SESSION["show_succ_payment"]))
	{
		$showSuccPayment = true;
		$showSuccPaymentObj = $_SESSION['show_succ_payment'];
	} else $showSuccPayment = false;

	if (isset($_SESSION['redirection']))
	{
		$sql = mysql_query("SELECT heading_".$USER->lang." AS heading, workshopID FROM workshops WHERE workshopID = '".$_SESSION['redirection']."'", DBC_STORE);
		while($t = mysql_fetch_object($sql)) $wsh_redirection = $t;
		$sh_redirection = true;
		unset($_SESSION['redirection']);
		unset($_SESSION['self']);
	} else $sh_redirection = false;	

	$sqlINACTIVE_SUB = mysql_query("SELECT `tbl`.* , `packages`.`name_".$USER->lang."` AS name FROM (SELECT `subscriptions`.* FROM `subscriptions` WHERE BINARY username='".$USER->username."' AND user_deact=1 AND active=0) tbl INNER JOIN `packages` ON `tbl`.`type` = `packages`.`packageID` ORDER BY tbl.date_end DESC LIMIT 1", DBC_STORE);

//	Get workshops for display
	$sql = mysql_query("SELECT `tbl4`.*, `narrators`.n_name_".$USER->lang." AS narrator_name FROM (SELECT `tbl3`.*, COALESCE(SUM(`reviews`.rating) / COUNT(`reviews`.workshopID),0) AS rating, COALESCE(SUM(`reviews`.workshopID), 0) AS comments FROM (SELECT `tbl2`.*, COALESCE(COUNT(`likes`.workshopID)) AS likes FROM (SELECT `tbl1`.*, CONCAT(`images`.imageID,'.',`images`.extension) AS image FROM (SELECT workshopID, languageID, narratorID, heading_".$USER->lang." AS heading, subheading_".$USER->lang." AS subheading, date_publish, views, f_index, price_RSD AS priceRSD, price_".$USER->currencyID." AS price FROM workshops WHERE active = 1 AND date_publish<=CURDATE()) tbl1 INNER JOIN images ON `images`.im_index = `tbl1`.f_index AND `images`.workshopID = `tbl1`.workshopID) tbl2 LEFT OUTER JOIN likes ON `likes`.workshopID = `tbl2`.workshopID GROUP BY `tbl2`.workshopID) tbl3 LEFT OUTER JOIN `reviews` ON `reviews`.workshopID = `tbl3`.workshopID GROUP BY `tbl3`.workshopID) tbl4 LEFT OUTER JOIN `narrators` ON `narrators`.narratorID = `tbl4`.narratorID ORDER BY date_publish DESC, rating DESC LIMIT 8", DBC_STORE);
	$TW = array(); $y=0;
	while($t = mysql_fetch_object($sql)) $TW[$y++] = $t;	

	$months = array();
	for($i = 0;$i<12;$i++) {
		$sql = mysql_query("SELECT * FROM cart WHERE BINARY start_month= '".($i+1)."' AND username='".$USER->username."'", DBC_STORE);
		$months[$i] = mysql_num_rows($sql);
	}

	//	Check if user is subscribed for month(s)
	include $prepath."requests/det_subscription_months.php";

	// TEMP, IP address
	$sql = mysql_query("SELECT * FROM `ipaddress` WHERE `ipaddress`.country_code = 'GR' AND BINARY username = '".$USER->username."'", DBC_STORE);
	$temp_show = mysql_num_rows($sql) > 0 ? true : false;

	// TEMP, IP address
	$sql = mysql_query("SELECT * FROM `ipaddress` WHERE `ipaddress`.country_code = 'RS' AND BINARY username = '".$USER->username."'", DBC_STORE);
	$temp_show1 = mysql_num_rows($sql) > 0 ? true : false;



?>

<!DOCTYPE html>
<html>
<head>
<?php print_HTML_data("head","user/index") ?>
</head>

<body class="<?= $bodyClass ?>">

	<?php printTopMenu(); ?>		
	<?php printMainMenu(); ?>
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main margin-b-60">			
		<?php printNavigation(); ?>
		
		<div class="row">
			<div class="col-lg-12 margin-t-20">
				<h1 class="page-header heading" style="margin: 0"><?php echo $lang->user_pannel ?></h1>
			</div>
		</div><!--/.row-->

		<?php if ($temp_show && $USER->lang!=="GR" && $USER->lang!=="SR") { ?>
		<div class="row">
			<div class="col-md-6 col-xs-12 col-sm-12 margin-b-10">
				<div class="panel">
					<div class="panel-body">
						<h4 class="strong" style="margin: 0"><i class="fa fa-language"></i> <?= $lang->areYouFromGreece ?> </h4>
						<div class="small">
							<?= $lang->greece_welcome ?>
						</div>
						<div class="margin-t-10 ">
							<?=  $lang->greece_goto ?> <a class="link-ord" href="<?= $FILE ?>user/settings/"><?=  $lang->greece_settings ?></a> <?=  $lang->greece_change ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php }; 
		if ($temp_show1 && $USER->lang!=="GR" && $USER->lang!=="SR") 
		{ ?>
		<div class="row">
			<div class="col-md-6 col-xs-12 col-sm-12 margin-b-10">
				<div class="panel">
					<div class="panel-body">
						<h4 class="strong" style="margin: 0"><i class="fa fa-language"></i> <?= $lang->areYouFromSerbia ?> </h4>
						<div class="small">
							<?= $lang->serbian_welcome ?>
						</div>
						<div class="margin-t-10 ">
							<?=  $lang->serbian_goto ?> <a class="link-ord" href="<?= $FILE ?>user/settings/"><?=  $lang->serbian_settings ?></a> <?=  $lang->serbian_change ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php }; ?>

		<?php if ($sh_redirection) { ?>
		<div class="row">
			<div class="col-md-6 col-xs-12 col-sm-12">
				<div class="panel bg-info">
					<div class="panel-body">
						<div class="strong" style="font-size: 130%"><i class="fa fa-info-circle"></i> <?php echo $lang->notification ?></div>
						<div class="margin-t-10"><?php echo $lang->have_alreadyBought ?> <span class="strong"><?= $wsh_redirection->heading ?></span> <?= mb_strtolower($lang->workshop_u) ?>.</div>
						<div class="margin-t-10">
							<a href="<?= $FILE ?>user/video/<?= $wsh_redirection->workshopID ?>">
								<div class="btn-white btn-ord bordered small"><i class="fa fa-video-camera"></i> <?php echo $lang->check_workshop ?></div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php }; ?>

		<?php 
		// Inactive subscription
		if (0) { ?>
		<div id="inactiveSubscription" class="row">
			<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
				<div class="panel">
					<div class="panel-heading bg-warning" style="padding-left: 10px; padding-top: 10px; font-weight: bold;">
						<span class="padding-l-10"><i class="fa fa-refresh"></i> <?php echo $lang->inactive_payment ?></span>
					</div>
					<div class="panel-body">
						<div class="padding-b-20">
							<?php echo $lang->payment_whichis ?> <span class="strong"><?php echo $lang->currently ?></span> <?php echo $lang->inactive ?>. 
							<div class="margin-t-10">
								<?php while($pom = mysql_fetch_object($sqlINACTIVE_SUB)) $inactiveSub = $pom; ?>
								 <div class="row">
								 	<div class="col-md-3 col-xs-6 col-sm-6 smaller uppercase"><?php echo $lang->name ?></div>
								 	<div class="md-db dn col-xs-6 col-sm-6 op-4"><?= $inactiveSub->name ?></div>
								 	<div class="col-md-4 col-xs-6 col-sm-6 smaller uppercase"><?php echo $lang->date_start ?></div>
								 	<div class="md-db dn col-xs-6 col-sm-6 op-4"><?= make_date(-1, $inactiveSub->date_start) ?></div>
								 	<div class="col-md-3 col-xs-6 col-sm-6 smaller uppercase"><?php echo $lang->date_expire ?></div>
								 	<div class="md-db dn col-xs-6 col-sm-6 strong"><?= make_date(-1,$inactiveSub->date_end) ?></div>
								 	<div class="col-md-2 col-xs-6 col-sm-6 smaller uppercase text-error">
								 		<span class="md-dn"><i class="fa fa-arrow-down"></i></span>
								 	</div>
								 	<div class="md-db dn col-xs-6 col-sm-6 text-error">
								 		<span class="md-inlb"><i class="fa fa-arrow-down"></i></span>
								 		<?php 
								 			$dts = strtotime($inactiveSub->date_start);
								 			$dte = strtotime($inactiveSub->date_end);
								 			echo ($dte-$dts)/(60*60*24);
								 		?>								 		
								 	</div>								 	
								 </div>
								 
								 <div class="row md-dn">
									 <div class="divider"></div>
									 <div class="col-md-3 col-xs-12 col-sm-12 strong op-4">
									 	<?= $inactiveSub->name ?>
									 </div>
								 	<div class="col-md-4 col-xs-12 col-sm-12 small op-4"><?= make_date(-1, $inactiveSub->date_start) ?></div>
								 	<div class="col-md-3 col-xs-12 col-sm-12 strong"><?= make_date(-1,$inactiveSub->date_end) ?></div>
								 	<div class="col-md-2 col-xs-12 col-sm-12 strong text-error"><?= ($dte-$dts)/(60*60*24) ?></div>
								 </div>
						
							</div>
						</div>						
					</div>
					<div class="panel-footer padding-t-none text-right">											
						<a id="actSub" data-subscr-id="<?= $inactiveSub->subscriptionID ?>" href="javascript: void(0)">
							<div class="btn-green btn-ord small margin-t-10">
								<i class="fa fa-check-square"></i> <span class="padding-l-5"><?php echo $lang->activate_sub ?></span>
							</div>
						</a>						
					</div>
				</div>
			</div>
		</div>
		 <?php }; ?> 

		<?php if ($showSuccPayment) { 
			$sql = mysql_query("SELECT * FROM payments WHERE paymentID = '".$showSuccPaymentObj->paymentID."' AND username = '".$USER->username."' LIMIT 1", DBC_STORE);
			$res = mysql_fetch_object($sql);
		?>
		<div class="panel bg-default">
			<div class="panel-heading">
				<span class="strong color-theme">
				<?php if ($res->payment_method == 2) { ?>
					<i class="fa fa-paypal"></i> <?php echo $lang->payment_successfull ?>
				<?php } elseif ($res->payment_method == 1) { ?>
					<i class="fa fa-credit-card"></i> <?php echo $lang->payment_successfull ?>
				<?php }; ?>
				</span>
			</div>
			<div class="panel-body">
				<?php echo $lang->cart_emptyCanProceed ?>
				<div class="margin-t-20">
					<div class=""><i class="fa fa-file"></i> <?php echo $lang->payment_details ?></div>
					<div class="padding-t-10">
						<table>
							<tr>
								<td width="120" class="uppercase small"><?php echo $lang->payment_ID ?></td>
								<td class="strong"><?= $res->paymentID ?></td>
							</tr>
							<tr>
								<td width="120" class="uppercase small"><?php echo $lang->invoice?></td>
								<td class="strong">IF-<?= substr($showSuccPaymentObj->paymentID,4,10) ?></td>
							</tr>
							<tr>
								<td width="120" class="uppercase small"><?php echo $lang->just_date ?></td>
								<td class="strong"><?= make_date(-1,$res->trans_date) ?></td>
							</tr>
							<tr>
								<td width="120" class="uppercase small"><?php echo $lang->ammount ?></td>
								<td class="strong"><?= print_money_PLAINTXT($res->payment_amount,2) ?></td>
							</tr>
							<tr>
								<td width="120" class="uppercase small"><?php echo $lang->currency ?></td>
								<td class="strong"><?= $res->trans_currencyID ?></td>
							</tr>
						</table>
					</div>
					<div class="margin-t-10">
						<a class="link-ord" href="<?= $FILE ?>user/receipt/<?= $res->paymentID ?>">
							<div class="btn-green btn-ord small"><i class="fa fa-bars"></i> <?php echo $lang->show_invoice ?></div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<?php }; unset($_SESSION['show_succ_payment']); ?>

		<div class="small margin-md-t-30">
			<?= $lang->welcome_To ?> <span class="strong"><?= $lang->user_Section ?></span> <?= $lang->site_a ?> handmadefantasyworld.com.
		</div>

		<?php if ($USER->grant_access) { ?>
	
			<div class="row">
				<div class="col-md-4 col-xs-12 col-sm-12 margin-t-20">
					<div class="panel bg-success">
						<div class="panel-body" style="padding: 7px 15px">
							<span style="color: white; margin: 0" class="strong"><i class="fa fa-check"></i> <?= $lang->accessAllVideos ?></span>
						</div>
					</div>
				</div>
			</div>

		<?php } else { ?>

		<div class="row">
			<div class="col-md-12 col-xs-12 col-sm-12">				
				<?php
				$PRICE_SECTION = array("printOffset" => false, "printCheckIcon" => false, "cart" => true);
				include "../code/price_section.php"; ?>
				<!-- 
				<div class="panel bg-default">
					<div class="panel-heading" style="line-height: inherit;">
						<div class="strong" style="font-size: 120%"><i class="fa fa-shopping-cart"></i> <?= $lang->subscribe_now ?> sd</div>
						<div class="smaller padding-l-30"><?= $lang->chooseMSUB ?></div>
						<div class="divider width-100"></div>
					</div>
					<div class="panel-body container-fluid">
						<?php for ($i=0, $n=0, $go = true; $i<$endMonth; $i++, $n++) { ?>	
						<?php if ($n%5===0 && $go) { $n=1; $go = false; ?><div class="row margin-t-20 margin-md-t-none margin-md-b-10 margin-b-40"><?php }; ?>
						<div class="col-md-2 col-lg-2 col-xs-6 col-sm-6 <?= ($i<$startMonth-1 ? "subscription-disabled" : "") ?> text-ceneter margin-md-t-20">
					
							<div class="strong"><?= $lang->c_months[$i]; ?><?= ($SUBSCRIPTION_MONTHS[$i] ? '<i class="fa fa-check-circle color-success relative" style="left: 3px"></i>' : '') ?></div>
							<?php if ($SUBSCRIPTION_MONTHS[$i]) {  ?>
							<div class="color-success strong smaller uppercase"><?= $lang->subscribed ?></div>
							<?php }; ?>				

							<?php if ($SUBSCRIPTION_MONTHS[$i]) { ?>
							<a href="<?= $FILE ?>user/workshops?month=<?= ($i+1) ?>" class="link-ord margin-t-10 small" style="display: inline-block;">			
								<i class="fa fa-video-camera"></i> <?= $lang->videos ?>			
							</a>
						
							<a href="<?= $FILE ?>user/receipt/<?= $SUBSCRIPTION_MONTHS[$i][1] ?>" class="link-ord padding-l-10 small padding-md-l-none" style="display: inline-block;">
								<i class="fa fa-file smaller"></i> <?= $lang->invoice ?>				
							</a>
							<?php } elseif (!$SUBSCRIPTION_MONTHS[$i] && $i>=$startMonth-1) { ?>

							<a class="subscribe-btn <?= ( $months[$i]? "disabled" : "") ?>" href="javascript: void(0)" data-success-message="<?= $lang->succAddedInCart ?>" data-fail-message="<?= $lang->failAddedCart ?>" data-alreadyCart="<?= $lang->inCart ?>" data-toggle="tooltip" data-placement="bottom" title=" <?= ( $months[$i] ? $lang->inCart  : $lang->add_toCart) ?>" data-start-month = "<?= $i+1 ?>"; style="display: inline-block;">				
								<div class="margin-t-10 <?= ( $months[$i]? "disabled" : "") ?> btn-find btn-green btn-ord small"><i class="fa fa-shopping-cart"></i> <span><?= ($months[$i] ? $lang->inCart : $lang->toCart) ?></span></div>						
							</a>
							<?php }; ?>
						</div>
						<?php if ($n%6===0) { $go = true; $n=-1; ?></div><?php }; ?>					
						<?php }; ?>
					</div>
				</div>
				-->
			</div>
		</div>

		<?php }; ?>

		<div class="row margin-t-30">
			<!-- 
			<div class="col-md-4 col-xs-12 col-sm-12">
				<div class="panel bg-default">
					<div class="panel-heading" style="line-height: 18px; padding-top: 15px;">
						<div class="strong"><i class="fa fa-globe"></i> <?= $lang->news_user ?></div>
						<div class="small"><?= $lang->check_News ?></div>
					</div>
				</div>
			</div>
			-->
			<div class="col-md-8 col-md-push-2 col-xs-12 col-sm-12">
				<div class="panel bg-default">
					<div class="panel-heading" style="line-height: 18px; padding-top: 15px;">
						<div class="fl-right">
							<a href="<?= $domain ?>workshops">
								<div class="btn-theme btn-ord" style="font-size: 70%; padding: 5px 15px"><i class="fa fa-search"></i> <?= $lang->explore_wS ?></div>
							</a>
						</div>
						<div class="strong"><i class="fa fa-video-camera"></i> <?= $lang->online_workshops_sm ?></div>
						<div class="small"><?= $lang->newest_Ws ?></div>						
					</div>
					<div class="divider width-100 margin-md-t-20" style="margin: 0"></div>
					<div class="panel-body" style="padding: 0">
						<?php for ($i=0; $i<count($TW); $i++) { ?>
							<div class="ws-dashboard-preview-placeholder container-fluid" data-wid="<?= $TW[$i]->workshopID ?>">
								<div class="col-md-8 col-xs-12 col-sm-12" style="padding: 0">
									<div class="image-placeholder">
										<a href="<?= $domain ?>workshop/<?= $TW[$i]->workshopID ?>">
											<img src="<?= $FILE ?>img/content/<?= $TW[$i]->image ?>">
										</a>
									</div>
									<div class="strong">
										<a href="<?= $domain ?>workshop/<?= $TW[$i]->workshopID ?>" class="link-ord">
											<span>
												<?php $v = false; $dat = strtotime($TW[$i]->date_publish); $dat += 7*60*60*24; if ($dat >= time()) { $v = true; ?>
												<span>
													<label class="label label-success"><?= $lang->new ?></label>
												</span>
												<?php }; ?>	
												<?= $TW[$i]->heading ?>																					
											</span>
										</a>
									</div>
									<div class="smaller <?= ($v ? "padding-t-5" : "") ?>">
										<span><i class="fa fa-calendar"></i> <?= make_date(-1,$TW[$i]->date_publish); ?></span>
										<span class="padding-l-10 star-color">
											<?php $g = determine_rating($TW[$i]->rating); ?>
											<?php for ($h=0; $h<count($g); $h++) { ?>
											<i class="fa <?= $g[$h]; ?>"></i>
											<?php }; ?>
										</span>
										<span class="padding-l-10">
											<i class="fa fa-thumbs-up"></i> <?= $TW[$i]->likes ?>
										</span>
										<span class="padding-l-10">
											<i class="fa fa-eye"></i> <?= $TW[$i]->views ?>
										</span>
										<span class="padding-l-10">
											<i class="fa fa-comments"></i> <?= $TW[$i]->comments ?>
										</span>
									</div>
									<div class="small">
										<span><i class="fa fa-globe"></i> <?= $lang->lang ?>: <span class="strong"><?= ($l = return_workshop_language($TW[$i]->languageID)) ?></span>
										<span class="smaller uppercase padding-l-10"><?= ($l !== $lang->L_english ? $lang->suppEngTitle : "") ?></span>
										</span>
										<div><i class="fa fa-paint-brush"></i> <?= $lang->ws_with ?> <span class="strong"><?= $TW[$i]->narrator_name ?></span></div>
									</div>
									<div class="smaller padding-t-5">
										<a href="https://www.facebook.com/sharer/sharer.php?u=<?= urlencode($domain."workshop/".$TW[$i]->workshopID) ?>" target="_blank">
										<span class="social fb" data-toggle="tooltip" data-placement="bottom" title="<?= $lang->share_ws_facebook ?>"><i class="fa fa-facebook-official fa-2x"></i></span>
										</a>
										<span class="padding-l-10">
											<a href="https://twitter.com/intent/tweet?text=NEW Handmade Fantasy World WORKSHOP ~ <?= urlencode($domain."workshop/".$TW[$i]->workshopID) ?>, come to enjoy" target="_blank">
											<span class="social twitter" data-toggle="tooltip" data-placement="bottom" title="<?= $lang->share_ws_Twitter ?>"><i class="fa fa-twitter fa-2x"></i></span>
											</a>						
										</span>			
									</div>									
								</div>
								<div class="col-md-4 col-xs-12 col-sm-12">
									<div class="text-right ws-actions">		
										<?php if (!$USER->grant_access) { ?>
										<div class="theme-loader ws-ajax-loader small fl-right"></div>
										<?php }; ?>							
										<?php if (!$USER->grant_access) { // Regular user ?>

										<?php if ($HAS_SUBSCRIPTION) { // USER HAS SUBSCRIPTION ?>

										<div class="ws-view-price"><?= ($USER->currencyID === "RSD" ? $PRICES->bojana["rsd"] : $PRICES->bojana["foreign"]) ?> <?= $USER->currencyID ?></div>

										<?php } else { // USER DOES NOT HAVE SUBSCRIPTION ?> 

										<div class="ws-view-price"><?= print_money_PLAINTXT($TW[$i]->price,2); ?> <?= $USER->currencyID ?></div>

										<?php }; ?>

										<div class="ws-action-btn v-dn margin-t-10" data-set="false">
											<a href="javascript: void(0)">
												<div class="btn-theme add-to-cart btn-ord" style="font-size: 80%; padding: 5px 15px"><i class="fa fa-shopping-cart"></i> <span><?= $lang->add_toCart ?></span></div>
											</a>
										</div>
										<?php } else { // Privileged user ?>
										<div class="ws-action-btn">
											<a href="<?= $FILE ?>user/video/<?= $TW[$i]->workshopID ?>">
												<div class="btn-theme btn-ord" style="font-size: 80%; padding: 5px 15px"><i class="fa fa-play"></i> <?= $lang->watch_video ?></div>
											</a>
										</div>
										<?php }; ?>
									</div>
								</div>
							</div>
						<?php }; ?>
					</div>
				</div>
			</div>			
		</div>
	</div>	<!--/.main-->
	

<?php print_HTML_data("script","user/index") ?>
</body>

</html>
