<?php 
	
	session_start();
//	Prep for include
	$prepath = '../';
	include $prepath."requests/userManagementCheck.php";
	$INCLUDE = (object) array("getDATA" => true);
	include $prepath."functions.php";
	include $prepath."connect.php";
	include $prepath."global.php";
	include $prepath."getDATA.php";
	include $prepath."pages.php";
	include $prepath."lang/func.php";
	include $prepath."lang/user_".strtolower($lang_acr).".php";
	include $prepath."requests/userManagement.php";
	include $prepath."requests/det_subscription_months.php";

	$pass = false;
	if (!$USER->grant_access)
	{
		$query_substring = "";
		for ($i=0, $ft = true, $h = 0; $i<12; $i++)
		{
			$h = $SUBSCRIPTION_MONTHS[$i] || $h;
			if ($SUBSCRIPTION_MONTHS[$i])
			{
				$query_substring .= (!$ft ? " OR " : "")." MONTH(date_publish) = '".($i+1)."'";
				if ($ft) $ft = false;
			};
		};
		if (!$h) $query_substring = "(0)";

		$FETCH_WORKSHOPS = array(); $j = 0;
		// Subscribed workshops
		$sql = mysql_query("SELECT workshopID FROM workshops WHERE (".$query_substring.") AND active = 1", DBC_STORE);
		while($t = mysql_fetch_object($sql)) $FETCH_WORKSHOPS[$j++] = $t;

		// Bought workshops
		$sql = mysql_query("SELECT workshopID FROM boughtworkshops INNER JOIN payments ON `boughtworkshops`.paymentID = `payments`.paymentID AND `payments`.paid = 1 WHERE BINARY `boughtworkshops`.username = '".$USER->username."'", DBC_STORE);
		while ($t = mysql_fetch_object($sql)) $FETCH_WORKSHOPS[$j++] = $t;

		// Coupon workshops
		$sql = mysql_query("SELECT workshopID FROM coupons WHERE BINARY username = '".$USER->username."' AND active = 1 AND expiration_date >= CURDATE()", DBC_STORE);
		while ($t = mysql_fetch_object($sql)) $FETCH_WORKSHOPS[$j++] = $t;

		$query_substring = "";
		$i = 0;
		for ($ft = true; $i<count($FETCH_WORKSHOPS); $i++)
		{
			$query_substring .= (!$ft ? " OR " : "")." BINARY workshopID = '".$FETCH_WORKSHOPS[$i]->workshopID."'";
			if ($ft) $ft = false;
		};
		if ($i==0) { $query_substring = "0"; $pass = false; } else { $pass = true; }

		// Make query for all workshops
		$sql = mysql_query("SELECT `tbl9`.*, `narrators`.n_name_".$USER->lang." AS narrator_name FROM (SELECT `tbl8`.*, COALESCE(COUNT(`likes`.workshopID),0) AS likes FROM (SELECT `tbl7`.* FROM (SELECT `tbl6`.* FROM (SELECT `outer_tbl`.*, `subscriptions`.subscriptionID, MONTH(`outer_tbl`.date_publish) AS w_month FROM (SELECT `wshrw`.*, CONCAT(`images`.`imageID`,'.',`images`.`extension`) AS image FROM (SELECT `wshr`.*, COUNT(`wishlist`.workshopID) AS wishlist FROM (SELECT `wsh`.*, COUNT(`reviews`.`workshopID`) AS reviews, COALESCE(SUM(`reviews`.`rating`) / COUNT(`reviews`.`workshopID`),0) AS rating FROM (SELECT workshopID, heading_".$USER->lang." AS heading, subheading_".$USER->lang." AS subheading, date_publish, date_end, forsale, sale, views, price_".$USER->currencyID." AS price, video_id AS videoID, languageID, narratorID FROM workshops WHERE active = 1 AND forsale = 0 AND (".$query_substring.")) wsh LEFT OUTER JOIN reviews ON `reviews`.`workshopID` = `wsh`.workshopID AND `reviews`.status = 1 GROUP BY `wsh`.workshopID) wshr LEFT OUTER JOIN `wishlist` ON `wishlist`.`workshopID` = `wshr`.workshopID GROUP BY `wshr`.workshopID) wshrw LEFT OUTER JOIN images ON `wshrw`.workshopID = `images`.workshopID AND `images`.`im_index` = 1 ORDER BY `wshrw`.rating DESC) outer_tbl LEFT OUTER JOIN `subscriptions` ON `outer_tbl`.date_publish >= `subscriptions`.date_start AND `subscriptions`.`date_end`>= `outer_tbl`.date_end AND CURDATE() >= `outer_tbl`.date_publish AND `subscriptions`.username = '".$USER->username."') tbl6) tbl7) tbl8 LEFT OUTER JOIN likes ON `likes`.workshopID = `tbl8`.workshopID GROUP BY `tbl8`.workshopID) tbl9 LEFT OUTER JOIN `narrators` ON `narrators`.narratorID = `tbl9`.narratorID ORDER BY w_month DESC", DBC_STORE);
	} else
	{
		$sql = mysql_query("SELECT `tbl2`.*, `narrators`.n_name_".$USER->lang." AS narrator_name FROM (SELECT `tbl1`.*, COALESCE(COUNT(`likes`.workshopID),0) AS likes FROM (SELECT `wshrw`.*, CONCAT(`images`.`imageID`,'.',`images`.`extension`) AS image, MONTH(`wshrw`.date_publish) AS w_month FROM (SELECT `wshr`.*, COUNT(`wishlist`.workshopID) AS wishlist FROM (SELECT `wsh`.*, DAY(`wsh`.date_publish) AS w_day, COUNT(`reviews`.`workshopID`) AS reviews, COALESCE(SUM(`reviews`.`rating`) / COUNT(`reviews`.`workshopID`),0) AS rating FROM (SELECT workshopID, heading_".$USER->lang." AS heading, subheading_".$USER->lang." AS subheading, date_publish, date_end, forsale, sale, views, price_".$USER->currencyID." AS price, video_id AS videoID, languageID, narratorID FROM workshops WHERE `workshops`.active = 1 AND `workshops`.date_publish <= CURDATE()) wsh LEFT OUTER JOIN reviews ON `reviews`.`workshopID` = `wsh`.workshopID AND `reviews`.status = 1 GROUP BY `wsh`.workshopID) wshr LEFT OUTER JOIN `wishlist` ON `wishlist`.`workshopID` = `wshr`.workshopID GROUP BY `wshr`.workshopID) wshrw LEFT OUTER JOIN images ON `wshrw`.workshopID = `images`.workshopID AND `images`.`im_index` = 1) tbl1 LEFT OUTER JOIN `likes` ON `likes`.workshopID = `tbl1`.workshopID GROUP BY `tbl1`.workshopID) tbl2 LEFT OUTER JOIN `narrators` ON `narrators`.narratorID = `tbl2`.narratorID ORDER BY w_day DESC, rating DESC", DBC_STORE);
		$pass = (bool) mysql_num_rows($sql);

	}

	$i = 0; $WAVAILABLE = array();
	while($t = mysql_fetch_object($sql)) $WAVAILABLE[$i++] = $t;



?>

<!DOCTYPE html>
<html>
<head>
<?php print_HTML_data("head","user/workshops") ?>
</head>

<body class="<?= $bodyClass ?>">
	<?php printTopMenu(); ?>	
	<?php printMainMenu(2); ?>
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main margin-b-60">			
		<?php printNavigation(1,[$lang->my_workshops]); ?>
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header heading"><?php echo $lang->my_workshops ?></h1>
			</div>
		</div><!--/.row-->		

		<?php if ($USER->grant_access) { ?>
	
			<div class="row">
				<div class="col-md-4 col-xs-12 col-sm-12">
					<div class="panel bg-success">
						<div class="panel-body">
							<h4 style="color: white; margin: 0" class="strong"><i class="fa fa-check"></i> <?= $lang->accessAllVideos ?></h4>
						</div>
					</div>
				</div>
			</div>

		<?php }; ?>

		<?php if ($pass) { ?>
			
		<div class="row">
			<div class="col-md-8 col-md-push-2 col-xs-12 col-sm-12">
				<div class="panel bg-default">
					<div class="panel-heading" style="line-height: 18px; padding-top: 15px;">
						<div class="fl-right">
							<a href="<?= $domain ?>workshops">
								<div class="btn-theme btn-ord" style="font-size: 70%; padding: 5px 15px"><i class="fa fa-search"></i> <?= $lang->all_workshops ?></div>
							</a>
						</div>
						<div class="strong"><i class="fa fa-video-camera"></i> <?= $lang->online_workshops_sm ?></div>
						<div class="small"><?= $lang->listOf_YourWs ?></div>						
					</div>
					<div class="divider width-100 margin-md-t-20" style="margin: 0"></div>
					<div class="panel-body" style="padding: 0">
						<?php for ($i=0, $TW = $WAVAILABLE; $i<count($TW); $i++) { ?>
							<div class="ws-dashboard-preview-placeholder" data-wid="<?= $TW[$i]->workshopID ?>">
								<div class="image-placeholder">
									<a href="<?= $domain ?>workshop/<?= $TW[$i]->workshopID ?>">
										<img src="<?= $FILE ?>img/content/<?= $TW[$i]->image ?>">
									</a>
								</div>
								<div class="strong">
									<a href="<?= $domain ?>workshop/<?= $TW[$i]->workshopID ?>" class="link-ord">
										<span>
											<?php $v = false; $dat = strtotime($TW[$i]->date_publish); $dat += 7*60*60*24; if ($dat >= time()) { $v = true; ?>
											<span>
												<label class="label label-success"><?= $lang->new ?></label>
											</span>
											<?php }; ?>	
											<?= $TW[$i]->heading ?>																					
										</span>
									</a>
								</div>
								<div class="smaller <?= ($v ? "padding-t-5" : "") ?>">
									<span><i class="fa fa-calendar"></i> <?= make_date(-1,$TW[$i]->date_publish); ?></span>
									<span class="padding-l-10 star-color">
										<?php $g = determine_rating($TW[$i]->rating); ?>
										<?php for ($h=0; $h<count($g); $h++) { ?>
										<i class="fa <?= $g[$h]; ?>"></i>
										<?php }; ?>
									</span>
									<span class="padding-l-10">
										<i class="fa fa-thumbs-up"></i> <?= $TW[$i]->likes ?>
									</span>
									<span class="padding-l-10">
										<i class="fa fa-eye"></i> <?= $TW[$i]->views ?>
									</span>
									<span class="padding-l-10">
										<i class="fa fa-comments"></i> <?= $TW[$i]->reviews ?>
									</span>
								</div>
								<div class="small">
									<span><i class="fa fa-globe"></i> <?= $lang->lang ?>: <span class="strong"><?= return_workshop_language($TW[$i]->languageID) ?></span></span>
									<div><i class="fa fa-paint-brush"></i> <?= $lang->ws_with ?> <span class="strong"><?= $TW[$i]->narrator_name ?></span></div>
										
								</div>
								<div class="smaller padding-t-5">
									<a href="https://www.facebook.com/sharer/sharer.php?u=<?= urlencode($domain."workshop/".$TW[$i]->workshopID) ?>" target="_blank">
									<span class="social fb" data-toggle="tooltip" data-placement="bottom" title="<?= $lang->share_ws_facebook ?>"><i class="fa fa-facebook-official fa-2x"></i></span>
									</a>
									<span class="padding-l-10">
										<a href="https://twitter.com/intent/tweet?text=NEW Handmade Fantasy World WORKSHOP ~ <?= urlencode($domain."workshop/".$TW[$i]->workshopID) ?>, come to enjoy" target="_blank">
										<span class="social twitter" data-toggle="tooltip" data-placement="bottom" title="<?= $lang->share_ws_Twitter ?>"><i class="fa fa-twitter fa-2x"></i></span>
										</a>						
									</span>			
								</div>
								<div class="text-right ws-actions">																	
									
									<div class="ws-action-btn">
										<a href="<?= $FILE ?>user/video/<?= $TW[$i]->workshopID ?>">
											<div class="btn-theme btn-ord" style="font-size: 80%; padding: 5px 15px"><i class="fa fa-play"></i> <?= $lang->watch_video ?></div>
										</a>
									</div>									
								</div>
							</div>
						<?php }; ?>
					</div>
				</div>
			</div>
		</div>

		<?php } else { ?>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<span class="strong"><i class="fa fa-ban"></i> <?php echo $lang->noWS_ATM ?>.</span>
						</div>
						<div class="panel-body">
							<?php echo $lang->no_WSOnyourAcc ?>
							<div class="margin-t-10 margin-b-10">
								<a href="<?= $domain; ?>workshops"><div class="btn-ord btn-green small"><i class="fa fa-paint-brush"></i> <?php echo $lang->all_workshops ?></div></a>
							</div>
						</div>						
					</div>
				</div>
			</div>
		<?php }; ?>

						
	</div>	<!--/.main-->
	

	<?php print_HTML_data("script","user/workshops") ?>
</body>

</html>
