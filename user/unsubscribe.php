<?php 

session_start();
include "../global.php";

if (isset($_GET['username']) && trim($_GET['username']) !== "" && isset($_GET['email']) && trim($_GET['email']) !== "")
{
	include "../functions.php";
	include "../connect.php";

	$sql = mysql_query("SELECT * FROM `users` WHERE BINARY `users`.`username` = '".htmlspecialchars($_GET['username'], ENT_QUOTES)."' AND BINARY `users`.`email` = '".htmlspecialchars($_GET['email'], ENT_QUOTES)."' LIMIT 1", DBC_STORE);
	if (mysql_num_rows($sql)) 
	{
		$user = mysql_fetch_object($sql);
		$sql = mysql_query("UPDATE `users` SET `users`.e_newsletter = 0 WHERE BINARY `users`.username = '".htmlspecialchars($_GET['username'], ENT_QUOTES)."' AND BINARY `users`.email = '".htmlspecialchars($_GET['email'], ENT_QUOTES)."'", DBC_STORE);
		$print = (bool) $sql;
		$lang = return_lang(strtolower($user->lang),"../");
	} else
	{
		header("location: ".$FILE);
	};
} else
{
	header("location: ".$FILE);
}

if ($print) { ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?= $lang->unsubscribed ?> | Handmade Fantasy World</title>
</head>
<body>
	<?= $lang->succ_unsubscribed ?>
	<br>
	<?= $lang->redirection_in ?> <span id="timer">10</span>...
	<script type="text/javascript">
		var link   =  "<?= $FILE ?>";
		var time   =  10;
		var timer  =  window.setInterval(function() {
			time--;
			document.getElementById("timer").innerHTML = time;
			if (time === 0)
			{
				window.clearInterval(timer);
				window.location = link;
			}
		},1000);
	</script>
</body>
</html>

<?php }; ?>