<?php 

	session_start();
	if (!isset($_GET['pid']) || (isset($_GET['pid']) && trim($_GET['pid'])==="")) header("location: index.php");
	$pid = $_GET['pid'];
//	Prep for include
	$prepath = '../';
	$INCLUDE = (object) array("getDATA" => false);
	include $prepath."functions.php";
	include $prepath."connect.php";

//	User sent from email to receipt
	if (isset($_GET['token']))
	{
		$sql = mysql_query("SELECT `users`.`username` FROM `users` WHERE BINARY `users`.`security_token` = '".$_GET['token']."' LIMIT 1", DBC_STORE);
		if (mysql_num_rows($sql)) 
		{ 
			$a = ""; 
			$pom = mysql_fetch_object($sql);			
			$ACCESS                   = true;
			$ACCESS_USER              = $_SESSION['hfw_username'];
			$tok                      = generate_string(20);
			$_SESSION['hfw_username'] = $pom->username;
			$_SESSION['access_token'] = $tok;
			$sql                      = mysql_query("UPDATE `users` SET `users`.`access_token` = '".$tok."', `users`.`timestamp` = '".time()."'", DBC_STORE);
			$ip                       = getUserIP();
			$sql                      = mysql_query("SELECT `ipaddress`.`ip_address` FROM ipaddress WHERE BINARY `ipaddress`.`ip_address` = '".$ip."' AND BINARY `ipaddress`.`username` = '".$_SESSION['hfw_username']."'", DBC_STORE);
			if (!mysql_num_rows($sql))
			{
				$resp = json_decode(file_get_contents("http://freegeoip.net/json/".$ip));
				$sql = mysql_query("INSERT INTO ipaddress(ip_address, date, timestamp, country_code, country_name, region_name, city, zip_code, timezone, latitude, longitude, username) VALUES ('".$ip."', CURDATE(), '".time()."', '".$resp->countryCode."', '".$resp->country."', '".$resp->region_name."', '".$resp->city."', '".$resp->zip."', '".$resp->timezone."', '".$resp->lat."', '".$resp->lon."', '".$username."')", DBC_STORE);
			};
			$user = $_SESSION['hfw_username'];			
		} else { header("location: ../"); exit(); }
	} else
	{
		if (!isset($_SESSION['hfw_username'])) { header("location: ".$domain); exit(); };
	};	
	include $prepath."requests/userManagementCheck.php";
	include $prepath."global.php";
	include $prepath."getDATA.php";

	$sql = mysql_query("SELECT `payments`.*, `name_".$USER->lang."` AS paymentMethodName FROM payments INNER JOIN `paymentmethods` ON `paymentmethods`.pmID = `payments`.payment_method WHERE BINARY username = '".$USER->username."' AND BINARY paymentID = '".$pid."' LIMIT 1", DBC_STORE);
	if (!mysql_num_rows($sql)) header("location: ".$domain); else
	{
		$PAYMENT = (object) array(); $INVOICE = (object) array();

		while($t = mysql_fetch_object($sql)) $PAYMENT = $t;			// Get payment data
//		SELECT boughtworkshops and all necessary fields 
		$sql = mysql_query("SELECT `subTbl1`.*, `paymentmethods`.`name_".$USER->lang."` AS paymentMethodName FROM (SELECT `tblPW`.*, `workshops`.heading_".$USER->lang." AS w_heading, `workshops`.`subheading_".$USER->lang."` AS w_subheading, `workshops`.`price_".$PAYMENT->trans_currencyID."` AS price, `workshops`.`m_subscription`, `workshops`.`ind_subscription`, `workshops`.`active`, `workshops`.`forsale` FROM (SELECT `tbl`.*, `boughtworkshops`.`bwID`, `boughtworkshops`.`workshopID` FROM (SELECT * FROM `payments` WHERE username='".$USER->username."' AND paymentID = '".$pid."') tbl LEFT OUTER JOIN `boughtworkshops` ON `boughtworkshops`.`paymentID` = `tbl`.paymentID) tblPW INNER JOIN workshops ON `workshops`.`workshopID` = `tblPW`.`workshopID`) subTbl1 INNER JOIN paymentmethods ON `paymentmethods`.pmID = `subTbl1`.payment_method", DBC_STORE);
		$obj = array(); $i = 0; $dataSet = 0; 
		while($t = mysql_fetch_object($sql)) $obj[$i++] = $t;
		if (count($obj)) { 
				$INVOICE->bw = $obj;
		} else $INVOICE->bw = NULL;

//		SELECT  subscriptions
		$sql = mysql_query("SELECT `subTbl1`.*, `paymentmethods`.`pmID`, `paymentmethods`.`name_".$USER->lang."` AS paymentMethodName FROM (SELECT `subTbl`.paymentID, `subTbl`.trans_currencyID, `subTbl`.payment_amount, `subTbl`.payment_method, `subTbl`.trans_date, `subTbl`.receiptID, `subTbl`.username, `subTbl`.subscriptionID, `subTbl`.date_start, `subTbl`.date_end, `subTbl`.s_start, `subTbl`.s_end, `packages`.`packageID`, `packages`.`price_".$PAYMENT->trans_currencyID."` AS price, `packages`.`flag` FROM (SELECT `pwd`.*, `subscriptions`.`subscriptionID`, `subscriptions`.type, `subscriptions`.date_start, `subscriptions`.date_end, MONTH(`subscriptions`.`date_start`) AS s_start, MONTH(`subscriptions`.date_end) AS s_end FROM (SELECT * FROM payments WHERE `payments`.username='".$USER->username."' AND BINARY `payments`.paymentID='$pid') pwd INNER JOIN subscriptions ON `subscriptions`.`paymentID` = `pwd`.`paymentID`) subTbl INNER JOIN packages ON `subTbl`.type = `packages`.packageID) subTbl1 INNER JOIN paymentmethods ON paymentmethods.pmID = `subTbl1`.payment_method", DBC_STORE);
		$obj = array(); $i = 0;
		while($t = mysql_fetch_object($sql)) $obj[$i++] = $t;
		if (count($obj)) { 
			$INVOICE->sub = $obj;
		} else $INVOICE->sub = NULL;
	};

	$sql_buyer = mysql_query("SELECT * FROM paymentinfo WHERE piID = (SELECT piID FROM payments WHERE BINARY `payments`.paymentID = '".$pid."') AND BINARY username = '".$USER->username."'", DBC_STORE);
	$BUYER = (object) array();
	while($t = mysql_fetch_object($sql_buyer)) $BUYER = $t;

	include $prepath."pages.php";
	include $prepath."lang/func.php";
	include $prepath."lang/user_".strtolower($lang_acr).".php";
	include $prepath."requests/userManagement.php";

	$sql = mysql_query("SELECT * FROM currencies WHERE BINARY currencyID = '".$PAYMENT->trans_currencyID."'", DBC_STORE);
	$currency = mysql_fetch_object($sql);

	$prepath = $domain;

?>

<!DOCTYPE html>
<html>
<head>
<?php print_HTML_data("head","receipt") ?>
</head>

<body class="<?= $bodyClass ?>">

	<?php printTopMenu(); ?>		
	<?php printMainMenu(-1); ?>
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main margin-b-60">			
		<?php printNavigation(2,[$lang->invoice_view,"IF - ".$PAYMENT->receiptID], [$FILE."user/subscriptions"]); ?>
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header heading"><?php echo $lang->invoice_view ?></h1>
			</div>
		</div><!--/.row-->

		<div class="row">
			<div class="col-md-8 col-xs-12 col-sm-12">
				<div class="panel bg-default">
					<div class="panel-body">
						<div class="text-right">
							<div class=""><img src="<?= $FILE ?>img/logo.png" title="Handmade Fantasy World Logo" width="220" /></div>
							<div class="strong uppercase margin-t-10">Handmade Fantasy World</div>
							<div class="smaller"><?php echo $lang->belgrade ?>, <?php echo $lang->serbia ?></div>
							<div class="small"><span class="uppercase smaller strong"><?php echo $lang->pib ?></span> <?= $COMPANY_INFO->PIB ?></div>
							<div class="small"><span class="uppercase smaller strong"><?php echo $lang->mb ?></span> <?= $COMPANY_INFO->MB ?></div>					
							<div class="small"><span class="uppercase smaller strong"><?php echo $lang->email_email ?></span> <a class="link-ord" href="mailto:<?= $COMPANY_INFO->email ?>"><?= $COMPANY_INFO->email ?></a></div>
						</div>

						<div class="margin-t-20 strong" style="font-size: 160%">IF - <?= $PAYMENT->receiptID ?></div>
						<div class="margin-t-20">
							<div class="uppercase small"><?php echo $lang->invoice_individual ?></div>
							<?php if (mysql_num_rows($sql_buyer)) { ?>
							<div class="margin-t-10">
								<div class="strong"><?= $BUYER->name ?></div>
								<div class="small"><?php echo $lang->adress ?>: <span class="strong"><?= $BUYER->address ?></div>
								<div class="small"><?php echo $lang->city ?>: <span class="strong"><?= $BUYER->city ?></div>
								<div class="small"><?php echo $lang->contact_phone ?>: <span class="strong"><?= $BUYER->telephone ?></div>
								<div class="small"><?php echo $lang->user_nameone ?>: <span class="strong"><?= $USER->username ?></span></div>
								<div class="small"><?php echo $lang->email_email ?>: <span class="strong"><?= $USER->email ?></span></div>
							</div>
							<?php }; ?>
						</div>
						<div class="smaller margin-t-20"><span class="strong"><?php echo $lang->warning ?>:</span> <?php echo $lang->pdv_sistem ?></div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 col-sm-12">
				<div class="panel bg-default">
					<div class="panel-body">
						<div>
							<span class="uppercase small"><?php echo $lang->payment_status ?></span> 
							<?php if ($PAYMENT->paid) { ?>
							<div class="fl-right text-success strong uppercase">
								<span class="smaller"><i class="fa fa-circle smaller" style="position: relative; top: -2px"></i></span> <span class="small"><?php echo $lang->paid ?></span>
							</div>
							<?php } else { ?>
							<div class="fl-right text-danger uppercase strong">
								<span class="smaller"><i class="fa fa-circle-o smaller" style="position: relative; top: -2px"></i></span> <span class="small"><?php echo $lang->in_process ?></span>
							</div>
							<?php }; ?>
						</div>
						<div>
							<span class="uppercase small"><?php echo $lang->payment_date ?></span> 
							<div class="fl-right small">
								<?= make_date(-1, $PAYMENT->trans_date) ?>
							</div>
						</div>						
						<div>
							<span class="uppercase small"><?php echo $lang->payment_method ?></span> 
							<div class="fl-right small">								
								<?php 
								// Credit Card - AIK checkout
								if ($PAYMENT->payment_method == 1) {  ?>
								<i class="fa fa-credit-card"></i> <?= $PAYMENT->paymentMethodName; ?>
								<?php
								// PayPal Express Checkout
								} elseif ($PAYMENT->payment_method == 2) { ?>
								<span class="paypal-text strong"><i class="fa fa-paypal"></i> <?= $PAYMENT->paymentMethodName; ?></span>
								<?php } elseif ($PAYMENT->payment_method == 3) { ?>
								<span class="paypal-text strong"><i class="fa fa-money"></i> <?= $lang->cash ?></span>
								<?php }; ?>
							</div>
						</div>

						<div class="margin-t-20 text-right">
							<div class="uppercase small strong"><?php echo $lang->id_payment?></div> 
							<div class="small"><?= $PAYMENT->paymentID ?></div>
						</div>

						<div class="margin-t-10 text-right">
							<div class="uppercase small strong"><?php echo $lang->invoice_number ?></div> 
							<div class="small"><?= $PAYMENT->receiptID ?></div>
						</div>

						<div class="divider"></div>

						<div class="margin-t-10">
							<div>
								<span class="uppercase"><?php echo $lang->price_withoutPDV ?></span> <div class="fl-right">
									<?= $PAYMENT->trans_currencyID." ".print_money_PLAINTXT($PAYMENT->payment_amount - $PAYMENT->payment_amount*0.2,2); ?>
								</div>
							</div>
							<div>
								<span class="uppercase"><?php echo $lang->pdv ?></span> <div class="fl-right">
									<?= $PAYMENT->trans_currencyID." ".print_money_PLAINTXT($PAYMENT->payment_amount*0.2,2); ?>
								</div>
							</div>
							<div class="color-theme strong" style="font-size: 150%">
								<span class="uppercase"><?php echo $lang->total ?></span> <div class="fl-right">
									<?= $PAYMENT->trans_currencyID." ".print_money_PLAINTXT($PAYMENT->payment_amount,2); ?>
								</div>
							</div>
						</div>
					</div>
				</div>				
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 col-xs-12 col-sm-12">
				<div class="panel bg-default">
					<div class="panel-body" style="padding: 0">
						<table class="table table-hover tbl-theme">
							<tr class="heading-row">
								<th width="20"><?php echo $lang->number ?></th>								
								<th><?php echo $lang->name_package ?></th>
								<th width="100" class="text-right"><?php echo $lang->price_pdvA ?> <span class="small">[<?= $currency->name ?>]</span></th>
								<th width="80" class="text-right"><?php echo $lang->pdv ?> <span class="small">[<?= $currency->name ?>]</span></th>
								<th width="80" class="text-right"><?php echo $lang->price ?> <span class="small">[<?= $currency->name ?>]</span></th>
							</tr>
							<?php for ($i=0; $i<count($INVOICE->sub); $i++) { ?>
							<tr>
								<td><?= $i+1 ?></td>
								<td class="small">
									<?php if ($INVOICE->sub[$i]->packageID) { ?> 
									<div class="strong"><?php echo $lang->package ?> - <?php echo print_duration($INVOICE->sub[$i]->flag) ?></div>
									<div class="small"><?php echo $lang->sub_lasts ?> <?= print_duration($INVOICE->sub[$i]->flag); ?> [<?= make_date(-1,$INVOICE->sub[$i]->date_start) ?> - <?= make_date(-1,$INVOICE->sub[$i]->date_end) ?>]
									</div>
									<?php } elseif ($INVOICE->sub[$i]->workshopID) { ?>
									<a href="<?= $domain ?>workshop/<?= $INVOICE->sub[$i]->workshopID ?>"><?php echo $INVOICE->sub[$i]->w_heading ?></a> - <span class="color-theme uppercase small"><?php echo $lang->ws_lowchar ?></span>
									<?php }; ?>
								</td>
								<td class="text-right"><?= print_money_PLAINTXT($INVOICE->sub[$i]->price - $INVOICE->sub[$i]->price*0.2,2); ?></td>
								<td class="text-right"><?= print_money_PLAINTXT($INVOICE->sub[$i]->price*0.2,2); ?></td>
								<td class="text-right"><?= print_money_PLAINTXT($INVOICE->sub[$i]->price,2); ?></td>	
							</tr>
							<?php }; ?>	

							<?php for ($i=0, $m = count($INVOICE->bw); $i<$m; $i++) { ?>
							<tr>
								<td><?= $i+1 ?></td>
								<td class="small">									
									<a href="<?= $domain ?>workshop/<?= $INVOICE->bw[$i]->workshopID ?>"><?php echo $INVOICE->bw[$i]->w_heading ?></a> - <span class="color-theme uppercase small"><?php echo $lang->ws_lowchar ?></span>
								</td>
								<td class="text-right"><?= print_money_PLAINTXT(determine_item_price($m, $PAYMENT->trans_currencyID) - 0.2*determine_item_price($m, $PAYMENT->trans_currencyID),2); ?></td>
								<td class="text-right"><?= print_money_PLAINTXT(determine_item_price($m, $PAYMENT->trans_currencyID)*0.2,2); ?></td>
								<td class="text-right"><?= print_money_PLAINTXT(determine_item_price($m, $PAYMENT->trans_currencyID),2); ?></td>	
							</tr>
							<?php }; ?>								
						</table>
					</div>
				</div>
			</div>			
		</div>
		
		
	

					
	</div>	<!--/.main-->
	
	<?php print_HTML_data("script","receipt") ?>
</body>

</html>
