<?php 
//	All shopping AJAX requests
	if (!isset($_POST['type']) || !isset($_POST['data'])) exit();
	session_start();
	//if (!isset($_SESSION['hfw_username'])) exit();
	$prepath  = '../';
	$INCLUDE  = (object) array("getDATA" => false);
	$REDIRECT = false;
	include $prepath."connect.php";
	include $prepath."functions.php";
	include $prepath."global.php";
	include $prepath."getDATA.php";
	//if ($userActive===0) exit();

	$TYPE     = base64_decode($_POST['type']);
	$DATA     = json_decode($_POST['data']);
	$arr_info = array();
	$arr_data = array();
	$RESPONSE = (object) array("info" => &$arr_info, "data" => &$arr_data);

	switch($TYPE)
	{
	case "addWSToShoppingCart":
		$sql = mysql_query("SELECT * FROM workshops WHERE BINARY workshopID = '".htmlspecialchars($DATA->wid, ENT_QUOTES, "UTF-8")."'", DBC_STORE);
		if (!mysql_num_rows($sql))
		{
			$arr_info["response"] = false;
			$arr_data = NULL;
		};
		// Workshop is found
		$arr_info["response"] = true;
		$arr_info["active"] = isset($_SESSION['hfw_username']);
		if (!$arr_info["active"])
		{
			$arr_info["redirect"] = true;
			$arr_info["redirect_url"] = $domain."login?action=addCart&type=workshop&cid=".$DATA->wid;
		} else
		{
			$sql = mysql_query("SELECT * FROM cart WHERE BINARY workshopID = '".$DATA->wid."' AND BINARY username = '".$USER->username."'", DBC_STORE);
			$arr_data["action"] = mysql_num_rows($sql) ? "delete" : "add";
			$sql = mysql_num_rows($sql) ? mysql_query("DELETE FROM cart WHERE BINARY workshopID = '".$DATA->wid."' AND BINARY username = '".$USER->username."'", DBC_STORE) :  mysql_query("INSERT INTO cart(itemID, workshopID, username) VALUES('"._SQL_escape_exstring(["SELECT * FROM cart WHERE BINARY itemID = '","'"], 20)."', '".$DATA->wid."', '".$USER->username."')", DBC_STORE);

			// Successfully added/deleted from cart, fetch required data
			if ($sql)
			{
				include_once $prepath."requests/det_subscription_months.php";
				if ($HAS_SUBSCRIPTION)
				{
					$sql = mysql_query("SELECT * FROM cart WHERE BINARY username = '".$USER->username."'", DBC_STORE);
					$arr_data["cart_items"] = mysql_num_rows($sql);
					$arr_data["cart_value"] = print_money_PLAINTXT(determine_cart_price(mysql_num_rows($sql), "RSD", 1), 2);
				} else
				{
					$sql = mysql_query("SELECT `cart`.*, `workshops`.price_RSD AS priceRSD, `workshops`.price_".$USER->currencyID." AS price FROM cart INNER JOIN workshops ON `workshops`.workshopID = `cart`.workshopID AND `workshops`.active = 1 WHERE BINARY username = '".$USER->username."'", DBC_STORE);
					$sum = 0;
					while($t = mysql_fetch_object($sql))
					{
						$sum += determine_item_price(0, "RSD", $t->priceRSD);
					};			
					$arr_data["cart_value"] = print_money_PLAINTXT($sum,2);
				}
				$arr_data["cart_items"] = mysql_num_rows($sql);
				$arr_data["d"]          = array("in_cart" => $lang->inCart, "add_to_cart" => $lang->add_toCart);
			};		
		}
		break;
	case "checkShoppingCart":
		$arr_info["response"] = true;
		$arr_info["active"] = (bool) $userActive;
		if ($arr_info["active"] && !$USER->grant_access)
		{
			// Fetch workshops in cart
			include "../requests/det_subscription_months.php";
			if ($HAS_SUBSCRIPTION)
			{
				$sql = mysql_query("SELECT * FROM cart WHERE BINARY username = '".$USER->username."'", DBC_STORE);
				$arr_data["cart_items"] = mysql_num_rows($sql);
				$arr_data["cart_value"] = print_money_PLAINTXT(determine_cart_price(mysql_num_rows($sql), "RSD", 1), 2);
			} else
			{
				$sql = mysql_query("SELECT `cart`.*, `workshops`.price_RSD AS priceRSD, `workshops`.price_".$USER->currencyID." AS price FROM cart INNER JOIN workshops ON `workshops`.workshopID = `cart`.workshopID AND `workshops`.active = 1 WHERE BINARY username = '".$USER->username."' ", DBC_STORE);
				$sum = 0;
				while($t = mysql_fetch_object($sql))
				{
					$sum += determine_item_price(0, "RSD", $t->priceRSD);
				};			
				$arr_data["cart_value"] = print_money_PLAINTXT($sum,2);
			}
			$arr_data["cart_items"] = mysql_num_rows($sql);
			

			// Fetch workshops in subscription
			$query_substring = "";
			for ($i=0, $ft = true, $h = false; $i<12; $i++ )
			{
				$h = $SUBSCRIPTION_MONTHS[$i] || $h;
				if ($SUBSCRIPTION_MONTHS[$i])
				{
					$query_substring .= (!$ft ? " OR " : "")." MONTH(date_publish) = '".($i+1)."'";
					if ($ft) $ft = false;
				};
			};
			$g = 0;
			$available_ws = array(); 
			if ($h)
			{
				$sql = mysql_query("SELECT workshopID, CONCAT('".$FILE."user/video/', `workshops`.workshopID) AS url FROM workshops WHERE (".$query_substring.") AND active = 1", DBC_STORE);
				while($t = mysql_fetch_object($sql))
				{
					$available_ws[$g++] = $t;
				};
			};

			// Add boughtworkshops
			$sql = mysql_query("SELECT `boughtworkshops`.workshopID, CONCAT('".$FILE."user/video/', `boughtworkshops`.workshopID) AS url FROM boughtworkshops INNER JOIN workshops ON `workshops`.workshopID = `boughtworkshops`.workshopID AND `workshops`.active = 1 WHERE BINARY `boughtworkshops`.username = '".$USER->username."'", DBC_STORE);
			while($t = mysql_fetch_object($sql))
			{
				$available_ws[$g++] = $t;
			};

			//	Add workshops with coupons
			$sql = mysql_query("SELECT `coupons`.workshopID, CONCAT('".$FILE."user/video/', `coupons`.workshopID) AS url FROM coupons INNER JOIN workshops ON `workshops`.workshopID = `coupons`.workshopID AND `workshops`.active = 1 WHERE BINARY `coupons`.username = '".$USER->username."' AND `coupons`.active = 1 AND `coupons`.expiration_date>=CURDATE()", DBC_STORE);
			while($t = mysql_fetch_object($sql))
			{
				$available_ws[$g++] = $t;
			};
			$arr_data["ws_status"] = $available_ws;			

			// Fetch workshops in cart
			$g = 0;
			$r = array();
			$sql = mysql_query("SELECT `cart`.workshopID FROM cart WHERE BINARY username = '".$USER->username."' AND workshopID IS NOT NULL", DBC_STORE);
			while($t = mysql_fetch_object($sql))
			{
				$r[$g++] = $t;
			};

			$arr_data["ws_cart"] = $r;
			$arr_data["d"] = array("watch_video" => $lang->watch_video, "in_cart" => $lang->inCart);
		} else
		{
			$arr_info["access"] = true;
		}
		break;

	case "checkWorkshopsUserSection":
		if (!$userActive) { $arr_info["response"] = false; $arr_info["redirect"] = true; $arr_info["redirect_url"] = $domain."workshops"; } else
		{
			if ($USER->grant_access)
			{
				$arr_info["response"]   = true;
				$arr_info["privileged"] = true;
				$arr_data = NULL;
			} else
			{
				$arr_info["response"]   = true;
				$arr_info["privileged"] = false;
				// Fetch workshops in subscription
				include "../requests/det_subscription_months.php";
				$query_substring = "";
				for ($i=0, $ft = true, $h = false; $i<12; $i++ )
				{
					$h = $SUBSCRIPTION_MONTHS[$i] || $h;
					if ($SUBSCRIPTION_MONTHS[$i])
					{
						$query_substring .= (!$ft ? " OR " : "")." MONTH(date_publish) = '".($i+1)."'";
						if ($ft) $ft = false;
					};
				};
				$g = 0;
				$available_ws = array(); 
				if ($h)
				{
					$sql = mysql_query("SELECT workshopID, CONCAT('".$FILE."user/video/', `workshops`.workshopID) AS url FROM workshops WHERE (".$query_substring.") AND active = 1 AND date_publish<=CURDATE()", DBC_STORE);
					while($t = mysql_fetch_object($sql))
					{
						$available_ws[$g] = $t; $available_ws[$g]->cid = NULL;
						$g++;
					};
				};

				// Add boughtworkshops
				$sql = mysql_query("SELECT `boughtworkshops`.workshopID, CONCAT('".$FILE."user/video/', `boughtworkshops`.workshopID) AS url FROM boughtworkshops INNER JOIN workshops ON `workshops`.workshopID = `boughtworkshops`.workshopID AND `workshops`.active = 1 AND date_publish<=CURDATE() WHERE BINARY `boughtworkshops`.username = '".$USER->username."'", DBC_STORE);
				while($t = mysql_fetch_object($sql))
				{
					$available_ws[$g] = $t; $available_ws[$g]->cid = NULL;
					$g++;
				};

				//	Add workshops with coupons
				$sql = mysql_query("SELECT `coupons`.couponID AS cid,`coupons`.workshopID, CONCAT('".$FILE."user/video/', `coupons`.workshopID) AS url FROM coupons INNER JOIN workshops ON `workshops`.workshopID = `coupons`.workshopID AND `workshops`.active = 1 AND date_publish<=CURDATE() WHERE BINARY `coupons`.username = '".$USER->username."' AND `coupons`.active = 1 AND `coupons`.expiration_date>=CURDATE()", DBC_STORE);
				while($t = mysql_fetch_object($sql))
				{
					$available_ws[$g++] = $t;
				};
				$arr_data["ws_status"] = $available_ws;
				$arr_data["d"] = array("in_cart" => $lang->inCart, "watch_video" => $lang->watch_video);

				// Check workshops in cart
				$f = 0; $ws_cart = array();
				$sql = mysql_query("SELECT `cart`.workshopID FROM cart WHERE BINARY username = '".$USER->username."' AND workshopID IS NOT NULL", DBC_STORE);
				while($t = mysql_fetch_object($sql))
				{
					$ws_cart[$f++] = $t;
				}
				$arr_data["ws_cart"] = $ws_cart;

			}
		}

		break;

	default:
		$arr_info["response"] = false;
		$arr_data = NULL;
	}

	echo json_encode($RESPONSE);


?>