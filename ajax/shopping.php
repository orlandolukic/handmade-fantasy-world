<?php 
//	All shopping AJAX requests
	if (!isset($_POST['type']) || !isset($_POST['data'])) exit();
	session_start();
	if (!isset($_SESSION['hfw_username'])) exit();
	$prepath  = '../';
	$INCLUDE  = (object) array("getDATA" => false);
	$REDIRECT = false;
	include $prepath."connect.php";
	include $prepath."functions.php";
	include $prepath."global.php";
	include $prepath."getDATA.php";
	if ($userActive===0) exit();

	$TYPE     = base64_decode($_POST['type']);
	$DATA     = json_decode($_POST['data']);
	$arr_info = array();
	$arr_data = array();
	$RESPONSE = (object) array("info" => &$arr_info, "data" => &$arr_data);

	switch($TYPE)
	{
	case "addToShoppingCart":
		$sql = mysql_query("SELECT * FROM cart WHERE username='".$USER->username."' AND workshopID = '".$DATA->workshopID."'", DBC_STORE);
		if ($sql && mysql_num_rows($sql)===0)
		{
			$str = generate_string(20);
			$sq1 = mysql_query("SELECT * FROM cart WHERE itemID = '$str'", DBC_STORE);
			if (!mysql_num_rows($sq1))
			{
				while(mysql_num_rows($sq1))
				{
					$str = generate_string(20);
					$sq1 = mysql_query("SELECT * FROM cart WHERE itemID = '$str'", DBC_STORE);
				};
			};
		//	Check if ID exists
			$sql = mysql_query("INSERT INTO cart (itemID, workshopID, username) VALUES ('".$str."','".$DATA->workshopID."', '".$USER->username."')", DBC_STORE);
			if ($sql)
			{
				$sql = mysql_query("SELECT * FROM cart WHERE username='".$USER->username."'", DBC_STORE);
				$arr_info = array("response" => true); $arr_data = array("items" => ($sql ? mysql_num_rows($sql) : 0));
			} else
			{
				$arr_info = array("response" => false, "errorCode" => 400); $arr_data = NULL;
			};
		} else
		{
			$arr_info = array("response" => false, "errorCode" => 401); $arr_data = NULL;
		}
		break;

	case "checkPaymentAmount":
		checkUser();
		include $prepath."requests/shopping.php";
		if ($sql && $sql_sum) {
			$arr_info = array("response" => true);
			$SUM = floatval($RSD_SUM);
			$SUM_CURR = isset($CURR_SUM) ? $CURR_SUM : 0;
			$arr_data = array("price" => array( 
												"total"                => print_money_PLAINTXT($SUM,2), 
												"total_noformat"       => $SUM,
												"tax"                  => print_money_PLAINTXT($SUM*0.2,2), 
												"pwt"                  => print_money_PLAINTXT($SUM-$SUM*0.2,2), 
												"totalForeignCurrency" => print_money_PLAINTXT($SUM_CURR,2)
											  ),
							  "items"         => mysql_num_rows($sql), 
							  "morePackages"  => $morePackages
							  );
		};
		break;

	case "removeFromShoppingCart":
	//	Execute remove query
		$sql = mysql_query("DELETE FROM cart WHERE username='".$USER->username."' AND itemID='".$DATA->cartItemID."'", DBC_STORE);
	//	Begin with business
		if ($sql)
		{
			include $prepath."requests/shopping.php";
			if ($sql && $sql_sum) {
				$arr_info = array("response" => true);
				$SUM = floatval($RSD_SUM);
				$SUM_CURR = isset($CURR_SUM) ? $CURR_SUM : 0;
				$arr_data = array("price" => array( 
													"total"                => print_money_PLAINTXT($SUM,2), 
													"total_noformat"       => $SUM,
													"tax"                  => print_money_PLAINTXT($SUM*0.2,2), 
													"pwt"                  => print_money_PLAINTXT($SUM-$SUM*0.2,2), 
													"totalForeignCurrency" => print_money_PLAINTXT($SUM_CURR,2)
												  ),
								  "items"         => mysql_num_rows($sql), 
								  "morePackages"  => $morePackages,
								  "prices"        => array("national_price"  => print_money_PLAINTXT(determine_item_price(mysql_num_rows($sql), "RSD"),2),
								                           "foreign_price"   => ($USER->currencyID !== "RSD" ? print_money_PLAINTXT(determine_item_price(mysql_num_rows($sql), $USER->currencyID),2) : NULL))
								  );
			} else
			{
				$arr_info = array("response" => false); $arr_data = NULL;
			};
		} else 
		{
			$arr_info = array("response" => false); $arr_data = NULL;
		};
		break;

	case "removeAllFromShoppingCart":
	//	Execute remove query
		$sql = mysql_query("DELETE FROM cart WHERE username='".$USER->username."'", DBC_STORE);
	//	Begin with business
		if ($sql)
		{
			$arr_info = array("response" => true);
			$arr_data = NULL;			
		} else 
		{
			$arr_info = array("response" => false); $arr_data = NULL;
		};
		break;

	case "likeVideo":
		checkUser();
		$sql_GL = mysql_query("SELECT * FROM likes WHERE BINARY workshopID = '".$DATA->wid."' AND username != '".$USER->username."'", DBC_STORE);
		$likes = mysql_num_rows($sql_GL);
		$sql = mysql_query("SELECT workshopID FROM workshops WHERE BINARY workshopID = '".$DATA->wid."'", DBC_STORE);
		if (!mysql_num_rows($sql)) 
		{ 
			$arr_info["response"] = false; $arr_data = NULL; break; 
		} else
		{
			$sql = mysql_query("SELECT * FROM likes WHERE BINARY username = '".$USER->username."' AND BINARY workshopID = '".$DATA->wid."'", DBC_STORE);
			if (mysql_num_rows($sql))
			{
				$sql = mysql_query("DELETE FROM likes WHERE BINARY workshopID = '".$DATA->wid."' AND BINARY username = '".$USER->username."'", DBC_STORE);
				$arr_info["response"] = true; $arr_data["liked"] = false; 
				
			// Determine likes for specific languages
			   $liked = false;
			   $wid = $DATA->wid;
			   include "../code/determine_like_text.php";			
			   $arr_data['message'] = $expostring;

				break;
			} else
			{
				$sql = mysql_query("INSERT INTO likes(username, workshopID, date) VALUES
				                   ( '".$USER->username."', '".$DATA->wid."', CURDATE() )", DBC_STORE);
				if ($sql)
				{
					$liked = true;
					$wid = $DATA->wid;
					include "../code/determine_like_text.php";					
					$arr_info["response"] = true; $arr_data["liked"] = true;
					$arr_data['message'] = $expostring;
				} else
				{
					$arr_info["response"] = false; $arr_info["errorCode"] = 401; $arr_data = NULL;
				}
			};
		};
		break;
	}

	echo json_encode($RESPONSE);


?>