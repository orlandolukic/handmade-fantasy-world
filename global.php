<?php
//	Begin with script
	if (isset($_SESSION['hfw_username']) || isset($ACCESS) && $ACCESS)
	{
		$userActive = 1;
		$user = isset($ACCESS) ? $ACCESS_USER : $_SESSION['hfw_username'];
	} else $userActive = 0;

	if (isset($_SESSION['hfw_admin']))
	{
		$adminActive = 1;
		$admin = $_SESSION['hfw_admin'];
	} else $adminActive = 0;

//	Global Vars

	$FILE = "handma.com/";

//	Determine which language should the program take
	if (isset($_GET['lang']) && trim($_GET['lang'])!=="")
	{
		switch($_GET['lang'])
		{
		case "GR":
			$lang_acr  = $_SESSION['lang'] = "GR";
			$curr      = "EUR";
			$currName  = "€";
			$domain = $FILE.strtolower($_GET['lang'])."/";
			break;
		case "SR":
			$lang_acr  = $_SESSION['lang'] = "SR";
			$curr      = "RSD";
			$currName  = "RSD";
			$domain = $FILE.strtolower($_GET['lang'])."/";
			break;
			/*
		case "RU":
			$lang_acr  = $_SESSION['lang'] = "RU";
			$curr      = "EUR";
			$currName  = "EUR";
			$domain = $FILE.strtolower($_GET['lang'])."/";
			break;
		case "SP":
			$lang_acr  = $_SESSION['lang'] = "SP";
			$curr      = "EUR";
			$currName  = "EUR";
			$domain = $FILE.strtolower($_GET['lang'])."/";
			break;
		case "PR":
			$lang_acr  = $_SESSION['lang'] = "PR";
			$curr      = "EUR";
			$currName  = "EUR";
			$domain = $FILE.strtolower($_GET['lang'])."/";
			break;
			*/
		default:
			$lang_acr  = $_SESSION['lang'] = "EN";
			$curr      = "EUR";
			$currName  = "€";
			$domain = $FILE;
		};
	} else
	{
		if (!$userActive && !$adminActive)
		{
			if (isset($_COOKIE['country']))
			{
				//exit( $FILE.getLangFromCountry($_COOKIE['country']) );
				if (isset($page) && $page == "index")
				{
					header("location: ".$FILE.getLangFromCountry($_COOKIE['country']));
					exit();
				} else
				{
					if (isset($_SESSION['lang']) && $_SESSION['lang'] !== "EN")
					{
						$obj       = return_country_values();
						$lang_acr  = $_SESSION['lang'] = $obj->language;
						$curr      = $obj->curr;
						$currName  = $obj->currName;
						$domain    = $FILE.strtolower($_SESSION['lang'])."/";
					} else
					{
						$lang_acr  = $_SESSION['lang'] = "EN";
						$curr      = "EUR";
						$currName  = "€";
						$domain    = $FILE;
					}
				}
			} else
			{
				if (isset($page) && $page == "index") include "code/determine_country.php";
				if (isset($_SESSION['lang']) && $_SESSION['lang'] !== "EN")
				{
					$obj       = return_country_values();
					$lang_acr  = $_SESSION['lang'] = $obj->language;
					$curr      = $obj->curr;
					$currName  = $obj->currName;
					$domain    = $FILE.strtolower($_SESSION['lang'])."/";
				} else
				{
					$lang_acr  = $_SESSION['lang'] = "EN";
					$curr      = "EUR";
					$currName  = "€";
					$domain    = $FILE;
				}
			}
		} else
		{
			if (isset($_SESSION['lang']) && $_SESSION['lang'] !== "EN")
			{
				$obj       = return_country_values();
				$lang_acr  = $_SESSION['lang'] = $obj->language;
				$curr      = $obj->curr;
				$currName  = $obj->currName;
				$domain    = $FILE.strtolower($_SESSION['lang'])."/";
			} else
			{
				$lang_acr  = $_SESSION['lang'] = "EN";
				$curr      = "EUR";
				$currName  = "€";
				$domain    = $FILE;
			}
		}
	};

	$bodyClass  = "theme-vintage";
	$year       = 2017;
	$startMonth = 3;
	$endMonth   = 5;
	$dSeparator = '.';
	$PRICES     = (object) array("bojana"  => array("rsd" => 860,  "foreign" => 7),
	                             "antonis" => array("rsd" => 1750, "foreign" => 15));

	// Payment data
	$STORE_KEY    = "12345AIK";
	$CLIENT_ID    = "510030000";
	$PAYMENT_LINK = "https://entegrasyon.asseco-see.com.tr/fim/est3Dgate";

	/* Email addresses */
	$EMAIL = (object) array("owner"   => "oluki1996@gmail.com",
	                        "antonis" => "oluki1996@gmail.com",
	                        "banking" => "oluki1996@gmail.com");
	/*
	$EMAIL = (object) array("owner"   => "office@handmadefantasyworld.com",
	                        "antonis" => "antonis@handmadefantasyworld.com",
	                        "banking" => "banking@handmadefantasyworld.com");*/

	include "print_HTML_data.php";

	$COMPANY_INFO = (object) array("PIB"         => "100236547",
	                               "MB"          => "07821568",
	                               "contact"     => "/",
	                               "email"       => "info@handmadefantasyworld.com",
	                               "e_office"    => "office@handmadefantasyworld.com",
	                               "location_SR" => "Beograd, Srbija",
	                               "location_EN" => "Belgrade, Serbia",
	                               "facebook"    => "https://www.facebook.com/handmadefantasybyantony/",
	                               "instagram"   => "https://www.instagram.com/antonistzanidakis/");



?>
