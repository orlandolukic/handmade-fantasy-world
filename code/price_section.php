	
	<?php 
	$offset1 = ($PRICE_SECTION["printOffset"] ? ($cart_number==1 ? "plan" : "off") : ""); 
    $offset2 = ($PRICE_SECTION["printOffset"] ? ($cart_number==2 ? "plan" : "off") : ""); 
	$offset3 = ($PRICE_SECTION["printOffset"] ? ($cart_number==3 ? "plan" : "off") : ""); 
	$offset4 = ($PRICE_SECTION["printOffset"] ? ($cart_number>=4 ? "plan" : "off") : "");

	if (0) {
	?>
	<!-- Price section -->
	<div class="row price-row-placeholder p-cart margin-t-20">			
		<div class="col-md-3 col-xs-12 col-sm-12">						 		
			<div class="text-center price-container <?= $offset1 ?>" data-price-group="1">
				<?php if ($PRICE_SECTION["printCheckIcon"]) { ?>
				<div class="check-button"><i class="fa fa-check"></i></div>		
				<?php }; ?>									
				<div class="small strong">1 <?= $lang->workshop ?></div>
				<div class="price-placeholder">
					<span class="currency">RSD</span> <span class="price-number">1.100</span><span class="price-dot-number">00</span>	
				</div>
				<div class="strong small">EUR 9.00</div>														
			</div>				
		</div>
		<div class="col-md-3 col-xs-12 col-sm-12">
			<div class="text-center price-container margin-md-t-10 <?= $offset2 ?>" data-price-group="2">	
				<?php if ($PRICE_SECTION["printCheckIcon"]) { ?>
				<div class="check-button"><i class="fa fa-check"></i></div>		
				<?php }; ?>									
				<div class="small strong">2 <?= $lang->workshops ?></div>
				<div class="price-placeholder">
					<span class="currency">RSD</span> <span class="price-number">1.700</span><span class="price-dot-number">00</span>
					<div class="price-discount-placeholder">
						<span class="strong">EUR 14.00</span>
						<span class="old-price">RSD 2.200.00</span> <span class="discount">- 18%</span>
					</div>
				</div>				
			</div>	
		</div>
		<div class="col-md-3 col-xs-12 col-sm-12">
			<div class="text-center price-container margin-md-t-10 <?= $offset3 ?>" data-price-group="3">	
				<?php if ($PRICE_SECTION["printCheckIcon"]) { ?>
				<div class="check-button"><i class="fa fa-check"></i></div>		
				<?php }; ?>			
				<div class="small strong">3 <?= $lang->workshops ?></div>
				<div class="price-placeholder">
					<span class="currency">RSD</span> <span class="price-number">2.500</span><span class="price-dot-number">00</span>					
					<div class="price-discount-placeholder">
						<span class="strong">EUR 18.00</span>
						<span class="old-price">RSD 3.300.00</span> <span class="discount">- 23%</span>
					</div>						
				</div>					
			</div>	
		</div>
		<div class="col-md-3 col-xs-12 col-sm-12">
			<div class="text-center price-container margin-md-t-10 <?= $offset4 ?>" data-price-group="4+">		
				<?php if ($PRICE_SECTION["printCheckIcon"]) { ?>
				<div class="check-button"><i class="fa fa-check"></i></div>		
				<?php }; ?>					
				<div class="small strong">4+ <?= $lang->workshops ?></div>
				<div class="price-placeholder">
					<span class="currency">RSD</span> <span class="price-number">750</span><span class="price-dot-number">00</span> /<?= $lang->per_ws_acr ?>
				</div>		
				<div class="small"><span class="strong">EUR 5.00</span> <?= $lang->per_ws ?></div>		
			</div>
		</div>			
	</div>
	<!-- /Price section -->

	<?php } else { ?>
	<!-- Price section -->
	<div class="row price-row-placeholder <?= ($PRICE_SECTION["cart"] ? "p-cart" : "") ?> margin-t-20">			
		<div class="col-md-8 col-xs-12 col-sm-12">						 		
			<div class="price-container ord" data-price-group="1">
				<div class="padding-t-5">
					<span class="strong uppercase small"><?= $lang->whatYouGet ?></span>
				</div>
				<ul class="margin-l-20 margin-t-10 padding-b-10">
					<li><?= $lang->trait_4 ?></li>					
					<li><?= $lang->trait_1 ?></li>
					<li><?= $lang->trait_2 ?></li>
					<li><?= $lang->trait_3 ?></li>
				</ul>													
			</div>				
		</div>
		<?php if (0) { ?>
		<div class="col-md-4 col-xs-12 col-sm-12">
			<div class="text-center price-container price-full-width-container margin-md-t-10" data-price-group="2">											
				<div class="strong"><?= $lang->price_pw ?></div>
				<div class="price-placeholder">
					<span class="currency strong">RSD</span> <span class="price-number">1.100</span><span class="price-dot-number">00</span> /<?= $lang->per_ws_acr ?>
					<div class="price-discount-placeholder">						
						<!--<span class="old-price">RSD 2.200.00</span> <span class="discount">- 18%</span>-->
					</div>
					<br>
					<div class="strong small" style="background: none">EUR 9.00</div>
				</div>				
			</div>	
		</div>		
		<?php }; ?>
	</div>
	<!-- /Price section -->
	<?php }; ?>
