<?php 

	//  NEED TO BE SET
	// =================
	// [int] $likes, [object] $lang, [bool] $liked, [string] $wid

	if ($USER->lang === "EN" || $USER->lang === "GR") // For English users, determine likes
	{
		if ($likes > 0)
		{
			if ($liked)
			{
				$expostring = $lang->like_yand . " " . $likes." ".($likes>=2 ? $lang->persons : $lang->person)." ";
				switch(true)
				{
				case ($likes>=1): $expostring .= $lang->like_this; break;				
				}
			} else
			{
				$expostring = $likes." ".($likes>=2 ? $lang->persons : $lang->person)." ";
				switch(true)
				{
				case ($likes>=2): $expostring .= $lang->like_this; break;
				case ($likes === 1): $expostring .= $lang->likes_this; break;
				}
			}			
		} else
		{
			$sql = mysql_query("SELECT * FROM likes WHERE BINARY workshopID = '".$DATA->wid."'", DBC_STORE);
			if ($num = mysql_num_rows($sql)) {
				$expostring = $num." ".($num>=2 && $num<=4 ? $lang->persons : $lang->person)." ".($num===1 ? $lang->likes_this : $lang->like_this);
			} else
			{
				if ($liked)
				{
					$expostring = $lang->youLikeThis;
				} else
				{
					$expostring = $lang->beFirstToLike;
				};				
			}
		};
		
	} elseif ($USER->lang === "SR")	// For Serbian users, determine likes
	{
		if ($likes > 0)
		{
			if ($liked)
			{
				$expostring = $lang->like_yand . " " . $likes.($likes>=2 && $likes<=4 ? " ".$lang->persons : " ".$lang->person)." ";
				switch(true)
				{
				case ($likes>=2): $expostring .= $lang->like_this; break;
				case ($likes === 1): $expostring .= $lang->likes_this; break;
				}
			} else
			{
				$expostring = $likes." ".($likes>=2 && $likes<=4 ? $lang->persons : $lang->person)." ";
				switch(true)
				{
				case ($likes>=2): $expostring .= $lang->like_this; break;
				case ($likes === 1): $expostring .= $lang->loves; break;
				}
			}				
		} else
		{
			if ($liked)
			{
				$expostring = $lang->youLikeThis;
			} else
			{
				$sql = mysql_query("SELECT * FROM likes WHERE BINARY workshopID = '".$wid."'", DBC_STORE);
				if ($num = mysql_num_rows($sql)) {
					$expostring = $num." ".($num>=2 && $num<=4 ? $lang->persons : $lang->person)." ";
					switch($num)
					{
					case 1: $expostring .= $lang->loves; break;
					case 2: case 3: case 4: $expostring .= $lang->love_e; break;
					default: $expostring .= $lang->loves; break;
					}
				} else
				{
					$expostring = $lang->beFirstToLike;
				}
			};
		};
	};	

	//   OUT PARAMETERS
	// ==================
	// [string] $expostring

?>