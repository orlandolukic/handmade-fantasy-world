
"use strict";

$(document).on("click", function(e) {
	//if (!$(e.originalEvent.srcElement).parents(".ui-modal-content").length && !($(e.originalEvent.srcElement).parents(".ui-modal-other-content").length)) _mClose();
});

$(document).on("keyup", function(e) {
	if (e.keyCode === 27)
	{
		if ($(".ui-modal.active").length) _mClose();
	}
});

$(window).resize(function(e) {
	var modal = $(".ui-modal.active");
	if (modal.length)
	{
		var h_heading = modal.find(".ui-modal-heading").outerHeight(),
		    h_footer  = modal.find(".ui-modal-footer").outerHeight(),
		    h_content = modal.find(".ui-modal-content").outerHeight();
		//modal.find(".ui-modal-body").css({height: (h_content - h_heading - h_footer + 20)+"px"});
	};

	var modals = $(".ui-modal");
	if (modals.length > 0)
	{
		modals.each(function(v,m) {
			var globalObj = m;
			$(m).find(".ui-modal-other-content").each(function(t,p) {
				var image = $(this).find(".window-resize");
				if ( $(this).outerHeight() <= $(document).height()*0.5 )
				image.css({width: ($(this).height() * parseFloat(image.attr("data-aspect-ratio")))+"px"})
			});		
		});		
	};
});

function _mCloseOtherContents()
{
	$(".ui-modal .ui-modal-close-btn").on("click", function() {
		_mClose();
	});

	$(".ui-modal .ui-modal-other-content .ui-modal-other-content-close").on("click", function() {
		$(this).parents(".ui-modal-other-content").fadeOut(250);
	})
}

function loadContent(cid)
{
	this.find(".ui-modal-other-content[data-content-id='"+cid+"']").fadeIn();
}

function _mClose()
{
	/* Remove Event Listeners */
	$(".ui-modal .ui-modal-footer-success-btn, .ui-modal .ui-modal-footer-close-btn").off("click");

	$(".ui-modal.active .ui-modal-content").removeClass("ui-modal-content-show");
	$(".ui-modal.active").fadeOut(0, function() {
		$(this).removeClass("active").removeClass("ui-modal-show").find(".ui-modal-content").removeClass("ui-modal-content-show");
	})
}

function _optimise(a, b = 0)
{
	
	var content   = a.find(".ui-modal-content"),
	    h_heading = content.find(".ui-modal-heading").outerHeight(),
	    h_footer  = content.find(".ui-modal-footer").outerHeight(),
	    h_content = content.outerHeight(),
	    tek       = a;
	content.find(".ui-modal-body").css({height: (h_content - (h_heading + h_footer))+"px"});
	if (b)
	{
		a.find(".ui-modal-other-content").css({height: "40%"});
		a.find(".ui-modal-other-content .window-resize").css({width: ((a.find(".ui-modal-other-content").height() * parseFloat(a.find(".ui-modal-other-content .window-resize").attr("data-aspect-ratio")))-10)+"px"});
	};
}

function _checkClassName(clss)
{
	switch (clss)
	{
	case "medium": case "small": case "large": return clss;
	default: return "medium";
	}
}

$.fn.modalContent = function(act = "startModal", pr = null) 
{
//	Default modal's settings
	var settings  = { 
					  loading:   true,
					  class:     "medium",
					  height:    "auto",
					  scrollBody: true,
					  icon:       null,
					  top:        "auto",
					  buttons:    { success: { class: "btn-submit", icon: "check"}, close: { class: "btn-white", icon: "times" } },

					  // Functions
					  init: null,
					  main_onclick: null,
					  main_onclick_close: null,
					  exit_onclick: null,
					  exit_onclick_close: null
	                };
//	Methods
	var functions = {
		startModal : function() 
		{
			if (this.length)
			{
				// 	Optimise height of modal body
				var content   = this.find(".ui-modal-content"),
				    h_heading = content.find(".ui-modal-heading").outerHeight(),
				    h_footer  = content.find(".ui-modal-footer").outerHeight(),
				    h_content = content.height(),
				    tek       = this;

				this.find(".ui-modal-body").css({height: (h_content - h_heading - h_footer)+"px"});				
				this.find(".window-resize").css({width: (this.find(".ui-modal-other-content").height() * parseFloat(this.find(".window-resize").attr("data-aspect-ratio")))+"px"});
				this.find("[data-toggle='content']").on("click", function() {
					loadContent.call(tek, $(this).attr("data-cid"));
				});			
				_mCloseOtherContents();				

				// Put settings
				settings.class = _checkClassName(settings.class);
				if (settings.height !== "auto") { this.find(".ui-modal-content .ui-modal-body").css({height: settings.height}); } else 
				{ 
					this.find(".ui-modal-body").css({height: "auto"});
					if (!settings.top) this.find(".ui-modal-content").css({bottom: 0}); else this.find(".ui-modal-body").css({height: "40px"});
				};
				if (!settings.scrollBody) this.find(".ui-modal-body").css({overflowY: "hidden"});
				if (!settings.icon) { this.find(".ui-modal-heading-icon").css({display: "none"}); } else { this.find(".ui-modal-heading-icon i")[0].className = "fa fa-"+settings.icon  };
				if (settings.top !== "auto")
				{
					this.find(".ui-modal-content").css({bottom: "auto", top: settings.top});
				}	
				if (!settings.loading) this.find(".ui-modal-loader-placeholder").hide(); else this.find(".ui-modal-loader-placeholder").show();

				if (settings.buttons.success)
				{
					this.find(".ui-modal-footer-success-btn").addClass(settings.buttons.success.class);
					if (settings.buttons.success.icon) this.find(".ui-modal-footer-success-btn i")[0].className = "fa fa-"+settings.buttons.success.icon; else this.find(".ui-modal-footer-success-btn i").hide();
				}
				
				if (settings.buttons.close)
				{
					this.find(".ui-modal-footer-close-btn").addClass(settings.buttons.close.class);
					if (settings.buttons.close.icon) this.find(".ui-modal-footer-close-btn i")[0].className = "fa fa-"+settings.buttons.close.icon; else this.find(".ui-modal-footer-close-btn i").hide();
				}

				if (settings.main_onclick) 
					this.find(".ui-modal-footer-success-btn").on("click", function() { // Append click function to this button
						var f = settings.main_onclick.call(tek, settings);
						if (settings.main_onclick_close) functions["close"].call(tek); else { return f; }
					});

				if (settings.exit_onclick) 
					this.find(".ui-modal-footer-close-btn").on("click", function() { // Append click function to this button
						var f = settings.exit_onclick.call(tek, settings);
						if (settings.main_onclick_close) functions["close"].call(tek); else { return f; }
					});
				this.addClass("active").addClass("ui-modal-show").show();
				this.find(".ui-modal-content").show().addClass(!settings.loading ? "ui-modal-content-show-direct" : "ui-modal-content-show").addClass("ui-modal-"+settings.class);
				_optimise(this);
				if (settings.loading)
				{
					var m = this;
					var tm = setTimeout(function() {
						m.find(".ui-modal-loader-placeholder").fadeOut(550);
					}, 1200);					
				};
				if (settings.init) settings.init.call(this);
			};
		},

		close : function() 
		{
			/* Remove Event Listeners */
			this.find(".ui-modal-footer-success-btn, .ui-modal-footer-close-btn").off("click");

			this.find(".ui-modal-content").removeClass("ui-modal-content-show");
			this.fadeOut(0, function() {
				$(this).removeClass("active").removeClass("ui-modal-show").find(".ui-modal-content").removeClass("ui-modal-content-show");
			})
		}
	}; 

//	Extend default settings
	if (typeof pr === typeof {} && act === "startModal")
	{
		$.extend(settings, pr);
	};

	if (functions[act])
	{
		functions[act].call(this);
	};

//	Return current object for performing another actions
	return this;
};