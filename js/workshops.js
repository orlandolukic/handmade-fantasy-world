
o = { ws_top: $(".ws-preview").offset().top - 20 }

$(window).on("load", function(e) {
	var bw = $(document).width();
	if (bw <= 1516) $(".external-shopping-cart").addClass("dn");

	$(".link-order").on("click", function(ef) {
		var id = $(this).parents(".ws-preview-placeholder").attr("data-wid");
		var f  = $(this);
		f.find("i")[0].className = "fa fa-circle-o-notch fa-spin";
		$(".external-shopping-cart .loading").removeClass("dn");

		_SD_DB("cdws", {type: btoa("addWSToShoppingCart"), data: JSON.stringify({wid: id})}, function(t) {
			t = JSON.parse(t);
			if (t.info.response)
			{
				if (!t.info.active)
				{
					if (t.info.redirect) window.location.href = t.info.redirect_url;
				} else
				{
					if (t.data.cart_items>0)
					{						
						$(".external-shopping-cart .cart-items-number, .internal-shopping-cart .cart-items-number").html(t.data.cart_items);	
						$(".internal-shopping-cart .cart-items-number").removeClass("dn");
					} else
					{
						$(".external-shopping-cart .cart-items-number").html("-");
						$(".internal-shopping-cart .cart-items-number").addClass("dn");
					}

					$(".external-shopping-cart .checkout-sum .sum").html(t.data.cart_value);										
					
					$(".external-shopping-cart .loading").addClass("dn");					
					if (t.data.action==="delete")
					{
						f.removeClass("in-cart");
						f.find("span").html(t.data.d.add_to_cart);
						f.find("i")[0].className = "fa fa-shopping-cart";
					} else
					{
						f.addClass("in-cart");
						f.find("span").html(t.data.d.in_cart);
						f.find("i")[0].className = "fa fa-check";
					};					
				}
			}
		});
	});

	_SD_DB("cdws", {type: btoa("checkShoppingCart"), data: JSON.stringify({})}, function(g) {
		g = JSON.parse(g);
		if (g.info.response)
		{
			if (g.info.active)
			{
				if (g.data.cart_items>0)
				{
					$(".external-shopping-cart .cart-items-number, .internal-shopping-cart .cart-items-number").html(g.data.cart_items);
					$(".internal-shopping-cart .cart-items-number").removeClass("dn");
				} else
				{
					$(".external-shopping-cart .cart-items-number, .internal-shopping-cart .cart-items-number").html("-");
				}				
				$(".external-shopping-cart .checkout-sum .sum").html(g.data.cart_value);

				// Work with bought workshops
				for (var i=0, b = g.data.ws_status; i<b.length; i++)
				{
					var obj = $(".ws-preview-placeholder[data-wid='"+b[i].workshopID+"'] .link-order");
					obj.find("i")[0].className = 'fa fa-play';					
					obj.find("span").html(g.data.d.watch_video);
					obj.attr("href", b[i].url);
					obj.off("click");
				}

				for (var i=0, b = g.data.ws_cart; i<b.length; i++)
				{
					var obj = $(".ws-preview-placeholder[data-wid='"+b[i].workshopID+"'] .link-order");
					obj.find("i")[0].className = 'fa fa-check';					
					obj.addClass("in-cart");
					obj.find("span").html(g.data.d.in_cart);
				}
			}
		}
	});
});

$(window).on("scroll", function(g) {
	if ($(this).scrollTop()>o.ws_top)
	{
		$(".external-shopping-cart").addClass("active");
	} else
	{
		$(".external-shopping-cart").removeClass("active");
	}
});