<?php 

// Set access controls for certain pages
if (defined("PAGE"))
{
	switch(PAGE)
	{
	case "login":
		$CONTROLS = (object) array("admin_redirection"    => true,
		                           "define_vars"          => false,
		                           "verify_administrator" => false,
		                           "block_content"        => false,
		                           "ajax_page"			  => false);
		break;
	case "dashboard":
		$CONTROLS = (object) array("admin_redirection"    => false,
		                           "define_vars"          => true,
		                           "verify_administrator" => true,
		                           "block_content"        => true,
		                           "ajax_page"			  => false);
		break;
	case "workshops":
		$CONTROLS = (object) array("admin_redirection"    => false,
		                           "define_vars"          => true,
		                           "verify_administrator" => true,
		                           "block_content"        => true,
		                           "ajax_page"			  => false);
		break;
	case "payments":
		$CONTROLS = (object) array("admin_redirection"    => false,
		                           "define_vars"          => true,
		                           "verify_administrator" => true,
		                           "block_content"        => true,
		                           "ajax_page"			  => false);
		break;	
	case "banners":
		$CONTROLS = (object) array("admin_redirection"    => false,
		                           "define_vars"          => true,
		                           "verify_administrator" => true,
		                           "block_content"        => true,
		                           "ajax_page"			  => false);
		break;	
	// AJAX	
	case "control":
		$CONTROLS = (object) array("admin_redirection"    => false,
		                           "define_vars"          => true,
		                           "verify_administrator" => true,
		                           "block_content"        => true,
		                           "ajax_page"			  => true);
		break;		

	case "pull":
	$CONTROLS = (object) array("admin_redirection"    => false,
	                           "define_vars"          => true,
	                           "verify_administrator" => true,
	                           "block_content"        => true,
	                           "ajax_page"			  => true);
		break;		
	default:
		$CONTROLS = NULL;
	}
} else
{
	$CONTROLS = NULL;
}

?>