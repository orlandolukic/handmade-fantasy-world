<div class="sidebar" data-color="blue" data-image="assets/img/sidebar-6.JPG">
	<div class="sidebar-wrapper">
        <div class="logo">
            <a href="http://www.creative-tim.com" class="simple-text">
               Admin Panel
            </a>
        </div>

        <ul class="nav">
            <li <?= (PAGE == "dashboard" ? "class=\"active\"" : "") ?> >
                <a href="<?= FILE ?>dashboard">
                    <i class="fa fa-dashboard"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li <?= (PAGE == "banners" ? "class=\"active\"" : "") ?>>
                <a href="<?= FILE ?>banners">
                    <i class="fa fa-sticky-note"></i>
                    <p>Banners Settings</p>
                </a>
            </li>
            <li <?= (PAGE == "workshops" ? "class=\"active\"" : "") ?>>
                <a href="<?= FILE ?>workshops">
                    <i class="fa fa-paint-brush"></i>
                    <p>Workshops</p>
                </a>
            </li>
             <li <?= (PAGE == "cookies" ? "class=\"active\"" : "") ?>>
                <a href="<?= FILE ?>cookies">
                    <i class="fa fa-birthday-cake"></i>
                    <p>Cookies</p>
                </a>
            </li>                                              
            <li <?= (PAGE == "payments" ? "class=\"active\"" : "") ?>>
                <a href="<?= FILE ?>payments">
                    <i class="fa fa-shopping-cart"></i>
                    <p>Payments</p>
                </a>
            </li>
            <li <?= (PAGE == "subscriptions" ? "class=\"active\"" : "") ?>>
                <a href="<?= FILE ?>subscriptions">
                    <i class="fa fa-list"></i>
                    <p>All Subscriptions</p>
                </a>
            </li>
            <li <?= (PAGE == "users" ? "class=\"active\"" : "") ?>>
                <a href="<?= FILE ?>users">
                    <i class="fa fa-users"></i>
                    <p>List of Users</p>
                </a>
            </li> 
            <li <?= (PAGE == "ingredients" ? "class=\"active\"" : "") ?>>
                <a href="<?= FILE ?>ingredients">
                    <i class="fa fa-code-fork"></i>
                    <p>Ingredients</p>
                </a>
            </li>
            <li <?= (PAGE == "events" ? "class=\"active\"" : "") ?>> 
                <a href="<?= FILE ?>events">
                    <i class="fa fa-calendar"></i>
                    <p>Events</p>
                </a>
            </li>
            <li <?= (PAGE == "links" ? "class=\"active\"" : "") ?>> 
                <a href="<?= FILE ?>links">
                    <i class="fa fa-external-link"></i>
                    <p>Buy Links</p>
                </a>
            </li>
            <li <?= (PAGE == "coupons" ? "class=\"active\"" : "") ?>> 
                <a href="<?= FILE ?>coupons">
                    <i class="fa fa-ticket"></i>
                    <p>Coupons</p>
                </a>
            </li>
            <li <?= (PAGE == "reviews" ? "class=\"active\"" : "") ?>> 
                <a href="<?= FILE ?>reviews">
                    <i class="fa fa-commenting"></i>
                    <p>Reviews</p>
                </a>
            </li>
            <li <?= (PAGE == "news" ? "class=\"active\"" : "") ?>> 
                <a href="<?= FILE ?>news">
                    <i class="fa fa-bullhorn"></i>
                    <p>News</p>
                </a>
            </li>
            <li <?= (PAGE == "manufactures" ? "class=\"active\"" : "") ?>> 
                <a href="<?= FILE ?>manufactures">
                    <i class="fa fa-industry"></i>
                    <p>Manufacturers</p>
                </a>
            </li>				
        </ul>
	</div>
</div>