<?php 

//	 Include only if $_SESSION['hfw_admin'] && $ADMIN is active
// ======================================================

//  Check if access_token is the same
	if ($_SESSION['token'] !== $ADMIN->access_token) 
	{
		// Logout user based on access_token
		//$URL = $FILE.($USER->lang !== "EN" ? strtolower($USER->lang)."/" : "")."login?security=token";
		// logout.php?type=access_token&redirect=".urlencode($URL)."&session_name=security&session_value=token
		if ($CONTROLS && isset($CONTROLS->ajax_page) && !$CONTROLS->ajax_page) 
		{ 
			header("location: ".FILE."logout"); 
			exit(); 
			
		} elseif ($CONTROLS && isset($CONTROLS->ajax_page) && $CONTROLS->ajax_page) 
		{
			echo json_encode(array("info" => array("response" => false, "redirect" => true, "href" => FILE."logout")));
			exit(); 
		}
	};

	// Check Session Timeout
	if (time() > intval($ADMIN->timestamp)+30*60)
	{
		//$URL = $FILE.($USER->lang !== "EN" ? strtolower($USER->lang)."/" : "")."login?security=session_expired";
		//header("location: ".$FILE."user/logout.php?type=session_expired&redirect=".urlencode($URL)."&session_name=security&session_value=session_expired");
		if ($CONTROLS && isset($CONTROLS->ajax_page) && !$CONTROLS->ajax_page) 
		{ 
			header("location: ".FILE."logout"); 
			exit(); 
			
		} elseif ($CONTROLS && isset($CONTROLS->ajax_page) && $CONTROLS->ajax_page) 
		{
			echo json_encode(array("info" => array("response" => false, "redirect" => true, "href" => FILE."logout")));
			exit(); 
		}
	} else
	{
		updateTimestamp();		
	};

?>