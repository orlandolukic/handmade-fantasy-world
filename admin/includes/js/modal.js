
"use strict";
$(document).ready(function() {
	var modals = $(".ui-modal");
	modals.each(function(i,v) {
		var content = $(v).find(".ui-modal-content"),
		    h_heading = content.find(".ui-modal-heading").outerHeight(),
		    h_footer  = content.find(".ui-modal-footer").outerHeight(),
		    h_content = content.height(),
		    tek       = $(v);
		content.find(".ui-modal-body").css({height: (h_content - (h_heading + h_footer))+"px"});
		$(v).find(".window-resize").css({width: ($(v).find(".ui-modal-other-content").height() * parseFloat($(v).find(".window-resize").attr("data-aspect-ratio")))+"px"});
		$(v).find("[data-toggle='content']").on("click", function() {
			loadContent.call(tek, $(this).attr("data-cid"));
		})
	});

	_mCloseOtherContents();
});

$(document).on("click", function(e) {
	if (!$(e.originalEvent.srcElement).parents(".ui-modal-content").length && !($(e.originalEvent.srcElement).parents(".ui-modal-other-content").length)) _mClose();
});

$(document).on("keyup", function(e) {
	if (e.keyCode === 27)
	{
		if ($(".ui-modal.active").length) _mClose();
	}
});

$(window).resize(function(e) {
	var modal = $(".ui-modal.active");
	if (modal.length)
	{
		var h_heading = modal.find(".ui-modal-heading").outerHeight(),
		    h_footer  = modal.find(".ui-modal-footer").outerHeight(),
		    h_content = modal.find(".ui-modal-content").outerHeight();
		modal.find(".ui-modal-body").css({height: (h_content - h_heading - h_footer)+"px"});
	};

	var modals = $(".ui-modal");
	if (modals.length > 0)
	{
		modals.each(function(v,m) {
			var globalObj = m;
			$(m).find(".ui-modal-other-content").each(function(t,p) {
				var image = $(this).find(".window-resize");
				if ( $(this).outerHeight() <= $(document).height()*0.5 )
				image.css({width: ($(this).height() * parseFloat(image.attr("data-aspect-ratio")))+"px"})
			});		
		});
		
	}
});

function _mCloseOtherContents()
{
	$(".ui-modal .ui-modal-close-btn").on("click", function() {
		_mClose();
	});

	$(".ui-modal .ui-modal-other-content .ui-modal-other-content-close").on("click", function() {
		$(this).parents(".ui-modal-other-content").fadeOut(250);
	})
}

function loadContent(cid)
{
	this.find(".ui-modal-other-content[data-content-id='"+cid+"']").fadeIn();
}

function _mClose()
{
	$(".ui-modal.active .ui-modal-content").removeClass("ui-modal-content-show");
	$(".ui-modal.active").fadeOut(0, function() {
		$(this).removeClass("active").removeClass("ui-modal-show").find(".ui-modal-content").removeClass("ui-modal-content-show");
	})
}

function startModal(q)
{
	var l = $(".ui-modal[data-modal-id='"+q+"']");
	if (l.length)
	{
		$(window).resize();
		l.addClass("active").addClass("ui-modal-show").show();
		l.find(".ui-modal-content").addClass("ui-modal-content-show");
		_optimise(l);
	}
}

function _optimise(a)
{
	
	var content =  a.find(".ui-modal-content"),
	    h_heading = content.find(".ui-modal-heading").outerHeight(),
	    h_footer  = content.find(".ui-modal-footer").outerHeight(),
	    h_content = content.height(),
	    tek       = a;
	content.find(".ui-modal-body").css({height: (h_content - (h_heading + h_footer))+"px"});
	a.find(".ui-modal-other-content").css({height: "40%"});
	a.find(".ui-modal-other-content .window-resize").css({width: ((a.find(".ui-modal-other-content").height() * parseFloat(a.find(".ui-modal-other-content .window-resize").attr("data-aspect-ratio")))-10)+"px"});


}