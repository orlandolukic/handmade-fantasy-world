$(document).on("ready",function(e)
{


	$("#inputPicture").on("change", function() {
	 	if (this.value !== "")
	 	{
	 		$("#workshopPictureFORM").submit();	 		
	 	}
	 });


	$("#workshopPictureFORM").on("submit", function(e) {
		e.preventDefault();	
		var obj = $(this);
		var formDATA = new FormData();
		formDATA.append('req', btoa("ws-upload-image"));
		formDATA.append('workshopID', $(this).attr("data-workshop-ID"));
		formDATA.append('picture', $(this).find("#inputPicture")[0].files[0]);

 		_xhr_upload(formDATA, function(r) {	
 			if (r.info.response)
 			{
 				if (r.info.uploaded) 
 				{ 		
 					if(!r.info.message.error){

 						$(".all-pictures").find("div.no-pictures").hide();

	 					$(".all-pictures").append(r.data.image_placeholder);

	 					var newImage = $(".all-pictures").find("div[data-imageID='"+r.data.imageID+"']");

	 					newImage.find("img.edit-image").on("click", function() {
	 						ws_change_image.call($(this));
	 					});

	 					newImageChanges = newImage.find("div.cancel-placeholder div.tooltip-placement");
	 					newImageChanges.tooltip();

	 					newImage.find("div.cancel-placeholder").on("click", function() {
	 						ws_delete_image.call( $(this) );
	 					});

	 					if( $(".all-pictures").find("div.edit-ws-image-placeholder").length == 1 ){
	 						$(".all-pictures").find("div.edit-ws-image-placeholder").addClass("is-active");
	 					}

	 					$.notify({
				        	icon: "fa fa-check",
				        	message: r.info.message.messageText
				        },{
				            type: r.info.message.messageType,
				            timer: 4000,
				            placement: {
				                from: "bottom",
				                align: "left"
				            }
				        });
 					} else {
 						$.notify({
				        	icon: "fa fa-times",
				        	message: r.info.message.messageText
				        },{
				            type: r.info.message.messageType,
				            timer: 4000,
				            placement: {
				                from: "bottom",
				                align: "left"
				            }
				        });
 					}
 				} else
 				{
 					$.notify({
				        	icon: "fa fa-check",
				        	message: r.info.message.messageText
				        },{
				            type: r.info.message.messageType,
				            timer: 4000,
				            placement: {
				                from: "bottom",
				                align: "left"
				            }
				    });
 				}
 			}
 		}, function(f) { console.log(f); }); 	 	

	});



	
	/* Max 40 letters in WS_Heading and if length==0 */
	eval($("script[data-main-script='true']").attr("data-languages"));
	var keyd_short = ["SR", "EN", "PL", "RU", "GR"]; 
	var keyd_obj   = ["ws-heading-serbian", "ws-heading-english", "ws-heading-polish", "ws-heading-russian", "ws-heading-greek"]; 
	var maxLength = 39;
	for (var i=0; i<keyd_obj.length; i++)
	{
		if (languages[keyd_short[i]])
		{
			(function(param) {
				$("."+keyd_obj[param]).keydown(function(e) {
					var obj = $(this);
					var count = obj.siblings(".count-length");
					var length = obj.val().trim().length;
					length = maxLength - length;

					if(length<0){
						if (e.keyCode !== 8)
						{
							e.preventDefault();
							return false;
						}
					}else{
						count.text(length);
					}
				});

				$("."+keyd_obj[param]).keyup(function(e) {
					var obj = $(this);
					var count = obj.siblings(".count-length");
					var length = obj.val().trim().length;
					length = maxLength - length;

					if( obj.val().trim().length === 0 ){
						obj.addClass("edit-ws-textCounter");
					}else
					{
						obj.removeClass("edit-ws-textCounter");
					}

				});

			})(i);
		};
	}

	/* WS_Subheading and if length==0 */
	var keyd_short1 = ["SR", "EN", "PL", "RU", "GR"]; 
	var keyd_obj1   = ["ws-subheading-serbian", "ws-subheading-english", "ws-subheading-polish", "ws-subheading-russian", "ws-subheading-greek"]; 
	
	for (var i=0; i<keyd_obj1.length; i++)
	{
		if (languages[keyd_short1[i]])
		{
			(function(param) {
				$("."+keyd_obj1[param]).keyup(function(e) {
					var obj = $(this);

					if(obj.text().trim().length === 0){
						obj.addClass("edit-ws-textCounter");
					}else
					{
						obj.removeClass("edit-ws-textCounter");
					}

				});
			})(i);
		};
	}

	/* WS_Text and if length==0 */
	var keyd_short1 = ["SR", "EN", "PL", "RU", "GR"]; 
	var keyd_obj1   = ["ws-text-serbian", "ws-text-english", "ws-text-polish", "ws-text-russian", "ws-text-greek"]; 
	
	for (var i=0; i<keyd_obj1.length; i++)
	{
		if (languages[keyd_short1[i]])
		{
			(function(param) {
				$("."+keyd_obj1[param]).keyup(function(e) {
					var obj = $(this);

					if(obj.text().trim().length === 0){
						obj.addClass("edit-ws-textCounter");
					}else
					{
						obj.removeClass("edit-ws-textCounter");
					}

				});
			})(i);
		};
	}




	/*EDIT - CHANGE MAIN WORKSHOP IMAGE */
	$(".edit-ws-image-placeholder img.edit-image").on("click",function(e){
		ws_change_image.call($(this));	
	});

	/*EDIT - DELETE WORKSHOP IMAGE */
	$(".edit-ws-image-placeholder .cancel-placeholder").on("click",function(e){
		ws_delete_image.call($(this));
	});	

	/*ACTIVATE WORKSHOP */
	$("div.activate-placeholder span.Activate").on("click", function(e) {
		var obj = $(this);
		var wID = obj.parents(".activate-placeholder").attr("ws-ID");
		_xhr("control",{ req: btoa("activate_WS"), workshopID: wID }, function (resp) {
				if(resp.info.response){
					$.notify({
			        	icon: "fa fa-check",
			        	message: obj.parents(".activate-placeholder").attr("activate-message")
			        },{
			            type: "success",
			            timer: 4000,
			            placement: {
			                from: "bottom",
			                align: "left"
			            }
			        });
			        
					/* Dugme */
					obj.parents(".activate-placeholder").addClass("disabled");
					obj.parents(".activate-placeholder").addClass("ff");

					/* icon-e */
					obj.siblings(".i-Active").removeClass("hover");
					obj.siblings(".i-Active").addClass("non-hover");

					obj.siblings(".i-Inactive").removeClass("non-hover");
					obj.siblings(".i-Inactive").addClass("hover");

					/* Spanovi */
					obj.siblings(".Deactivate").addClass("hover");
					obj.siblings(".Deactivate").removeClass("non-hover");

					obj.addClass("dn");

					obj.siblings(".Activated").addClass("non-hover");
					obj.siblings(".Activated").removeClass("dn");
					
				}
			}) 

			

	});

	/*DEACTIVATE WORKSHOP */
	$("div.activate-placeholder span.Deactivate").on("click", function(e) {
		var obj = $(this);
		var wID = obj.parents(".activate-placeholder").attr("ws-ID");
		_xhr("control",{ req: btoa("deactivate_WS"), workshopID: wID }, function (resp) {
				if(resp.info.response){
					$.notify({
			        	icon: "fa fa-check",
			        	message: obj.parents(".activate-placeholder").attr("deactivate-message")
			        },{
			            type: "success",
			            timer: 4000,
			            placement: {
			                from: "bottom",
			                align: "left"
			            }
			        });
				/* Dugme */
				obj.parents(".activate-placeholder").removeClass("disabled");
				obj.parents(".activate-placeholder").removeClass("ff");

				/* icon-e */
				obj.siblings(".i-Active").addClass("hover");
				obj.siblings(".i-Active").removeClass("non-hover");

				obj.siblings(".i-Inactive").addClass("non-hover");
				obj.siblings(".i-Inactive").removeClass("hover");
				obj.siblings(".i-Inactive").addClass("dn");

				/* Spanovi */
				obj.addClass("non-hover");
				obj.removeClass("hover");

				obj.siblings(".Activate").removeClass("dn");

				obj.siblings(".Activated").addClass("dn");
				obj.siblings(".Activated").removeClass("non-hover");					
				}
			}) 	
	});



	/* EDIT - UPDATE WORKSHOP */
	$("#update_WS").on("click",function(e){
		var obj = $(this);

		var wID = obj.attr("update-workshop-ID");


		/* Headings */ 
		var heading_SR = ( $(".ws-heading-serbian").val() && $(".ws-heading-serbian").val().trim().length ? $(".ws-heading-serbian").val() : -1 ); 
		var heading_EN = ( $(".ws-heading-english").val() && $(".ws-heading-english").val().trim().length ? $(".ws-heading-english").val() : -1 );
		var heading_PL = ( $(".ws-heading-polish").val() && $(".ws-heading-polish").val().trim().length ? $(".ws-heading-polish").val() : -1 );
		var heading_RU = ( $(".ws-heading-russian").val() && $(".ws-heading-russian").val().trim().length ? $(".ws-heading-russian").val() : -1 );
		var heading_GR = ( $(".ws-heading-greek").val() && $(".ws-heading-greek").val().trim().length ? $(".ws-heading-greek").val() : -1 );

		/* Subheadings */ 
		var subheading_SR = $(".ws-subheading-serbian").html() && $(".ws-subheading-serbian").html().trim().length ? $(".ws-subheading-serbian").html() : -1;
		var subheading_EN = $(".ws-subheading-english").html() && $(".ws-subheading-english").html().trim().length ? $(".ws-subheading-english").html() : -1;
		var subheading_PL = $(".ws-subheading-polish").html() && $(".ws-subheading-polish").html().trim().length ? $(".ws-subheading-polish").html() : -1;
		var subheading_RU = $(".ws-subheading-russian").html() && $(".ws-subheading-russian").html().trim().length ? $(".ws-subheading-russian").html() : -1;
		var subheading_GR = $(".ws-subheading-greek").html() && $(".ws-subheading-greek").html().trim().length ? $(".ws-subheading-greek").html() : -1;

		/* Text */ 
		var text_SR = $(".ws-text-serbian").html() && $(".ws-text-serbian").html().trim().length ? $(".ws-text-serbian").html() : -1;
		var text_EN = $(".ws-text-english").html() && $(".ws-text-english").html().trim().length ? $(".ws-text-english").html() : -1;
		var text_PL = $(".ws-text-polish").html() && $(".ws-text-polish").html().trim().length ? $(".ws-text-polish").html() : -1;
		var text_RU = $(".ws-text-russian").html() && $(".ws-text-russian").html().trim().length ? $(".ws-text-russian").html() : -1;
		var text_GR = $(".ws-text-greek").html() && $(".ws-text-greek").html().trim().length ? $(".ws-text-greek").html() : -1;

		/* Narrators */
		var narratorID = $(".narators").val().length ? $(".narators").val() : -1;

		if( $(".ws-heading-serbian").hasClass("edit-ws-textCounter") || 
			$(".ws-heading-english").hasClass("edit-ws-textCounter") ||
			$(".ws-heading-polish").hasClass("edit-ws-textCounter") ||
			$(".ws-heading-russian").hasClass("edit-ws-textCounter") ||
			$(".ws-heading-greek").hasClass("edit-ws-textCounter") ||

			$(".ws-subheading-serbian").hasClass("edit-ws-textCounter") || 
			$(".ws-subheading-english").hasClass("edit-ws-textCounter") ||
			$(".ws-subheading-polish").hasClass("edit-ws-textCounter") ||
			$(".ws-subheading-russian").hasClass("edit-ws-textCounter") ||
			$(".ws-subheading-greek").hasClass("edit-ws-textCounter") ||

			$(".ws-text-serbian").hasClass("edit-ws-textCounter") || 
			$(".ws-text-english").hasClass("edit-ws-textCounter") ||
			$(".ws-text-polish").hasClass("edit-ws-textCounter") ||
			$(".ws-text-russian").hasClass("edit-ws-textCounter") ||
			$(".ws-text-greek").hasClass("edit-ws-textCounter") ) {

			$.notify({
	        	icon: "fa fa-check",
	        	message: obj.attr("update-empty-text-message")
	        },{
	            type: "danger",
	            timer: 4000,
	            placement: {
	                from: "bottom",
	                align: "left"
	            }
	        });

		return false;

		}	


		/* Ostalo */
		var videoID = $(".video-ID").val() ? $(".video-ID").val() : -1;
		if($("div.beginning-subtitles input").prop("checked")) {var subtitles = true;}  else {var subtitles = false;}

		_xhr("control",{ req: btoa("update_Workshop"), 
			workshop: JSON.stringify({ workshopID: wID , heading_SR: heading_SR, heading_EN: heading_EN, heading_PL: heading_PL,
			heading_RU: heading_RU, heading_GR: heading_GR, subheading_SR: subheading_SR, subheading_EN: subheading_EN, subheading_PL: subheading_PL,
			subheading_RU: subheading_RU, subheading_GR: subheading_GR, text_SR: text_SR, text_EN: text_EN, text_PL: text_PL,
			text_RU: text_RU, text_GR: text_GR, video_id: videoID, mssg_subtitle: subtitles, narratorID : narratorID}) }, function (resp) {
				if(resp.info.response){
					$.notify({
			        	icon: "fa fa-check",
			        	message: obj.attr("update-success-message")
			        },{
			            type: "success",
			            timer: 4000,
			            placement: {
			                from: "bottom",
			                align: "left"
			            }
			        });
				}else{
					$.notify({
			        	icon: "fa fa-check",
			        	message: obj.attr("update-failure-message")
			        },{
			            type: "danger",
			            timer: 4000,
			            placement: {
			                from: "bottom",
			                align: "left"
			            }
			        });
				}
			}) 
					
	});	

})



function ws_delete_image() {
		var obj = this;
		var imageID = obj.parents(".edit-ws-image-placeholder").find("img.edit-image").attr("data-image");
		var wID = obj.parents(".edit-ws-image-placeholder").find("img.edit-image").attr("data-wsID");

		$("div[data-modal-id='ws-image-delete']").modalContent("startModal", {
			loading: true, 
			height: "100px", 
			scrollBody: false, 
			icon: "trash", 
			top: "40px", 
			buttons: { 
				success: { class: "btn-red-danger", icon: "trash" },
				close:   { class: "btn-white", icon: "times" } 
			},
			main_onclick_close: true,
			main_onclick: function(s) {	
							
				_xhr("control",{ req: btoa("delete_Image"), imageID: imageID, workshopID: wID }, function (resp) {
				if(resp.info.response){
					$.notify({
			        	icon: "fa fa-check",
			        	message: obj.parents(".all-pictures").attr("data-delete-image")
			        },{
			            type: "success",
			            timer: 4000,
			            placement: {
			                from: "bottom",
			                align: "left"
			            }
			        });
			        $("div.edit-ws-image-placeholder img[data-image='"+resp.data.imageID+"']").parents(".edit-ws-image-placeholder").addClass("is-active");
					obj.off("click");
					obj.parents(".edit-ws-image-placeholder").fadeOut(650,function() { $(this).remove(); })
					
				}else
				{
					$.notify({
			        	icon: "fa fa-times",
			        	message: obj.parents(".all-pictures").attr("data-image-false")
			        },{
			            type: "danger",
			            timer: 4000,
			            placement: {
			                from: "bottom",
			                align: "left"
			            }
			        });
				}
			})
				return true;
			}
		});
}


function ws_change_image(){
	var obj = this;
		if(obj.parents(".edit-ws-image-placeholder").hasClass("is-active")) return false;
		else{
			_xhr("control",{ req: btoa("change_Image"), imageID: obj.attr("data-image"), workshopID: obj.attr("data-wsID") }, function (resp) {
				if(resp.info.response){
					$.notify({
			        	icon: "fa fa-check",
			        	message: obj.parents(".all-pictures").attr("data-image-message")
			        },{
			            type: "success",
			            timer: 4000,
			            placement: {
			                from: "bottom",
			                align: "left"
			            }
			        });

			        obj.parents(".all-pictures").find(".edit-ws-image-placeholder").removeClass("is-active");
					obj.parents(".edit-ws-image-placeholder").addClass("is-active");
				}
			}) 
			
		};		
}
