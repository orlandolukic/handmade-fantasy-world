
<div class="card">
	<div class="container-fluid">
		<div class="row has-divider">
			<div class="col-md-6 col-xs-12 col-sm-12" style="padding: 0">
				<div class="header">
					<h4 class="title"><i class="fa fa-edit"></i> Edit banner</h4>
					<div class="category">Set banner preferences</div>					
				</div>		
			</div>
			<div class="col-md-6 col-xs-12 col-sm-12 md-text-center text-right">
				<div class="padding-20 div-inl">
					<a href="<?= FILE ?>banners">
						<div class="btn-white btn-base"><i class="fa fa-chevron-left"></i> All Banners</div>
					</a>
					<div class="margin-l-10 margin-md-t-10">
						<div class="btn-submit btn-base"><i class="fa fa-check"></i> Save settings</div>					
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="content" style="padding-top: 0">
		<div class="padding-b-20">
			<div class="container-fluid">
				<?php if ($BANNER->c_editor) { ?>
				<div class="row">
					<div class="col-md-12 col-xs-12 col-sm-12" style="padding: 0">
						<div class="small">
							<div class="category margin-t-20" style="font-size: inherit;">
								<i class="fa fa-refresh"></i> Last change was made by <span class="strong"><?= ($BANNER->c_editor !== $ADMIN->username ? $BANNER->c_editor_name : "you" ) ?></span> at <?= make_time($BANNER->c_editor_timestamp); ?>h, <?= make_date(-1, date("Y-m-d", $BANNER->c_editor_timestamp)) ?>
							</div>	
						</div>
					</div>
				</div>
				<?php }; ?>

				<div class="row heading-section" data-empty-text="(No heading)">
					<div class="col-md-4 col-xs-12 col-sm-12">
						<div class="row">
							<div class="col-md-12 col-xs-12 col-sm-12 padding-l-none">
								<h3><i class="fa fa-header"></i> <span class="padding-l-10">Heading</span></h3>
								<div class="category">Personalize title for this banner</div>
							</div>
						</div>
						
						<?php if ($LANGUAGE->serbian) { ?>
						<div class="row padding-t-20">
							<div class="col-md-12 col-xs-12 col-sm-12 padding-l-none">
								<h6>Heading Serbian</h6>
								<div class="<?= ($ADMIN->lang === "SR" ? "active-heading" : "") ?> padding-t-10">
									<input type="text" class="form-control <?= ($ADMIN->lang==="SR" ? "active-heading-input" : "") ?>" value="<?= $BANNER->heading_SR ?>" placeholder="Type Banner heading">						
									<?php if ($ADMIN->lang === "SR") { ?>		
									<div class="text-right<?= ($ADMIN->lang === "SR" ? " active-heading-check" : "") ?>">
										<i class="fa fa-eye"  data-placement="top" data-toggle="tooltip" title="On preview"></i>
									</div>						
									<?php }; ?>						
								</div>
							</div>				
						</div>
						<?php }; ?>

						<?php if ($LANGUAGE->english) { ?>
						<div class="row margin-t-20">
							<div class="col-md-12 col-xs-12 col-sm-12 padding-l-none">
								<h6>Heading English</h6>
								<div class="<?= ($ADMIN->lang === "EN" ? "active-heading" : "") ?> padding-t-10">
									<input type="text" class="form-control <?= ($ADMIN->lang==="EN" ? "active-heading-input" : "") ?>" value="<?= $BANNER->heading_EN ?>" placeholder="Type Banner heading">					
									<?php if ($ADMIN->lang === "EN") { ?>		
									<div class="text-right<?= ($ADMIN->lang === "EN" ? " active-heading-check" : "") ?>">
										<i class="fa fa-eye"  data-placement="top" data-toggle="tooltip" title="On preview"></i>
									</div>						
									<?php }; ?>					
								</div>
							</div>
						</div>
						<?php }; ?>

						<?php if ($LANGUAGE->russian) { ?>
						<div class="row margin-t-20">
							<div class="col-md-12 col-xs-12 col-sm-12 padding-l-none">
								<h6>Heading Russian</h6>
								<div class="<?= ($ADMIN->lang === "RU" ? "active-heading" : "") ?> padding-t-10">
									<input type="text" class="form-control <?= ($ADMIN->lang==="RU" ? "active-heading-input" : "") ?>" value="<?= $BANNER->heading_RU ?>" placeholder="Type Banner heading">	
									<?php if ($ADMIN->lang === "RU") { ?>		
									<div class="text-right<?= ($ADMIN->lang === "RU" ? " active-heading-check" : "") ?>">
										<i class="fa fa-eye"  data-placement="top" data-toggle="tooltip" title="On preview"></i>
									</div>						
									<?php }; ?>																		
								</div>
							</div>
						</div>
						<?php }; ?>

						<?php if ($LANGUAGE->polish) { ?>
						<div class="row margin-t-20">
							<div class="col-md-12 col-xs-12 col-sm-12 padding-l-none">
								<h6>Heading Polish</h6>
								<div class="<?= ($ADMIN->lang === "PL" ? "active-heading" : "") ?> padding-t-10">
									<input type="text" class="form-control <?= ($ADMIN->lang==="PL" ? "active-heading-input" : "") ?>" value="<?= $BANNER->heading_PL ?>" placeholder="Type Banner heading">					
									<?php if ($ADMIN->lang === "PL") { ?>		
									<div class="text-right<?= ($ADMIN->lang === "PL" ? " active-heading-check" : "") ?>">
										<i class="fa fa-eye"  data-placement="top" data-toggle="tooltip" title="On preview"></i>
									</div>						
									<?php }; ?>					
								</div>
							</div>
						</div>
						<?php }; ?>

						<?php if ($LANGUAGE->greek) { ?>
						<div class="row margin-t-20">
							<div class="col-md-12 col-xs-12 col-sm-12 padding-l-none">
								<h6>Heading Greek</h6>
								<div class="<?= ($ADMIN->lang === "GR" ? "active-heading" : "") ?> padding-t-10">
									<input type="text" class="form-control <?= ($ADMIN->lang==="GR" ? "active-heading-input" : "") ?>" value="<?= $BANNER->heading_GR ?>" placeholder="Type Banner heading">			
									<?php if ($ADMIN->lang === "GR") { ?>		
									<div class="text-right<?= ($ADMIN->lang === "GR" ? " active-heading-check" : "") ?>">
										<i class="fa fa-eye"  data-placement="top" data-toggle="tooltip" title="On preview"></i>
									</div>						
									<?php }; ?>
								</div>
							</div>
						</div>
						<?php }; ?>

						<!-- Icon  -->
						<div class="row margin-t-20">
							<div class="col-md-12 col-xs-12 col-sm-12 padding-l-none">
								<h3><i class="fa fa-object-ungroup"></i> <span class="padding-l-20">Heading icon</span></h3>
								<div class="category">Choose heading icon</div>
							</div>
						</div>				
						<div class="row margin-t-10">
							<div class="col-md-12 col-xs-12 col-sm-12 padding-l-none">
								<div class="text-overlay-placeholder">
									<div class="content-placeholder has-divider">
										<label class="checkbox pointer" style="margin: 0">
		                                    <span class="icons"><span class="first-icon fa fa-square-o"></span><span class="second-icon fa fa-check-square-o"></span></span><input class="banner-heading-icon-checkbox" type="checkbox" value="" data-toggle="checkbox" <?= ($BANNER->heading_icon ? "checked" : ""); ?>> Heading icon
		                                </label>
									</div>
									<div class="content-placeholder container-fluid banner-content-heading-icon" <?= (!$BANNER->heading_icon ? "style=\"opacity: 0.2; pointer-events: none;\"" : "") ?>>
										<div class="col-md-3 col-xs-4 col-sm-4 padding-l-none">
											<div class="current-heading-icon-status" data-af-text="Current icon" data-dcl-text="No icon"><?= ($BANNER->heading_icon ? "Current icon" : "No icon"); ?></div>
											<div class="current-heading-icon">
												<?php if ($BANNER->heading_icon) { ?>
												<i class="fa <?= $BANNER->heading_icon ?> fa-2x"></i>
												<?php } else { ?>
												<i class="fa fa-ban fa-2x" style="opacity: 0.6"></i>
												<?php }; ?>
											</div>
										</div>
										<div class="col-md-8 col-xs-8 col-sm-8 padding-t-10">
											<div class="btn-submit banner-btn-heading-icon btn-base" data-heading-icon="<?= $BANNER->heading_icon ?>">
												<i class="fa fa-search"></i> Search icons
											</div>
										</div>										
									</div>
								</div>								
							</div>
						</div>

						<!-- Text bellow banner -->
						<div class="row margin-t-20">
							<div class="col-md-12 col-xs-12 col-sm-12 padding-l-none">
								<h3><i class="fa fa-edit"></i> <span class="padding-l-10">Text</span></h3>
								<div class="category">Text bellow image</div>
							</div>
						</div>
						<div class="row margin-t-20">
							<div class="col-md-12 col-xs-12 col-sm-12 padding-l-none">
								<label class="checkbox pointer" style="margin: 0">
                                    <span class="icons"><span class="first-icon fa fa-square-o"></span><span class="second-icon fa fa-check-square-o"></span></span>
                                    <input type="checkbox" class="banner-subtext-check" value="" data-toggle="checkbox" <?= ($BANNER->date ? "checked" : ""); ?>> Date info
                                </label>
							</div>
						</div>
						<div class="row margin-t-20 subtext-section" data-empty-text="(No date/text)">
							<div class="col-md-12 col-xs-12 col-sm-12 padding-l-none">
								<input type="text" class="form-control banner-subtext <?= (!$BANNER->date ? "disabled" : "") ?>" placeholder="e.i. 27.03.2017" value="<?= $BANNER->date ?>" >
							</div>
						</div>

						

					</div>
					<div class="col-md-5 col-xs-12 col-sm-12">

						<div class="row">
							<div class="col-md-12 col-xs-12 col-sm-12">
								<h3><i class="fa fa-camera"></i> <span class="padding-l-10">Background image</span></h3>
								<div class="category">Choose banner background</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 col-xs-12 col-sm-12 margin-t-10">
								<div class="text-overlay-placeholder">								
									<div class="content-placeholder has-divider">
										<div class="pull-right">
	                                			<div class="uploaded-new-image uni-banner-background dn">+ 1 image</div>
	                                		</div>
                                		<div class="pull-right banner-background-loader dn">
                               	    		<div class="t-loader"></div> Uploading...
                               	    	</div>
	                               	    <div><i class="fa fa-upload"></i> Choose different picture</div>
	                               	    <form id="bannerBackgroundUploadForm" method="POST" enctype="multipart/form-data" data-error-upload="An error occured during upload. Please try again.">
		                               	    <div class="margin-t-10 div-inl">
		                               	    	<div class="ui-search-images btn-submit btn-base"><i class="fa fa-search"></i> Search from Website</div>
		                               	    	<div class="padding-l-10 padding-r-10">or</div>
		                               	    	<div class="btn-white btn-base btn-upload">
		                               	    		<i class="fa fa-upload"></i> Upload from Your device
		                               	    		<input type="file" name="picture">
		                               	    	</div>	                               	    	
		                               	    </div>
	                               	    </form>
	                               	    <?php if ($BANNER->slide) { ?>
	                               	    <div class="margin-t-20 small">
	                               	    	<div class="category" style="font-size: inherit"><i class="fa fa-exclamation-triangle"></i> Note: Changing this background image will effect slideshow background</div>			
	                               	    </div>
	                               	    <?php }; ?>
                               	    </div>
                               	    <div class="content-placeholder">
										Active background																	
									</div>

                                	<div class="active-image-placeholder ">
                                		<img src="<?= WROOT ?>img/banners_backgrounds/<?= $BANNER->banner_background ?>" >
                                	</div>
								</div>
							</div>
						</div>

						<?php if ($BANNER->partner) { // Partners div ?>
						<div class="row">
							<div class="col-md-12 col-xs-12 col-sm-12">
								<h3><i class="fa fa-image"></i> <span class="padding-l-10">Partners</span></h3>
								<div class="category">Choose partner images</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 col-xs-12 col-sm-12 margin-t-10">
								<div class="text-overlay-placeholder">
									<div class="has-divider padding-none">
										<div class="content-placeholder">
											<label class="checkbox pointer">
			                                    <span class="icons"><span class="first-icon fa fa-square-o"></span><span class="second-icon fa fa-check-square-o"></span></span><input type="checkbox" value="" data-toggle="checkbox"> Has image
			                                </label>
		                                </div>
	                                </div>
	                                <div class="upload-placeholder upload-partner-images">
	                                	<div class="content-placeholder has-divider">
	                                		<div class="pull-right">
	                                			sd
	                                		</div>
	                                		<div class="pull-right banner-partner-images-loader dn">
	                               	    		<div class="t-loader"></div> Uploading...
	                               	    	</div>
		                               	    <div><i class="fa fa-upload"></i> Select way to upload picture</div>
		                               	    <div class="margin-t-10 div-inl">
		                               	    	<div class="btn-submit btn-base"><i class="fa fa-search"></i> Search from Website</div>
		                               	    	<div class="padding-l-10 padding-r-10">or</div>
		                               	    	<div class="btn-white btn-base btn-upload">
		                               	    		<i class="fa fa-upload"></i> Upload from Your device
		                               	    		<input type="file" name="image_upload">
		                               	    	</div>	                               	    	
		                               	    </div>
	                               	    </div>
	                               	    <div class="content-placeholder">
	                               	    	<div><i class="fa fa-image"></i> Current image</div>
	                               	    </div>
	                               	    <div class="">sdsd</div>
	                                </div>
								</div>
							</div>
						</div>

						<?php } else { // Image for banner ?>

						<div class="row">
							<div class="col-md-12 col-xs-12 col-sm-12">
								<h3><i class="fa fa-image"></i> <span class="padding-l-10">Image</span></h3>
								<div class="category">Choose image for banner</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 col-xs-12 col-sm-12 margin-t-10">
								<div class="text-overlay-placeholder padding-b-20">
									<div class="has-divider padding-none">										
										<div class="content-placeholder">
											<div class="pull-right">
	                                			<div class="uploaded-new-image diss-set">+1 image</div>
	                                		</div>
											<label class="checkbox pointer">
			                                    <span class="icons"><span class="first-icon fa fa-square-o"></span><span class="second-icon fa fa-check-square-o"></span></span><input class="banner-content-image-checkbox" type="checkbox" data-toggle="checkbox" <?= $BANNER->image ? "checked" : "" ?>> Has image
			                                </label>
		                                </div>
	                                </div>
	                                <div class="upload-placeholder upload-content-image">
	                                	<div class="content-placeholder has-divider">
	                                		<div class="pull-right banner-content-image-loader dn">
	                               	    		<div class="t-loader"></div> Uploading...
	                               	    	</div>
		                               	    <div><i class="fa fa-upload"></i> Select way to upload picture</div>
		                               	    <div class="margin-t-10 div-inl">
		                               	    	<div class="btn-submit btn-base"><i class="fa fa-search"></i> Search from Website</div>
		                               	    	<div class="padding-l-10 padding-r-10">or</div>
		                               	    	<div class="btn-white btn-base btn-upload">
		                               	    		<i class="fa fa-upload"></i> Upload from Your device
		                               	    		<input type="file" name="image_upload">
		                               	    	</div>	                               	    	
		                               	    </div>
	                               	    </div>
	                               	    <div class="content-placeholder">
	                               	    	<div><i class="fa fa-image"></i> Current image</div>
	                               	    </div>	                               	    

	                               	    <div class="active-image-placeholder">
	                               	    	<div class="close" data-banner-id="<?= $BANNER->bannerID ?>" data-toggle="tooltip" data-placement="left" title="Delete this image"><i class="fa fa-times"></i></div>

	                               	      	<div class="row">
		                               	    	<div class="col-md-4 text-center">
		                               	    		<div><img src="<?= WROOT ?>img/banners/<?= $BANNER->image ?>" ></div>
		                               	    		<div class="margin-t-10 text-left">
		                               	    			<a class="t-link banner-image-reload" href="javascript: void(0)"><i class="fa fa-refresh"></i> <span>Reload image</span></a>
		                               	    		</div>
		                               	    	</div>
		                               	    	<div class="col-md-7">
		                               	    		<div>	
		                               	    			<label class="checkbox pointer" style="margin: 0">
						                                    <span class="icons"><span class="first-icon fa fa-square-o"></span><span class="second-icon fa fa-check-square-o"></span></span>
						                                    <input type="checkbox" value="" class="banner-image-optimised" data-toggle="checkbox" <?= ($BANNER->image_optimised ? "checked" : "") ?>> Auto width and height
						                                </label>		                               	    			
		                               	    		</div>

		                               	    		<div class="margin-t-10">
		                               	    			<i class="fa fa-arrows-h"></i> Width <span class="small strong">[%]</span>:
		                               	    			<input type="text" class="form-control banner-image-width" data-err-mssg="You have to type a valid number." style="width: auto;  height: auto; padding: 5px 10px" value="<?= $BANNER->image_width ?>" disabled="disabled">	
		                               	    		</div>

		                               	    	</div>		                               	    	
	                               	    	</div>	                               	    	
	                               	    	<div class="margin-t-10">
	                               	    		<div class="div-inl">
	                               	    			<div class="strong"><i class="fa fa-external-link"></i> Link (on image) </div>
	                               	    			<div class="padding-l-10">
	                               	    				<label class="checkbox pointer">
						                                    <span class="icons"><span class="first-icon fa fa-square-o"></span><span class="second-icon fa fa-check-square-o"></span></span>
						                                    <input class="banner-image-link-checkbox" type="checkbox" value="" <?= ($BANNER->image_link ? "checked" : "") ?> data-toggle="checkbox"> Link
						                                </label>
	                               	    			</div>
	                               	    		</div>
	                               	    		<div class="padding-t-5">
	                               	    			<div contenteditable="true" data-placeholder="Copy link here" class="banner-image-datalink content-editable-textarea<?= (!$BANNER->image_link ? " disabled" : "") ?>"><?= ($BANNER->image_link ? $BANNER->image_link : "") ?></div>              	    			
	                               	    		</div> 
	                               	    		<div class="padding-t-5 small"><i class="fa fa-pencil"></i> Example: www.facebook.com/handmadefantasybyanthony</div>
	                               	    	</div>
	                               	    </div>
	                                </div>
								</div>
							</div>
						</div>
						<?php }; ?>

					</div>
					<!-- Second column, print Banner -->
					<div class="col-md-3 col-xs-12 col-sm-12 text-right" style="padding: 0">
						<div class="margin-t-30 text-left">
							<div class="margin-b-10 text-large">
								<div class="strong"><h4 style="margin: 0"><i class="fa fa-eye"></i> <span class="padding-l-5">Banner Preview</span></h4></div>
								<div class="category">Personalize banner & apply changes</div>
							</div>
							<div class="pinned-news" style="height: auto; padding: 30px 20px 60px 30px; background: url(<?= WROOT ?>img/banners_backgrounds/<?= $BANNER->banner_background ?>); background-size: 100% 100%; background-repeat: no-repeat; position: relative; ">
								<div class="pinned-news-loader">
									<div class="content">
										<div class="t-loader"></div>
									</div>
								</div>
	                            <div class="pinned-note note-conventry">
	                                <div class="text-large padding-t-<?= $BANNER->padding_top ?> font-comial">
	                                	 <span class="banner-heading-icon">
	                                	 <i class="fa <?= ($BANNER->heading_icon ? $BANNER->heading_icon : "") ?>" style="display: <?= ($BANNER->heading_icon ? "inline-block" : "none") ?>"></i>
	                                     </span>
	                                     <span class="banner-heading-text"><?= $BANNER->heading ?></span>
	                                </div>
	                                <div class="divider"></div>                                                                                      
	                                    <div class="padding-r-10 text-center"> 
	                                        <?php if (isset($PARTNERS) && count($PARTNERS) && $BANNER->parent_id == 2) { // PARTNERS printing ?>                     
	                                        <?php for ($m=0; $m<count($PARTNERS); $m++) { // FOR ?>
	                                            <?php if ($PARTNERS[$m]->website) { // Link image ?>
	                                            <a href="<?= $PARTNERS[$m]->website ?>" target="_blank">
	                                            <?php }; ?>
	                                            <div>                                                
	                                            <img src="<?= WROOT ?>img/banners/<?= $PARTNERS[$m]->partner_image ?>" style="width: <?= $BANNER->image_width ?>%">
	                                            </div>
	                                            <?php if ($PARTNERS[$m]->website) {  ?>
	                                            </a>
	                                            <?php }; // /image_link ?>
	                                        <?php }; // /FOR ?>
	                                        <?php } else { // NOT SET partners ?>   

	                                        <div class="banner-image-container ui-widget-content">           
		                                        <div id="resizable" class="banner-image-placeholder" style="margin: auto;">  
		                                
	                                        	 <a class="banner-image-link" href="<?= ($BANNER->image_link ? $BANNER->image_link : "javascript: void(0)") ?>" target="_blank">          
		                                         	<img class="banner-image" src="<?= WROOT ?>img/banners/<?= $BANNER->image ?>" style="width: 100%">
		                                         </a>
		                  
		                                        </div>

		                                        <div class="banner-image-notif">
		                                       		<i class="fa fa-expand"></i> Resize image
		                                        </div>

		                                        <div class="banner-image-refresh-resize" data-toggle="tooltip" data-placement="left" title="Refresh resizing">
		                                        	<i class="fa fa-refresh"></i>
		                                        </div>
	                                        </div>
	                                        <?php if ($BANNER->image_link) { // Link image ?>
	                                        </a>
	                                        <?php }; ?>
	                                        <?php }; ?>
	                                    </div>                                

	                                <div class="smaller margin-t-10 banner-preview-subtext date-banner <?= (!$BANNER->date ? "dn" : "") ?>">
	                                	<span class="fa fa-calendar"></span> <span class="banner-preview-subtext-field"><?= $BANNER->date ?></span>
	                                </div> 	                               
	                            </div> 
	                        </div>
	                        <!-- /.banner -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	<?php 
		$SCANDIR_PREPATH = "../";
		$ALLOWED_FOLDERS = array("banners"              => true, 
				                 "banners_backgrounds"  => true, 
				                 "content"              => false, 
				                 "gallery"              => false, 
				                 "manufacturers"        => false, 
				                 "partners"             => false, 
				                 "users"                => false);
		include "modals/banners_icons.php"; 
		include "modals/workshops_image_delete.php";
		include "modals/search_images.php";
	?>