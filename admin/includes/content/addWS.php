<div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Workshop Creator</h4>
                                <p class="category">Here you can create your workshop</p>
                            </div>

                            <!-- Sadrzaj -->
                            <div class="content">
                            
                                <!-- Naslovi -->
                                <div class="card padding-10">
                                    <div class="header">
                                        <h4 class="title">Naslovi</h4>
                                    </div>
                                    <form>
                                        <?php if($LANGUAGE->serbian) { ?>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Naslov Srpski</label>
                                                        <span class="count-length pull-right">40</span>
                                                        <input type="text" class="ws-heading-serbian  form-control" value="" placeholder="Enter text here">
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php if($LANGUAGE->english) { ?>   
                                        <div class="row">    
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Naslov Engleski</label>
                                                    <span class="count-length pull-right">40</span>
                                                    <input type="text" class="ws-heading-english form-control" value="" placeholder="Enter text here">
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php if($LANGUAGE->polish) { ?>
                                        <div class="row">    
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Naslov Poljski</label>
                                                    <span class="count-length pull-right">40</span>
                                                    <input type="text" class="ws-heading-polish form-control" value="" placeholder="Enter text here">
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                         <?php if($LANGUAGE->russian) { ?>
                                        <div class="row">    
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Naslov Ruski</label>
                                                    <span class="count-length pull-right">40</span>
                                                    <input type="text" class="ws-heading-russian form-control" value="" placeholder="Enter text here">
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                         <?php if($LANGUAGE->greek) { ?>
                                        <div class="row">    
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Naslov Grcki</label>
                                                    <span class="count-length pull-right">40</span>
                                                    <input type="text" class="ws-heading-greek form-control" value="" placeholder="Enter text here">
                                                </div>
                                            </div>
                                        </div>  
                                        <?php } ?>   
                                    </form>
                                </div>

                                <!-- Podnaslovi -->
                                <div class="card padding-10">
                                    <div class="header">
                                        <h4 class="title">Podnaslovi</h4>
                                    </div>
                                    <form>
                                        <?php if($LANGUAGE->serbian) { ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Podnaslov Srpski</label>
                                                    <div class="ws-subheading-serbian content-editable-textarea" contenteditable="true" data-placeholder="Enter description here"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php if($LANGUAGE->english) { ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Podnaslov Engleski</label>
                                                    <div class="ws-subheading-english content-editable-textarea" contenteditable="true" data-placeholder="Enter description here"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php if($LANGUAGE->polish) { ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Podnaslov Poljski</label>
                                                    <div class="ws-subheading-polish content-editable-textarea" contenteditable="true" data-placeholder="Enter description here"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php if($LANGUAGE->russian) { ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Podnaslov Ruski</label>
                                                    <div class="ws-subheading-russian content-editable-textarea" contenteditable="true" data-placeholder="Enter description here"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php if($LANGUAGE->greek) { ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Podnaslov Grcki</label>
                                                    <div class="ws-subheading-greek content-editable-textarea" contenteditable="true" data-placeholder="Enter description here"></div>
                                                </div>
                                            </div>
                                        </div>  
                                        <?php } ?>   
                                    </form>
                                </div>

                                <!-- Opis -->
                                <div class="card padding-10">
                                    <div class="header">
                                        <h4 class="title">Opisi</h4>
                                    </div>
                                    <form>
                                        <?php if($LANGUAGE->serbian) { ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Opis Srpski</label>
                                                    <div class="ws-text-serbian content-editable-textarea" contenteditable="true" data-placeholder="Enter description here"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php if($LANGUAGE->english) { ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Opis Engleski</label>
                                                    <div class="ws-text-english content-editable-textarea" contenteditable="true" data-placeholder="Enter description here"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php if($LANGUAGE->polish) { ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Opis Poljski</label>
                                                    <div class="ws-text-polish content-editable-textarea" contenteditable="true" data-placeholder="Enter description here"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php if($LANGUAGE->russian) { ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Opis Ruski</label>
                                                    <div class="ws-text-russian content-editable-textarea" contenteditable="true" data-placeholder="Enter description here"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php if($LANGUAGE->greek) { ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Opis Grcki</label>
                                                    <div class="ws-text-greek content-editable-textarea" contenteditable="true" data-placeholder="Enter description here"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>     
                                    </form>
                                </div>

                                <!-- Narators -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Workshop with</label>
                                            <span class="padding-l-10">
                                                <select class="narators">
                                                    <option value="" >Antonis Tzanidakis</option>
                                                    <?php for( $i=0; $i<count($narrators); $i++) { ?>
                                                        <option value="<?= $narrators[$i]->narratorID ?>"> <?= $narrators[$i]->name ?> </option>   
                                                    <?php } ?>
                                                </select>
                                            </span>
                                        </div>
                                    </div>
                                </div>


                                <!-- Video ID -->
                                <div class="card padding-10">
                                    <div class="header">
                                        <div class="text-large">Video ID</div>
                                        <div class="small">Type videoID from <span class="strong">vimeo.com</span></div>
                                    </div>
                                    <form>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="video-ID form-control" value="" placeholder="Type videoID">
                                                    <div class="small padding-t-5">Example: http://www.vimeo.com/<span class="strong">209165485</span></div>
                                                </div>
                                            </div>
                                        </div>                                       
                                    </form>
                                    <div class="header">
                                        <div class="div-inl">
                                            <div class="beginning-subtitles"><input type="checkbox"></div>
                                            <div class="text-large">Obavestenje o pocetnim subtitlovima</div>                                            
                                        </div>
                                    </div>
                                </div>
                                <a id="create_WS" href="#" create-workshop-ID="<?= $ws_edit->workshopID ?>" create-failure-message="Failure" create-success-message="Success" create-empty-text-message="You must fill all available text areas!"> 
                                    <div  class="btn-submit btn-base pull-right">Create Workshop</div>
                                </a>    
                                <div class="clearfix"></div>
                         </div>
                    </div>
            </div>  

            <!-- Slika -->
            <div class="col-md-4">
                <div class="card">
                    <div class="row">
                        <div class="col-md-12" align="center">
                            <div class="content margin-t-20">
                                <h6 class="title">Choose an image from the website</h6>
                                <a href="#"> 
                                    <div class="btn-submit btn-base margin-t-10"><i class="fa fa-search"></i> Search from Website</div>
                                </a>    
                            </div>
                        </div>
                        <div class="col-md-12" align="center">
                            <div class="content margin-b-20">
                                <h4 class="title">OR</h4>
                            </div>
                        </div>
                        <form id="workshopPictureFORM" method="post" action="" enctype="multipart/form-data" data-workshop-ID="<?= $ws_edit->workshopID ?>">
                            <div class="col-md-12 margin-b-20" align="center">                     
                                <div class="image-edit">
                                    <input id="inputPicture" type="file" name="picture" />
                                    <div class="content text-center">
                                        <i class="fa fa-upload fa-3x"></i>
                                        <div class="padding-t-5">Upload an image from your computer</div>
                                    </div>
                                </div>                          
                            </div>                            
                        </form>
                    </div>
                </div>

                <div class="card">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <h4 class="header" style="margin: 0;">Active images</h4>
                                <div style="margin-left: 15px;">The one with the green check is the active image</div>
                            </div>  
                            <div class="div-inl all-pictures" data-image-message="You have successfully set your default picture." data-delete-image="You have successfully deleted this image." data-image-false="You cannot delete this picture!">
                                <?php for($j = 0; $j < count($image); $j++){ ?>  
                                    <div data-imageID = "<= $image[$j]->imageID ?>" class="edit-ws-image-placeholder <?= ($image[$j]->im_index==1 ? "is-active" : ""); ?>" style="vertical-align: top;">

                                        <img data-wsID="<?= $ws_edit->workshopID ?>" data-image="<?= $image[$j]->imageID ?>" class="edit-image" src="<?= WROOT ?>img/content/<?= $image[$j]->image ?>">
                                      
                                        <div class="check">
                                            <i class="arrow fa fa-check fa-2x"></i>
                                        </div>
      
                                        <div class="cancel-placeholder">
                                            <div class="tooltip-placement" data-toggle="tooltip" data-placement="right" title="Delete image"></div>
                                            <div class="cancel inactive" >
                                                <i class="ex fa fa-times"></i>
                                            </div>
                                        </div>
                                    </div>
                                <?php } 

                                if($j==0){ ?>
                                    <div class="margin-20 no-pictures">
                                        <i class="fa fa-ban"></i> No pictures yet! 
                                    </div>
                               <?php } ?>
                            </div>
                            
                        </div>
                </div>
            </div>

        </div>   
    </div> 
</div>    

<?php include "modals/workshops_image_delete.php"; ?>