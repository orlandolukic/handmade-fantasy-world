<?php 

//	Available icons
	$icons = array("check", "trash", "area-chart", "calendar", "external-link", "home");
	
?>
	<div data-modal-id="heading-icon-select" class="ui-modal ui-modal-dark">
		<!-- Modal Placeholder -->
		<div class="ui-modal-content">
			<div class="ui-modal-heading">
				<h4 class="strong" style="margin: 0"><span class="ui-modal-heading-icon"><i class="fa fa-trash"></i></span> Select icon</h4>
				<div class="smaller">Search for available icons</div>
			</div>

			<div class="ui-modal-body" style="padding: 0">
				<?php for($i=0; $i<count($icons); $i++) { ?>
				<span class="banner-heading-icon-placeholder" data-icon-id="fa-<?= $icons[$i] ?>"><i class="fa fa-<?= $icons[$i] ?>"></i></span>
				<?php }; ?>
			</div>

			<div class="ui-modal-footer small">				
				<div class="text-right div-inl">
					<div class="ui-modal-footer-close-btn ui-modal-close-btn btn-white btn-base bordered btn-ord"><i class="fa fa-times"></i> Cancel</div>
					<div class="ui-modal-footer-success-btn btn-base"><i class="fa fa-credit-card"></i> Change icon</div>
				</div>		
			</div>
			<div class="ui-modal-close ui-modal-close-btn"><i class="fa fa-times" data-toggle="tooltip" data-placement="left" title="Cancel"></i></div>

		</div>
		<!-- Loading Placeholder -->
		<div class="ui-modal-loader-placeholder">
			<div class="ui-modal-loader-text">
				<img src="<?= WROOT ?>img/logo.png" style="width: 250px;">
			</div>
			<div class="ui-modal-loader"></div>
		</div>
		<!-- /Loading Placeholder -->

		<!-- Left content -->
		<div class="ui-modal-other-content ui-modal-right-content dn" data-content-id="1">
			<div class="ui-modal-other-content-heading">
				<div class="padding-l-10"><?= $PI15 ?></div>
				<div class="ui-modal-other-content-close"><i class="fa fa-times"></i></div>
			</div>
			<div class="image-optimised margin-t-10 text-center"><img class="window-resize" data-aspect-ratio="<?= ($USER->lang !== "SR" ? "1.32" : "1.24") ?>1.32" src="<?= WROOT ?>img/pi2_<?= ($USER->lang !== "SR" ? "EN" : "SR") ?>.png"></div>			
		</div>
		<!-- /Left content -->
	</div>