<?php 

	// Define $SCANDIR_PREPATH to find WROOT	
	// Define $ALLOWED_FOLDERS in order to restrict access to specific folders
	// Available folders
	$folders = array("banners"              => array("name" => "Banners", "code" => "banners"), 
	                 "banners_backgrounds"  => array("name" => "Banners Backgrounds", "code" => "banners_backgrounds"), 
	                 "content"              => array("name" => "Workshop Images", "code" => "content"), 
	                 "gallery"              => array("name" => "Gallery Images", "code" => "gallery"), 
	                 "manufacturers"        => array("name" => "Manufacturers Logos", "code" => "manufacturers"), 
	                 "partners"             => array("name" => "Partners", "code" => "partners"), 
	                 "users"                => array("name" => "Users", "code" => "users"));

?>
	<div data-modal-id="search-images" class="ui-modal ui-modal-dark">
		<!-- Modal Placeholder -->
		<div class="ui-modal-content">
			<div class="ui-modal-heading">
				<h4 class="strong" style="margin: 0"><span class="ui-modal-heading-icon"><i></i></span> <span class="padding-l-5">Images</span></h4>
				<div class="smaller">Choose image from list</div>
			</div>

			<!-- Modal Body -->
			<div class="ui-modal-body" style="padding: 0">
			   <div class="pre-content">
				   <div class="text-content uppercase strong small"> Select destination folder</div>
				   <div class="divider"></div>

				   <?php foreach ($folders as $key => $value) { ?>
				   <?php if (!$ALLOWED_FOLDERS[$value["code"]]) continue; ?>
				   <div class="text-content text-content-hover ui-modal-folder-select" data-folder-id="<?= $value["code"] ?>"><i class="fa fa-folder"></i> 
				   		<?= $folders[$key]["name"] ?>
				   </div>
				   <?php }; ?>
				</div>

			   <div class="all-folders">			   	   
				   <?php 
				   $c_images  = 0;
				   $c_folders = 0;
				   foreach($folders as $key => $value) { ?>
				   <?php if (!$ALLOWED_FOLDERS[$value["code"]]) continue; ?>
				   <div class="ui-modal-folder-placeholder dn" data-folder-id="<?= $value["code"] ?>">
				   		<div class="text-content">
					   	   <div class="div-inl">
							   <div class="btn-base btn-white ui-modal-show-all-folders"><i class="fa fa-chevron-left"></i> All folders</div>
							   <div class="padding-l-10" style="position: relative; top: 4px"><h4 class="strong" style="margin: 0"><i class="fa fa-folder-open"></i> <span class="padding-l-5"><?= $value["name"] ?></span></h4></div>
						   </div>
						   <div class="uppercase margin-t-10">
						   	  <div class="small">Total images: <span class="total-images-spec-folder" data-folder-id="<?= $value["code"]; ?>">5</span></div>
						   </div>						   
					   </div>
					   <div class="divider"></div>
					   <div class="images">					   
				   	    <?php
				   	    	$c_folders++;
				   	    	$files = array_diff(scandir($SCANDIR_PREPATH."img/".$value["code"]), array("..",".", ".htaccess", "23.htaccess")); 
				   	    	foreach($files as $key1 => $value1) { $c_images++;	?>	
				   	    	<div class="ui-modal-image-list-item" data-image-id="<?= substr($value1,0,strlen($value1)-4) ?>" data-image="<?= $value1 ?>">
				   	    		<img src="<?= WROOT."img/".$value['code']."/".$value1 ?>">
				   	    		<div class="image-select"><i class="fa fa-check"></i>
				   	    		</div>
				   	    	</div>			   	    
				   	    	<?php }; ?>
				   	    </div>
				   </div>			   
			   <?php }; ?>
			   	   <div class="ui-modal-images-info">
					   <div class="divider"></div>
					   <div class="text-content div-inl">
					   	  <div class="uppercase"><span class="small">Total Folders:</span> <span class="strong"><?= $c_folders ?></span></div>
					   	  <div class="uppercase padding-l-10"><span class="small">Total images:</span> <span class="strong"><?= $c_images ?></span></div>		   	  
					   </div>			   
				   </div>
			   </div>		   
			   
			</div>
			<!-- /Modal Body -->

			<div class="ui-modal-footer small">				
				<div class="text-right div-inl">
					<div class="ui-modal-footer-close-btn ui-modal-close-btn btn-base bordered btn-ord"><i class="fa fa-times"></i> Cancel</div>
					<div class="ui-modal-footer-success-btn btn-base disabled-pt"><i class="fa fa-credit-card"></i> Choose image</div>
				</div>		
			</div>
			<div class="ui-modal-close ui-modal-close-btn"><i class="fa fa-times" data-toggle="tooltip" data-placement="left" title="Cancel"></i></div>
		</div>
		<!-- Loading Placeholder -->
		<div class="ui-modal-loader-placeholder">
			<div class="ui-modal-loader-text">
				<img src="<?= WROOT ?>img/logo.png" style="width: 250px;">
			</div>
			<div class="ui-modal-loader"></div>
		</div>
		<!-- /Loading Placeholder -->

	</div>