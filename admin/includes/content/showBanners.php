    <div class="card">
        <div class="header">                    
            <h4 class="title">Banners Settings</h4>
            <p class="category">Make display and textual settings to homepage banners</p>
        </div>
        <div class="content">                   
         
            <div class="container-fluid">
                    <div class="row">
                <?php if ($banners) { ?>         

                    <?php for ($i=0, $f = count($BANNERS), $show = true, $ft = false; $i<$f; $i++) { ?>
                    <?php if ($show) { ?>
                    <div class="col-md-3 col-xs-12 col-sm-12">
                    <?php }; ?>
                        <?php if (!$ft && $BANNERS[$i]->parent_id) { ?>
                        <div class="margin-b-10">
                            <a href="<?= FILE ?>banners?action=add">
                                <div class="btn-base btn-submit"><i class="fa fa-plus"></i> Add slide banner</div>
                            </a>
                        </div>
                        <?php $ft = true; }; ?>
                        <div class="pinned-news" style="height: auto; padding: 30px 20px 60px 30px; background: url(<?= WROOT ?>img/banners_backgrounds/<?= $BANNERS[$i]->banner_background ?>); background-size: 100% 100%; background-repeat: no-repeat; ">
                            <div class="pinned-note note-conventry">
                                <div class="text-large padding-t-<?= $BANNERS[$i]->padding_top ?> font-comial">
                                     <?= ($BANNERS[$i]->heading_icon ? "<i class=\"fa ".$BANNERS[$i]->heading_icon."\"></i>" : "") ?> <?= $BANNERS[$i]->heading ?>
                                </div>
                                <div class="divider"></div>                                                                                      
                                    <div class="padding-r-10 text-center"> 
                                        <?php if (count($PARTNERS) && $BANNERS[$i]->parent_id == 2) { // PARTNERS printing ?>                     
                                        <?php for ($m=0; $m<count($PARTNERS); $m++) { // FOR ?>
                                            <?php if ($PARTNERS[$m]->website) { // Link image ?>
                                            <a href="<?= $PARTNERS[$m]->website ?>" target="_blank">
                                            <?php }; ?>
                                            <div>                                                
                                            <img src="<?= WROOT ?>img/banners/<?= $PARTNERS[$m]->partner_image ?>" style="width: <?= $BANNERS[$i]->image_width ?>%">
                                            </div>
                                            <?php if ($PARTNERS[$m]->website) {  ?>
                                            </a>
                                            <?php }; // /image_link ?>
                                        <?php }; // /FOR ?>
                                        <?php } else { // NOT SET partners ?>   

                                        <?php if ($BANNERS[$i]->image_link) { // Link image ?>
                                        <a href="<?= $BANNERS[$i]->image_link ?>" target="_blank">
                                        <?php }; ?>                   
                                        <img src="<?= WROOT ?>img/banners/<?= $BANNERS[$i]->image ?>" style="width: <?= $BANNERS[$i]->image_width ?>%">
                                        <?php if ($BANNERS[$i]->image_link) { // Link image ?>
                                        </a>
                                        <?php }; ?>
                                        <?php }; ?>
                                    </div>     

                         
                                <?php if (0) { ?>            
                                <div class="padding-t-10 color-theme strong"><?= $BANNERS->workshop->heading ?></div>   
                                <?php }; ?> 

                                <?php if ($BANNERS[$i]->date) { ?>
                                <div class="smaller margin-t-10 date-banner"><span class="fa fa-calendar"></span> <?= $BANNERS[$i]->date ?></div> 
                                <?php }; ?>

                                <?php if (defined("PAGE")) { ?>
                                <div class="margin-t-20">
                                    <a href="<?= FILE ?>banners?action=edit&bannerID=<?= $BANNERS[$i]->bannerID ?>">
                                    <div class="btn-base btn-white">
                                        <i class="fa fa-edit"></i> Edit banner's content
                                    </div>
                                    </a>
                                </div>
                                <?php }; ?>
                            </div> 
                        </div>   

                    <?php if ($i==$f-1 || $i+1 < $f && $BANNERS[$i]->parent_id !== $BANNERS[$i+1]->parent_id) { ?>                        
                    </div>
                    <?php $show = true; } else { $show = false; } ?> 
                    <?php }; ?> 
                    </div>
                </div>               
                <?php } else { ?>         
                <div class="col-md-12 col-xs-12 col-sm-12">
                    Currently, You don't have banners.
                </div>
                <?php }; ?>
            </div>
        </div>