<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 margin-b-20 margin-t-20" >
                <a href="<?= FILE ?>workshops?action=add& ?>">
                    <div class="btn-submit btn-base"><i class="fa fa-plus"></i>Add New Workshop</div>
                </a>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Available workshops</h4>
                    </div>
                    <div class="content table-responsive ">
                        <table class="table table-hover table-striped">
                            <thead>
                                <th>#</th>
                                <th>Heading</th>
                                <th>Subheading</th>
                                <th>Date Published</th>
                                <th>Views</th>
                                <th>Edit</th>
                                <th>Status</th>
                                <th>Send Newsletter</th>
                            </thead>
                            <tbody>
                            <?php for($j = 0; $j < count($pom); $j++) {?>
                                <tr>
                                    <td style="width: 40px;"><?= $j+1 ?></td>
                                    <td class="col-md-2"><?= $pom[$j]->heading_SR ?></td>
                                    <td class="col-md-4"><?= $pom[$j]->subheading_SR ?></td>
                                    <td style="width: 100px;"><?= make_date(-1,$pom[$j]->date_publish) ?></td>
                                    <td style="width: 50px;"><?= $pom[$j]->views ?></td>
                                    <td>
                                        <a href="<?= FILE ?>workshops?action=edit&wid=<?= $pom[$j]->workshopID ?>"><i class="fa fa-edit"></i></a>
                                    </td>
                                    <td style="width: 150px;">
                                        <div class="activate-placeholder btn-submit btn-base <?= ($pom[$j]->active!=1 ? "" : "disabled ff") ?>" ws-ID="<?= $pom[$j]->workshopID ?>" activate-message="You have successfully activated this workshop!" deactivate-message="You have successfully deactivated this workshop!">
                                            
                                            <i class="i-Active fa fa-check <?= ($pom[$j]->active==1 ? "non-hover" : "hover") ?>"> </i>
                                            
                                            <i class="i-Inactive fa fa-times <?= ($pom[$j]->active==1 ? "hover" : "non-hover") ?>
                                            <?=  ($pom[$j]->active==1 ? "" : "dn") ?> "></i>

                                            <?php if($pom[$j]->active==1) { ?>
                                                <span class="Deactivate hover dn">Deactivate</span>
                                                <span class="Activate dn init">Activate</span>
                                                <span class="Activated non-hover">Activated</span>
                                            <?php } else { ?>
                                                <span class="Deactivate non-hover dn">Deactivate</span>
                                                <span class="Activate init">Activate</span>
                                                <span class="Activated dn">Activated</span>
                                            <?php } ?>    
                                         </div> 
                                     </td>
                                    <td >
                                        <div class=" btn-submit btn-base <?= ($pom[$j]->send_email!=1 ? "" : "disabled ff") ?>">
                                      
                                        <i class="fa fa-check <?= ($pom[$j]->send_email==1 ? "non-hover" : "hover") ?>"> </i>
                                        
                                        <i class="fa fa-times <?= ($pom[$j]->send_email!=1 ? "non-hover" : "hover") ?> 
                                        <?=  ($pom[$j]->send_email==1 ? "" : "dn") ?> "></i>
                                   
                                        <?php 
                                            if($pom[$j]->send_email==1)
                                                echo "Email Sent";
                                            else
                                                echo "Send Email";
                                         ?> </div> 
                                     </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
