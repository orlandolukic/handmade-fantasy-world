<?php 
    session_start();
    define("PREPATH", "");
    define("PAGE",    "login");
    include "includes/functions.php";
    include "includes/controls.php";
    include "global.php";
    include "lang/".strtolower(WLANG).".php";

//  User attempts to login
    if (isset($_POST['username']) && isset($_POST['password']))   
    {
        $_POST['username'] = htmlspecialchars($_POST['username'], ENT_QUOTES);
        $_POST['password'] = htmlspecialchars($_POST['password'], ENT_QUOTES);
        $sql = mysql_query("SELECT * FROM users WHERE BINARY username = '".$_POST['username']."' AND BINARY password = '".$_POST['password']."' AND force_logout = 0 AND active = 1 LIMIT 1", DBC_ADMIN);
        if (mysql_num_rows($sql) === 1)
        {
           LOGIN($_POST['username']);
           header("location: ".FILE."dashboard");
           exit();
        } else
        {
           $errorCode = 1;
           $security  = "token_missmatch";
        };
    } else
    {
        $errorCode = 0;
    };


?>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title><?= $lang->adminSection ?> | Handmade Fantasy World</title>  
  <link rel="stylesheet" href="<?= FILE ?>css/style.css"> 
  <link rel="stylesheet" href="<?= FILE ?>assets/css/style.css"> 
  <link rel="stylesheet" href="<?= FILE ?>assets/css/core.css">
  <link rel="icon" type="image/png" href="<?= WROOT ?>img/favicon.png">
  <meta name="viewport" content="initial-scale=1.0, user-scalable = no">
</head>

<body>
  <div class="container">

  <div id="login-form">

    <div class="logo-placeholder">
       <a href="<?= WCONTENT ?>">
         <img src="assets/img/logo.png" width="270">
       </a>
    </div>

    <div class="error-fields dn">
       Neispravno korisničko ime ili lozinka
    </div>

    <fieldset>

      <form action="<?= FILE ?>login" method="POST">

        <div class="strong margin-b-10">Administrator Login</div>
        <input type="username" placeholder="Username" name="username" autofocus="autofocus" autocomplete="off" required>
        <input type="password" placeholder="Password" name="password" required>
        <input type="submit" value="Login">

      </form>

    </fieldset>

  </div> <!-- end login-form -->

</div>
  
  
</body>
</html>
<?php
// Close connections with a databases
   mysql_close(DBC_STORE);
   mysql_close(DBC_ADMIN);
?>
