<?php 
    session_start();
    define("PREPATH", "");
    define("PAGE",    "workshops");
    include "includes/functions.php";
    include "includes/controls.php";
    include "global.php";
    include "includes/language_control.php";
    include "print_HTML_data.php";
    include "lang/".strtolower(WLANG).".php";
    include "includes/actions/workshops.php";
    include "includes/sql/workshops.php";

?>
<!doctype html>
<html lang="en">
<head>
    <?php print_HTML_data("head", "head_core"); ?>
    <?php print_HTML_data("head", "workshops"); ?>
    <title>HFW | Dashboard</title>
</head>
<body>

<div class="wrapper">
    <?php include "includes/pages/sidebar.php" ?>
    <div class="main-panel">
        <?php include "includes/pages/navbar.php" ?>

        <div class="content">
        <?php if($SWITCH) {

                if($ACTION == "edit"){
                    include "includes/content/editWS.php";
                } elseif($ACTION = "add") {
                    include "includes/content/addWS.php";
                }

              }else 
              {
                include "includes/content/showWS.php";
              } 
        ?>
            
        </div>
        
        <?php include "includes/pages/footer.php" ?>
    </div>
</div>


</body>

    <?php print_HTML_data("script", "core")?>
    <?php print_HTML_data("script", "workshops")?>
     

</html>
