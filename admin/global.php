<?php 

	include PREPATH."includes/database.php";

//	Global vars for admin section
	if (!defined("FILE"))     define( "FILE", "http://localhost/pr/admin/");
	if (!defined("WCONTENT")) define( "WCONTENT", "http://www.handmadefantasyworld.com/");
	if (!defined("WROOT"))    define( "WROOT", "http://localhost/pr/");

	if (isset($_SESSION['hfw_admin']))
	{
		if ($CONTROLS && isset($CONTROLS->admin_redirection) && $CONTROLS->admin_redirection) { header("location: ".FILE."dashboard"); exit(); };
		// FETCH info

		$sql = mysql_query("SELECT * FROM users WHERE BINARY username = '".$_SESSION['hfw_admin']."' AND active = 1 AND force_logout = 0", DBC_ADMIN);
		if (mysql_num_rows($sql))
		{
			$ADMIN = mysql_fetch_object($sql);
			// Verify user
			if ($CONTROLS && isset($CONTROLS->verify_administrator) && $CONTROLS->verify_administrator) include PREPATH."includes/verify.php";
			$adminActive = 1;
			define("WLANG", $ADMIN->lang);
		} else
		{
			if ($CONTROLS && isset($CONTROLS->ajax_page) && !$CONTROLS->ajax_page) 
			{ 
				header("location: ".FILE."logout"); 
				exit(); 
			} elseif ($CONTROLS && isset($CONTROLS->ajax_page) && $CONTROLS->ajax_page) 
			{
				echo json_encode(array("info" => array("response" => false, "redirect" => true, "href" => FILE."logout")));
				exit(); 
			}
		}
	} else 
	{
		$adminActive = 0;
		define("WLANG", "SR");
		if ($CONTROLS && isset($CONTROLS->block_content) && $CONTROLS->block_content) 
		{ 
			if ($CONTROLS && isset($CONTROLS->ajax_page) && $CONTROLS->ajax_page) 
			{
				echo json_encode(array("info" => array("response" => false, "redirect" => true, "href" => FILE."logout")));
				exit(); 
			} else
			{
				header("location: ".FILE."login"); exit(); 
			};
		};
	};

?>