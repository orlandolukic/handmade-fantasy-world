<?php 
    session_start();
    define("PREPATH", "");
    define("PAGE",    "banners");
    include "includes/functions.php";
    include "includes/controls.php";
    include "print_HTML_data.php";
    include "global.php";
    include "includes/language_control.php";
    include "lang/".strtolower(WLANG).".php";
    include "includes/actions/banners.php";
    include "includes/sql/banners.php";
?>
<!doctype html>
<html lang="en">
<head>
    <?php print_HTML_data("head", "head_core") ?>
    <?php print_HTML_data("head", "modal") ?>
    <?php print_HTML_data("head", "banners") ?>
    <title>HFW | Banners Settings</title>
</head>
<body>

<div class="wrapper">
    <?php include "includes/pages/sidebar.php" ?>
    <div class="main-panel">
        <?php include "includes/pages/navbar.php" ?>

        <div class="content">
            <?php if ($SWITCH) { 
                switch($CONTENT)
                {
                case "edit": include "includes/content/editBanners.php"; break;
                case "add": include "includes/content/addBanners.php"; break;                
                }
            ?>            
            <?php } else include "includes/content/showBanners.php"; ?>
        </div>


        <?php include "includes/pages/footer.php" ?>
    </div>
</div>

    <?php print_HTML_data("script", "core"); ?>
    <?php print_HTML_data("script", "modal"); ?>
    <?php print_HTML_data("script", "banners"); ?>
</body>
</html>
