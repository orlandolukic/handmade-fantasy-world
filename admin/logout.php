<?php 
	session_start();
    define("PREPATH", "");
    define("PAGE",    NULL);
    include "includes/functions.php";
    include "includes/controls.php";
    include "global.php";

    session_destroy();
    header("location: ".FILE."login");

?>