<?php 

function print_HTML_data($type,$file)
{

//					           Style verions
	$VERSIONS = (object) array("styles"  => (object) array("style"        => "?v=1.0.1",
	                                                       "core"         => "?v=1.0.0",
	                                                       "theme"        => "?v=1.0.1",
	                                                       "modal"        => "?v=1.0.0",
	                                                       "banners"      => "?v=1.0.0"),

//					           Script verions
	                           "scripts" => (object) array("register"      => "?v=1.0.1",
	                                                       "modal"         => "?v=1.0.0",
	                                                       "modalRequests" => "?v=1.0.0"));

	if($type==="script")
	{
		switch($file)
		{
		case "core":
 	  echo '<script src="'.FILE.'assets/js/jquery-1.10.2.js" type="text/javascript"></script>
		    <script src="'.FILE.'assets/js/bootstrap.min.js" type="text/javascript"></script>
		    <script src="'.FILE.'assets/js/bootstrap-checkbox-radio-switch.js"></script>
		    <script src="'.FILE.'assets/js/chartist.min.js"></script>
		    <script src="'.FILE.'assets/js/bootstrap-notify.js"></script>
		    <script src="'.FILE.'assets/js/light-bootstrap-dashboard.js"></script>
		    <script src="'.FILE.'assets/js/global.js"></script>';

		    if (0) {
		    echo '<script type="text/javascript">
		        $(document).ready(function(){

		            demo.initChartist();        

		        });
		    </script>';
			};
			break;
		case "modal":
			echo '<script src="'.FILE.'assets/js/modal.js"></script>';
			break;
		case "workshops":
			global $LANGUAGE;
			function g($param)
			{
				return $param ? "true" : "false";
			}
			echo '<script type="text/javascript" src="includes/js/workshops.js" data-main-script="true" data-languages="var languages = { EN: '.g($LANGUAGE->english).',SR: '.g($LANGUAGE->serbian).', PL: '.g($LANGUAGE->polish).', RU: '.g($LANGUAGE->russian).', GR: '.g($LANGUAGE->greek).'}"></script> 
				  <script src="'.FILE.'assets/js/modal.js"></script>';
		break;	

		// Personalised pages
		case "banners":
			if (array_key_exists('BANNER', $GLOBALS)) global $BANNER; else { $BANNER = (object) array("bannerID" => "null", "image_width" => "null"); };
			echo '<script src="'.FILE.'assets/js/banners.js" data-page-script="true" data-id="'.$BANNER->bannerID.'" data-banner-image-width="'.$BANNER->image_width.'"></script>
			<script src="'.FILE.'assets/js/jquery-ui.js"></script>
			<script src="'.FILE.'assets/js/search-images.js"></script>';
			break;
		}
	} elseif($type === "head")
	{
		switch($file)
		{
		case "head_core":
			echo '
			<meta charset="utf-8">
		    <link rel="icon" type="image/png" href="'.WROOT.'img/favicon.png">
		    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

		    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		    <link href="'.FILE.'assets/css/bootstrap.min.css" rel="stylesheet" />
		    <link href="'.FILE.'assets/css/animate.min.css" rel="stylesheet"/>
		    <link href="'.FILE.'assets/css/light-bootstrap-dashboard.css'.$VERSIONS->styles->theme.'" rel="stylesheet"/>
		    <link href="'.FILE.'assets/css/font-awesome.min.css" rel="stylesheet">
		    <link rel="stylesheet" type="text/css" href="'.FILE.'assets/css/core.css'.$VERSIONS->styles->core.'">
		    <link rel="stylesheet" type="text/css" href="'.FILE.'assets/css/style.css'.$VERSIONS->styles->style.'">';
			break;
		case "modal":
			echo '<link href="'.FILE.'assets/css/modal.css'.$VERSIONS->styles->modal.'" rel="stylesheet" />';
			break;
		case "workshops":
			echo '<link href="'.FILE.'assets/css/workshops.css" rel="stylesheet"/>
			<link href="'.FILE.'assets/css/modal.css'.$VERSIONS->styles->modal.'" rel="stylesheet" />';
		break;
		case "banners":
			echo '<link href="'.FILE.'assets/css/banners.css'.$VERSIONS->styles->banners.'" rel="stylesheet"/>
			      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">';	
		}
	}

}


?>