<?php
//	Control PHP AJAX Requests
    session_start();
    define("PREPATH", "");
    define("PAGE",    "pull");
	include "../includes/functions.php";
	include "../includes/controls.php";
	include "../global.php";
	if (!isset($_POST['req']))
	{
		echo json_encode(array("info" => array("response" => false, "redirect" => true, "href" => FILE)));
		exit();
	};

	$req      = base64_decode($_POST['req']);
	$arr_info = array();
	$arr_data = array();
	$found    = false;
	$RESPONSE = array("info" => &$arr_info, "data" => &$arr_data);

//	Include for colision avoidance
	include "../temp/pull_LOSMI.php";
	include "../temp/pull_KALU.php";

	if (!$found)
	{
		switch($req)
		{
		default: 
			echo json_encode(array("info" => array("response" => false, "redirect" => true, "href" => FILE)));
			exit();
		};
	};

	if (!isset($arr_info["redirect"])) $arr_info["redirect"] = false;
	if (!isset($arr_info["response"])) $arr_info["response"] = false;

	echo json_encode($RESPONSE);
	exit();
?>