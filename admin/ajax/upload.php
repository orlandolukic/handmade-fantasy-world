<?php 
//	Control PHP AJAX Requests
    session_start();
    define("PREPATH", "");
    define("PAGE",    "upload");
	include "../includes/functions.php";
	include "../includes/controls.php";
	include "../global.php";
	include "../lang/sr.php";
	if (!isset($_POST['req']))
	{
		echo json_encode(array("info" => array("response" => false, "redirect" => true, "href" => FILE)));
		exit();
	};

	$req      = base64_decode($_POST['req']);
	$arr_info = array();
	$arr_data = array();
	$RESPONSE = array("info" => &$arr_info, "data" => &$arr_data);
	$found = false;

//	Include for colision avoidance
	include "../temp/upload_LOSMI.php";
	include "../temp/upload_KALU.php";

	if (!$found)
	{
		switch($req)
		{
		/*
		case "some_value":
			// ...
			break;
		*/
		default: 
			echo json_encode(array("info" => array("response" => false, "redirect" => true, "href" => FILE)));
			exit();
		};
	};

	echo json_encode($RESPONSE);
	exit();


?>