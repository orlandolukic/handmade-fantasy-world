<?php 

switch($req)
{
	case "banners-background-image":
		$found = true;
		if ( isset($_FILES["picture"]) )
		{
			$validextensions = array("jpeg", "jpg", "png", "JPG");
			$temporary       = explode(".", $_FILES["picture"]["name"]);
			$file_extension  = end($temporary);

			if (!(($_FILES["picture"]["type"] === "image/png") || ($_FILES["picture"]["type"] === "image/jpg") || ($_FILES["picture"]["type"] === "image/jpeg")))
			{

				$arr_info["response"]   = true;
				$arr_info["uploaded"]   = false;
				$arr_info["hasMessage"] = true;

				$arr_info["message"] = (object) array(
												"error" 	  => true,
												"messageType" => "danger",	
												"messageText" => "error" 
												);

			} elseif (floatval($_FILES["picture"]["size"]) > 5*1024*1024) // =5MB
			{
				$arr_info["response"]   = true;
				$arr_info["uploaded"]   = false;
				$arr_info["hasMessage"] = true;
				$arr_info["message"] = (object) array(
												"error" 	  => true,
												"messageType" => "danger",	
												"messageText" => "error" 
												);

			} elseif (!in_array($file_extension, $validextensions))
			{
				$arr_info["response"]   = true;
				$arr_info["uploaded"]   = false;
				$arr_info["hasMessage"] = true;
				$arr_info["message"] = (object) array(
												"error" 	  => true,
												"messageType" => "danger",	
												"messageText" => "error" 
												);

			} else // Passed on check conditions
			{
				
				if ($_FILES["picture"]["error"] > 0)
				{
					$arr_info["response"]   = true;
					$arr_info["uploaded"]   = false;
					$arr_info["hasMessage"] = true;
					$arr_info["message"] = (object) array(
												"error" 	  => true,
												"messageType" => "danger",	
												"messageText" => "error" 
												);

				} else
				{
					
					$name = generate_string(20);
					$newName  = $name.".".$file_extension;

					// Storing source path of the file
					$sourcePath = $_FILES['picture']['tmp_name'];
					// Target path
					$targetPath = "../../img/banners_backgrounds/".basename($_FILES["picture"]["name"]);

					move_uploaded_file($sourcePath, $targetPath); 		// Moving Uploaded file
					if (rename($targetPath, "../../img/banners_backgrounds/".$newName).".".$file_extension)
					{
					//      INFO
					// ==============
						$arr_info["response"]   = true;
						$arr_info["uploaded"]   = true;
						$arr_info["hasMessage"] = true;
					// ==============
					//     DATA
					// ==============
						$arr_info["message"] = (object) array(
												"error" 	  => false,
												"messageType" => "success",	
												"messageText" => "success" 
												);
						$arr_data["image"]   = make_image_content($newName, WROOT);						
						/*
						$arr_data["image_placeholder"] = '
							<div data-imageID = "'.$name.'" class="edit-ws-image-placeholder" style="vertical-align: top;">

                                <img data-wsID="'.$wID.'" data-image="'.$name.'" class="edit-image" src="'.$arr_data["image"].'">
                              
                                <div class="check">
                                    <i class="arrow fa fa-check fa-2x"></i>
                                </div>

                                <div class="cancel-placeholder">
                                    <div class="tooltip-placement" data-toggle="tooltip" data-placement="right" title="Delete image"></div>
                                    <div class="cancel inactive" >
                                        <i class="ex fa fa-times"></i>
                                    </div>
                                </div>
                            </div>
						';
						*/
						$arr_data["imageID"] = $name;
					} else
					{
						unlink($FILE."img/users/".$newName); // Delete new uploaded file
						$arr_info["response"]  = true; 
						$arr_info["uploaded"] = true;  
						$arr_info["hasMessage"] = true;
						$arr_info["message"] = (object) array(
														"error" 	  => true,
														"messageType" => "danger",	
														"messageText" => "error" 
														);
					};							
				};
			};
		};


		break;
};


?>