<?php 

switch($req)
{
case "getFolderImagesNumber":
	$found = true;
	$allowed = array("banners", "banners_backgrounds", "content", "gallery", "manufacturers", "partners", "users");
	if (!in_array(base64_decode($_POST['folder']), $allowed))
	{
		$arr_info["redirect"] = false;
		$arr_info["response"] = false;
		$arr_info["href"]     = FILE."dashboard";
	} else
	{
		$ret = array_diff(scandir("../../img/".base64_decode($_POST['folder'])), array(".", "..", ".htaccess", "23.htaccess"));
		$arr_data["count"]    = count($ret);
		$arr_info["redirect"] = false;
		$arr_info["response"] = true;
		$arr_data['text'] = '';
	    	foreach($ret as $key1 => $value1) {
	        $arr_data['text'] .= '
	    	<div class="ui-modal-image-list-item" data-image-id="'.substr($value1,0,strlen($value1)-4).'" data-image="'.$value1.'">
	    		<img src="'.WROOT."img/".base64_decode($_POST['folder'])."/".$value1.'">
	    		<div class="image-select"><i class="fa fa-check"></i>
	    		</div>
	    	</div>';		   	    
   	    };
   	    $arr_data['text'] .= '</div>';
	};
	break;
}


?>