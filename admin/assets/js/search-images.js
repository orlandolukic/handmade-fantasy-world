

$(document).ready(function() {
	$(".ui-search-images").on("click", function() {
		$("div[data-modal-id='search-images']").modalContent("startModal", { loading: false, scrollBody: true, icon: "photo", init: function() {
			this.find(".ui-modal-footer-success-btn").addClass("disabled-pt");
			this.attr("data-selected-image-id", "null")
											       .attr("data-selected-image", "null")
											       .attr("data-selected-folder", "null");
			this.find(".ui-modal-image-list-item").removeClass("selected");
			this.find(".ui-modal-folder-placeholder").hide();
			this.find(".ui-modal-body .pre-content, .ui-modal-body .ui-modal-images-info").show();
		},
		exit_onlick_close: true,
		exit_onclick: function() {
			this.find(".ui-modal-footer-success-btn").addClass("disabled-pt");
			this.attr("data-selected-image-id", "null")
											       .attr("data-selected-image", "null")
											       .attr("data-selected-folder", "null");
			this.find(".ui-modal-image-list-item").removeClass("selected");
			this.find(".ui-modal-folder-placeholder").hide();
			this.find(".ui-modal-body .pre-content, .ui-modal-body .ui-modal-images-info").show();
		},
		main_onclick_close: true,
		main_onclick: function() {
			alert(256);
		} })
	});

	$(".ui-modal-image-list-item").on("click", function() {
		_si_imageListItem.call(this);
	});

	$(".ui-modal-folder-select").on("click", function() {
		var tek = this, folder;
		_xhr("pull", {req: btoa("getFolderImagesNumber"), folder: btoa(folder = $(this).attr("data-folder-id"))}, function(r) {
			if (r.info.response)
			{
				$(".total-images-spec-folder[data-folder-id='"+folder+"']").text(r.data.count);
				$(".ui-modal-folder-placeholder[data-folder-id='"+folder+"'] .ui-modal-image-list-item").off("click");
				$(".ui-modal-folder-placeholder[data-folder-id='"+folder+"'] .images").html(r.data.text);				
				$(".ui-modal-folder-placeholder[data-folder-id='"+folder+"'] .ui-modal-image-list-item").on("click", function() {
					_si_imageListItem.call(this);
				});
			};
		});
		$(".ui-modal-images-info").fadeOut(350);
		$(this).parent().fadeOut(350,function() {
			$(".ui-modal-folder-placeholder[data-folder-id='"+$(tek).attr("data-folder-id")+"']").fadeIn();			
		});
		$(this).parents(".ui-modal-body").find(".all-folders div[data-folder-id='"+$(this).attr("data-folder-id")+"'] .ui-modal-image-list-item").removeClass("selected");
	});

	$(".ui-modal-show-all-folders").on("click", function() {
		$(".ui-modal-images-info").fadeIn(350);
		$(this).parents(".ui-modal-folder-placeholder").fadeOut(120,function() {
			$(".pre-content").fadeIn(250);	
		});
		$("div[data-modal-id='search-images'] .ui-modal-footer-success-btn").addClass("disabled-pt");
		$("div[data-modal-id='search-images']").attr("data-selected-image-id", "null")
										       .attr("data-selected-image", "null")
										       .attr("data-selected-folder", "null");
	});
});

function _si_imageListItem()
{
	$(this).toggleClass("selected").siblings().removeClass("selected");
	if ($(this).hasClass("selected"))
	{
		$("div[data-modal-id='search-images']").attr("data-selected-image-id", $(this).attr("data-image-id"))
										   .attr("data-selected-image", $(this).attr("data-image"))
										   .attr("data-selected-folder", $(this).parents(".ui-modal-folder-placeholder").attr("data-folder-id"));
		$("div[data-modal-id='search-images'] .ui-modal-footer-success-btn").removeClass("disabled-pt");											
	} else
	{
		$("div[data-modal-id='search-images'] .ui-modal-footer-success-btn").addClass("disabled-pt");
		$("div[data-modal-id='search-images']").attr("data-selected-image-id", "null")
										       .attr("data-selected-image", "null")
										       .attr("data-selected-folder", "null");
	};	
}