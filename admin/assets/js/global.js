
$(document).ready(function() {
	$("[data-toggle='tooltip']").tooltip();
});

// Example
/* _xhr("control", {type: btoa("newRequest"), field1: "data1", field2: "data2"}, function(response) {
	// callback function, code goes bellow and it executes after AJAX request is successfull => 200 status code
	response ===> {(object) "info", (object) "data"}
	================================================
	Available and obligatory fields:
	info .response // Indicator if response is successfully executed (queries, upload, etc.)
		 .redirect // Indicator that shows if the user has to be redirected to certain page ==> see field info.href
		 		   // This object field is checked on line 30, has to be set always - on each request
	data = NULL (default, if not trasfering data)
})
*/ 

function _xhr(p, d, s = function(a) {})
{
	var x, arr = ["control", "pull", "email"], found = false;
	for (var i=0; i<arr.length; i++) if (arr[i] === p) { found = true; break; }
	if (!found) { alert("Unable to perform AJAX request."); x = null; } else
	{
		$.ajax({
			url:    window.location.origin+"/pr/admin/ajax/"+p,
			method: "POST",
			cache:  false,
			data:   d,
			async: false,
			success: function(resp) {
				resp = JSON.parse(resp);
				if (resp.info.redirect) { window.location = resp.info.href; } else { x = s(resp); }
			},
			error: function() {
				alert("XMLHttpRequest error. Please contact administrator.");
			}
		});
	}
	return x;
}

function _xhr_upload(d, s = function(a) {}) // d - form parameter
{
	var x;
	$.ajax({
		url:    window.location.origin+"/pr/admin/ajax/upload",
		method: "POST",
		cache:  false,
		data:   d,
		async:  false,
		processData: false,
		contentType: false,
		success: function(resp) {
			resp = JSON.parse(resp);
			if (resp.info.redirect) { window.location = resp.info.href; } else { x = s(resp); }
		},
		error: function() {
			alert("XMLHttpRequest error. Please contact administrator.");
		}
	});
	return x;
}