
$(document).ready(function() {
	var t = window.setTimeout(function() {}, 0);
	var elemt = window.setTimeout(function() {}, 0);

	$(".active-image-placeholder .close").on("click", function() {
		$("div[data-modal-id='delete-notification']").modalContent("startModal", {loading: false, height: "100px", scrollBody: false, icon: "trash", top: "50px", buttons: { 
		success: { class: "btn-red-danger", icon: "trash" },
		close: { class: "btn-white", icon: "times" } }});
		return false;
	});

	$(".active-heading input.active-heading-input").on("keyup", function() {
		var elem = $("span.banner-heading-text"),
		    mssg = $("div.heading-section").attr("data-empty-text");
		if (this.value.trim().length === 0) elem.html("<span style=\"opacity:0.6\">"+mssg+"</span>"); else elem.html(this.value);
	});

	$("input.banner-subtext-check").on("change", function() {
		if (this.checked)
		{
			$("input.banner-subtext").removeClass("disabled").prop("disabled", false).focus();
			$("div.banner-preview-subtext").fadeIn(450);
		} else
		{
			$("input.banner-subtext").addClass("disabled").prop("disabled", true);
			$("div.banner-preview-subtext").fadeOut(250);
		}
	});

	$("input.banner-subtext").on("keyup", function() {
		var elem = $("span.banner-preview-subtext-field"),
		    mssg = $("div.subtext-section").attr("data-empty-text");
		if (this.value.trim().length === 0) elem.html("<span style=\"opacity:0.6\">"+mssg+"</span>"); else elem.html(this.value);
	});

	$("input.banner-heading-icon-checkbox").on("change", function() {
		if (this.checked)
		{
			$(".banner-heading-icon").fadeIn();
			$(".banner-content-heading-icon").animate({opacity: "1", pointerEvents: "auto"}, 400)[0].style.pointerEvents = "";
		} else
		{
			$(".banner-heading-icon").fadeOut();
			$(".banner-content-heading-icon").animate({opacity: "0.2"}, 250)[0].style.pointerEvents = "none";
		}
	});

	$(".banner-btn-heading-icon").on("click", function() {
		$(".banner-heading-icon-placeholder[data-icon-id='"+$(this).attr("data-heading-icon")+"']").addClass("selected").siblings().removeClass("selected");
		$("div[data-modal-id='heading-icon-select']").modalContent("startModal", {
			loading: false, 
			height: "130px", 
			scrollBody: false, 
			icon: "search", 
			top: "70px", 
			buttons: { 
				success: { class: "btn-submit", icon: "refresh" },
				close:   { class: "btn-white", icon: "times" } 
			},
			main_onclick_close: true,
			main_onclick: function(s) {				
				$(".current-heading-icon i").css({opacity: 1})[0].className = "fa fa-2x "+$(this).attr("data-selected-icon");
				$(".current-heading-icon-status").html($(".current-heading-icon-status").attr("data-af-text"));
				$(".banner-heading-icon i").show()[0].className = "fa "+$(this).attr("data-selected-icon");
				$(".banner-btn-heading-icon").attr("data-heading-icon", $(this).attr("data-selected-icon"));
			}
		});
		return false;
	});

	$(".banner-heading-icon-placeholder").on("click", function() {
		$(this).addClass("selected").siblings().removeClass("selected");
		$("div[data-modal-id='heading-icon-select']").attr("data-selected-icon", $(this).attr("data-icon-id"));
	});

	$("input.banner-image-optimised").on("change", function() {
		if (this.checked)
		{
			$("input.banner-image-width").prop("disabled", true);
		} else 
		{
			$("input.banner-image-width").prop("disabled", false).val($("input.banner-image-width").val()).focus();
		}
	});

	$("input.banner-image-width").on("keyup", function(ev) {
		if (ev.keyCode === 13)
		{
			imageReload();
		}
	});

	$(".banner-image-link-checkbox").on("change", function() {
		if (this.checked)
		{
			$(".banner-image-datalink").removeClass("disabled");
			$(".banner-image-datalink").focus();
			if ($(".banner-image-datalink").text().trim().length === 0)
			{
				$(".banner-image-link").attr("href", "javascript: void(0)");				
			} else
			{
				$(".banner-image-link").attr("href", "http://"+$(".banner-image-datalink").text())
			}
		} else
		{
			$(".banner-image-datalink").addClass("disabled");
			$(".banner-image-link").attr("href", "javascript: void(0)");	
		}
	});

	$(".banner-image-datalink").on("keyup", function(e) {
		if ($(this).text().trim().length > 0)
		{
			$(".banner-image-link").attr("href", "http://"+$(this).text().trim());	
		} else
		{
			$(".banner-image-link").attr("href", "javascript: void(0)");	
		}
	});

	$(".banner-image-reload").on("click", function(f) {
		imageReload();
	});

	var initBW = $(".banner-image-container").width();
	var initBH = $(".banner-image-container").outerHeight();
	$( "#resizable" ).resizable({containment: ".banner-image-container", aspectRatio: initBW/initBH, resize: function(ev, ui) {
		var BW = $(".banner-image-placeholder").width();
		var BH = $(".banner-image-placeholder").outerHeight();
		$("input.banner-image-width").val(Math.floor(BW/initBW*100))
	}});

	$(".banner-image-refresh-resize").on("click", function() {		
		$(".banner-image-placeholder").css({width: "auto", height: "auto"});
		$("input.banner-image-width").val(100);
		initBW = $(".banner-image-container").width();
		initBH = $(".banner-image-container").outerHeight();
	});

	$(".banner-content-image-checkbox").on("change", function(e) {
		if (!this.checked) 
		{	
			$(".upload-content-image").addClass("disabled-pt");
			$(".banner-image-placeholder").fadeOut();
		} else 
		{ 
			$(".upload-content-image").removeClass("disabled-pt"); 
			$(".banner-image-placeholder").fadeIn();
		};
	});

	$("#bannerBackgroundUploadForm").on("change", function(e) {
		var obj = $(this);
		e.preventDefault();
		$(".banner-background-loader").show();
		elemt = window.setTimeout(function() {
			$.notify({
	        	icon: "fa fa-times",
	        	message: obj.attr("data-error-upload")
	        },{
	            type: "danger",
	            timer: 4000,
	            placement: {
	                from: "bottom",
	                align: "left"
	            }
	        });
			$(".banner-background-loader").hide();
		}, 10000);
		var d = new FormData();
		d.append("req", btoa("banners-background-image"));
		d.append("picture", $(this).find("input[name='picture']")[0].files[0]);
		_xhr_upload(d, function(r) {
			if (r.info.response)
			{
				window.clearTimeout(elemt);
				$(".uni-banner-background").show().addClass("diss-set").removeClass("dn");
				$("#bannerBackgroundUploadForm input[name='picture']").val('');
				$(".banner-background-loader").hide();
				t = window.setTimeout(function() {
					$(".uni-banner-background").fadeOut(200, function() {
						$(this).removeClass("diss-set").removeClass("dn");
					});
				}, 4000);
			}
		});
	});

	const bid  = $(document).find("script[data-page-script='true']").attr("data-id");
	const _cbw = parseInt($(document).find("script[data-page-script='true']").attr("data-banner-image-width"));

	$(".banner-image-placeholder").css({width: _cbw+"%"});
	$(".pinned-news-loader").fadeOut(300);
});

function imageReload()
{
	var obj = $("input.banner-image-width");
	if (!$("input.banner-image-optimised")[0].checked)
	{
		if (!parseFloat(obj.val()) || isNaN(obj.val()))
		{
			$.notify({
	        	icon: "fa fa-times",
	        	message: obj.attr("data-err-mssg")
	        },{
	            type: "danger",
	            timer: 4000,
	            placement: {
	                from: "bottom",
	                align: "left"
	            }
	        });
		} else if (parseInt(obj.val()) > 100)
		{
			obj.val(100);
			$(".banner-image-refresh-resize").click();
		} else if (parseInt(obj.val()) < 10)
		{
			obj.val(10);
			$(".banner-image-placeholder").width($("input.banner-image-width").val()+"%").height($("input.banner-image-width").val()+"%");
		} else $(".banner-image-placeholder").width($("input.banner-image-width").val()+"%").height($("input.banner-image-width").val()+"%");
	} else
	{
		$(".banner-image-refresh-resize").click();
	}
}





