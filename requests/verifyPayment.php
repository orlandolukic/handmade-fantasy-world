<?php 
	session_start();
	$prepath = "../";
	require "../connect.php";
	require "../functions.php";
	require "../lang/func.php";
	$REDIRECT = false;
	require "../global.php";
	require "../getDATA.php";
	
	if (!isset($_SESSION['hfw_username'])) header("location: ".$domain);
	if (

		(!isset($_GET['pid']) && !isset($_SESSION['bank_protocol_payment_id'])) || 
		!isset($_GET['paymentType']) || 
		(isset($_GET['paymentType']) && (trim($_GET['paymentType'])!=="credit-card" && trim($_GET['paymentType'])!=="paypal"))

		) header("location: ".$FILE."?access=denied&pg=verifyPayment.php");

	if ($_GET['paymentType']==="credit-card" && isset($_POST['mdStatus'])) 
	{
		$mdStatus = $_POST['mdStatus'];
		if ($mdStatus =="1" || $mdStatus == "2" || $mdStatus == "3" || $mdStatus == "4")
		{ 			   
		   $Response = $_POST["Response"];			   
		   if ( $Response != "Approved")
			{
				mysql_query("UPDATE payments SET bank_mssg = '".$_POST['ErrMssg']."' WHERE BINARY username = '".$USER->username."' AND BINARY paymentID = '".$_SESSION['bank_protocol_payment_id']."'", DBC_STORE);
				$_SESSION['bank_protocol_payment_id'] = $_GET['pid'];				
				header("location: ".$FILE."verifyPayment/canceled");
				exit();
			}			
		};	
	} elseif ($_GET['paymentType']==="credit-card" && !isset($_POST['mdStatus']))
	{
		header("location: ".$FILE."verifyPayment/canceled");
		exit();
	};


	$tmp_pid  = ($_GET['paymentType']!=="paypal" ? $_SESSION['bank_protocol_payment_id'] : $_GET['pid']);
	$tmp_curr = ($_GET['paymentType']==="paypal" ? $USER->currencyID : "RSD");
	$sql = mysql_query("SELECT * FROM payments WHERE paymentID = '".$tmp_pid."' AND username = '".$USER->username."' AND paid = 0", DBC_STORE);
	if (!mysql_num_rows($sql)) header("location: ".$domain."?atm=paymentVerifyParams");

	$sql = mysql_query("UPDATE payments SET paid = 1, bank_mssg = '".(isset($_POST['ErrMsg']) ? $_POST['ErrMsg'] : "")."' WHERE username = '".$USER->username."' AND paymentID = '".$tmp_pid."'", DBC_STORE);

	$obj = array(); $i=0;
	$sql_add = mysql_query("SELECT `cart_workshops`.*, `packages`.price_".$USER->currencyID." AS pricePackage, `packages`.`price_RSD` AS pricePackageRSD,  `packages`.flag FROM (SELECT `cart`.*, `workshops`.price_".$USER->currencyID." AS priceWorkshop, `workshops`.`price_RSD` AS priceWorkshopRSD FROM cart LEFT OUTER JOIN `workshops` ON `cart`.workshopID = `workshops`.workshopID WHERE BINARY username = '".$USER->username."') cart_workshops LEFT OUTER JOIN `packages` ON `cart_workshops`.packageID = `packages`.`packageID`", DBC_STORE);
	while($t = mysql_fetch_object($sql_add)) $obj[$i++] = $t;

	$hasPackage = false;

	for ($i=0; $i<count($obj); $i++)
	{
		if ($obj[$i]->workshopID)
		{
			$p_sql = mysql_query("INSERT INTO boughtworkshops(username, workshopID, paymentID) VALUES 
			                     ('".$USER->username."',
			                      '".$obj[$i]->workshopID."',
			                      '".$tmp_pid."'
			                     )", DBC_STORE);
			if ($_GET['paymentType'] === "credit-card") { $obj[$i]->priceWorkshop = $obj[$i]->priceWorkshopRSD; }			
			// Delete wishlist items
			$sql = mysql_query("DELETE FROM wishlist WHERE BINARY workshopID = '".$obj[$i]->workshopID."'", DBC_STORE);
		} else if ($obj[$i]->packageID)
		{
			$hasPackage   = true;
			$time         = strtotime("+".$obj[$i]->flag." month -1 day", strtotime($year."-".$obj[$i]->start_month."-01"));			
			$date_end     = date("Y-m-d", $time);
			$p_sql        = mysql_query("INSERT INTO subscriptions(username, type, date_start, date_end, paymentID, start_month) VALUES 
			                     ('".$USER->username."',
			                      '".$obj[$i]->packageID."',
			                      '".$year."-".$obj[$i]->start_month."-01',
			                      '".$date_end."',
			                      '".$tmp_pid."',
			                      '".$obj[$i]->start_month."'
			                     )", DBC_STORE);
			if ($_GET['paymentType'] === "credit-card") { $obj[$i]->pricePackage = $obj[$i]->pricePackageRSD; }		

			// Notify admin about subscription
			mysql_query("UPDATE coupons SET subscribed = 1, active = 0 WHERE BINARY `coupons`.username = '".$USER->username."' AND `coupons`.workshopID IN (SELECT `workshops`.workshopID FROM workshops WHERE ".$obj[$i]->start_month." <= MONTH(`workshops`.date_publish) AND ".date("m", $time)." >= MONTH(`workshops`.date_end))", DBC_STORE);	
		};
	};

	if ($sql)
	{
		//	Delete all workshops from wishlist ON forsale = 0;
		if ($hasPackage) 
		{
			mysql_query("DELETE FROM wishlist WHERE BINARY username = '".$USER->username."' AND workshopID IN (SELECT workshopID FROM workshops WHERE forsale = 0 AND active = 1)", DBC_STORE);

			// Make notification about coupons { ... }
		}

		if ($_GET['paymentType'] === "paypal") 
		{
			$_SESSION['show_succ_payment'] = (object) array("data" => $_GET['pid'], "show" => true, "paymentMethod" => "paypal", "paymentID" => $_GET['pid']);
		} elseif ($_GET['paymentType'] === "credit-card") 
		{
			$_SESSION['show_succ_payment'] = (object) array("data" => $_SESSION['bank_protocol_payment_id'], "show" => true, "paymentMethod" => "credit-card", "paymentID" => $_SESSION['bank_protocol_payment_id']);
		};
		mysql_query("DELETE FROM cart WHERE BINARY username = '".$USER->username."'", DBC_STORE);
		if (isset($_SESSION['bank_protocol_payment_id'])) unset($_SESSION['bank_protocol_payment_id']);

		$TEMPLATE_SELECT = "payment_receipt";
		$PAYMENT_INFO    = (object) array("paymentID" => $_SESSION["show_succ_payment"]->data, "printButton" => true);
		include "sendEmail.php";
		send_email();
		$PAYMENT_INFO    = (object) array("paymentID" => $_SESSION["show_succ_payment"]->data, "printButton" => false);
		$lang = return_lang("en","../");
		send_email();
		header("location: ".$FILE."user/");
 	};


?>