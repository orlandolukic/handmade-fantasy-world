<?php 
	// Enter file
	if (!isset($REDIRECT) || isset($REDIRECT) && $REDIRECT) { header("location: ../404.php"); exit(); }

	// Delete workshops from cart that are in boughtworkshops of user's account
	$sql = mysql_query("DELETE FROM cart WHERE BINARY username = '".$USER->username."' AND workshopID IN (SELECT `tbl1`.* FROM (SELECT workshopID FROM boughtworkshops INNER JOIN payments ON `payments`.paymentID = `boughtworkshops`.paymentID AND `payments`.username = `boughtworkshops`.username AND `payments`.paid = 1 WHERE BINARY `boughtworkshops`.username = '".$USER->username."') tbl1 )", DBC_STORE);

	// Delete workshops from cart that user is subscribed for
	$sql = mysql_query("DELETE FROM cart WHERE BINARY username = '".$USER->username."' AND workshopID IN (SELECT `tbl2`.workshopID FROM (SELECT `tbl1`.*, `subscriptions`.username, `subscriptions`.paymentID FROM (SELECT workshopID, date_publish FROM workshops WHERE active = 1 AND date_publish<=CURDATE()) tbl1 INNER JOIN `subscriptions` ON `subscriptions`.date_start <= `tbl1`.date_publish AND `subscriptions`.date_end >= `tbl1`.date_publish AND `subscriptions`.active = 1 AND BINARY `subscriptions`.username = '".$USER->username."') tbl2 INNER JOIN payments ON `payments`.paymentID = `tbl2`.paymentID AND `payments`.paid = 1 AND `tbl2`.username = `payments`.username)", DBC_STORE);

	// Delete workshops from cart that are in coupon's list workshop
	$sql = mysql_query("DELETE FROM cart WHERE BINARY username = '".$USER->username."' AND workshopID IN (SELECT workshopID FROM coupons WHERE BINARY username = '".$USER->username."' AND coupons.active = 1 AND expiration_date>=CURDATE())", DBC_STORE);
?>