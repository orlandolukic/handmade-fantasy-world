<?php 

//  User attempt to open this page
if (!(isset($REDIRECT) && !$REDIRECT)) { header("location: ../index.php"); $REDIRECT = NULL; exit(); }

	// User subscription for months
	include_once $prepath."requests/det_subscription_months.php";

	$sql = mysql_query("SELECT * FROM cart WHERE BINARY username = '".$USER->username."' AND workshopID IS NULL", DBC_STORE);
	$morePackages = mysql_num_rows($sql)<=1 ? false : true;
	$hasActivePackages = false;
	if (mysql_num_rows($sql))
	{
		$sql = mysql_query("SELECT * FROM subscriptions WHERE BINARY username = '".$USER->username."' AND active = 1 AND CURDATE()>=date_start AND CURDATE()<=date_end ORDER BY date_end DESC", DBC_STORE);
		if (mysql_num_rows($sql)) $hasActivePackages = true;
	}

//	Get data for shopping cart
	$SHOPPING_CART = array();
	$sql = mysql_query("SELECT `tbl1`.*, `narrators`.n_name_".$USER->lang." AS narrator_name FROM (SELECT `subTbl`.*, `packages`.price_".$USER->currencyID." AS pricePackage ,`packages`.`price_RSD` AS pricePackageRSD, `packages`.text_".$USER->lang." AS headingPackage, `packages`.flag FROM 
	                   (SELECT `tbl`.*, CONCAT(`images`.imageID, '.',`images`.`extension`) AS image FROM 
	                   		(SELECT `cart`.*, `workshops`.active AS active , `workshops`.price_".$USER->currencyID." AS price, `workshops`.`price_RSD` AS priceRSD, `workshops`.heading_".$USER->lang." AS heading, `workshops`.subheading_".$USER->lang." AS subheading, `workshops`.narratorID FROM cart 
	                   			LEFT OUTER JOIN workshops ON `cart`.workshopID = `workshops`.workshopID WHERE BINARY `cart`.username='".$USER->username."'
	                   		) tbl LEFT OUTER JOIN images ON `tbl`.workshopID = `images`.workshopID AND `images`.im_index = 1 ORDER BY `images`.`im_index` ASC
	                   ) subTbl 
	                   LEFT OUTER JOIN packages ON `packages`.packageID = `subTbl`.packageID WHERE `subTbl`.active=1 OR `subTbl`.active IS NULL) tbl1 LEFT OUTER JOIN narrators ON `narrators`.narratorID = `tbl1`.narratorID ORDER BY `tbl1`.workshopID ASC", DBC_STORE);

	$sql_sum = mysql_query("SELECT COALESCE(SUM(priceRSD),0) + COALESCE(SUM(pricePackageRSD), 0) AS paymentAmountRSD, COALESCE(SUM(priceCurr),0) + COALESCE(SUM(pricePackageCurr), 0) AS paymentAmount FROM (SELECT `subTbl`.*, `packages`.price_".$USER->currencyID." AS pricePackageCurr, `packages`.`price_RSD` AS pricePackageRSD FROM (SELECT `tbl`.*, CONCAT(`images`.imageID, '.',`images`.`extension`) AS image, `images`.extension AS ext FROM (SELECT `cart`.*, `workshops`.active, `workshops`.price_".$USER->currencyID." AS priceCurr, `workshops`.`price_RSD` AS priceRSD,  `workshops`.heading_".$USER->lang." AS heading, `workshops`.subheading_".$USER->lang." AS subheading FROM cart LEFT OUTER JOIN workshops ON `cart`.workshopID = `workshops`.workshopID WHERE BINARY `cart`.username='".$USER->username."') tbl LEFT OUTER JOIN images ON `tbl`.workshopID = `images`.workshopID AND `images`.im_index = 1) subTbl LEFT OUTER JOIN packages ON `packages`.packageID = `subTbl`.packageID) tableResult WHERE `tableResult`.active=1 OR `tableResult`.active IS NULL", DBC_STORE);
	if (mysql_num_rows($sql) && $sql_sum)
	{
		$i = 0;
		while($t = mysql_fetch_object($sql)) $SHOPPING_CART[$i++] = $t;
		$obj = mysql_fetch_object($sql_sum);
		$RSD_SUM  = determine_cart_price(count($SHOPPING_CART), "RSD", $obj->paymentAmountRSD);
		$CURR_SUM = determine_cart_price(count($SHOPPING_CART), $USER->currencyID, ($USER->currencyID == "RSD" ? $obj->paymentAmountRSD : $obj->paymentAmount));

	} else $RSD_SUM = 0;


?>