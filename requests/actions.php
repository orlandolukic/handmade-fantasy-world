<?php 

//  User attempt to open this page
if (!(isset($REDIRECT) && !$REDIRECT) || !isset($ACTIONS)) { header("location: ".$domain); exit(); }

// Begin with specified action
if (isset($_GET['action']) && trim($_GET['action']) !== "")
{

switch($_GET['action'])
{
/* 
	ADD TO CART ACTION
 =======================
 Prefered od page /user/index.php. Can be included on every other page with $REDIRECT set to false and previously
 connected to database.
*/
case "addCart":
//	Regular request
	if (isset($_GET['type']) && trim($_GET['type'])!=="" && isset($_GET['cid']) && trim($_GET['cid'])!=="")
	{
		switch($_GET['type'])
		{
			/*
		case "subscription":
			$sql = mysql_query("SELECT * FROM packages WHERE BINARY packageID = '".$_GET['cid']."'", DBC_STORE);
			if ($sql && mysql_num_rows($sql) && isset($_GET['month']) && trim($_GET['month']) !== "" && is_numeric($_GET['month']) && intval($_GET['month'])>0 && intval($_GET['month'])<13)
			{
				if ($ACTIONS->execute)
				{
					$sql1 = mysql_query("SELECT * FROM (SELECT MONTH(`hfw`.`subscriptions`.date_start) AS smonth, MONTH(`hfw`.`subscriptions`.date_end) AS emonth, `hfw`.`subscriptions`.paymentID FROM subscriptions WHERE BINARY username = '".$USER->username."') tbl1 INNER JOIN `hfw`.payments ON `hfw`.`payments`.paymentID = `tbl1`.paymentID AND `hfw`.`payments`.paid = 1 WHERE  `tbl1`.smonth <= '".$_GET['month']."' AND `tbl1`.emonth >= '".$_GET['month']."'", DBC_STORE);
					if (mysql_num_rows($sql1) && $ACTIONS->redirect) 
					{ 
						if (isset($_GET['wid']) && trim($_GET['wid'])!=="") {
							header("location: ".$FILE."user/video/".$_GET['wid']); 
						} else 
						{ 
							header("location: ".$FILE."user/workshops?month=".$_GET['month']); 
						};
						exit();
					}
					$sql2 = mysql_query("SELECT * FROM cart WHERE start_month = '".$_GET['month']."' AND BINARY username = '".$USER->username."'", DBC_STORE);
					if (mysql_num_rows($sql2) && $ACTIONS->redirect) { header("location: ".$FILE."user/cart"); exit(); }
					
					$itemID = _SQL_escape_exstring(["SELECT * FROM cart WHERE BINARY itemID = '","'"], 20);

					$sql = mysql_query("INSERT INTO cart (itemID, workshopID, packageID, username, start_month) VALUES 
					                   ('".$itemID."', NULL, '".$_GET['cid']."', '".$USER->username."', ".$_GET['month'].")", DBC_STORE);
					if ($sql && $ACTIONS->redirect) { header("location: ".$FILE."user/cart"); exit(); }
				} else
				{
					$ACTIONS->valid = true;
				}
			} else header("location: ".$ACTIONS->redirection->SQL_error."?response=false");

			if (isset($ACTIONS->redirection->urlToPostData) && $ACTIONS->redirection->urlToPostData)
			{
				// Set month condition
				$monthCond = isset($_GET['month']) && trim($_GET['month']) !== "" && is_numeric($_GET['month']) && intval($_GET['month'])>0 && intval($_GET['month']) < 13;
				$workshopCond = isset($_GET['wid']) && trim($_GET['wid']) !== "";

				$urlToPostData = "login.php?action=addCart&type=".$_GET['type']."&cid=".$_GET['cid'];
				$ACTIONS->redirection->urlToPostData = "?action=addCart&type=".$_GET['type']."&cid=".$_GET['cid'];
				if ($monthCond)
				{
					$urlToPostData .= "&month=".$_GET['month'];
					$ACTIONS->redirection->urlToPostData .= "&month=".$_GET['month'];
				} 				
				if ($workshopCond) 
				{
					$urlToPostData .= "&wid=".$_GET['wid'];
					$ACTIONS->redirection->urlToPostData .= "&wid=".$_GET['wid'];
				}
			};
			break;*/

		case "workshop":
			$sql = mysql_query("SELECT * FROM workshops WHERE BINARY workshopID = '".$_GET['cid']."' AND active = 1", DBC_STORE);
			if ($sql && mysql_num_rows($sql))
			{
				if ($ACTIONS->execute)
				{		
				//	Check if workshop already exsist in bought workshops
					$sql_control = mysql_query("SELECT workshopID FROM boughtworkshops WHERE BINARY username = '".$USER->username."' AND BINARY workshopID = '".$_GET['cid']."'", DBC_STORE);

					// Select subscribed workshops
					$sql1 = mysql_query("SELECT `tbl2`.* FROM (SELECT `tbl1`.*, `subscriptions`.username, `subscriptions`.paymentID FROM (SELECT workshopID, date_publish FROM workshops WHERE active = 1 AND date_publish<=CURDATE() AND BINARY workshopID = '".$_GET['cid']."') tbl1  INNER JOIN `subscriptions` ON `subscriptions`.date_start <= `tbl1`.date_publish AND `subscriptions`.date_end >= `tbl1`.date_publish AND `subscriptions`.active = 1 AND BINARY `subscriptions`.username = '".$USER->username."') tbl2 INNER JOIN payments ON `payments`.paymentID = `tbl2`.paymentID AND `payments`.paid = 1 AND `tbl2`.username = `payments`.username", DBC_STORE);	

					// Select workshop in coupon section
					$sql2 = mysql_query("SELECT * FROM coupons WHERE BINARY workshopID = '".$_GET['cid']."' AND active = 1 AND expiration_date>=CURDATE() AND BINARY username = '".$USER->username."'", DBC_STORE);

					if (!mysql_num_rows($sql_control) && !mysql_num_rows($sql1) && !mysql_num_rows($sql2))
					{
						$itemID = _SQL_escape_exstring(["SELECT * FROM cart WHERE BINARY itemID = '","'"], 20);
						$sql = mysql_query("INSERT INTO cart (itemID, workshopID, packageID, username) VALUES 
					                   ('".$itemID."', '".$_GET['cid']."', NULL, '".$USER->username."')", DBC_STORE);
						if ($sql && $ACTIONS->redirect) { header("location: ".$FILE."user/cart"); }
					} else
					{
						$_SESSION['redirection'] = $_GET['cid'];
						$_SESSION['self'] = true;
						header("location: ".$ACTIONS->redirection->SQL_exsist);
						exit();
					}									
				} else
				{
					$ACTIONS->valid = true;
				}
			} else header("location: ".$ACTIONS->redirection->SQL_error);

			if (isset($ACTIONS->redirection->urlToPostData) && $ACTIONS->redirection->urlToPostData)
			{
				$urlToPostData = "login.php?action=addCart&type=".$_GET['type']."&cid=".$_GET['cid'];
				$ACTIONS->redirection->urlToPostData = "?action=addCart&type=".$_GET['type']."&cid=".$_GET['cid'];
			};
			break;
		}
	} else 
	{
		if (isset($ACTIONS->redirection->GET_error) && $ACTIONS->redirection->GET_error!=="") header("location: ".$ACTIONS->redirection->GET_error);
	};
	break;

/* 
	USER VALIDATION
 =======================

*/

case "userValidate":
	if (!isset($_GET['username']) || !isset($_GET['token'])) header("location: ".$ACTIONS->redirection->error_params);
	$sql = mysql_query("UPDATE users SET verified = 1 WHERE BINARY username = '".$_GET['username']."' AND BINARY security_token = '".$_GET['token']."'", DBC_STORE);

	if ($sql)
	{
		$_SESSION['hfw_username'] = $_GET['username'];
		if ($ACTIONS->redirect) header("location: ".$ACTIONS->redirection->success_req);
		exit();
	} else header("location: ".$ACTIONS->redirection->SQL_error);

	exit();
	break;

case "logout":
	$posted = 1; $timeout = 7000; $type = "force_logout"; $found = 1;
	break;

};

};

?>