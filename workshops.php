<?php 
	
	session_start();
	$prepath = '';
	$page 	 = "workshops";
	$show_footer = true;
	include $prepath."functions.php";
	include $prepath."connect.php";
	include "global.php";
	include "getDATA.php";
	include $prepath."pages.php";
	include $prepath."lang/func.php";
	include $prepath."lang/write_".strtolower($lang_acr).".php";

	if (isset($_SESSION['user_view']) && isset($_SESSION['user_view']->from) && $_SESSION['user_view']->from->status) $_SESSION['user_view']->from->status = false;

	$show = isset($_GET['month']) && isset($_GET['year']);
	if ($show)
	{
		$month = $_GET['month']; $year = $_GET['year'];
		if (!(is_numeric($month) && is_finite($month) && is_numeric($year) && is_finite($year))) $show = false; else
		{
			$month = intval($month); $year = intval($year);
			if ($month > 12 || $month < 1) $show = false;
			if ($year < 2017) $show = false; 
			if ($month < $startMonth || $month > $endMonth) $show = false; 
		};
	};

//  Set @REDIRECT@ parameter to ensure `page closing` on `user open attempt`
	$REDIRECT = false;
	include $prepath."requests/workshops.php";
	if (mysql_num_rows($sql_DATA))
	{
		$i     = 0;
		$query = array();
		if ($DATA_SQL->addDATA && $DATA_SQL->first)
		{
			for ($p=0; $p<count($O); $p++) $query[$i++] = $O[$p];
		};
		if ($DATA_SQL->read) while($t = mysql_fetch_object($sql_DATA)) $query[$i++] = $t; else { $query = $O; }
		if ($DATA_SQL->addDATA && !$DATA_SQL->first && $DATA_SQL->first !== NULL)
		{
			for ($p=0; $p<count($O); $p++) $query[$i++] = $O[$p];
		};
	};

	$sql = mysql_query("SELECT packageID, text_".$lang_acr." AS text, price_".$curr." AS pprice, price_RSD AS ppriceRSD FROM packages", DBC_STORE);
	$i = 0; $PACKAGES = array();
	while($t = mysql_fetch_object($sql)) $PACKAGES[$i++] = $t;
	
	if ($userActive && $show)
	{
		$sql = mysql_query("SELECT * FROM subscriptions WHERE start_month = '".$_GET['month']."' AND BINARY username = '".$USER->username."'", DBC_STORE);
		if (mysql_num_rows($sql)==1) $subscribed = true; else $subscribed = false;
	} else $subscribed = false;
?>

<!DOCTYPE html>
<html>
<head>
<?php print_HTML_data("head","workshops") ?>
</head>
<body class="<?= $bodyClass ?>">

	<?php echo $mobileMenu; $mobileMenu = NULL; ?>

	<!-- Fixed Menu -->
	<?php printMainMenu(1,1); ?>

	<!-- Header container -->
	<div class="header-container md-dn">
		<?php echo $headerInfo; $headerInfo = NULL; ?>
		<?php printMainMenu(0,3); ?>		
	</div>
	<!-- /Header container -->

	<div class="container margin-t-20 margin-md-t-90">
		<div class="row margin-b-20">
			<div class="col-md-12 col-xs-12 col-sm-12 text-center">
				<h1 style="margin: 0;" class="strong"><?php echo $lang->online_workshops ?></h1>
				<span class="subheading-text small"><?= $lang->enjoy_ourPassion ?></span>
			</div>
		</div>
		<?php $PRICE_SECTION = array("printOffset" => false, "printCheckIcon" => false, "cart" => false); include $prepath."code/price_section.php"; ?>

		<div class="row margin-t-20">
			<div class="col-md-7 col-xs-12 col-sm-12">				

			</div>
			<div class="col-md-5 col-xs-12 col-sm-12 text-right">		

				<?php if ($userActive && !$USER->grant_access) { ?>
				<span class="margin-r-20 internal-shopping-cart" data-toggle="tooltip" data-placement="left" title="<?= $lang->show_shoppingCart ?>">
					<a href="<?= $FILE ?>user/cart">
						<i class="fa fa-shopping-cart fa-2x"></i>	
						<div class="cart-items-number items-number dn">5</div>	
					</a>			
				</span>
				<?php }; ?>

				<span class="strong small uppercase"><?= $lang->sort ?></span>
				<div id="sort" class="sort-options fold small" style="display: inline-block;">
					<div class="default-option"><?= $DATA_SQL->sortName; ?></div>
					<ul class="options text-left">
						<li><a href="<?= $domain ?>workshops/latest"><i class="icon-ord fa fa-plus-square"></i> <?php echo $lang->newest ?></a></li>
						<li><a href="<?= $domain ?>workshops/oldest"><i class="icon-ord fa fa-clock-o"></i> <?php echo $lang->oldest ?></a></li>
						<li><a href="<?= $domain ?>workshops/topRated"><i class="icon-ord fa fa-star"></i> <?php echo $lang->best_rated ?></a></li>
						<li><a href="<?= $domain ?>workshops/alpha/asc"><i class="icon-ord fa fa-sort-alpha-asc"></i> <?php echo $lang->a_toZ ?></a></li>
						<li><a href="<?= $domain ?>workshops/alpha/desc"><i class="icon-ord fa fa-sort-alpha-desc"></i> <?php echo $lang->z_toa ?></a></li>
					</ul>
					<i class="fold fa fa-chevron-down"></i>
					<i class="unfolded fa fa-chevron-up"></i>
				</div>				
			</div>
		</div>

		<div class="row margin-t-20 ws-preview">
			<?php 
				// Printing workshops from query
				  for ($i=0, $t=0, $f=true, $c=-2; $i<count($query); $i++, $c++) { ?>
			<?php if (!$f && $t==0) { ?>
			<div class="row">
			<?php }; ?>
				<div class="col-md-4 <?= ($c>=1 ? "margin-t-30" : "") ?> <?= ($i>0 ? "margin-md-t-30" : "") ?>">
					<div class="ws-preview-placeholder" data-wid="<?= $query[$i]->workshopID ?>">						
						<div class="ws-preview-image">
							<a href="<?= $FILE ?>workshop/<?= $query[$i]->workshopID ?>">
								<img src="<?= $FILE ?>img/content/<?= $query[$i]->image ?>">
								<div class="ws-preview-video-icon"><i class="fa fa-play"></i></div>
								<?php $dat = strtotime($query[$i]->date_publish); $dat += 7*60*60*24; if ($dat >= time()) { ?>
								<div class="ws-preview-new-notif small"><?= $lang->new_stuff ?></div>
								<?php }; ?>									
							</a>
							<?php if ($query[$i]->narratorID) { ?>	
							<a href="<?= $FILE ?>workshop/<?= $query[$i]->workshopID ?>">				
								<div class="ws-preview-leader">
									<img src="<?= $FILE ?>img/authors/<?= $query[$i]->narrator_image ?>">
								</div>							
							</a>
							<div class="ws-preview-leader-text">
								<div class="uppercase smaller"><?= $lang->ws_with ?></div>
								<a href="<?= (($vv = $query[$i]->narrator_external_link) ? $vv : "#") ?>" target="<?= ($vv ? "_blank" : "_self") ?>" class="ws-leader-ex-link"><span><?= $query[$i]->narrator_name ?></span></a>
							</div>							
							<?php }; ?>
						</div>
						
						<div class="ws-preview-body">
							<a href="<?= $FILE ?>workshop/<?= $query[$i]->workshopID ?>" class="ws-preview-heading-link"><div class="ws-preview-name"><?= $query[$i]->heading ?></div></a>
							<span class="smaller">								
								<i class="fa fa-calendar"></i> <?= make_date(-1, $query[$i]->date_publish) ?>
								<?php $m = determine_rating($query[$i]->result); ?>
								<span class="padding-l-10 star-color"><?php for ($y=0; $y<count($m); $y++) { ?><i class="fa <?= $m[$y] ?>"></i><?php }; ?></span>
								<span class="padding-l-10">
									<i class="fa fa-thumbs-up"></i> <?= $query[$i]->likes ?>
								</span>	
								<span class="padding-l-10">
									<i class="fa fa-comments"></i> <?= $query[$i]->comments ?>
								</span>								
							</span>							
							<div class="padding-t-5">
								<span class="small"><i class="fa fa-globe"></i> <?= $lang->lang ?>: <span class="strong"><?= return_workshop_language(intval($query[$i]->languageID)); ?></span></span>
							</div>
							<div class="small padding-t-5"><?= make_subheading($query[$i]->subheading) ?></div>
						</div>
						<div class="ws-preview-footer">
							<?php if ($userActive && !$USER->grant_access || !$userActive) { ?>
							<a href="javascript: void(0)" class="link-order link-ord"><i class="fa fa-shopping-cart"></i> <span><?= $lang->add_toCart ?></span></a>
							<?php } else { ?>
							<a href="<?= $FILE ?>user/video/<?= $query[$i]->workshopID ?>"><i class="fa fa-play"></i> <?= $lang->watch_video ?></a>
							<?php }; ?>
						</div>
					</div>
				</div>
			<?php if ($t==2) { if ($f) $f = false; $t=0; ?>
			</div>
			<?php } else $t++; ?>
			<?php }; ?>
		</div>

		<div class="row">
			<div class="col-md-6 col-xs-12 col-sm-12 relative">
				<?php if ($show) { ?>
				<div class="div-inl">					
					<div class="small">
						<div id="sort" class="sort-options fold" style="margin-left: 0">
							<div class="default-option"> <?= $lang->c_months[$month-1]; ?></div>
							<ul class="options text-left">
								<?php for ($i=$startMonth-1; $i<$endMonth; $i++) { ?>
								<li><a href="<?= $domain ?>workshops/<?= $i+1 ?>/<?= $year ?>"> <span class="strong"><?= ($i>8 ? ($i+1) : "0".($i+1)) ?></span> - <?= $lang->c_months[$i] ?></a></li>
								<?php }; ?>								
							</ul>
							<i class="fold fa fa-chevron-down"></i>
							<i class="unfolded fa fa-chevron-up"></i>
						</div>						
					</div>
					<div class="padding-l-10 strong"><?= $year ?></div>
					
				</div>
				<?php }; ?>
			</div>
			<?php if ($show) { ?>
			<div class="col-md-6 col-xs-12 col-sm-12 margin-md-t-20 small">
				<div class="text-right div-inl">
				<?php echo $lang->sort ?>:	
				<div id="sort" class="sort-options fold">
					<div class="default-option"><?= $DATA_SQL->sortName; ?></div>
					<ul class="options text-left">
						<li><a href="<?= $domain ?>workshops/<?= $month ?>/<?= $year ?>/latest"><i class="icon-ord fa fa-plus-square"></i> <?php echo $lang->newest ?></a></li>
						<li><a href="<?= $domain ?>workshops/<?= $month ?>/<?= $year ?>/oldest"><i class="icon-ord fa fa-clock-o"></i> <?php echo $lang->oldest ?></a></li>
						<li><a href="<?= $domain ?>workshops/<?= $month ?>/<?= $year ?>/topRated"><i class="icon-ord fa fa-star"></i> <?php echo $lang->best_rated ?></a></li>
						<li><a href="<?= $domain ?>workshops/<?= $month ?>/<?= $year ?>/alpha/asc"><i class="icon-ord fa fa-sort-alpha-asc"></i> <?php echo $lang->a_toZ ?></a></li>
						<li><a href="<?= $domain ?>workshops/<?= $month ?>/<?= $year ?>/alpha/desc"><i class="icon-ord fa fa-sort-alpha-desc"></i> <?php echo $lang->z_toa ?></a></li>
					</ul>
					<i class="fold fa fa-chevron-down"></i>
					<i class="unfolded fa fa-chevron-up"></i>
				</div>		
				</div>					
			</div>
			<?php }; ?>
		</div>
	</div>

	<div class="container">
		<?php if ($show) { ?>

		<div class="row">
			<div class="col-md-12 col-xs-12 col-sm-12 text-right margin-t-20">
				<div class="div-inl">
					<div class="pay-placeholder small">
						<div class="placeholder">
							<div class="main-price strong"><?= $PACKAGES[0]->pprice ?> <?= $curr ?></div>
							<?php if ($curr !== "RSD") { ?><div class="small"><?= print_money_PLAINTXT($PACKAGES[0]->ppriceRSD,2); ?> RSD</span></div><?php }; ?>
						</div>
						<?php if ($userActive) { ?>
							<?php if ($subscribed) { ?>
							<a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Subscribed">
								<div class="btn-green disabled btn-ord btn-block">
									<?php echo $lang->subbed_For ?> <span class="strong"><?= $lang->c_months[$month-1] ?></span>
								</div>	
							</a>	
							<?php } else { ?>					
							<a href="<?= $FILE ?>user/addCart/subscription/<?= $PACKAGES[0]->packageID ?>?month=<?= $month ?>">
								<div class="btn-green btn-ord btn-block">
									<?php echo $lang->subb_for ?> <span class="strong"><?= $lang->c_months[$month-1] ?></span>
								</div>	
							</a>	
							<?php }; ?>
						<?php } else { ?>
						<a href="<?= $domain ?>login.php?action=addCart&type=subscription&cid=<?= $PACKAGES[0]->packageID ?>&month=<?= $month ?>">
							<div class="btn-green btn-ord btn-block">
								<?php echo $lang->subb_for ?> <span class="strong"><?= $lang->c_months[$month-1] ?></span>
							</div>	
						</a>
						<?php }; ?>
					</div>					
				</div>				
			</div>
		</div>
		<div class="row">
			<?php if (mysql_num_rows($sql_DATA)) { ?>
			<?php for ($i = 0; $i < count($query); $i++) { ?>
			
			<div class="col-md-3 col-xs-12 col-sm-12 relative margin-t-30">
				<div class="product-item">
					<?php $dat = strtotime($query[$i]->date_publish); $dat += 5*60*60*24; if ($dat >= time()) { ?>
					<div class="eticket eticket-opt"><span class="strong"><?php echo $lang->newUP ?></span></div>
					<?php }; ?>
					<a href="<?php echo $domain ?>workshop/<?= $query[$i]->workshopID; ?>">
					<div class="top-content">
						<div class="image-optimised">												
							<img src="<?= $FILE ?>img/content/<?= $query[$i]->image ?>" style="height: auto" />	
						</div>
						<div class="paint-brush">
							<i class="fa fa-paint-brush fa-2x"></i>
						</div>
					</div>
					</a>
					<div class="padding-l-10 padding-r-10">
					<a href="<?= $domain ?>workshop/<?= $query[$i]->workshopID ?>">
						<div class="padding-t-10 strong text-center"><?= pop_obj($query[$i], "heading"); ?></div>
					</a>
					<div class="star-color text-center">
						<?php $stars = determine_rating($query[$i]->result) ?>
		    			<span class="fa <?= $stars[0] ?>"></span>
		    			<span class="fa <?= $stars[1] ?>"></span>
		    			<span class="fa <?= $stars[2] ?>"></span>
		    			<span class="fa <?= $stars[3] ?>"></span>
		    			<span class="fa <?= $stars[4] ?>"></span>					    			
		    		</div>
		    		<div class="text-center">
		    			<span class="text-center text-faded"><i class="fa fa-eye"></i> <?= $query[$i]->views ?></span>
		    			<span class="text-center text-faded padding-l-10"><i class="fa fa-heart"></i> <?= $query[$i]->num_wish ?></span>
		    			<span class="text-center text-faded padding-l-10"><i class="fa fa-comment"></i> <?= $query[$i]->comments ?></span>
		    		</div>
		    		</div>
				</div>
			</div>
			<?php }; // /for ?>	
			<?php }; // count($query) ?>				
		</div>
		<?php if (!mysql_num_rows($sql_DATA)) { $show_footer = false; ?>
		<div class="margin-t-20">
			<div class="row">
				<div class="col-md-12 col-xs-12 col-sm-12">
					<div class="large strong color-theme"><i class="fa fa-ban"></i> <?= $lang->no_WS ?></div>
					<div class=""><?= $lang->unableToFindWS ?> <span class="strong"><?= $lang->c_months[$month-1]; ?></span></div>
				</div>
			</div>
		</div>
		<?php }; // if (!mysql_num_rows($sql_DATA)) { .. } ?>
		<?php }; // if ($show) { ... } ?>
	</div>

	<?php if (!$show && 0) { ?>

	<div class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12 col-sm-12 text-center">
				<h2><?= $lang->searchForWS ?></h2>
				<div class="small"><?= $lang->sfw_selectedP ?></div>
			</div>
		</div>
		<div class="row margin-t-60">			
			<?php for ($i=$startMonth-1; $i<$endMonth; $i++) { ?>
			<div class="col-md-2 col-xs-6 col-sm-6 <?= ($i>7 ? "margin-t-30" : "") ?> <?= ($i>1 ? "margin-md-t-10" : "") ?>">
				<a class="month-choose-link" href="<?= $domain ?>workshops/<?= $i+1 ?>/<?= $year ?>">
					<div class="workshops-month text-center">
						<div class="large strong"><?= ($i<9 ? "0".($i+1) : $i+1) ?></div>
						<div><?= $lang->c_months[$i] ?></div>
					</div>				
				</a>
			</div>
			<?php }; ?>			
		</div>
	</div>

	<?php }; ?>

	<?php if ($show && mysql_num_rows($sql_DATA) && 0) { ?>
	<div class="container margin-t-30">
		<div class="row">
			<div class="col-md-6 col-xs-12 col-sm-12">
				<div class="div-inl">
					<?php echo $lang->workshop_perPage ?>:
					<div id="sort" class="sort-options fold small" style="padding-right: 30px">
						<div class="default-option"><?= ($ip ? $ipp : 16) ?></div>
						<ul class="options text-left">
							<li><a href="<?= $domain.$URL_to_redirect; ?>&ipp=16">16</a></li>
							<li><a href="<?= $domain.$URL_to_redirect; ?>&ipp=32">32</a></li>
							<li><a href="<?= $domain.$URL_to_redirect; ?>&ipp=48">48</a></li>
							<li><a href="<?= $domain.$URL_to_redirect; ?>&ipp=64">64</a></li>
							<li><a href="<?= $domain.$URL_to_redirect; ?>&ipp=80">80</a></li>						
							<li><a href="<?= $domain.$URL_to_redirect; ?>&ipp=96">96</a></li>
						</ul>
						<i class="fold fa fa-chevron-down"></i>
						<i class="unfolded fa fa-chevron-up"></i>
					</div>	
				</div>
			</div>
		</div>
		<div class="row"><!-- <?= $URL_to_redirect; ?>&page=<?= $i ?> -->
			<div class="col-md-12 col-xs-12 col-sm-12 text-center">
				<ul class="pagination">					
					<?php $REDIRECT = false; include $prepath."requests/pagePagination.php"; ?>					
				</ul>
			</div>
		</div>
	</div>
	<?php }; ?>

	<?php if (0) { ?>
	<div class="container margin-t-20">
		<div class="divider width-100"></div>
		<div class="row">
			<div class="col-md-12 col-xs-12 col-sm-12 margin-t-10 bordered-theme padding-l-30 padding-r-30 padding-t-20 padding-b-20">
				<div class="text-left color-theme">
					<h2 class="strong" style="margin: 0"> <i class="fa fa-info-circle"></i> <?php echo $WS_header1 ?>  </h2>
				</div>
				<div class="text-left margin-t-10 margin-b-10">
				 	&emsp;<?php echo $WS_para1 ?>
				</div>
				<div class="text-left margin-b-10">
					&emsp;<?php echo $WS_para2 ?>
				</div>
				<div class="text-left margin-b-10">
					&emsp;<?php echo $WS_para3 ?>
				</div>
				<div class="text-left margin-b-10">
					&emsp;<?php echo $WS_para4 ?>
				</div>
				<div class="text-left margin-b-10">
					&emsp;<?php echo $WS_para5 ?>
				</div>
			</div>
		</div>
	</div>
	<?php }; ?>

	<?php if ($userActive && !$USER->grant_access) { ?>
	<div class="external-shopping-cart md-dn">	
		<div class="loading dn"><i class="fa fa-circle-o-notch fa-spin"></i></div>	
		<div class="shopping-icon"><a href="<?= $FILE ?>user/cart" class="link-ord" data-toggle="tooltip" data-placement="left" title="<?= $lang->show_shoppingCart ?>"><i class="fa fa-shopping-cart fa-2x"></i></a></div>
		<div class="strong uppercase small"><?= $lang->your_Cart ?></div>
		<div class="smaller">
			<?= $lang->workshops ?> <div class="cart-items-number">-</div>
		</div>
		<div class="margin-t-5">
			<span class="uppercase smaller"><?= $lang->total ?></span> 
			<div class="checkout-sum">
				<span class="strong">RSD</span> <span class="sum">0,00</span>
			</div>
			<div class="small margin-t-10 text-center">
				<a class="link-ord" href="<?= $FILE ?>user/cart"><i class="fa fa-check-circle"></i> <?= $lang->finish_Purchase ?></a>
			</div>
		</div>
	</div>
	<?php }; ?>

	<?php printFixedMenuSidebar(); ?>
	<?php if ($show_footer) echo $footer; $footer = NULL; ?>

	<?php echo $upBtn; $upBtn = NULL; ?>

	<?php print_HTML_data("script","workshops") ?>

</body>
</html>