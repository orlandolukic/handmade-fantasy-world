<?php 

//	Global functions
	function make_date($s, $d, $sep = '.')
	{
		global $lang_acr;
		switch(strtoupper($lang_acr))
		{
		case "SR": case "RU": case "EN": case "GR": // Print for Serbian users
			return substr($d,8,2).$sep.substr($d,5,2).$sep.substr($d,0,4);
			break;
			/*
		case 1:	// Print for USA users
			return substr($d,5,2).$sep.substr($d,8,2).$sep.substr($d,0,4);
			break;*/
		}		
	}

	function make_date_format($t, $sep = '.')
	{
		global $lang_acr;
		switch(strtoupper($lang_acr))
		{
		case "SR": case "RU": case "EN": case "GR": // Serbian users
			return date("d".$sep."m".$sep."Y", $t);
			break;/*
		case 1: // Print for USA users
			return date("m".$sep."d".$sep."Y", $t);*/
		}
	}

	function return_country_values()
	{
		switch($_SESSION['lang'])
		{
		case "EN": return (object) array("language" => "EN", "curr" => "EUR", "currName" => "€"); 
		case "SR": return (object) array("language" => "SR", "curr" => "RSD", "currName" => "RSD"); 
		case "GR": return (object) array("language" => "GR", "curr" => "EUR", "currName" => "€"); 
		default:   return (object) array("language" => "EN", "curr" => "EUR", "currName" => "€"); 
		}
	}

	function return_workshop_language($id)
	{
		global $lang;
		switch($id)
		{
		case 1: // English
			return $lang->L_english;
			break;
		case 2: // Greek
			return $lang->L_greek;
			break;
		}
	};

	function checkLanguage($a)
	{
		return in_array($a, ["RS", "GR", "UK"]);
	}

	function getLangFromCountry($country)
	{
		switch($country)
		{
		case "RS": return "sr/";
		case "GR": return "gr/";
		case "UK": return "en/";
		default: return "?lang=en";
		}
	}

	function DEC($n)
	{
		$n = intval($n);
		return $n<10 ? "0".$n : $n;
	}

	function get_date_format()
	{
		global $lang_acr, $USER;
		$t = NULL;
		if (isset($USER))
		{
			switch($USER->lang)
			{
			case "SR": case "PL": case "GR":  $t = 0; break;
			case "EN": case "RU": $t = 1; break;
			}
		}		
		return $t;
	}

	function make_time($t)
	{
		return date("H:i", $t);
	}

	function make_image($im, $prepath='', $ext='')
	{
		if ($im == NULL)
		{
			return $prepath."img/user.png";
		} else if ($ext==='')
		{
			return $prepath."img/users/".$im;
		} else
		{
			return $prepath."img/users/".$im.".".$ext;
		}
	}

	function make_image_content($im, $prepath='', $ext='', $sym = "content")
	{
		if ($ext==='')
		{
			return $prepath."img/".$sym."/".$im;
		} else
		{
			return $prepath."img/".$sym."/".$im.".".$ext;
		}
	}

/*	
	Function that returns part of specific date.
	PARAMETERS:   $c {string} - code, tells witch part of date would be returned
				  $d {string} - date, wich will be returned edited
	=============================================================================
	RETURN VALUE:    {string}
*/
	function part_date($c, $d)
	{
		switch($c)
		{
		case 'D': case 'd':
			return substr($d,8,2);
			break;
		case 'M': case 'm':
		return substr($d,5,2);
			break;
		case 'Y': case 'y':
			return substr($d,0,4);
			break;
		}
	}

	function is_leap_year($y)
	{
		return $y%4==0 && $y%100!=0 || $y%400==0;
	}

	function pop_obj($o,$a)
	{
		return $o->$a;
	}

	//	$DAYS = array(array(31,28,31,30,31,30,31,31,30,31,30,31),
	//            array(31,29,31,30,31,30,31,31,30,31,30,31)
	//             );

	function print_duration($fl)
	{
		global $lang;
		switch($fl)
		{
		case 1:
			return pop("oneMonth");
			break;
		case 12:
			return pop("oneYear");
			break;
		case 3:
			return pop("threeMonths");
			break;
		}
	}

	function get_month_days($y)
	{
		$DAYS = array(
		              array(31,28,31,30,31,30,31,31,30,31,30,31),	// Not Leap Year - 365
	                  array(31,29,31,30,31,30,31,31,30,31,30,31)	// Leap Year - 366
	                 );
		return $DAYS[is_leap_year($y)];
	}

	function print_money($mon, $curr=1)
	{
		global $lang_acr;
		switch(strtoupper($lang_acr))
		{
		case "SR": case "PL":
			return number_format($mon,0,',','.').($curr ? " <span class=\"pm-currency\">".print_curr()."</span>" : '');			
			break;
		case "EN": case "RU":
			return number_format($mon,0,'.',',').($curr ? " <span class=\"pm-currency\">".print_curr()."</span>" : '');
			break;
		}
	}

	function print_money_PLAINTXT($mon, $dec=0, $pr_curr=0)
	{
		global $curr, $USER;
		switch(isset($USER) ? $USER->currencyID : $curr)
		{
		case "RSD": case "PLN":
			return number_format($mon,$dec,',','.').($pr_curr ? " <span class=\"pm-currency\">".print_curr()."</span>" : '');			
			break;
		case "EUR": case "RUB": case "USD":
			return number_format($mon,$dec,'.',',').($pr_curr ? " <span class=\"pm-currency\">".print_curr()."</span>" : '');
			break;
		}
	}

	function print_money_out($mon, $curr=1)
	{
		global $lang_acr;
		switch(strtoupper($lang_acr))
		{
		case "SR": case "PL":
			return "<span class=\"pm-value\">".number_format($mon,0,',','.')."</span>".($curr ? " <span class=\"pm-currency\">".print_curr()."</span>" : '');			
			break;
		case "EN": case "RU":
			return "<span class=\"pm-value\">".number_format($mon,0,'.',',')."</span>".($curr ? " <span class=\"pm-currency\">".print_curr()."</span>" : '');
			break;
		}
	}

	function print_curr($st="")
	{
		global $curr;
		switch($st === "" ? $curr : $st)
		{
		case "RSD":
			return "RSD";
			break;
		case "EUR":
			return "<i class=\"fa fa-eur\"></i>";
			break;
		case "USD":
			return "<i class=\"fa fa-usd\"></i>";
			break;
		}
	}

	function return_lang($language,$prepath)
	{
		$MAKE_LANG = false;	
		include_once $prepath."lang/".$language.".php";
		switch ($language) {
			case 'en': case "EN": return new Language_EN();
			case 'sr': case "SR": return new Language_SR();
			case 'gr': case "GR": return new Language_GR();
				break;
		};
	}

	function determine_rating($rating, $img = 0)
	{
		$star_o    = ( $img===0 ? 'fa-star-o' : 'Star_empty.png' );
		$star      = ( $img===0 ? 'fa-star' : 'Star_full.png' );
		$star_half = ( $img===0 ? $star.'-half-o' : 'Star_half.png' );					    
		$arr = array(0  => $star_o, 1  => $star_o, 2  => $star_o, 3  => $star_o, 4  => $star_o);
		$i = 0xF09 ^ 0xF09;					    			
		while($i<=4)
		{
			if ($i<$rating && $rating >= $i+1) $arr[$i] = $star; elseif ($rating==$i) $arr[$i] = $star_o; elseif($rating>$i) $arr[$i] = $star_half;		    
			$i++;
		};		
		return $arr;
	};

	
	function generate_string($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	};

	function checkUser()
	{
		global $USER;
		if (!isset($USER) || $USER === NULL) exit();
	}

	function _SQL_escape_exstring($sql, $len)
	{
		if (count($sql)!==2) return NULL;
		$str  = generate_string($len);
		$sql1 = mysql_query($sql[0].$str.$sql[1], DBC_STORE);
		if ($sql1 && mysql_num_rows($sql1))
		{
			while(mysql_num_rows($sql1)>0)
			{
				$str  = generate_string($len);
				$sql1 = mysql_query($sql[0].$str.$sql[1], DBC_STORE);
			};
		}
		return $str;
	}

	function print_language($a)
	{
		global $lang;
		switch($a)
		{
		case "SR": return $lang->L_serbian;
		case "EN": return $lang->L_english;
		case "RU": return $lang->L_russian;
		case "SP": return $lang->L_spanish;
		case "GR": return $lang->L_greek;
		}
	};

	function print_currency_name($f)
	{
		global $lang;
		switch($f)
		{
		case "RSD": return $lang->C_RSD;
		case "EUR": return $lang->C_EUR;
		case "PLN": return $lang->C_PLN;
		case "USD": return $lang->C_USD;
		case "RUB": return $lang->C_RUB;
		}
	}

	function logInUser(&$session, $res)
	{
		$session = $res->username;
		$tok = generate_string(20);
		$sql = mysql_query("UPDATE users SET timestamp = '".time()."', access_token = '".$tok."', last_login = CURDATE() WHERE BINARY username = '".$res->username."'", DBC_STORE);
		$_SESSION['access_token'] = $tok;
	};

	function getUserIP()
	{		
	    $client  = @$_SERVER['HTTP_CLIENT_IP'];
	    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
	    $remote  = $_SERVER['REMOTE_ADDR'];

	    if(filter_var($client, FILTER_VALIDATE_IP))
	    {
	        $ip = $client;
	    }
	    elseif(filter_var($forward, FILTER_VALIDATE_IP))
	    {
	        $ip = $forward;
	    }
	    else
	    {
	        $ip = $remote;
	    }
	    return $ip;
	}

	function make_subheading($t, $max=50)
	{
		if (strlen($t)<=$max) return $t;
		$parse = substr($t,0,$max);
		$end   = $t{$max-1};		
		switch($end)
		{
		case ".":
			return substr($t,0,$max)."..";
			break;
		case " ":
			return substr($t,0,$max)."...";
			break;
		default:
			for ($pos=1, $go = true; $go; $pos++)
			{
				$c = @$t{$max+$pos};
				if ($c == ' ' || $c == '.')
				{
					return substr($t,0,$max+$pos)."...";
				};
				if ($max+$pos < strlen($t)) $pos++; else $go = false;
			}			
		}
	}

	function determine_cart_price($n,$c,$sum=0)
	{
		global $HAS_SUBSCRIPTION;
		switch($c)
		{
		// For currency RSD	
		case "RSD":
			switch(true)
			{
			default: return ($HAS_SUBSCRIPTION ? $n*860 : $sum);/*
			case ($n==1): return 1100;
			case ($n==2): return 1700;
			case ($n==3): return 2500;
			case ($n>=4): return $n*750;*/
			}
			break;

		// For currency EUR
		case "EUR":
			switch(true)
			{
			default: return ($HAS_SUBSCRIPTION ? $n*7 : $sum);/*
			case ($n==1): return 9;
			case ($n==2): return 14;
			case ($n==3): return 18;
			case ($n>=4): return $n*5;*/
			}
			break;
		}		
	}

	function determine_item_price($n, $c, $p, $sub= false)
	{
		global $HAS_SUBSCRIPTION;
		$sub = isset($HAS_SUBSCRIPTION) ? $HAS_SUBSCRIPTION : $sub;
		switch($c)
		{
		// For currency RSD	
		case "RSD":
			switch(true)
			{
			default: return ($sub ? 860 : $p);/*
			case ($n==1): return 1100;
			case ($n==2): return 1700 / 2;
			case ($n==3): return 2500 / 3;
			case ($n>=4): return 750;*/
			}
			break;

		// For currency EUR
		case "EUR":
			switch(true)
			{
			default: return ($sub ? 7 : $p);/*
			case ($n==1): return 9;
			case ($n==2): return 14 / 2;
			case ($n==3): return 18 / 3;
			case ($n>=4): return 5;*/
			}
			break;
		}		
	}

	function determine_user_subscription($username)
	{
		//	Check if user is subscribed for month(s)
		$currentMonth = intval(date("m"));
		$SUBSCRIPTION_MONTHS = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$HAS_SUBSCRIPTION    = false;
		$sql = mysql_query("SELECT `subscriptions`.*, `packages`.flag FROM subscriptions LEFT OUTER JOIN packages ON `packages`.packageID = `subscriptions`.type WHERE BINARY username = '".$username."' AND `subscriptions`.active = 1", DBC_STORE);
		if (mysql_num_rows($sql))
		{
			$k = 0;
			while($t = mysql_fetch_object($sql))
			{
				$date_start = $t->date_start; $date_end = $t->date_end;
				$M_start = intval(part_date("M", $date_start)); $M_end = intval(part_date("M", $date_end))+1;
			
				if (intval($t->flag) > 1)
				{
					for ($j=$M_start-1; $j<$M_end-1; $j++) { $SUBSCRIPTION_MONTHS[$j] = 1; if (!$HAS_SUBSCRIPTION) $HAS_SUBSCRIPTION = true; }
				} else
				{
					$SUBSCRIPTION_MONTHS[$M_start-1] = 1; 
					if (!$HAS_SUBSCRIPTION) $HAS_SUBSCRIPTION = true;
				}
			}
		}
		return (object) array("months" => $SUBSCRIPTION_MONTHS, "has_subscription" => $HAS_SUBSCRIPTION);
	}


?>